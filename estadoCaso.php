<html>

<body>

  <table>

    <tr>
      <td>Category</td>
      <td>
      <?php
      // read the product categories from the database
      include_once 'estadoCaso.model.php';

      estadoCaso = new estadoCaso($db);
      $stmt = estadoCaso->read();
      echo $stmt;

      // put them in a select drop-down
      echo "<select class='form-control' name='category_id'>";
          echo "<option>Select category...</option>";

          while ($rows = $stmt->fetch(PDO::FETCH_ASSOC)){
              extract($rows);
              echo "<option value='{$id}'>{$name}</option>";
          }

      echo "</select>";
      ?>
      </td>
  </tr>

  </table>

</body>

</html>
