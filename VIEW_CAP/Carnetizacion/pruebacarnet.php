<!DOCTYPE html>
<html lang="en">
  <head>
   <?php include '../import_css.php'; ?>
 <?php include '../import_js.php';?>
   

  </head>

  <body >
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
  <section id="container" class="">
    
        <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
            <a class="logo" href="home.html"><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <a class="sublogo" href="home.html"><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            </div>
            <!--Finaliza logo-->
                <?php include '../Session.php' ?>

        </div>
      <!--header end-->


           <!--INICIO DE MENU -->
      <aside>

           <?php include 'menu.php';?>


      </aside>
      
      <!--FINAL DE MENU-->





<!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-11">
                     
                      <section class="panel">
                          <header class="panel-heading">
                              Crear Usuario
                          </header>
                          <div class="panel-body">

                                    

                          


                               <!-- INICIO TABLA DE CREACION DE USUARIOS-->
                           


                                       



                                       




                          </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
                     
                
     





                             <script type="text/javascript">
                                $(document).ready(function() {
                                    $("#add_row").on("click", function() {
                                        // Dynamic Rows Code
                                        
                                        // Get max row id and set new id
                                        var newid = 0;
                                        $.each($("#tab_logic tr"), function() {
                                            if (parseInt($(this).data("id")) > newid) {
                                                newid = parseInt($(this).data("id"));
                                            }
                                        });
                                        newid++;
                                        
                                        var tr = $("<tr></tr>", {
                                            id: "addr"+newid,
                                            "data-id": newid
                                        });
                                        
                                        // loop through each td and create new elements with name of newid
                                        $.each($("#tab_logic tbody tr:nth(0) td"), function() {
                                            var cur_td = $(this);
                                            
                                            var children = cur_td.children();
                                            
                                            // add new td and element if it has a nane
                                            if ($(this).data("name") != undefined) {
                                                var td = $("<td></td>", {
                                                    "data-name": $(cur_td).data("name")
                                                });
                                                
                                                var c = $(cur_td).find($(children[0]).prop('tagName')).clone().val("");
                                                c.attr("name", $(cur_td).data("name") + newid);
                                                c.appendTo($(td));
                                                td.appendTo($(tr));
                                            } else {
                                                var td = $("<td></td>", {
                                                    'text': $('#tab_logic tr').length
                                                }).appendTo($(tr));
                                            }
                                        });
                                        
                                        // add delete button and td
                                        /*
                                        $("<td></td>").append(
                                            $("<button class='btn btn-danger glyphicon glyphicon-remove row-remove'></button>")
                                                .click(function() {
                                                    $(this).closest("tr").remove();
                                                })
                                        ).appendTo($(tr));
                                        */
                                        
                                        // add the new row
                                        $(tr).appendTo($('#tab_logic'));
                                        
                                        $(tr).find("td button.row-remove").on("click", function() {
                                             $(this).closest("tr").remove();
                                        });
                                });




                                    // Sortable Code
                                    var fixHelperModified = function(e, tr) {
                                        var $originals = tr.children();
                                        var $helper = tr.clone();
                                    
                                        $helper.children().each(function(index) {
                                            $(this).width($originals.eq(index).width())
                                        });
                                        
                                        return $helper;
                                    };
                                  
                                    $(".table-sortable tbody").sortable({
                                        helper: fixHelperModified      
                                    }).disableSelection();

                                    $(".table-sortable thead").disableSelection();



                                    $("#add_row").trigger("click");
                                });
                            </script>

          



      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              Alcaldia de San Salvador.
              <a href="#" class="go-top">
                  <i class="icon-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

   <?php include '../import_js.php'; ?>

  </body>
</html>
