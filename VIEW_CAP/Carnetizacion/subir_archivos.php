<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Mosaddek">
  <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <link rel="shortcut icon" href="img/favicon.png">

  <title>Emisión de Carné</title>
<?php include '../import_css.php'; ?>
 <?php include '../import_js.php';?>
  <!-- Bootstrap core CSS -->
  <link href="../../Resources/css/bootstrap.min.css" rel="stylesheet">
  <link href="../../Resources/css/bootstrap-reset.css" rel="stylesheet">
  <!--external css-->
  <link href="../../Resources/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <!-- Custom styles for this template -->
  <link href="../../Resources/css/style.css" rel="stylesheet">
  <link href="../../Resources/css/style-responsive.css" rel="stylesheet" />

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="../../Resources/js/html5shiv.js"></script>
      <script src="../../Resources/js/respond.min.js"></script>
      <![endif]-->
    </head>

    <body>

      <section id="container" class="">

        <!--Comienza el Header-->
        <div class="header white-bg">
          <!--Inicio del Logo-->
          <div class="header">
            <a class="logo" href="home.html"><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <a class="sublogo" href="home.html"><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
          </div>


        </div>
        <!--header end-->

        <!--INICIO DE MENU -->
        <aside>

         <?php include 'menu.php';?>


       </aside>

       <!--FINAL DE MENU-->




       <!--main content start-->
       <section id="main-content">
        <section class="wrapper site-min-height">
          <!-- page start-->
          <div class="row">
            <div class="col-lg-11">

              <section class="panel">
                <header class="panel-heading">
                  Emisión de Carné
                </header>
                <div class="panel-body">
                  <div class="stepy-tab">
                    <ul id="default-titles" class="stepy-titles clearfix">
                      <li id="paso1" class="current-step">
                        <div>Subir documentos</div>
                      </li>

                    </ul>
                  </div>

            <?php 
            session_start();
            echo $_SESSION['IdDetaMenorInsertadoSess'];
            ?>
                  <fieldset title="Step 4" class="step" id="paso4" >
                    <legend> </legend>

                    <div class="form-group">


                        <div class="col-lg-12">

                                <form enctype="multipart/form-data" action="../../BUSINESS_CAP/Carnetizacion/subir-archivos.php" method="POST" id="subir_archivos_form">

                                    <table class="table table-bordered table-hover table-sortable" id="tab_logic">
                                              <thead>
                                                    <tr>
                                                       <th class="text-center">
                                                         Nombre
                                                       </th>
                                                       <th class="text-center">
                                                         Archivo
                                                       </th>
                                                       <th class="text-center">
                                                         Tipo
                                                       </th>

                                                       <th class="text-center" style="border-top: 1px solid #ffffff; border-right: 1px solid #ffffff;">
                                                       </th>
                                                    </tr>
                                             </thead>
                                             <tbody>
                                                  <tr id='addr0' data-id="0" class="">
                                                      <td data-name="">
                                                          <input type="text" name="name"  placeholder='Name' class="form-control"/>
                                                      </td>
                                                      <td data-name="">
                                                        <input type="file" name="archivo" placeholder='file' class="form-control"/>
                                                      </td>

                                                      <td data-name="">
                                                        <select name="tipo">
                                                          <?PHP include '../../BUSINESS_CAP/Carnetizacion/archivosDigitalizados.business.php';?>
                                                        </select>
                                                      </td>
                                                      <td data-name="del">
                                                       <button nam"del0" class='btn btn-danger glyphicon glyphicon-remove row-remove'> </button>
                                                     </td>
                                                  </tr>
                                              </tbody>
                                   </table>                                                           
                                   <button type="submit" form="subir_archivos_form" value="Submit" class="btn btn-shadow btn-success "> <i class="icon-plus"></i>&nbsp;&nbsp; GUARDAR
                                   </button>
                                </form>
                              <hr>
                                <table  class="display table table-bordered table-striped " id="example">
                                    <thead>

                                      <tr>
                                        <th>tipo</th>
                                        <th>Archivo </th>
                                        <th></th>

                                      </tr>

                                    </thead>


                                      <tbody> 


                                        <?php

                                        include '../../BUSINESS_CAP/Carnetizacion/mostrarArchivosDig.business.php';

                                        ?>  

                                      </tbody>

                                        <tfoot>
                                          <tr>
                                           <th>Tipo</th>
                                           <th>Archivo </th>
                                           <th>////</th>

                                         </tr>
                                       </tfoot>
                                </table>

                         </div>

                   </div> 
             </div>
           </section>
         </div>
       </div>
       <!-- page end-->
     </section>
   </section>
   <!--main content end-->


   <!--footer start-->
   <footer class="site-footer">
    <div class="text-center">
      Alcaldia de San Salvador.
      <a href="#" class="go-top">
        <i class="icon-angle-up"></i>
      </a>
    </div>
  </footer>
  <!--footer end-->
</section>

<!-- js placed at the end of the document so the pages load faster -->
<script src="../../Resources/js/jquery.js"></script>
<script src="../../Resources/js/bootstrap.min.js"></script>
<<<<<<< HEAD
<script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>
=======
<script class="include" type="text/javascript" src="../../RESOURCES/js/jquery.dcjqaccordion.2.7.js"></script>
>>>>>>> fd59283090bc2565c1c8c18d8672ffb06ccebf28
<script src="../../Resources/js/jquery.scrollTo.min.js"></script>
<script src="../../Resources/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="../../Resources/js/respond.min.js" ></script>


<!--common script for all pages-->
<script src="../../Resources/js/common-scripts.js"></script>

<!--script for this page-->
<script src="../../Resources/js/jquery.stepy.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $("#add_row").on("click", function() {
                                        // Dynamic Rows Code
                                        //Filas dinámicos Código
                                        
                                        // Get max row id and set new id
                                        //Obtener max Identificación fila y establecer nueva identificación
                                        var newid = 0;
                                        $.each($("#tab_logic tr"), function() {
                                          if (parseInt($(this).data("id")) > newid) {
                                            newid = parseInt($(this).data("id"));
                                          }
                                        });
                                        newid++;
                                        
                                        var tr = $("<tr></tr>", {                                                                                                                                     
                                          id: "addr"+newid,
                                          "data-id": newid
                                        });
                                        
                                        // loop through each td and create new elements with name of newid
                                        //recorrer cada td y crear nuevos elementos con el nombre de newid
                                        $.each($("#tab_logic tbody tr:nth(0) td"), function() {
                                          var cur_td = $(this);

                                          var children = cur_td.children();

                                            // add new td and element if it has a nane
                                            //agregar nueva td y el elemento si tiene una nane
                                            if ($(this).data("name") != undefined) {
                                              var td = $("<td></td>", {
                                                "data-name": $(cur_td).data("name")
                                              });

                                              var c = $(cur_td).find($(children[0]).prop('tagName')).clone().val("");
                                              c.attr("name", $(cur_td).data("name") + newid);
                                              c.appendTo($(td));
                                              td.appendTo($(tr));
                                            } else {
                                              var td = $("<td></td>", {
                                                'text': $('#tab_logic tr').length
                                              }).appendTo($(tr));
                                            }
                                          });

                                        // add delete button and td
                                        //añadir botón de borrar y td
                                        /*
                                        $("<td></td>").append( 
                                            $("<button class='btn btn-danger glyphicon glyphicon-remove row-remove'></button>")
                                                .click(function() {
                                                    $(this).closest("tr").remove();
                                                })
                                        ).appendTo($(tr));
*/

                                        // add the new row
                                        //Añadir la nueva fila
                                        $(tr).appendTo($('#tab_logic'));
                                        
                                        $(tr).find("td button.row-remove").on("click", function() {
                                         $(this).closest("tr").remove();
                                       });
                                      });




                                    // Sortable Code
                                    //Código Ordenable
                                    var fixHelperModified = function(e, tr) {
                                      var $originals = tr.children();
                                      var $helper = tr.clone();

                                      $helper.children().each(function(index) {
                                        $(this).width($originals.eq(index).width())
                                      });

                                      return $helper;
                                    };

                                    $(".table-sortable tbody").sortable({
                                      helper: fixHelperModified      
                                    }).disableSelection();

                                    $(".table-sortable thead").disableSelection();



                                    $("#add_row").trigger("click");
                                  });
</script>


</body>
</html>
