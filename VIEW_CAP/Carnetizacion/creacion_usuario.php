<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include '../import_css.php'; ?>
 <?php include '../import_js.php';?>
   

  </head>

  <body >
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
  <section id="container" class="">
    
        <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
            <a class="logo" href="home.html"><img src="../../RESOURCES/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <a class="sublogo" href="home.html"><img src="../../RESOURCES/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>

            <?php include '../Session.php' ?>

            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->


           <!--INICIO DE MENU -->
      <aside>

           <?php include 'menu.php';?>


      </aside>
      
      <!--FINAL DE MENU-->





<!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-11">
                     
                      <section class="panel">
                          <header class="panel-heading">
                              Crear Usuario
                          </header>
                          <div class="panel-body">

                                    

                          


                               <!-- INICIO TABLA DE CREACION DE USUARIOS-->
                           


                                       <form action="../../BUSINESS_CAP/Carnetizacion/BSS_crear_usuario.php" method="POST" id="form1" >

                                          <table class=" table table-bordered table-hover table-sortable" id="tab_logic">
                                                <thead>
                                                  <tr >
                                                    <th class="text-center">
                                                      --
                                                    </th>
                                                    <th class="text-center">
                                                      CARNE DE EMPLEADO
                                                    </th>
                                                    <th class="text-center">
                                                      CODIGO DE SUBSISTEMA
                                                    </th>
                                                     <th class="text-center" style="border-top: 1px solid #ffffff; border-right: 1px solid #ffffff;">
                                                    </th>
                                                  </tr>
                                                </thead>


                                                <tbody>
                                                    <tr id='addr0' data-id="0" >
                                                    <td data-name="" >
                                                        
                                                    </td>
                                                    <td data-name="mail">
                                                        <input type="text" name='id_empleado' placeholder='nombre' class="form-control"/>
                                                    </td>
                                                    <td data-name="cod_sub_sistem">
                                                        <select name="cod_sub_sistem">
                                                          <option value"">Select Option</option>
                                                          <option value"1">Carnetizacion</option>
                                                          <option value"2">2</option>
                                                          <option value"3">3</option>
                                                      </select>
                                                    </td>
                                                    
                                                                <td data-name="del">
                                                                    <button  nam"del0" class='btn btn-danger glyphicon glyphicon-remove row-remove'> </button>
                                                                </td>
                                                  </tr>
                                                </tbody>
                                           </table>
                                          </form>
                                 
                                      

                                   
                                        <button type="submit" form="form1" value="Submit" class="btn btn-shadow btn-success "> <i class="icon-plus"></i>&nbsp;&nbsp; GUARDAR
                                        </button>
                                 
                                        <button type="button" form="form1" class="btn btn-shadow btn-info "> <i class="icon-pencil"></i>&nbsp;&nbsp;ACTUALIZAR
                                        </button>

                                        

                                          <button type="button" id="add_row" class="btn btn-shadow btn-primary pull-right"><i class="icon-plus-sign"></i>&nbsp;&nbsp;AGREGAR
                                        </button>
                          <!-- FIN TABLA DE CREACION DE USUARIOS-->




                          </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
                     
                
     





                             <script type="text/javascript">
                                $(document).ready(function() {
                                    $("#add_row").on("click", function() {
                                        // Dynamic Rows Code
                                        
                                        // Get max row id and set new id
                                        var newid = 0;
                                        $.each($("#tab_logic tr"), function() {
                                            if (parseInt($(this).data("id")) > newid) {
                                                newid = parseInt($(this).data("id"));
                                            }
                                        });
                                        newid++;
                                        
                                        var tr = $("<tr></tr>", {
                                            id: "addr"+newid,
                                            "data-id": newid
                                        });
                                        
                                        // loop through each td and create new elements with name of newid
                                        $.each($("#tab_logic tbody tr:nth(0) td"), function() {
                                            var cur_td = $(this);
                                            
                                            var children = cur_td.children();
                                            
                                            // add new td and element if it has a nane
                                            if ($(this).data("name") != undefined) {
                                                var td = $("<td></td>", {
                                                    "data-name": $(cur_td).data("name")
                                                });
                                                
                                                var c = $(cur_td).find($(children[0]).prop('tagName')).clone().val("");
                                                c.attr("name", $(cur_td).data("name") + newid);
                                                c.appendTo($(td));
                                                td.appendTo($(tr));
                                            } else {
                                                var td = $("<td></td>", {
                                                    'text': $('#tab_logic tr').length
                                                }).appendTo($(tr));
                                            }
                                        });
                                        
                                        // add delete button and td
                                        /*
                                        $("<td></td>").append(
                                            $("<button class='btn btn-danger glyphicon glyphicon-remove row-remove'></button>")
                                                .click(function() {
                                                    $(this).closest("tr").remove();
                                                })
                                        ).appendTo($(tr));
                                        */
                                        
                                        // add the new row
                                        $(tr).appendTo($('#tab_logic'));
                                        
                                        $(tr).find("td button.row-remove").on("click", function() {
                                             $(this).closest("tr").remove();
                                        });
                                });




                                    // Sortable Code
                                    var fixHelperModified = function(e, tr) {
                                        var $originals = tr.children();
                                        var $helper = tr.clone();
                                    
                                        $helper.children().each(function(index) {
                                            $(this).width($originals.eq(index).width())
                                        });
                                        
                                        return $helper;
                                    };
                                  
                                    $(".table-sortable tbody").sortable({
                                        helper: fixHelperModified      
                                    }).disableSelection();

                                    $(".table-sortable thead").disableSelection();



                                    $("#add_row").trigger("click");
                                });
                            </script>

          



      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              Alcaldia de San Salvador.
              <a href="#" class="go-top">
                  <i class="icon-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

   <?php include '../import_js.php'; ?>

  </body>
</html>
