<!DOCTYPE html>
<html lang="en">
  <head>
    
<?php include '../import_css.php'; ?>
 <?php include '../import_js.php';?>
  
    <style type="text/css">
                    .filterable {
                margin-top: 15px;
            }
            .filterable .panel-heading .pull-right {
                margin-top: -20px;
            }
            .filterable .filters input[disabled] {
                background-color: transparent;
                border: none;
                cursor: auto;
                box-shadow: none;
                padding: 0;
                height: auto;
            }
            .filterable .filters input[disabled]::-webkit-input-placeholder {
                color: #333;
            }
            .filterable .filters input[disabled]::-moz-placeholder {
                color: #333;
            }
            .filterable .filters input[disabled]:-ms-input-placeholder {
                color: #333;
            }

       </style>


    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
  </head>

  <body>

  <section id="container" class="">
        <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
            <a class="logo" href="home.html"><img src="../../RESOURCES/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <a class="sublogo" href="home.html"><img src="../../RESOURCES/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>

            <?php include '../Session.php' ?>

            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->


             <!--INICIO DE MENU -->
      <aside>

           <?php include 'menu.php';?>


      </aside>
      <!--FINAL DE MENU-->
 <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-11">
                     
                      <section class="panel">
                          <header class="panel-heading">
                              Busqueda de Carné
                          </header>
                          <div class="panel-body">



         <div class="panel panel-primary filterable ">
            <div class="panel-heading">

                <h3 class="panel-title"> MENORES</h3>
                <div class="pull-right">
                    <button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> BUSQUEDA</button>
                </div>
            </div>
               <table class="table">
                <thead>
                    <tr class="filters">
                        <th><input type="text" class="form-control" placeholder="Carnet" disabled></th>
                        <th><input type="text" class="form-control" placeholder="Primer nombre" disabled></th>
                        <th><input type="text" class="form-control" placeholder="Segundo nombre" disabled></th>
                        <th><input type="text" class="form-control" placeholder="Primer apellido" disabled></th>
                        <th><input type="text" class="form-control" placeholder="Segundo apellido" disabled></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>97297</td>
                        <td>Carlos</td>
                        <td>Alexander</td>
                        <td>Lopez</td>
                        <td>Mendez</td>
                    </tr>
                    <tr>
                        <td>345678</td>
                        <td>Cristian</td>
                        <td>Salvador</td>
                        <td>García</td>
                        <td>Padilla</td>
                    </tr>
                    <tr>
                        <td>80231</td>
                        <td>Nelson</td>
                        <td>Ariel</td>
                        <td>Fuentes</td>
                        <td>Pérez</td>
                    </tr>
                    <tr>
                        <td>86761</td>
                        <td>Xiomara</td>
                        <td>Saraí</td>
                        <td>Tejada</td>
                        <td>Bolaños</td>
                    </tr>
                </tbody>
                </table>
            </div>
 

                          </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->




<script type="text/javascript">
/*
Please consider that the JS part isn't production ready at all, I just code it to show the concept of merging filters and titles together !
*/
$(document).ready(function(){
    $('.filterable .btn-filter').click(function(){
        var $panel = $(this).parents('.filterable'),
        $filters = $panel.find('.filters input'),
        $tbody = $panel.find('.table tbody');
        if ($filters.prop('disabled') == true) {
            $filters.prop('disabled', false);
            $filters.first().focus();
        } else {
            $filters.val('').prop('disabled', true);
            $tbody.find('.no-result').remove();
            $tbody.find('tr').show();
        }
    });

    $('.filterable .filters input').keyup(function(e){
        /* Ignore tab key */
        var code = e.keyCode || e.which;
        if (code == '9') return;
        /* Useful DOM data and selectors */
        var $input = $(this),
        inputContent = $input.val().toLowerCase(),
        $panel = $input.parents('.filterable'),
        column = $panel.find('.filters th').index($input.parents('th')),
        $table = $panel.find('.table'),
        $rows = $table.find('tbody tr');
        /* Dirtiest filter function ever ;) */
        var $filteredRows = $rows.filter(function(){
            var value = $(this).find('td').eq(column).text().toLowerCase();
            return value.indexOf(inputContent) === -1;
        });
        /* Clean previous no-result if exist */
        $table.find('tbody .no-result').remove();
        /* Show all rows, hide filtered ones (never do that outside of a demo ! xD) */
        $rows.show();
        $filteredRows.hide();
        /* Prepend no-result row if all rows are filtered */
        if ($filteredRows.length === $rows.length) {
            $table.find('tbody').prepend($('<tr class="no-result text-center"><td colspan="'+ $table.find('.filters th').length +'">No result found</td></tr>'));
        }
    });
});
</script>



      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              Alcaldia de San Salvador.
              <a href="#" class="go-top">
                  <i class="icon-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>
  
   <?php include '../import_js.php'; ?>


  </body>
</html>
