

          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
                  <li>
                      <a href="home.php">
                          <i class="icon-dashboard"></i>
                          <span>Inicio</span>
                      </a>
                  </li>
                     <li class="sub-menu">
                      <a href="creacion_usuario.php">
                          <i class="icon-male"></i>
                          <span>Creación de Usuario</span>
                      </a>
                    
                  </li>
                  <li class="sub-menu">
                      <a href="emision_carne.php">
                          <i class="icon-male"></i>
                          <span>Emisión de Carné</span>
                      </a>
                    
                  </li>
              
                   <li class="sub-menu">
                      <a href="#">
                          <i class="icon-file"></i>
                          <span>Creación de Reportes</span>
                      </a>

                      <ul class="sub">
                          <li><a  href="in_parametros_fecha_reporte.php">Reporte por Fechas</a></li>
                          <li><a  href="in_nombre_solicitante_reporte.php">Reporte por Solicitante</a></li>
                          
                      </ul>
                    
                  </li>
                
                

                  <li class="sub-menu">
                      <a href="#">
                          <i class="icon-cog"></i>
                          <span> Mantenimiento Catálogos </span>
                      </a>
                    <ul class="sub">
                          <li><a  href="color_piel.php">Color Piel</a></li>
                          <li><a  href="color_ojo.php">Color Ojo</a></li>
                          <li><a  href="color_pelo.php">Color pelo</a></li>
                          <li><a  href="rasgos_especiales.php">Rasgo Especial</a></li>
                          <li><a  href="tipo_factura.php">Tipo de Factura</a></li>
                          <li><a  href="tipo_ocupacion.php">Ocupación</a></li>
                          <li><a  href="procedimiento_carne.php">Tipo de procedimiento Carné</a></li>
                      </ul>

                  </li>
              </ul>
              <!-- sidebar menu end-->
          </div>
       
