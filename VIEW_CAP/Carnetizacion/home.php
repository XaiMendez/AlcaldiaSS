<!DOCTYPE html>
<html lang="en">
  <head>

<?php include '../import_css.php'; ?>
 <?php include '../import_js.php';?>

  </head>

  <body >

  <section id="container" class="">
    
        <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
            <a class="logo" href="index.html"><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <a class="sublogo" href="index.html"><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>

            <?php //include '../Session.php' ?>

            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->


       <!--INICIO DE MENU -->
      <aside>

           <?php include 'menu.php';?>


      </aside>
      <!--FINAL DE MENU-->




 <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                             CREACION DE USUARIOS
                          </header>
                          <div class="panel-body">
                                <div class="adv-table">

                                     <table  class="display table table-bordered table-striped " id="example">
                                      <thead>
                                       
                                      <tr>
                                          <th>Partida de nacimiento</th>
                                          <th>Nombre </th>
                                          <th>apellido</th>
                                          <th></th>
                                      </tr>
                                    
                                      </thead>


                                      <tbody> 
                                         

                                        <?php

                                        include '../../BUSINESS_CAP/Carnetizacion/home.business.php';
                                       
                                        ?>  

                                      </tbody>

                                      <tfoot>
                                      <tr>
                                           <th>Partida de nacimiento</th>
                                          <th>Nombre </th>
                                          <th>apellido</th>
                                          <th></th>
                                      </tr>
                                      </tfoot>
                          </table>


                                </div>
                          </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->

 


      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              Alcaldia de San Salvador.
              <a href="#" class="go-top">
                  <i class="icon-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

    <?php include '../import_js.php'; ?>


<!--common script for all pages-->
    <script src="js/common-scripts.js"></script>

    <!--script for this page only-->

      <script type="text/javascript" charset="utf-8">
          $(document).ready(function() {
              $('#example').dataTable( {
                  "aaSorting": [[ 4, "desc" ]]
              } );
          } );
      </script>


  </body>
</html>
