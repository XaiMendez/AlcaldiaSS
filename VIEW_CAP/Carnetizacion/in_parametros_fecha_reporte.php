<!DOCTYPE html>
<html lang="en">
<head>
<?php include '../import_css.php'; ?>
 <?php include '../import_js.php';?>


</head>

<body>

	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
	<section id="container" class="">

		<!--Comienza el Header-->
		<div class="header white-bg">
			<!--Inicio del Logo-->
			<div class="header">
				<a class="logo" href="home.html"><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
				<a class="sublogo" href="home.html"><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
			
			</div>


			<!--Finaliza logo-->


		</div>
		<!--header end-->


		<!--INICIO DE MENU -->
		<aside>

			<?php include 'menu.php';?>


		</aside>

		<!--FINAL DE MENU-->

		<!--main content start-->
		<section id="main-content">
			<section class="wrapper site-min-height">
				<!-- page start-->
				<div class="row">
					<div class="col-lg-11">



						<center><h1>REPORTE POR FECHA</h1></center>

						<br>
						<br>

						<form action="../../BUSINESS_CAP/Carnetizacion/rpt_por_fechas.php" method="POST">

							<center><legend>FECHA INICIO</legend>
								<input type="date" name="fecha1"></center>
								<br>
								<br>
								<br>
								<center><legend>FECHA FINAL</legend>
									<input type="date" name="fecha2" >
									<br>

								</center>
								<br>
								<br>
								<br>
								<br>
								<center><input type="Submit" value="GENERAR REPORTE" class="btn btn-danger" name="boton" /> </center>
								<br>
								

							</form>

							<!-- INICIO TABLA DE CREACION DE USUARIOS-->
						</div>
					</section>
				</div>
			</div>
			<!-- page end-->
		</section>
	</section>
	<!--main content end-->

	<!--footer start-->
	<footer class="site-footer">
		<div class="text-center">
			Alcaldia de San Salvador.
			<a href="#" class="go-top">
				<i class="icon-angle-up"></i>
			</a>
		</div>
	</footer>
	<!--footer end-->
</section>

<?php include '../import_js.php'; ?>

</body>
</html>
