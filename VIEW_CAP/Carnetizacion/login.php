<!DOCTYPE html>
<html lang="en">

<head>
  <?php include '../import_css.php'; ?>
 <?php include '../import_js.php';?>
</head>

<body class="login-screen">

    <div class="container">
      <form class="form-signin" action="Carnetizacion/home.html">
        <h2 class="form-signin-heading"><img src="../Resources/img/login.png"/><img src="../Resources/img/login1.png"/></h2>
        <div class="login-wrap">
            <br>
              <input type="text" class="form-control" placeholder="Usuario" autofocus>
              <input type="password" class="form-control" placeholder="Contraseña ">
            <p>&nbsp</p>
            <button class="btn btn-lg btn-shadow btn-primary btn-block" type="submit">Ingresar</button>

        </div>
      </form>
    </div>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="../Resources/js/jquery.js"></script>
    <script src="../Resources/js/bootstrap.min.js"></script>

</body>

</html>
