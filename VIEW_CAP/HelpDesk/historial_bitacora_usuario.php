
<!DOCTYPE html>
<html lang="en">

<!-- head -->
    <?php include '../import_css.php';?>
<!-- /End head -->


  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
            <a class="logo" href="index.html"><img src="../img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <a class="sublogo" href="index.html"><img src="../img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->

       <!-- Main -->
          <?php include '../main.php';?>
       <!-- /End Main -->


      <!--Comienza contenido principal-->

      <section id="main-content">
        <section class="wrapper">
            <!-- page start-->
            <div class="row">
              <div class="col-lg-12">
                <section class="panel">
                  <header class="panel-heading">
                    Caso # 100123
                  </header>
                  <div class="panel-body">
                    <div class="adv-table">
                      <table  class="display table table-bordered table-striped" id="example">
                        <thead>
                          <tr>
                            <th>Codigo de Bitacora</th> <!-- id_solicitud_caso -->
                            <th>Fecha de seguimiento</th> <!-- fecha_caso -->
                            <th>Descripcion de Caso</th> <!-- descripcion_seguimiento-->
                            <th>Estado del Caso</th> <!-- id_estado_caso-->
                            <th>Codigo de Equipo</th> <!-- id_equipo_tecnologico-->
                            <th>Tecnico Asignado</th> <!-- id_deta_persona_tecnico -->
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>12</td>
                              <td>25/07/2015</td>
                              <td>Asignacion de Caso al tecnico Carlos Arias</td>
                              <td> Aprobado </td>
                              <td>125215</td>
                              <td>Ing. Mauricio Cisneros</td>
                            </tr>
                            <tr>
                              <td>13</td>
                              <td>26/07/2015</td>
                              <td>Se revisa el equipo y se realizan diagnosticos</td>
                              <td> Activo </td>
                              <td>125215</td>
                              <td>Carlos Arias</td>
                            </tr>
                            <tr>
                              <td>14</td>
                              <td>28/07/2015</td>
                              <td>Problema de disco duro, diagnosticos dan error en sectores del disco </td>
                              <td> Activo </td>
                              <td>125215</td>
                              <td>Carlos Arias</td>
                            </tr>
                            <tr>
                              <td>15</td>
                              <td>29/07/2015</td>
                              <td>Reemplazo de Disco duro y reinstalacion de sistema operativo y programas</td>
                              <td> Activo </td>
                              <td>125215</td>
                              <td>Carlos Arias</td>
                            </tr>
                            <tr>
                              <td>16</td>
                              <td>29/07/2015</td>
                              <td>Caso Solventado y equipo funcionando</td>
                              <td> Completado </td>
                              <td>125215</td>
                              <td>Ing. Mauricio Cisneros</td>
                            </tr>
                          </tbody>
                      </table>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          <!-- page end-->
        </section>
      </section>
      <!--main content end-->
      <!--footer start-->
      <div class="site-footer">
        <div class="text-center">
          2015 &copy; Alcaldia Municipal de San Salvador.
        </div>
      </div>
      <!--footer end-->
  </section>

   <?php include '../import_js.php';?>
  </body>
</html>
