<!DOCTYPE html>
<html lang="en">

<!-- head -->
    <?php include '../import_css.php';?>
<!-- /End head -->


  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
                <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>

                <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->

       <!-- Main -->
          <?php include '../main.php';?>
       <!-- /End Main -->


      <!--Comienza contenido principal-->

      <section id="main-content">
        <section class="wrapper">
            <!-- page start-->
            <div class="row">
              <div class="col-lg-12">
                <section class="panel">
                  <header class="panel-heading">
                      <center><h3>GESTION DE CASOS</h3></center>
                  </header>
                  <div class="panel-body">
                    <h4>Caso Seleccionado</h4>
                      <table  class="display table table-bordered table-striped" id="example">
                        <thead>
                          <tr>
                            <th>Codigo de Caso</th> <!-- id_solicitud_caso -->
                            <th>Descripción de Caso</th> <!-- fecha_caso -->
                            <th>Fecha Solicitud</th> <!-- descripcion_seguimiento-->
                            <th>Solicitante</th> <!-- id_estado_caso-->
                            <th>Equipo en gestion</th> <!-- EQUIPO-->
                            <th>Estado Caso</th> <!-- id_equipo_tecnologico-->
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                            <?php
                                include("../../DAO_CAP/HelpDesk/model.gestion_casos.php");
                                $idTCasoLog = $_POST['txtIdCasoGestionar'];
                                $objMisCasos = new GestionBitacoras();
                                $rsMisCasos = $objMisCasos->SelectCasoVista($idTCasoLog);
                                while ($filaMisCasos = pg_fetch_assoc($rsMisCasos)){
                                    $idEmpleadoEquipo = $filaMisCasos['id_deta_empleado_solicitante'];
                                ?> 
                                  <th>
                                      <?php print $filaMisCasos['id_solicitud_caso']; ?>
                                      
                                      
                                  </th>
                              <th><?php print $filaMisCasos['descripcion_caso'];?></th>
                              <th><?php print $filaMisCasos['fecha'];?></th>
                              <th><?php print $filaMisCasos['solicitante']; ?></th>
                              <th><?php print $filaMisCasos['codigo_equipo']; ?></th>
                              <th><?php print $filaMisCasos['estado_caso']; ?></th>

                          </tr>
                          <?php } ?>
                          </tbody>
                      </table>
                  </div>
                </section>
                  <section class="panel">
                      <header class="panel-heading"></header>
                  <header class="panel-heading">
                      <center><h3>BITACORAS DEL CASO</h3></center>
                  </header>
                  <div class="panel-body">
                      <table  class="display table table-bordered table-striped" id="example">
                        <thead>
                          <tr>
                            <th>Id Bitacora</th>
                            <th>Descripción de Seguimiento</th> <!-- fecha_caso -->
                            <th>Fecha Seguimiento</th> <!-- descripcion_seguimiento-->
                            <th>Tipo Caso</th> <!-- id_estado_caso-->
                            <th>Escalabilidad</th> <!-- id_equipo_tecnologico-->
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                            <?php
                                $rsBitaco = $objMisCasos->SelectBitacorasGestionCaso($idTCasoLog);
                                while ($filaMisCasosBita = pg_fetch_assoc($rsBitaco)){
                                ?> 
                              <th><?php print $filaMisCasosBita['id_bitacora_caso']; ?></th>
                              <th><?php print $filaMisCasosBita['descripcion_seguimiento'];?></th>
                              <th><?php print $filaMisCasosBita['fecha_seguimiento'];?></th>
                              <th><?php print $filaMisCasosBita['tipo_caso']; ?></th>
                              <th><?php print $filaMisCasosBita['escalabilidad']; ?></th>
                          </tr>
                          <?php } ?>
                          </tbody>
                      </table>
                      
                  </section>
              </div>
            </div>
          <!-- page end-->
        </section>
      </section>
      <!--main content end-->
      <!--footer start-->
      <div class="site-footer">
        <div class="text-center">
          2015 &copy; Alcaldia Municipal de San Salvador.
        </div>
      </div>
      <!--footer end-->
  </section>

    <?php include '../import_js.php';?>
  </body>
</html>
