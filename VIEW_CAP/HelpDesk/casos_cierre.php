<!DOCTYPE html>
<html lang="en">

<!-- head -->
    <?php include '../import_css.php';?>
<!-- /End head -->
<body>
  <section id="container" >
    <!--Comienza el Header-->
    <div class="header white-bg">
      <!--Inicio del Logo-->
      <div class="header">
        <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
        <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
          <?php include '../Session.php' ?>
      </div>
      <!--Finaliza logo-->
    </div>
    <!--header end-->
    <!-- Main -->
    <?php include '../main.php';?>
    <!-- /End Main -->
    <!--Comienza contenido principal-->
    <section id="main-content">
      <section class="wrapper">
        <!-- page start-->
        <div class="row">
          <div class="col-lg-12">
            <section class="panel">
              <header class="panel-heading">
                <center><h3>Caso Seleccionado</h3></center>
              </header>
              <div class="panel-body">
                <table  class="table table-bordered table-striped table-condensed cf" id="example">
                  <thead>
                    <tr>
                      <th>Codigo de Caso</th> <!-- id_solicitud_caso -->
                      <th>Descripción de Caso</th> <!-- fecha_caso -->
                      <th>Fecha Solicitud</th> <!-- descripcion_seguimiento-->
                      <th>Solicitante</th> <!-- id_estado_caso-->
                      <th>Equipo en gestion</th> <!-- EQUIPO-->
                      <th>Estado Caso</th> <!-- id_equipo_tecnologico-->
                      <th>Calificación del Caso</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <?php
                        include("../../DAO_CAP/HelpDesk/model.gestion_casos.php");
                        $idTCasoLog = $_SESSION['IdFinalizarCaso'];
                        $objMisCasos = new GestionBitacoras();
                        $rsMisCasos = $objMisCasos->SelectCasoVista($idTCasoLog);
                          while ($filaMisCasos = pg_fetch_assoc($rsMisCasos)){
                            $idEmpleadoEquipo = $filaMisCasos['id_deta_empleado_solicitante'];
                      ?>
                      <th>
                        <?php print $filaMisCasos['id_solicitud_caso']; ?>
                      </th>
                      <th><?php print $filaMisCasos['descripcion_caso'];?></th>
                      <th><?php print $filaMisCasos['fecha'];?></th>
                      <th><?php print $filaMisCasos['solicitante']; ?></th>
                      <th><?php print $filaMisCasos['codigo_equipo']; ?></th>
                      <th><?php print $filaMisCasos['estado_caso']; ?></th>
                      <th><?php print $filaMisCasos['calificacion_caso']; ?></th>
                    </tr>
                      <?php } ?>
                  </tbody>
                </table>
              </div>
            </section>
            <section class="panel">
              <header class="panel-heading">
                <center><h3>Gestion de Caso</h3></center>
              </header>
              <div class="panel-body">
                <center>
                  <a href="#myModalFinalizarCaso" data-toggle="modal" class="btn btn-info">Finalizar Caso</a>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <a href="#myModalReAbrirCaso" data-toggle="modal" class="btn btn-info">Re-Abrir Caso</a>
                </center>
                <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalFinalizarCaso" class="modal fade">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                        <h4 class="modal-title">Finalizar Caso</h4>
                        </div>
                        <div class="modal-body">
                          <form action="../../BUSINESS_CAP/HelpDesk/ProcAsignarFinalizarCaso.php" method="POST">
                            <div class="form-group">
                              <label class=" p-head">Descripción</label>
                                <input type="text" name="txtDescFinalizarCaso" maxlength="100" class="form-control" placeholder="Describa la finalización del caso" required>
                                <br/>
                                <center><button type="submit" class="btn btn-default" name="FinalizarCaso">Finalizar Caso</button></center>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalReAbrirCaso" class="modal fade">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                          <h4 class="modal-title">Abrir Nuevamente el Caso</h4>
                          </div>
                          <div class="modal-body">
                            <form action="../../BUSINESS_CAP/HelpDesk/ProcAsignarFinalizarCaso.php" method="POST">
                              <div class="form-group">
                                <?php
                                include '../../DAO_CAP/HelpDesk/cbx_AsignarCaso.php' ;
                                ?>
                                <label class="control-label">Descripción</label>
                                  <input type="text" name="txtDescAsignacion" maxlength="400" class="form-control col-sm-12" placeholder="Describa la nueva apertura del caso" required>
                                </br></br>
                                <div class="form-group">
                                  <label class="col-sm-2 control-label">Escalabilidad:</label>
                                  <div class="col-sm-3">
                                    <?php echo combo_escalabilidad(); ?>
                                  </div>
                                  <label class="col-sm-3 control-label">Tipo de Caso:</label>
                                  <div class="col-sm-4">
                                    <?php echo combo_tipo_caso(); ?>
                                  </div>
                                </div>
                                <br/>
                                <center><button type="submit" class="btn btn-default" name="ReAbrirCaso">Abrir Nuevamente el Caso</button></center>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
              </section>
              <section class="panel">
                <header class="panel-heading">
                  <center><h3>BITACORAS DEL CASO</h3></center>
                </header>
                <div class="panel-body">
                  <table  class="display table table-bordered table-striped" id="example">
                    <thead>
                      <tr>
                        <th>Id Bitacora</th>
                        <th>Descripción de Seguimiento</th> <!-- fecha_caso -->
                        <th>Fecha Seguimiento</th> <!-- descripcion_seguimiento-->
                        <th>Tipo Caso</th> <!-- id_estado_caso-->
                        <th>Escalabilidad</th> <!-- id_equipo_tecnologico-->
                        <th>Creado por</th>
                      </tr>
                    </thead>
                  <tbody>
                    <tr>
                      <?php
                        $rsBitaco = $objMisCasos->SelectBitacorasGestionCaso($idTCasoLog);
                        while ($filaCasosBita = pg_fetch_assoc($rsBitaco)){
                      ?>
                    <th><?php print $filaCasosBita['id_bitacora_caso']; ?></th>
                    <th><?php print $filaCasosBita['descripcion_seguimiento'];?></th>
                    <th><?php print $filaCasosBita['fecha_seguimiento'];?></th>
                    <th><?php print $filaCasosBita['tipo_caso']; ?></th>
                    <th><?php print $filaCasosBita['escalabilidad']; ?></th>
                    <th><?php print $filaCasosBita['empleado']; ?></th>
                  </tr>
                    <?php
                       }
                    ?>
                </tbody>
              </table>
            </section>
          </div>
        </div>
        <!-- page end-->
      </section>
    </section>
    <!--main content end-->
    <!--footer start-->
    <div class="site-footer">
      <div class="text-center">
        2015 &copy; Alcaldia Municipal de San Salvador.
      </div>
    </div>
    <!--footer end-->
  </section>
    <?php include '../import_js.php';?>
  </body>
</html>
