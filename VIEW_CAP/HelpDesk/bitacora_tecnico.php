<!DOCTYPE html>
<html lang="en">

<!-- head -->
    <?php include '../import_css.php';?>
<!-- /End head -->


  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
            <a class="logo" href="index.html"><img src="../img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <a class="sublogo" href="index.html"><img src="../img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->

      <!-- Main -->
          <?php include '../main.php';?>
      <!-- /End Main -->

      <!--Comienza contenido principal-->

      <section id="main-content">
        <section class="wrapper">
            <!-- page start-->
            <div class="row">
              <div class="col-lg-12">
                <section class="panel">
                  <header class="panel-heading">
                    Crear bitacora
                  </header>
                  <div class="panel-body">
                    <form class="form-horizontal tasi-form" method="get">
                      <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Descripcion de Caso:</label>
                        <label class="col-sm-10 control-label" >
                            El Computador queda con pantalla negro y da mensaje en letra blanca
                        </label>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Fecha de Creacion:</label>
                        <label class="col-sm-3 control-label" >
                            25/07/2015
                        </label>
                        <label class="col-sm-2 col-sm-2 control-label">Solicitante del Caso:</label>
                          <label class="col-sm-3 control-label" >
                            Rodrigo Vaquerano
                          </label>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Codigo del Equipo:</label>
                        <label class="col-sm-3  control-label" >
                            51681321
                        </label>
                        <label class="col-sm-2 col-sm-2 control-label">Marca del Equipo:</label>
                          <label class="col-sm-3 control-label" >
                            Dell
                          </label>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Descripcion Caso:</label>
                          <div class="col-sm-3">
                            <textarea class="form-control" cols="60" rows="2"></textarea>
                          </div>
                        <label class="col-sm-2 col-sm-2 control-label">Tecnico a cargo:</label>
                          <div class="col-sm-3">
                            <select class="form-control m-bot15 ">
                              <option></option>
                              <option>Ronald Arias</option>
                              <option>Ricardo Lopez </option>
                              <option>KOsvaldo Serrano</option>
                            </select>
                          </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Escalabilidad:</label>
                        <div class="col-sm-3">
                            <select class="form-control m-bot15 ">
                              <option>Normal</option>
                              <option>Baja</option>
                              <option>Urgente</option>
                            </select>
                          </div>
                        <label class="col-sm-2 control-label">Tipo de Caso:</label>
                        <div class="col-sm-3">
                            <select class="form-control m-bot15 ">
                              <option>Hardware</option>
                              <option>Software</option>
                              <option>Red</option>
                              <option>Otro</option>
                            </select>
                          </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-2"></div>
                        <div class="col-sm-3">
                          <button class="btn btn-md btn-shadow btn-success btn-block " type="submit">Ingresar</button>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-sm-3">
                          <button class="btn btn-md btn-shadow btn-danger btn-block " type="submit">Cancelar</button>
                        </div>

                      </div>
                    </form>
                  </div>
                </section>
              </section>
            </div>
          </div>
          <!-- page end-->
        </section>
      </section>
      <!--main content end-->
      <!--footer start-->
      <div class="site-footer">
        <div class="text-center">
          2015 &copy; Alcaldia Municipal de San Salvador.
        </div>
      </div>
      <!--footer end-->
  </section>

    <?php include '../import_js.php';?>
    
  </body>
</html>
