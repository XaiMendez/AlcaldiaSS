<!DOCTYPE html>
<html lang="en">

<!-- head -->
    <?php include '../import_css.php';?>
<!-- /End head -->


  <body>

    <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
            <a class="logo" href="index.html"><img src="../img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <a class="sublogo" href="index.html"><img src="../img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->

      <!-- Main -->
          <?php include '../main.php';?>
      <!-- /End Main -->


        <!--Comienza contenido principal-->
      <section id="main-content">
        <section class="wrapper">
          <!-- page start-->

          <div class="row">
            <div class="col-lg-12">
              <section class="panel">
                <header class="panel-heading">
                  Casos del tecnico
                </header>
                 <form name="form1" method="post" action="">
<label>Buscar por fecha:
<input type="text" name="txtBusqueda" id="txtBusqueda">
</label>
<input type="submit" name="cmdBuscar" id="cmdBuscar" value="IR">
</form>
<td>
           <form name="form1" method="post" action="">
<label>Buscar por numero:
<input type="text" name="txtBusqueda" id="txtBusqueda">
</label>
<input type="submit" name="cmdBuscar" id="cmdBuscar" value="IR">
</form>
</td>
                <div class="panel-body">
                  <div class="adv-table">
                    <table  class="display table table-bordered table-striped" id="finalizar">
                      <thead>
                        <tr>
                          <th>Codigo de Caso</th> <!-- id_solicitud_caso -->
                          <th>Descripcion de Caso</th> <!-- descripcion_caso-->
                          <th>Fecha de Seguimiento</th> <!-- fecha_seguimiento -->
                          <th>Estado del Caso</th> <!-- id_estado_caso-->
                          <th>Historial del caso</th> <!-- boton para asignar caso -->
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>200123</td>
                          <td>Reemplazo de cooler y chequeo por sobrecalentamiento</td>
                          <td>25/07/2015</td>
                          <td>Completado</td>
                          <td>
                            <center>
                              <a class="edit" href="Gestion_casos.php">
                                <button class="btn btn-primary btn-sm btn-block">
                                  <i class="icon-file"></i>&nbsp&nbsp Historial
                                </button>
                              </a>
                          </td>
                        <tr>
                          <td>200124</td>
                          <td>Monitor mostraba imagen con resolucion pesima, cambio de monitor</td>
                          <td>26/07/2015</td>
                          <td>Completado</td>
                          <td>
                            <center>
                              <a class="edit" href="Gestion_casos.php">
                                <button class="btn btn-primary btn-sm btn-block">
                                  <i class="icon-file"></i>&nbsp&nbsp Historial
                                </button>
                              </a>
                          </td>
                        </tr>
                        <tr>
                          <td>200125</td>
                          <td>No hay acceso a internet, no se puede ingresar a facebook</td>
                          <td>27/07/2015</td>
                          <td>Pendiente</td>
                          <td>
                            <center>
                              <a class="edit" href="Gestion_casos.php">
                                <button class="btn btn-primary btn-sm btn-block">
                                  <i class="icon-file"></i>&nbsp&nbsp Historial
                                </button>
                              </a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </section>
            </div>
          </div>
            <!-- page end-->
        </section>
      </section>
        <!--main content end-->
        <!--footer start-->
      <div class="site-footer">
        <div class="text-center">
          2015 &copy; Alcaldia Municipal de San Salvador.
        </div>
      </div>
        <!--footer end-->
    </section>
    <?php include '../import_js.php';?>
  </body>
</html>
