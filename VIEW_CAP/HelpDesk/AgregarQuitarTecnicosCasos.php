
<!DOCTYPE html>
<html lang="en">
    <?php include '../import_css.php'; ?>

    <body>

        <section id="container" >
            <!--Comienza el Header-->
            <div class="header white-bg">
                <!--Inicio del Logo-->
                <div class="header">
                    <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                    <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>

                    <?php include '../Session.php' ?>
                </div>
                <!--Finaliza logo-->
            </div>
            <!--header end-->

            <!-- Main -->
            <?php include '../main.php'; ?>
            <!-- /End Main -->

            <!--Comienza contenido principal-->
            <section id="main-content">
                <section class="wrapper">
                    <div class="row"> <!-- div 1-->
                        <div class="col-lg-12"> <!-- div 2-->
                            <section class="panel">
                                <header class="panel-heading">
                                    <center><h2>MANTENIMIENTO DE TÉCNICOS EN CASO</h2></center>
                                </header>
                                <div class="panel-body"> <!-- div 3-->
                                    <center><button type="button" onclick="location='ver_casos_abiertos.php'" class="btn btn-info">Regresar</button></center>
                                        <div class="form-group">
                                            <label><h3>Técnicos Actuales</h3></label>
                                            <header class="panel-heading">
                                            </header>
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Id Técnico</th>
                                                        <th>Codigo Técnico</th>
                                                        <th>Nombre</th>
                                                        <th>Especialidad</th>
                                                        <th>Fecha Operación</th>
                                                        <th>Asignado Por</th>
                                                        <th> </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <?php
                                                        include("../../DAO_CAP/HelpDesk/model.gestion_casos.php");
                                                        $idCasoSelec = $_SESSION['IdCasoAbiertosAdmin'];
                                                        $objAsignarTecnicos = new GestionBitacoras();
                                                        $rsTecnicosActuales = $objAsignarTecnicos->SelectTecnicosEnCasoActivo($idCasoSelec);
                                                        while ($filaTecnicosActivos = pg_fetch_assoc($rsTecnicosActuales)) {
                                                        ?>
                                                        <form name="quitarTecnicoCaso" action="../../BUSINESS_CAP/HelpDesk/ProcAgregarQuitarTecnico.php" method="POST">
                                                        <th>
                                                            <?php print $filaTecnicosActivos['id_deta_empleado_tecnico']; ?>
                                                            <input name="txtIdTecnicoQuitar" value="<?php print $filaTecnicosActivos['id_caso_tecnico']; ?>" readonly hidden>
                                                        </th>
                                                        <th><?php print $filaTecnicosActivos['codigo_empleado']; ?></th>
                                                        <th><?php print $filaTecnicosActivos['nombre']; ?></th>
                                                        <th><?php print $filaTecnicosActivos['especialidad_tecnico']; ?></th>
                                                        <th><?php print $filaTecnicosActivos['fecha_operacion']; ?></th>
                                                        <th><?php print $filaTecnicosActivos['codigo_empleado']; ?></th>
                                                        <th>
                                                            <button type="submit" name="QuitarTecnicoCaso" class="btn btn-danger">Quitar Técnico</button>
                                                        </th>
                                                        </form>
                                                        </tr>
                                                        <?php
                                                        }
                                                        ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label><h3>Agregar Técnicos Al Caso</h3></label>
                                                <header class="panel-heading"></header>
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Id Técnico</th>
                                                            <th>Codigo Técnico</th>
                                                            <th>Nombre</th>
                                                            <th>Especialidad</th>
                                                            <th>Numero de Casos Abiertos</th>
                                                            <th> </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                        <?php
                                                        $rsTecnicosParaAsignar = $objAsignarTecnicos->SelectTecnicosParaAsignarCaso($idCasoSelec);
                                                        while ($filaTecnicosAsignar = pg_fetch_assoc($rsTecnicosParaAsignar)){
                                                        ?>
                                                        <form name="agregarTecnicoCaso" action="../../BUSINESS_CAP/HelpDesk/ProcAgregarQuitarTecnico.php" method="POST">
                                                        <th>
                                                            <?php print $filaTecnicosAsignar['id_deta_empleado']; ?>
                                                            <input name="txtIdTecnicoAsignar" value="<?php print $filaTecnicosAsignar['id_deta_empleado']; ?>" readonly hidden>
                                                            <input name="txtIdCasoAgregarTecnico" value="<?php print $idCasoSelec; ?>" readonly hidden>
                                                        </th>
                                                        <th><?php print $filaTecnicosAsignar['codigo_empleado']; ?></th>
                                                        <th><?php print $filaTecnicosAsignar['nombre']; ?></th>
                                                        <th><?php print $filaTecnicosAsignar['especialidad_tecnico']; ?></th>
                                                        <th><?php print $filaTecnicosAsignar['casosabiertos']; ?></th>
                                                        <th>
                                                            <button type="submit" name="AgregarTecnicoCaso" class="btn btn-success">Agregar Técnico</button>
                                                        </th>
                                                        </form>
                                                        </tr>
                                                        <?php
                                                        }
                                                        ?>
                                                        </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        </section>
                                </div> <!-- div 2-->
                        </div>  <!-- div 1-->
                </section>
            </section>


            <!--Finaliza contenido principal-->

            <!--footer start-->
            <footer class="site-footer">
                <div class="text-center">
                    2015 &copy; Alcaldia Municipal de San Salvador.
                </div>
            </footer>
            <!--footer end-->
        </section>

        <!-- js placed at the end of the document so the pages load faster -->
        <script src="../../Resources/js/jquery.js"></script>
        <script src="../../Resources/js/jquery-1.8.3.min.js"></script>
        <script src="../../Resources/js/bootstrap.min.js"></script>
        <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>
        <script src="../../Resources/js/jquery.scrollTo.min.js"></script>
        <script src="../../Resources/js/jquery.nicescroll.js" type="text/javascript"></script>
        <script src="../../Resources/js/jquery.sparkline.js" type="text/javascript"></script>
        <script src="../../Resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
        <script src="../../Resources/js/owl.carousel.js" ></script>
        <script src="../../Resources/js/jquery.customSelect.min.js" ></script>
        <script src="../../Resources/js/respond.min.js" ></script>

        <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>

        <!--common script for all pages-->
        <script src="../../Resources/js/common-scripts.js"></script>

        <!--script for this page-->
        <script src="../../Resources/js/sparkline-chart.js"></script>
        <script src="../../Resources/js/easy-pie-chart.js"></script>
        <script src="../../Resources/js/count.js"></script>

        <!-- SCRIPTS PARA LA DATA TABLE EDITABLE -->
        <script type="text/javascript" src="../../Resources/assets/data-tables/jquery.dataTables.js"></script>
        <script type="text/javascript" src="../../Resources/assets/data-tables/DT_bootstrap.js"></script>
        <script>
            jQuery(document).ready(function() {
                EditableTable.init();
            });
        </script>

        <!--SCRIPT PARA TABLA FILTRADA -->
        <script type="text/javascript" language="javascript" src="../assets/advanced-datatable/media/js/jquery.dataTables.js"></script>

        <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
            $('#example').dataTable({
                "aaSorting": [[4, "desc"]]
            });
        });
        </script>

        <script>

            //owl carousel

            $(document).ready(function() {
                $("#owl-demo").owlCarousel({
                    navigation: true,
                    slideSpeed: 300,
                    paginationSpeed: 400,
                    singleItem: true,
                    autoPlay: true

                });
            });

            //custom select box

            $(function() {
                $('select.styled').customSelect();
            });

        </script>

    </body>
</html>
