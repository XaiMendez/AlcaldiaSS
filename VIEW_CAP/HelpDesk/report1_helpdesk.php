
<!DOCTYPE html>
<html lang="en">

<!-- head -->
<?php include '../import_css.php';?>
<!-- /End head -->


<body>

  <section id="container" >
    <!--Comienza el Header-->
    <div class="header white-bg">
      <!--Inicio del Logo-->
      <div class="header">
        <a class="logo" href="index.html"><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
        <a class="sublogo" href="index.html"><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
        <?php include '../Session.php' ?>
      </div>
      <!--Finaliza logo-->
    </div>
    <!--header end-->

    <!-- Main -->
    <?php include '../main.php';?>
    <!-- /End Main -->
  <form name="formRptEquipos" action="../../BUSINESS_CAP/HelpDesk/Rpt_Historial_Tecnico.php" method="POST">

    <!--Comienza contenido principal-->


    <section id="main-content">
      <section class="wrapper">
        <!-- page start-->
        <div class="form-group">

          <header class="panel-heading">
            <center><h2>Historial de Caso por Tecnico </h2></center>
          </header>
          <?php
          require ("../../DAO_CAP/HelpDesk/model.report1_helpdesk.php");
          $objtecnico = new reportes1_helpdesk();
          ?>

          <div class ="col-lg-4">
              <header class ="panel-heading">
                Seleccionar Tecnico:
              </header>

              <div class="">
                <select name="cbxtecnico" class="form-control m-bot15">
                  <?php

                  $rstecnico = $objtecnico->SelectIdTecnico();
                  while ($tecnicoVar = pg_fetch_array($rstecnico)) {
                    ?>
                    <option value="<?php echo $tecnicoVar['id_persona']; ?>"><?php echo $tecnicoVar['nombre_tecnico']; ?></option>
                    
                    <?php
                  }
                  ?>
                </select>
            </div>
          </div>
        </div>


    </br>

        <button type="submit" name="GenerarPdf" class="btn btn-danger">Generar PDF</button>

            </br></br></br></br>
      <section class="panel">



        </section>
      </div>
    </div>
    <div class="row">

    <!-- page end-->
  </section>
</section>
<!--main content end-->
<!--footer start-->
<div class="site-footer">
  <div class="text-center">
    2015 &copy; Alcaldia Municipal de San Salvador.
  </div>
</div>
<!--footer end-->
</section>

<!-- js placed at the end of the document so the pages load faster -->
 <script src="../../Resources/js/jquery.js"></script>
<script src="../../Resources/js/jquery-1.8.3.min.js"></script>
<script src="../../Resources/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="../../Resources/js/jquery.scrollTo.min.js"></script>
<script src="../../Resources/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="../../Resources/js/jquery.sparkline.js" type="text/javascript"></script>
<script src="../../Resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
<script src="../../Resources/js/owl.carousel.js" ></script>
<script src="../../Resources/js/jquery.customSelect.min.js" ></script>
<script src="../../Resources/js/respond.min.js" ></script>

<script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>

<!--common script for all pages-->
<script src="../../Resources/js/common-scripts.js"></script>

<!--script for this page-->
<script src="../../Resources/js/sparkline-chart.js"></script>
<script src="../../Resources/js/easy-pie-chart.js"></script>
<script src="../../Resources/js/count.js"></script>

<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.js" type="text/javascript"></script>


<script>

//owl carousel

$(document).ready(function() {
    $("#owl-demo").owlCarousel({
        navigation : true,
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem : true,
  autoPlay:true

    });
});

//custom select box
$(function(){
    $('select.styled').customSelect();
});

</script>

<?php include '../import_js.php';?>
</body>
</html>
