<?php

//
include_once("../../DAO_CAP/Connection/Connection.class.php");

class Codigo_subsistema{


  private $id_codigo_subsistema;
	private $descripcion_subsistema;

	public function getId_codigo_subsistema(){
		return $this->id_codigo_subsistema;
	}

	public function setId_codigo_subsistema($id_codigo_subsistema){
		$this->id_codigo_subsistema = $id_codigo_subsistema;
	}

	public function getdescripcion_subsistema(){
		return $this->descripcion_subsistema;
	}

	public function setdescripcion_subsistema($descripcion_subsistema){
		$this->descripcion_subsistema = $descripcion_subsistema;
	}

	//--------------------------------------------

	function insertCodigo_Subsistema(){

		$fcnid_codigo_subsistema = $this -> setid_codigo_subsistema();
		$fcndescripcion_subsistema = $this -> setdescripcion_subsistema();
		$insert = "INSERT INTO codigo_subsistema (id_codigo_subsistema, descripcion_subsistema) values ('', '$fcndescripcion_subsistema')";
	
		$result = pg_query($con, $insert);

		if (!$result) {
			echo pg_last_error($con);
		}else{
			echo "Insertado con exito";
		}
		pg_close($con);
	}

	function selectCodigo_Subsistema(){
		$fcnid_codigo_subsistema = $this -> getid_codigo_subsistema();
		$fcndescripcion_subsistema = $this -> getdescripcion_subsistema();
		$select = "SELECT * FROM codigo_subsistema";

		$result = pg_query($con, $select);

		echo "Fetch row: <br>";

		while ($row = pg_fetch_row($result)) {
		print_r($row[1]);
		}
	}
	
	function updateCodigo_Subsistema(){
		$fcnid_codigo_subsistema = $this -> setid_codigo_subsistema();
		$fcndescripcion_subsistema = $this -> setdescripcion_subsistema();
		$update = "UPDATE codigo_subsistema SET descripcion_subsistema= '$fcndescripcion_subsistema' WHERE id_codigo_subsistema = '$fcnid_codigo_subsistema'";

		$result = pg_query($con, $update);
		if (!$result) {
			echo pg_last_error($con);
		}else {
			echo "Actualizado Correctamente";
		}
		pg_close($con);
	}

	function deleteCodigo_Subsistema(){
		$fcnid_codigo_subsistema = $this -> getid_codigo_subsistema();
		$delete = "DELETE FROM codigo_subsistema WHERE id_codigo_subsistema='$fcnid_codigo_subsistema'";

		$result = pg_query($con, $delete);
		if(!$result){
			  echo pg_last_error($con);
		} else {
	  		echo "Eliminado Correctamente\n";
		}

		pg_close($con);

	}
}

 ?>
