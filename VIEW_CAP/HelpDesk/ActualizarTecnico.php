<!DOCTYPE html>
<html lang="en">

<!-- head -->
    <?php include '../import_css.php';?>
<!-- /End head -->


  <body>

  <section id="container" class="">

      <!-- ***** Comienza el Header ****** -->
      <div class="header white-bg">
        <!--Inicio del Logo-->
        <div class="header">
          <div class="sidebar-toggle-box"> <!-- para esconder y mostrar la Sidebar -->
          <div data-original-title="Menu Emergente" data-placement="right" class="icon-reorder tooltips"></div>
        </div>
          <a class="logo" href="index.html"><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
          <a class="sublogo" href="index.html"><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>

          <?php include '../Session.php' ?>

        </div>
            <!--Finaliza logo-->
      </div>
      <!-- ****** header end ****** -->

      <!-- Main -->
        <?php include '../main.php';


        $id_deta_empleado=$_GET['iddetaempleado'];
         $nombre=$_GET['nombre'];
         $especialidad_tecnico=$_GET['especialidad_tecnico'];
         $estado_empleado=$_GET['estado_empleado'];
         $area=$_GET['area'];


        ?>
      <!-- /End Main -->

      <section id="main-content">
        <section class="wrapper">
          <!-- page start-->
          <div class="row">
            <div class="col-lg-12">
              <div class="panel">
                <header class="panel-heading">
                  Ingreso de Técnicos
                </header>
              </div>
              <div class="col-lg-6">
                <section class="panel">
                  <header class="panel-heading">
                    Datos
                  </header>
                  <div class="panel-body">
                      <?php
                      require ("../../DAO_CAP/HelpDesk/model.ingreso_tecnico.php");
                      $objIngresoTec = new IngresoTecnico();
                      ?>

                    <form action="../../BUSINESS_CAP/HelpDesk/ProcIngresoTecnico.php" name="IngresoTecnico" method="POST">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Código</label>
                        <input type="text" name="txtCodTec" maxlength="25" minlength="5" class="form-control"
                           value="<?php echo $id_deta_empleado; ?>"required>
                      </div>
                      <label for="exampleInputEmail1">Código</label>
                      <input type="text" name="cbxPersona" maxlength="25" minlength="5" class="form-control"
                         value="<?php echo $nombre; ?>"required>


                      <div class="form-group">
                        <label for="exampleInputEmail1">Especialidad Técnico</label>
                        <div class="">
                          <select name="cbxEspecialidad" class="form-control m-bot15">
                            <?php
                            $rsEspecialidadTec = $objIngresoTec->SelectIdEspecialidadTec();
                            while ($especialidadTecVar = pg_fetch_array($rsEspecialidadTec)) {
                            ?>
                            <option value="<?php echo $especialidadTecVar['id_especialidad_tecnico']; ?>"><?php echo $especialidadTecVar['especialidad_tecnico']; ?></option>
                              <?php
                                }
                              ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Estado</label>
                        <div class="">
                          <select name="cbxEstadoEmpleado" class="form-control m-bot15">
                            <?php
                            $rsEstadoEmp = $objIngresoTec->SelectIdEstadoEmpleado();
                            while ($estadoEmpVar = pg_fetch_array($rsEstadoEmp)) {
                            ?>
                            <option value="<?php echo $estadoEmpVar['id_estado_empleado']; ?>"><?php echo $estadoEmpVar['estado_empleado']; ?></option>
                              <?php
                                }
                              ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Área Distrito</label>
                        <div class="">
                          <select name="cbxAreaDistrito" class="form-control m-bot15">
                            <?php
                            $rsAreaDistrito = $objIngresoTec->SelectIdAreaDistrito();
                            while ($areaDistritoVar = pg_fetch_array($rsAreaDistrito)) {
                            ?>
                            <option value="<?php echo $areaDistritoVar['id_area_distrito']; ?>"><?php echo $areaDistritoVar['area']; ?></option>
                              <?php
                                }
                              ?>
                          </select>
                        </div>
                      </div>
                  </div>
                  <center><button type="submit" class="btn btn-shadow btn-success btn-md " name="ActualizarTecnico">Actualizar Técnico</button>
                          <button type="submit" class="btn btn-shadow btn-danger btn-md" name="Cancelar">Cancelar</button>
                  </center>
                </form>
                </section>
              </div>


            </div>
          </div>
            <!-- page end-->
        </section>
      </section>

      <!--footer start-->
      <div class="site-footer">
        <div class="text-center">
          2015 &copy; Alcaldia Municipal de San Salvador.
        </div>
      </div>
      <!--footer end-->
    </section>

      <?php include '../import_js.php';?>
  </body>
</html>
