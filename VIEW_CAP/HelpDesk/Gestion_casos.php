<!DOCTYPE html>
<html lang="en">

<!-- head -->
    <?php include '../import_css.php';?>
<!-- /End head -->


  <body>

    <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
                <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>

                <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->

       <!-- Main -->
          <?php include '../main.php';?>
       <!-- /End Main -->


        <!--Comienza contenido principal-->
      <section id="main-content">
        <section class="wrapper">
          <!-- page start-->
          <div class="row">
            <div class="col-lg-12">
              <section class="panel">
                <header class="panel-heading">
                  Gestion de casos
                </header>

                <div class="panel-body">
                  <div class="adv-table">
                    <table  class="display table table-bordered table-striped" id="">
                      <thead>
                        <tr>
                          <th>Numero de Caso</th> <!-- codigo_empleado -->
                          <th>Descripción del Caso</th>
                          <th>Estado</th> <!-- usuario_deta_persona-->
                          <th>Escalabilidad</th>
                          <th>Tipo Caso</th>
                          <th>Fecha de Caso</th>
                          <th>Solicitante</th> <!-- id_deta_persona_asignada -->
                          <th>Equipo</th>
                          <th> </th>
                       </tr>
                      </thead>
                      <tbody>
                          <tr>
                             <?php
                                include("../../DAO_CAP/HelpDesk/model.gestion_casos.php");
                                $idTecnicoLog = $_SESSION['IngresoSistema'];
                                $objMisCasos = new GestionBitacoras();
                                $rsMisCasos = $objMisCasos->SelectCasosPorTecnicoAbiertos($idTecnicoLog);
                                while ($filaMisCasos = pg_fetch_assoc($rsMisCasos)){
                                ?>
                              <form action="../../BUSINESS_CAP/HelpDesk/ProcGestionCasos.php" method="POST">    
                                  <th>
                                      <?php print $filaMisCasos['id_solicitud_caso']; ?>
                                      <input type="text" name="txtIdCasoGestionar" value="<?php print $filaMisCasos['id_solicitud_caso']; ?>" readonly hidden>
                                  </th>
                              <th><?php print $filaMisCasos['descripcion_caso'];?></th>
                              <th><?php print $filaMisCasos['estado_caso'];?></th>
                              <th><?php print $filaMisCasos['escalabilidad'];?></th>
                              <th><?php print $filaMisCasos['tipo_caso']; ?></th>
                              <th><?php print $filaMisCasos['fecha'];?></th>
                              <th><?php print $filaMisCasos['solicitante']; ?></th>
                              <th><?php print $filaMisCasos['codigo_equipo']; ?></th>
                              <th>
                              <center>
                                  <button type="submit" name="GestionarCasos" class="btn btn-info">Gestionar Caso</button>
                              </center>
                              </th>
                              </form>
                          </tr>
                          <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </section>
            </div>
          </div>

            <!-- page end-->
        </section>
      </section>
        <!--main content end-->
        <!--footer start-->
      <div class="site-footer">
        <div class="text-center">
          2015 &copy; Alcaldia Municipal de San Salvador.
        </div>
      </div>
        <!--footer end-->
    </section>

    <?php include '../import_js.php';?>
  </body>
</html>
