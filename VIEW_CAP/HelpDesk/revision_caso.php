
<!DOCTYPE html>
<html lang="en">

<!-- head -->
    <?php include '../import_css.php';?>
<!-- /End head -->


  <body>

    <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
            <a class="logo" href="index.html"><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <a class="sublogo" href="index.html"><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->

      <!-- Main -->
          <?php include '../main.php';?>
      <!-- /End Main -->


        <!--Comienza contenido principal-->
      <section id="main-content">
        <section class="wrapper">
          <!-- page start-->
              <section class="panel">
                <header class="panel-heading">
                  Casos por Asignar
                </header>
                <div class="panel-body">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Numero de caso</th>
                      <th>Descripción de caso</th>
                      <th>Fecha de creacion</th>
                      <th>Solicitante</th>
                      <th>Estado del caso</th>
                      <th> </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <?php
                      require ("../../DAO_CAP/HelpDesk/model.revision_caso.php");
                      $objRevisionCaso = new RevisionCaso();
<<<<<<< HEAD
                      $rsRevisionCaso = $objRevisionCaso->SelectAsignarCaso();

                      while ($rowTabla = pg_fetch_assoc($rsRevisionCaso)) { ?>
                        <form action="../../BUSINESS_CAP/HelpDesk/ProcRevisionCaso.php" method="POST">
                          <th>
                            <?php print $rowTabla['id_solicitud_caso'] ?>
                            <input name="txtIdSoliCaso" value="<?php echo $rowTabla['id_soli'] ?>" readonly hidden>
                          </th>
                          <th><?php print $rowTabla ['descripcion_caso']; ?></th>
                          <th><?php print $rowTabla ['fecha']; ?></th>
                          <th><?php print $rowTabla ['nombre_log']; ?></th>
                          <th><?php print $rowTabla ['estado_caso']; ?></th>
                          <th>
                            <center>
                              <button type="submit" name="VerAsigCaso" class="btn btn-info">VER</button>
                            </center>
                          </th>
                        </form>
=======
                      $rsRevisionCasoMan = $objRevisionCaso ->SelectAsignarCaso();
                      $rsRowsRevisionCaso = pg_num_rows($rsRevisionCasoMan);

                      for ($varControRev=0; $varControRev < $rsRowsRevisionCaso ; $varControRev++) {
                        $idCasoModal = pg_fetch_result($rsRevisionCasoMan, $varControRev, 'id_solicitud_caso');
                        $descCasoModal= pg_fetch_result($rsRevisionCasoMan, $varControRev, 'descripcion_caso');
                        $descFechaModal= pg_fetch_result($rsRevisionCasoMan, $varControRev, 'fecha');
                        $idSoliModal= pg_fetch_result($rsRevisionCasoMan, $varControRev, 'id_deta_empleado_solicitante');
                        $idEstadoModal= pg_fetch_result($rsRevisionCasoMan, $varControRev, 'id_estado_caso');

                        ?>
                        <th><?php print $idCasoModal; ?></th>
                        <th><?php print $descCasoModal; ?></th>
                        <th><?php print $descFechaModal; ?></th>
                        <th><?php print $idSoliModal; ?></th>
                        <th><?php print $idEstadoModal; ?></th>

>>>>>>> 0b82db7c1b27b91982c9046b0def825360803da5
                      </tr>
                    </tbody>
                    <?php
                             }
                             ?>
                  </table>
                </div>
              </section>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12">
              <section class="panel">
                <header class="panel-heading">
                  Casos pendientes de Cierre
                </header>
<<<<<<< HEAD
                <div class="panel-body">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Numero de caso</th>
                      <th>Descripción de caso</th>
                      <th>Fecha de creacion</th>
                      <th>Solicitante</th>
                      <th>Estado del caso</th>
                      <th> </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <?php
                      $rsFinalizarCasoMan = $objRevisionCaso ->SelectFinalizarCaso();
                      $rsRowsFinalizarCaso = pg_num_rows($rsFinalizarCasoMan);

                      for ($varControFin=0; $varControFin < $rsRowsFinalizarCaso ; $varControFin++) {
                        $idCasoModal = pg_fetch_result($rsFinalizarCasoMan, $varControFin, 'id_solicitud_caso');
                        $descCasoModal= pg_fetch_result($rsFinalizarCasoMan, $varControFin, 'descripcion_caso');
                        $descFechaModal= pg_fetch_result($rsFinalizarCasoMan, $varControFin, 'fecha');
                        $idSoliModal= pg_fetch_result($rsFinalizarCasoMan, $varControFin, 'NOMBRE_LOG');
                        $idEstadoModal= pg_fetch_result($rsFinalizarCasoMan, $varControFin, 'estado_caso');

                        ?>
                        <th><?php print $idCasoModal; ?></th>
                        <th><?php print $descCasoModal; ?></th>
                        <th><?php print date("d/m/y", strtotime($descFechaModal)); ?></th>
                        <th><?php print $idSoliModal; ?></th>
                        <th><?php print $idEstadoModal; ?></th>

                      </tr>
                    </tbody>
                    <?php
                             }
                             ?>
                  </table>
                </div>
=======
>>>>>>> 0b82db7c1b27b91982c9046b0def825360803da5
              </section>
            </div>
          </div>
            <!-- page end-->
        </section>
      </section>
        <!--main content end-->
        <!--footer start-->
      <div class="site-footer">
        <div class="text-center">
          2015 &copy; Alcaldia Municipal de San Salvador.
        </div>
      </div>
        <!--footer end-->
    </section>

    <?php include '../import_js.php';?>
  </body>
</html>
