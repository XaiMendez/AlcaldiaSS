<?php

//
include_once("../../DAO_CAP/Connection/Connection.class.php");

class Historial_bitacora_usuario{


	private $id_solicitud_caso;
	private $id_bitacora_caso;
	private $fecha_seguimiento;
	private $descripcion_seguimiento;
	private $id_estado_caso;
	private $id_deta_empleado_tecnico;
	private $id_equipo_tecnologico;


	public function getId_solicitud_caso(){
		return $this->id_solicitud_caso;
	}

	public function setId_solicitud_caso($id_solicitud_caso){
		$this->id_solicitud_caso = $id_solicitud_caso;
	}

	public function getId_bitacora_caso(){
		return $this->id_bitacora_caso;
	}

	public function setId_bitacora_caso($id_bitacora_caso){
		$this->id_bitacora_caso = $id_bitacora_caso;
	}

	public function getFecha_seguimiento(){
		return $this->fecha_seguimiento;
	}

	public function setFecha_seguimiento($fecha_seguimiento){
		$this->fecha_seguimiento = $fecha_seguimiento;
	}

	public function getdescripcion_seguimiento(){
		return $this->descripcion_seguimiento;
	}

	public function setdescripcion_seguimiento($descripcion_seguimiento){
		$this->descripcion_seguimiento = $descripcion_seguimiento;
	}

	public function getId_estado_caso(){
		return $this->id_estado_caso;
	}

	public function setId_estado_caso($id_estado_caso){
		$this->id_estado_caso = $id_estado_caso;
	}

	public function getId_deta_empleado_tecnico(){
		return $this->id_deta_empleado_tecnico;
	}

	public function setId_deta_empleado_tecnico($id_deta_empleado_tecnico){
		$this->id_deta_empleado_tecnico = $id_deta_empleado_tecnico;
	}

	public function getId_equipo_tecnologico(){
		return $this->id_equipo_tecnologico;
	}

	public function setId_equipo_tecnologico($id_equipo_tecnologico){
		$this->id_equipo_tecnologico = $id_equipo_tecnologico;
	}

	//--------------------------------------------

	function historial_caso_usuario(){

		$fcnid_solicitud_caso = $this -> getId_solicitud_caso();
		$select = "SELECT id_solicitud_caso, id_bitacora_caso, fecha_seguimiento, descripcion_seguimiento, id_estado_caso, id_deta_empleado_tecnico,  FROM bitacora_caso
		INNER JOIN detalle_empleado ON bitacora_caso.id_deta_empleado_tecnico= detalle_empleado.id_deta_empleado
		INNER JOIN estado_caso ON bitacora_caso.id_estado_caso= estado_caso.id_estado_caso
		INNER JOIN escalabilidad ON bitacora_caso.id_escalabilidad= escalabilidad.id_escalabilidad
		INNER JOIN tipo_caso ON bitacora_caso.id_tipo_caso= tipo_caso.id_tipo_caso
		WHERE id_solicitud_caso= '$fcnid_solicitud_caso'  ORDER BY fecha_seguimiento ASC";

		$result = pg_query($con, $select);

		echo "Fetch row: <br>";

		while ($row = pg_fetch_row($result)) {
		print_r($row[1]);

		pg_close($con);
		}
	}

}

 ?>
