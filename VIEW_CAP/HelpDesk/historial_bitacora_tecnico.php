<!DOCTYPE html>
<html lang="en">

<!-- head -->
    <?php include '../import_css.php';?>
<!-- /End head -->


  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
                <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>

                <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->

       <!-- Main -->
          <?php include '../main.php';?>
       <!-- /End Main -->


      <!--Comienza contenido principal-->

      <section id="main-content">
        <section class="wrapper">
            <!-- page start-->
            <div class="row">
              <div class="col-lg-12">
                <section class="panel">
                  <header class="panel-heading">
                      <center><h3>GESTION DE CASOS</h3></center>
                  </header>
                  <div class="panel-body">
                    <h4>Caso Seleccionado</h4>
                      <table  class="display table table-bordered table-striped" id="example">
                        <thead>
                          <tr>
                            <th>Codigo de Caso</th> <!-- id_solicitud_caso -->
                            <th>Descripción de Caso</th> <!-- fecha_caso -->
                            <th>Fecha Solicitud</th> <!-- descripcion_seguimiento-->
                            <th>Solicitante</th> <!-- id_estado_caso-->
                            <th>Equipo en gestion</th> <!-- EQUIPO-->
                            <th>Estado Caso</th> <!-- id_equipo_tecnologico-->
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                            <?php
                                include("../../DAO_CAP/HelpDesk/model.gestion_casos.php");
                                $idTCasoLog = $_SESSION['IdCasoEnGestion'];
                                $objMisCasos = new GestionBitacoras();
                                $rsMisCasos = $objMisCasos->SelectCasoVista($idTCasoLog);
                                while ($filaMisCasos = pg_fetch_assoc($rsMisCasos)){
                                    $idEmpleadoEquipo = $filaMisCasos['id_deta_empleado_solicitante'];
                                ?> 
                                  <th>
                                      <?php print $filaMisCasos['id_solicitud_caso']; ?>
                                      
                                      
                                  </th>
                              <th><?php print $filaMisCasos['descripcion_caso'];?></th>
                              <th><?php print $filaMisCasos['fecha'];?></th>
                              <th><?php print $filaMisCasos['solicitante']; ?></th>
                              <th><?php print $filaMisCasos['codigo_equipo']; ?></th>
                              <th><?php print $filaMisCasos['estado_caso']; ?></th>

                          </tr>
                          <?php } ?>
                          </tbody>
                      </table>
                  </div>
                </section>
                  <section class="panel">
                      <header class="panel-heading"></header>
                      <center>
                      <a href="#myModalBitacora" data-toggle="modal" class="btn btn-success">
                          INGRESAR UNA NUEVA BITACORA
                      </a>
                      </center>
                  <header class="panel-heading">
                      <center><h3>BITACORAS DEL CASO</h3></center>
                  </header>
                  <div class="panel-body">
                      <table  class="display table table-bordered table-striped" id="example">
                        <thead>
                          <tr>
                            <th>Id Bitacora</th>
                            <th>Descripción de Seguimiento</th> <!-- fecha_caso -->
                            <th>Fecha Seguimiento</th> <!-- descripcion_seguimiento-->
                            <th>Tipo Caso</th> <!-- id_estado_caso-->
                            <th>Escalabilidad</th> <!-- id_equipo_tecnologico-->
                            <th>Creado por:</th>
                            <th>Operación:</th>
                            
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                            <?php
                                $rsBitaco = $objMisCasos->SelectBitacorasGestionCaso($idTCasoLog);
                                while ($filaMisCasosBita = pg_fetch_assoc($rsBitaco)){
                                ?> 
                          <form name="OperacionBitacora" action="#" method="POST">
                              <th><?php print $filaMisCasosBita['id_bitacora_caso']; ?></th>
                              <th><?php print $filaMisCasosBita['descripcion_seguimiento'];?></th>
                              <th><?php print $filaMisCasosBita['fecha_seguimiento'];?></th>
                              <th><?php print $filaMisCasosBita['tipo_caso']; ?></th>
                              <th><?php print $filaMisCasosBita['escalabilidad']; ?></th>
                              <th><?php print $filaMisCasosBita['empleado']; ?></th>
                              <th>
                              <center>
                                  <button data-original-title="Cambio de Pieza" 
                                          data-content="Gestionara un Cambio de pieza con el caso seleccionado." 
                                          data-placement="top" data-trigger="hover" 
                                          class="btn btn-warning popovers"><i class=" icon-wrench"></i>
                                  </button>
                                  <button data-original-title="Mover Equipo Bodega" 
                                          data-content="Solicite mover el equipo a bodega para realizar trabajos en el" 
                                          data-placement="top" data-trigger="hover" 
                                          class="btn btn-info popovers"><i class=" icon-truck"></i>
                                  </button>
                                  <button data-original-title="Instalar Software a Equipo" 
                                          data-content="Instalar algun software o controlador que se maneje en la empresa." 
                                          data-placement="top" data-trigger="hover" 
                                          class="btn btn-success popovers"><i class=" icon-windows"></i>
                                  </button>
                                  <button data-original-title="Asignar un Equipo" 
                                          data-content="Asignación de un equipo." 
                                          data-placement="top" data-trigger="hover" 
                                          class="btn btn-danger popovers"><i class=" icon-desktop"></i>
                                  </button>
                              </center>
                              </th>
                          </form>
                          </tr>
                          <?php } ?>
                          </tbody>
                      </table>
                      
                  </section>
                  <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalBitacora" class="modal fade">
                      <div class="modal-dialog">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                  <h4 class="modal-title">Ingreso de Bitacora al Caso</h4>
                              </div>
                              <div class="modal-body">
                                      <div class="form-group">
                                          <?php require_once '../../DAO_CAP/HelpDesk/cbx_AsignarCaso.php'; ?>
                                          <form action="../../BUSINESS_CAP/HelpDesk/ProcGestionCasos.php" method="POST">
                                              <label class="">Descripcion de Seguimiento:</label>
                                              <textarea name="txtDescAsignacion" class="form-control" cols="60" rows="2" maxlength="400" required></textarea>
                                              <br/>
                                              <label class="">Tipo Caso:</label>
                                              <?php echo combo_tipo_caso(); ?>
                                              <input type="text" name="txtIdCasoGestionar2" value="<?php print $idTCasoLog; ?>" readonly hidden>
                                              <br/>
                                              
                                              <center>
                                              <button type="submit" name="IngresarBiatcoraCasp" class="btn btn-send">Ingresar Bitacora</button>
                                              <button type="submit" name="CompletarCaso" class="btn btn-send">Completar Caso</button>
                                              </center>
                                          </form>
                                      </div>
                              </div>
                          </div>
                      </div>

                  </div>
              </div>
            </div>
          <!-- page end-->
        </section>
      </section>
      <!--main content end-->
      <!--footer start-->
      <div class="site-footer">
        <div class="text-center">
          2015 &copy; Alcaldia Municipal de San Salvador.
        </div>
      </div>
      <!--footer end-->
  </section>

    <?php include '../import_js.php';?>
  </body>
</html>
