<!DOCTYPE html>
<html lang="en">

<!-- head -->
    <?php include '../import_css.php';?>
<!-- /End head -->


  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
              <div class="sidebar-toggle-box">
                <div data-original-title="Toggle Navigation" data-placement="right" class="icon-reorder tooltips"></div>
            </div>
                <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>

                <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->

      <!-- Main -->

          <?php include '../main.php';?>
      <!-- /End Main -->

      <!--Comienza contenido principal-->

      <section id="main-content">
        <section class="wrapper">
            <!-- page start-->
            <div class="row">
              <div class="col-lg-12">
                <section class="panel">
                    <form class="form-horizontal tasi-form" action="../../BUSINESS_CAP/HelpDesk/ProcAsignarFinalizarCaso.php" method="POST">
                 <?php
                     include '../../DAO_CAP/HelpDesk/model.revision_caso.php';
                     $idCasoSeleccionadoAsignar = $_SESSION['IdAsignarCaso'];
                     $objAsignarCaso = new AsignarFinalizarCaso();
                     $objAsignarCaso->setIdSolicitudCaso($idCasoSeleccionadoAsignar);
                     $rsCasoSeleccionado = $objAsignarCaso->SelectCasoParaAsignar();
                     $rowCasoAsig = pg_fetch_array($rsCasoSeleccionado);
                 ?>
                  <header class="panel-heading">
                    Asignar Caso # <?php print $rowCasoAsig['id_solicitud_caso'];?>
                    <input type="text" name="idSoliCaso" value="<?php print $rowCasoAsig['id_solicitud_caso'];?>" readonly hidden>
                  </header>
                  <div class="panel-body">

                      <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Descripcion de Caso:</label>
                        <label class="col-sm-10 control-label" >
                            <?php print $rowCasoAsig['descripcion_caso'];?>
                        </label>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Equipo a gestionar:</label>
                        <label class="col-sm-10 control-label" >
                            <?php print $rowCasoAsig['codigo_equipo'];?>
                        </label>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Fecha de Creacion:</label>
                        <label class="col-sm-3 control-label" >
                            <?php print $rowCasoAsig['fecha'];?>
                        </label>
                        <label class="col-sm-2 col-sm-2 control-label">Solicitante del Caso:</label>
                          <label class="col-sm-3 control-label" >
                            <?php print $rowCasoAsig['solicitante'];?>
                          </label>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Ubicación del Solicitante:</label>
                        <label class="col-sm-3  control-label" >
                            <?php print $rowCasoAsig['ubicacionsoli'];?>
                        </label>
                        <label class="col-sm-2 col-sm-2 control-label">Estado del Caso:</label>
                          <label class="col-sm-3 control-label" >
                            <?php print $rowCasoAsig['estado_caso'];?>
                          </label>
                      </div>
                      <div class="form-group">
                          <?php
                          include '../../DAO_CAP/HelpDesk/cbx_AsignarCaso.php' ;
                          ?>
                        <label class="col-sm-2 col-sm-2 control-label">Descripcion Caso:</label>
                          <div class="col-sm-8">
                              <textarea name="txtDescAsignacion" class="form-control" cols="60" rows="2" required></textarea>
                          </div>

                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Escalabilidad:</label>
                        <div class="col-sm-3">
                            <?php echo combo_escalabilidad(); ?>
                          </div>
                        <label class="col-sm-2 control-label">Tipo de Caso:</label>
                        <div class="col-sm-3">
                            <?php echo combo_tipo_caso(); ?>
                          </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-2"></div>
                        <div class="col-sm-3">
                            <button name="AbrirCasoProc" class="btn btn-md btn-shadow btn-success btn-block " type="submit">Abiri Caso</button>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-sm-3">
                            <button name="DenegarCasoProc" class="btn btn-md btn-shadow btn-danger btn-block " type="submit">Denegar Caso</button>
                        </div>

                      </div>
                  </div>
                 </form>
                </section>
              </section>
            </div>
          </div>
          <!-- page end-->
        </section>
      </section>
      <!--main content end-->
      <!--footer start-->
      <div class="site-footer">
        <div class="text-center">
          2015 &copy; Alcaldia Municipal de San Salvador.
        </div>
      </div>
      <!--footer end-->
  </section>
		<?php include '../import_js.php';?>

    </body>
</html>
