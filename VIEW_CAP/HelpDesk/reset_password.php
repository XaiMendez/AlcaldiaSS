<!DOCTYPE html>
<html lang="en">

<!-- head -->
    <?php include '../import_css.php';?>
<!-- /End head -->


  <body>

  <section id="container" class="">

      <!-- ***** Comienza el Header ****** -->
      <div class="header white-bg">
        <!--Inicio del Logo-->
        <div class="header">
          <div class="sidebar-toggle-box"> <!-- para esconder y mostrar la Sidebar -->
          <div data-original-title="Menu Emergente" data-placement="right" class="icon-reorder tooltips"></div>
        </div>
          <a class="logo" href="index.html"><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
          <a class="sublogo" href="index.html"><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>

          <?php include '../Session.php' ?>

        </div>
            <!--Finaliza logo-->
      </div>
      <!-- ****** header end ****** -->

      <!-- Main -->
        <?php include '../main.php';

        $id_deta_empleado=$_GET['iddetaempleado'];
         $nombre=$_GET['nombre'];
         $nombre=$_GET['nombre'];

        ?>
      <!-- /End Main -->

      <section id="main-content">
        <section class="wrapper">
          <!-- page start-->
          <div class="row">
            <div class="col-lg-12">
              <div class="panel">
                <header class="panel-heading">
                  Reset Password
                </header>
              </div>
              <div class="col-lg-6">
                <section class="panel">
                  <header class="panel-heading">
                    Reset Password
                  </header>
                  <div class="panel-body">
                      <?php
                      require ("../../DAO_CAP/HelpDesk/model.ingreso_tecnico.php");
                      $objIngresoTec = new IngresoTecnico();
                      ?>

                    <form action="../../BUSINESS_CAP/HelpDesk/ProcIngresoTecnico.php" name="IngresoTecnico" method="POST">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Código</label>
                        <input type="text" name="txtCodTec" maxlength="25" minlength="5" class="form-control"
                           value="<?php echo $id_deta_empleado; ?>"required>
                           <label for="exampleInputEmail1">Usuario</label>
                           <input type="text" name="cbxPersona" maxlength="25" minlength="5" class="form-control"
                              value="<?php echo $nombre; ?>"required>
                      </div>


                          <div class="form-group">
                            <label for="exampleInputPassword1">Contraseña</label>
                            <input type="password" name="txtPass" maxlength="25" minlength="5" class="form-control"
                              placeholder="Ingresar Contraseña" required>
                            </br>
                              <input type="password" name="txtPassConf"  maxlength="25" minlength="5" class="form-control"
                                placeholder="Confirmar Contraseña" required>
                          </div>

                        </br>
                          <center><button type="submit" class="btn btn-shadow btn-success btn-md " name="changePass">Cambiar Password</button>
                                  <button type="submit" class="btn btn-shadow btn-danger btn-md" name="Cancelar">Cancelar</button>
                          </center>
                        </br>
                        </form>






                  </div>
                </section>
              </div>


            </div>
          </div>
            <!-- page end-->
        </section>
      </section>

      <!--footer start-->
      <div class="site-footer">
        <div class="text-center">
          2015 &copy; Alcaldia Municipal de San Salvador.
        </div>
      </div>
      <!--footer end-->
    </section>

      <?php include '../import_js.php';?>
  </body>
</html>
