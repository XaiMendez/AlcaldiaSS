
<!DOCTYPE html>
<html lang="en">

<!-- head -->
    <?php include '../import_css.php';

    ?>
<!-- /End head -->


  <body>

  <section id="container" class="">
      <!-- ***** Comienza el Header ****** -->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
            <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>

            <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!-- ****** header end ****** -->

      <!-- Main -->
          <?php include '../main.php';?>
      <!-- /End Main -->

      <!--****** MAIN-CONTENT START ******-->
      <section id="main-content">
        <section class="wrapper">
              <!-- page start-->
        <form action="../../BUSINESS_CAP/HelpDesk/variables_solicitudCaso.php" method="POST">
          <div class="row">
            <div class="col-lg-12">
              <div class="panel">
                <header class="panel-heading">
                  Ingreso de Caso
                </header>
              </div>
               <div class="col-lg-6">
                <section class="panel">
                  <header class="panel-heading">
                    Datos del Caso
                  </header>
                  <div class="panel-body">
                      <div class="form-group">
                        <label for="fecha">Fecha</label>
                          <input type="text" class="form-control" id="date" name="date" placeholder="<?php echo date("d-m-Y"); ?>" readonly="true">
                          <input type="hidden" name="UserSolicitante" value="<?php echo $_SESSION['IngresoSistema']?>">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Descripción</label>
                          <textarea class="form-control " id="description" name="description" required="" placeholder="Ingrese descripción del caso"></textarea>
                      </div>
                        <div class="form-group">
                        <label for="exampleInputEmail1">Equipo</label>
                           <select name="cbxMisEquipos" class="form-control m-bot15" id="" >
                            <?php 
                            include '../../DAO_CAP/HelpDesk/model.solicitudCaso.php';
                            $objMisEq = new modelSolicitudCaso();
                            $idEmp = $_SESSION['IngresoSistema'];
                            $rsMisEq =  $objMisEq->SelectEquiposUsuarioParaSolicitudCaso($idEmp);
                            while ($proMisEq = pg_fetch_array($rsMisEq)) {
                            ?>
                                <option value="<?php echo $proMisEq['id_equipo']; ?>"><?php echo $proMisEq['equipo']; ?></option>   
                            <?php
                            }
                            ?>
                        </select>
                      </div>
                  </div>
                </section>
              </div>
              <div class="col-lg-6">
                <br><br><br><br><br><br><br><br><br><br><br>
                <input class="btn btn-shadow btn-success btn-lg btn-block" type="submit" name="button" value="Enviar">
                <input class="btn btn-shadow btn-danger btn-lg btn-block" type="submit" name="button" value="Cancelar">
              </div>
            </div>
          </div>
        </form>
            <!-- page end-->
        </section>
      </section>
      <!--****** END MAIN-CONTENT START ******-->

       <!--footer start-->
      <div class="site-footer">
        <div class="text-center">
          2015 &copy; Alcaldia Municipal de San Salvador.
        </div>
      </div>
      <!--footer end-->
  </section>

   <?php include '../import_js.php';?>
  </body>
</html>
