
<!DOCTYPE html>
<html lang="en">
  <?php include '../import_css.php'; ?>

  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
                <a class="logo" href="index.html"><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                <a class="sublogo" href="index.html"><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>

            <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->

      <!-- Main -->
          <?php include '../main.php';?>
      <!-- /End Main -->

      <!--Comienza contenido principal-->
      <section id="main-content">
          <section class="wrapper">
          <div class="row"> <!-- div 1-->
                  <div class="col-lg-12"> <!-- div 2-->
                      <section class="panel">
                          <header class="panel-heading">
                              <center><h2>LISTA DE TECNICOS</h2></center>
                          </header>
                          <div class="panel-body"> <!--div 3-->
                               <div class="adv-table">
                                   <table  class="display table table-bordered table-striped" id="example">
                                      <thead>
                                      <tr>
                                          <th> Id </th>
                                          <th> Persona </th>
                                          <th> Especialidad </th>
                                          <th> Estado</th>
                                          <th> Area Distrito </th>
                                          <th> </th>
                                          <th> </th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      <tr>
                                      <?php
                                      include '../../DAO_CAP/Conexion/admon_conexion.php';
                                      $queryTabla = "SELECT de.id_deta_empleado, (nombre1_persona||' ' ||nombre2_persona|| ', ' ||apellido1_persona||' ' ||apellido2_persona) as nombre,
ee.estado_empleado, et.especialidad_tecnico, ad.area,
de.usuario_deta_persona, de.pass_deta_persona
FROM persona p, detalle_empleado de, estado_empleado ee, especialidad_tecnico et, area_distrito ad
where p.id_persona=de.id_persona and
      de.id_estado_empleado=ee.id_estado_empleado and
      de.id_especialidad_tecnico=et.id_especialidad_tecnico and
      de.id_area_distrito=ad.id_area_distrito";
                                      $rsTabla = pg_query($queryTabla);

                                    while ($rowTabla = pg_fetch_array($rsTabla)) { ?>
                                      <form action="EditarSolicitudesCompra.php" method="POST"> <!-- FORM -->
                                      <th>
                                          <?php print $rowTabla['id_deta_empleado']?>
                                          <input name="txtIdSoliCmpra" value="<?php echo $rowTabla['id_deta_empleado']?>"
                                                 readonly hidden>
                                      </th>
                                      <th><?php print $rowTabla['nombre']?></th>
                                      <th><?php print $rowTabla['especialidad_tecnico']?></th>
                                      <th><?php print $rowTabla['estado_empleado']?></th>
                                      <th><?php print $rowTabla['area']?></th>
                                      <th>

                                        <?php
                                            echo "
                                            <a href='ActualizarTecnico.php?iddetaempleado=".$rowTabla['id_deta_empleado']."
                                               &nombre=".$rowTabla['nombre']."
                                               &especialidad_tecnico=".$rowTabla['especialidad_tecnico']."
                                               &estado_empleado=".$rowTabla['estado_empleado']."
                                               &area=".$rowTabla['area']."
                                               '>Actualizar</a>";
                                               ?>
                                      </th>
                                      <th>
                                        <?php
                                            echo "   <a href='reset_password.php?iddetaempleado=".$rowTabla['id_deta_empleado']."
                                                  &usuario=".$rowTabla['usuario_deta_persona']."
                                                  &password=".$rowTabla['pass_deta_persona']."
                                                  &nombre=".$rowTabla['nombre']."
                                                  '>Reset Password</a>
                                            ";

                                         ?>

                                      </th>
                                      </form>
                                      </tr>
                                      </tbody>
                                      <?php } ?>
                                    </table>
                                </div>

                          </div> <!-- div 3-->
                      </section>
                  </div> <!-- div 2-->
              </div>  <!-- div 1-->
          </section>
      </section>


      <!--Finaliza contenido principal-->

          <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2015 &copy; Alcaldia Municipal de San Salvador.
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="../../Resources/js/jquery.js"></script>
    <script src="../../Resources/js/jquery-1.8.3.min.js"></script>
    <script src="../../Resources/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../../Resources/js/jquery.scrollTo.min.js"></script>
    <script src="../../Resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../../Resources/js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="../../Resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="../../Resources/js/owl.carousel.js" ></script>
    <script src="../../Resources/js/jquery.customSelect.min.js" ></script>
    <script src="../../Resources/js/respond.min.js" ></script>

    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>

    <!--common script for all pages-->
    <script src="../../Resources/js/common-scripts.js"></script>

    <!--script for this page-->
    <script src="../../Resources/js/sparkline-chart.js"></script>
    <script src="../../Resources/js/easy-pie-chart.js"></script>
    <script src="../../Resources/js/count.js"></script>



    <!--SCRIPT DE TABLA DINAMICA -->
    <script type="text/javascript" language="javascript" src="../../Resources/assets/advanced-datatable/media/js/jquery.dataTables.js"></script>

        <script type="text/javascript" charset="utf-8">
          $(document).ready(function() {
              $('#example').dataTable( {
                  "aaSorting": [[ 3, "desc" ]]
              } );
          } );
      </script>


    <!--script for this page only-->



  <script>

      //owl carousel

      $(document).ready(function() {
          $("#owl-demo").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true,
			  autoPlay:true

          });
      });

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>


  </body>
</html>
