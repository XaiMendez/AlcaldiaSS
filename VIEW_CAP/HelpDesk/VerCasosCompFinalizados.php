<!DOCTYPE html>
<html lang="en">

<!-- head -->
    <?php include '../import_css.php';?>
<!-- /End head -->


  <body>

    <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
                <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>

                <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->

       <!-- Main -->
          <?php include '../main.php';?>
       <!-- /End Main -->


        <!--Comienza contenido principal-->
      <section id="main-content">
        <section class="wrapper">
          <!-- page start-->
          <div class="row">
            <div class="col-lg-12">
              <section class="panel">
                <header class="panel-heading">
                  Gestion de casos
                </header>

                <div class="panel-body">
                  <div class="adv-table">
                    <table  class="display table table-bordered table-striped" id="">
                      <thead>
                        <tr>
                          <th>Numero de Caso</th> <!-- codigo_empleado -->
                          <th>Estado</th> <!-- usuario_deta_persona-->
                          <th>Escalabilidad</th>
                          <th>Fecha de Caso</th>
                          <th>Solicitante</th> <!-- id_deta_persona_asignada -->
                          <th>Fecha de aprovación</th>
                          <th>Aprovado</th> <!-- 7 -->
                          <th>  </th>
                       </tr>
                      </thead>
                      <tbody>
                          <tr>
                             <?php
                                include("../../DAO_CAP/HelpDesk/model.gestion_casos.php");
                                $idTecnicoLogCF = $_SESSION['IngresoSistema'];
                                $objMisCasosCF = new GestionBitacoras();
                                $rsMisCasosCF = $objMisCasosCF->SelectCasosPorTecnicoCompletadoFinalizado($idTecnicoLogCF);
                                while ($filaMisCasosCF = pg_fetch_assoc($rsMisCasosCF)){
                                ?>
                                <form action="../../VIEW_CAP/HelpDesk/historial_bitacora_tecnicoCF.php" method="POST">    
                                  <th>
                                      <?php print $filaMisCasosCF['id_solicitud_caso']; ?>
                                      <input type="text" name="txtIdCasoGestionar" value="<?php print $filaMisCasosCF['id_solicitud_caso']; ?>" readonly hidden>
                                  </th>
                              <th><?php print $filaMisCasosCF['estado_caso'];?></th>
                              <th><?php print $filaMisCasosCF['escalabilidad'];?></th>
                              <th><?php print $filaMisCasosCF['fecha'];?></th>
                              <th><?php print $filaMisCasosCF['solicitante']; ?></th>
                              <th><?php print $filaMisCasosCF['fecha_seguimiento']; ?></th>
                              <th><?php print $filaMisCasosCF['aprovado']; ?></th>
                              <th>
                              <center>
                                  <button type="submit" name="GestionarCasosCF" class="btn btn-info">Gestionar Caso</button>
                              </center>
                              </th>
                              </form>
                          </tr>
                          <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </section>
            </div>
          </div>

            <!-- page end-->
        </section>
      </section>
        <!--main content end-->
        <!--footer start-->
      <div class="site-footer">
        <div class="text-center">
          2015 &copy; Alcaldia Municipal de San Salvador.
        </div>
      </div>
        <!--footer end-->
    </section>

    <?php include '../import_js.php';?>
  </body>
</html>


