<!DOCTYPE html>
<html lang="en">

<!-- head -->
    <?php include '../import_css.php';?>
<!-- /End head -->


  <body>

  <section id="container" class="">

      <!-- ***** Comienza el Header ****** -->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
              <a class="logo" href="index.html"><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
              <a class="sublogo" href="index.html"><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>

              <?php include '../Session.php' ?>

            </div>
            <!--Finaliza logo-->
        </div>
      <!-- ****** header end ****** -->

      <!-- Main -->
          <?php include '../main.php';?>
      <!-- /End Main -->


      <!--****** MAIN-CONTENT START ******-->
      <section id="main-content">
          <section class="wrapper">
              <!-- page start-->

          <div class="row">

          <div class="col-lg-12">
          <div class="panel">
          <header class="panel-heading">
                  Modificar Datos Técnico
          </header>
          </div>


          <div class="col-lg-6">
            <section class="panel">
              <header class="panel-heading">
                  Datos
              </header>
                 <div class="panel-body">
                            <form role="form">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Código</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Ingrese el Código de Técnico">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Persona</label>
                                        <div class="">
                                          <select class="form-control m-bot15">
                                              <!--<option>Juana</option>-->
                                          </select>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Estado</label>
                                     <div class="">
                                          <select class="form-control m-bot15">
                                              <option>Activo</option>
                                              <option>Inactivo</option>
                                          </select>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Área Distrito</label>
                                     <div class="">
                                          <select class="form-control m-bot15">
                                              <option>Tecnologías de la Información</option>                                          </select>
                                        </div>
                                </div>
                            </form>
                  </div>
            </section>
          </div>

          <div class="col-lg-6">

            <button type="button" class="btn btn-shadow btn-success btn-lg btn-block">
            Guardar
            </button>

            <button type="button" class="btn btn-shadow btn-danger btn-lg btn-block">
            Cancelar
            </button>
          </div>


          </div>

          </div>


              <!-- page end-->
          </section>
      </section>
      <!--****** END MAIN-CONTENT START ******-->

      <!--footer start-->
      <div class="site-footer">
        <div class="text-center">
          2015 &copy; Alcaldia Municipal de San Salvador.
        </div>
      </div>
      <!--footer end-->
  </section>

  <?php include '../import_js.php';?>
  </body>
</html>
