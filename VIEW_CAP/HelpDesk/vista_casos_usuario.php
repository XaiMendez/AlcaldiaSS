
<!DOCTYPE html>
<html lang="en">

    <!-- head -->
    <?php include '../import_css.php'; ?>
    <!-- /End head -->


    <body>

        <section id="container" >
            <!--Comienza el Header-->
            <div class="header white-bg">
                <!--Inicio del Logo-->
                <div class="header">
                    <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                    <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>

                    <?php include '../Session.php' ?>
                </div>
                <!--Finaliza logo-->
            </div>
            <!--header end-->

            <!-- Main -->
            <?php include '../main.php'; ?>
            <!-- /End Main -->


            <!--Comienza contenido principal-->
            <section id="main-content">
                <section class="wrapper">
                    <!-- page start-->
                    <div class="row">
                        <div class="col-lg-12">
                            <section class="panel">
                                <header class="panel-heading">
                                    Mis Casos Abiertos
                                </header>
                                <div class="panel-body">
                                    <div class="adv-table">
                                        <table  class="display table table-bordered table-striped" id="">
                                            <thead>
                                                <tr>
                                                    <th>Codigo de Caso</th> <!-- id_solicitud_caso -->
                                                    <th>Descripcion de Caso</th> <!-- descripcion_caso-->
                                                    <th>Fecha de Creación</th> <!-- fecha_caso -->
                                                    <th>Estado de Caso</th> <!-- id_deta_persona-->
                                                    <th>Equipo Seleccionado</th>
                                                    <th>Marca del Equipo</th> <!-- id_estado_caso-->
                                                    <th></th> <!-- id_estado_caso-->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <?php
                                                    include '../../DAO_CAP/HelpDesk/model.usuariosCasos.php';
                                                    $idSolicitanteSes = $_SESSION['IngresoSistema'];
                                                    $objCasosUsuario = new CasosUsuarios();
                                                    $objCasosUsuario->setIdSolicitante($idSolicitanteSes);
                                                    $rsCasosUsuariosOpen = $objCasosUsuario->SelectCasosAbiertosUsuarioVerificacion();
                                                    $numFilasRsCasosUsuarioOpen = pg_num_rows($rsCasosUsuariosOpen);
                                                    for ($varControlOpen = 0; $varControlOpen < $numFilasRsCasosUsuarioOpen; $varControlOpen++) {
                                                        $idCasoUsuarioO = pg_fetch_result($rsCasosUsuariosOpen, $varControlOpen, 'id_solicitud_caso');
                                                        $descCasoUsuarioO = pg_fetch_result($rsCasosUsuariosOpen, $varControlOpen, 'descripcion_caso');
                                                        $fechaCasoUsuarioO = pg_fetch_result($rsCasosUsuariosOpen, $varControlOpen, 'fecha');
                                                        $estadoCasoUsuarioO = pg_fetch_result($rsCasosUsuariosOpen, $varControlOpen, 'estado_caso');
                                                        $idEquipoUsuarioO = pg_fetch_result($rsCasosUsuariosOpen, $varControlOpen, 'codigo_equipo');
                                                        $marcaEquipoUsuarioO = pg_fetch_result($rsCasosUsuariosOpen, $varControlOpen, 'marca');
                                                        ?>
                                                        <th><?php print $idCasoUsuarioO; ?></th>
                                                        <th><?php print $descCasoUsuarioO; ?></th>
                                                        <th><?php print $fechaCasoUsuarioO; ?></th>
                                                        <th><?php print $estadoCasoUsuarioO; ?></th>
                                                        <th><?php print $idEquipoUsuarioO; ?></th>
                                                        <th><?php print $marcaEquipoUsuarioO; ?></th>
                                                        <th>
                                                <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalCasoUsuarioOpen<?php print $varControlOpen; ?>" class="modal fade">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                                <h4 class="modal-title">Ver Estados del Caso</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <table class="table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Estado del Caso</th>
                                                                            <th>Fecha de Gestión</th>
                                                                            <th>Empleado gestionó</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <?php
                                                                            $rsBitacoraEstadosCaso = $objCasosUsuario->SelectEstadosDelCaso($idCasoUsuarioO);
                                                                            while ($filaBitacoraEstados = pg_fetch_assoc($rsBitacoraEstadosCaso)) {
                                                                                ?>
                                                                                <th><?php print $filaBitacoraEstados['estado_caso']; ?></th>
                                                                                <th><?php print $filaBitacoraEstados['fecha_estado']; ?></th>    
                                                                                <th><?php print $filaBitacoraEstados['empleado']; ?></th> 
                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </tbody>

                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <center>
                                                    <a href="#myModalCasoUsuarioOpen<?php print $varControlOpen; ?>" data-toggle="modal" class="btn btn-info">
                                                        Ver Estados del Caso
                                                    </a>
                                                </center>
                                                </th>
                                                </tr>
                                                </tbody>
                                                <?php
                                            }
                                            ?>
                                        </table>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <section class="panel">
                                <header class="panel-heading">
                                    Casos Pendientes de Aprobación
                                </header>
                                <div class="panel-body">
                                    <div class="adv-table">
                                        <table  class="display table table-bordered table-striped" id="">
                                            <thead>
                                                <tr>
                                                    <th>Codigo de Caso</th> <!-- id_solicitud_caso -->
                                                    <th>Descripcion de Caso</th> <!-- descripcion_caso-->
                                                    <th>Fecha de Creación</th> <!-- fecha_caso -->
                                                    <th>Estado de Caso</th> <!-- id_deta_persona-->
                                                    <th>Equipo Seleccionado</th>
                                                    <th>Marca del Equipo</th> <!-- id_estado_caso-->
                                                    <th></th> <!-- id_estado_caso-->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <?php
                                                    $objCasosUsuario->setIdSolicitante($idSolicitanteSes);
                                                    $rsCasosUsuariosPend = $objCasosUsuario->SelectCasosSolicitantePendientes();
                                                    $numFilasRsCasosUsuarioPend = pg_num_rows($rsCasosUsuariosPend);
                                                    for ($varControlUsu = 0; $varControlUsu < $numFilasRsCasosUsuarioPend; $varControlUsu++) {
                                                        $idCasoUsuario = pg_fetch_result($rsCasosUsuariosPend, $varControlUsu, 'id_solicitud_caso');
                                                        $descCasoUsuario = pg_fetch_result($rsCasosUsuariosPend, $varControlUsu, 'descripcion_caso');
                                                        $fechaCasoUsuario = pg_fetch_result($rsCasosUsuariosPend, $varControlUsu, 'fecha');
                                                        $estadoCasoUsuario = pg_fetch_result($rsCasosUsuariosPend, $varControlUsu, 'estado_caso');
                                                        $idEquipoUsuario = pg_fetch_result($rsCasosUsuariosPend, $varControlUsu, 'codigo_equipo');
                                                        $marcaEquipoUsuario = pg_fetch_result($rsCasosUsuariosPend, $varControlUsu, 'marca');
                                                        ?>
                                                        <th><?php print $idCasoUsuario; ?></th>
                                                        <th><?php print $descCasoUsuario; ?></th>
                                                        <th><?php print $fechaCasoUsuario; ?></th>
                                                        <th><?php print $estadoCasoUsuario; ?></th>
                                                        <th><?php print $idEquipoUsuario; ?></th>
                                                        <th><?php print $marcaEquipoUsuario; ?></th>
                                                        <th>
                                                <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalCasoUsuario<?php print $varControlUsu; ?>" class="modal fade">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                                <h4 class="modal-title">Cancelar Caso</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form action="../../BUSINESS_CAP/HelpDesk/ProcRevisionCasoUsuario.php" method="POST">
                                                                    <div class="form-group">
                                                                        <label class="">Describa el motivo por el que cancela su caso:</label>
                                                                        <textarea name="txtDescCancelar" class="form-control" cols="60" rows="2" maxlength="400" required></textarea>
                                                                        <br/>
                                                                        <input type="text" name="txtIdCasoCancelar" value="<?php print $idCasoUsuario; ?>" readonly hidden>
                                                                        <br/>
                                                                    </div>
                                                                    <center>
                                                                        <button type="submit" name="CancelarCaso" class="btn btn-default">Cancelar Caso</button>
                                                                    </center>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <center>
                                                    <a href="#myModalCasoUsuario<?php print $varControlUsu; ?>" data-toggle="modal" class="btn btn-info">
                                                        Cancelar Caso
                                                    </a>
                                                </center>
                                                </th>
                                                </tr>
                                                </tbody>
                                                <?php
                                            }
                                            ?>
                                        </table>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <section class="panel">
                                <header class="panel-heading">
                                    Casos Completados esperando Calificación
                                </header>
                                <div class="panel-body">

                                    <div class="adv-table">
                                        <table  class="display table table-bordered table-striped" id="example">
                                            <thead>
                                                <tr>
                                                    <th>Codigo de Caso</th> <!-- id_solicitud_caso -->
                                                    <th>Descripcion de Caso</th> <!-- descripcion_caso-->
                                                    <th>Fecha de Creación</th> <!-- fecha_caso -->
                                                    <th>Estado de Caso</th> <!-- id_deta_persona-->
                                                    <th>Equipo Seleccionado</th>
                                                    <th>Marca del Equipo</th> <!-- id_estado_caso-->
                                                    <th></th> <!-- id_estado_caso-->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <?php
                                                    $rsCasosUsuariosComp = $objCasosUsuario->SelectCasosSolicitanteCompletados();
                                                    $numFilasRsCasosUsuarioComp = pg_num_rows($rsCasosUsuariosComp);
                                                    for ($varControlUsuC = 0; $varControlUsuC < $numFilasRsCasosUsuarioComp; $varControlUsuC++) {
                                                        $idCasoUsuarioC = pg_fetch_result($rsCasosUsuariosComp, $varControlUsuC, 'id_solicitud_caso');
                                                        $descCasoUsuarioC = pg_fetch_result($rsCasosUsuariosComp, $varControlUsuC, 'descripcion_caso');
                                                        $fechaCasoUsuarioC = pg_fetch_result($rsCasosUsuariosComp, $varControlUsuC, 'fecha');
                                                        $estadoCasoUsuarioC = pg_fetch_result($rsCasosUsuariosComp, $varControlUsuC, 'estado_caso');
                                                        $idEquipoUsuarioC = pg_fetch_result($rsCasosUsuariosComp, $varControlUsuC, 'codigo_equipo');
                                                        $marcaEquipoUsuarioC = pg_fetch_result($rsCasosUsuariosComp, $varControlUsuC, 'marca');
                                                        ?>
                                                        <th><?php print $idCasoUsuarioC; ?></th>
                                                        <th><?php print $descCasoUsuarioC; ?></th>
                                                        <th><?php print $fechaCasoUsuarioC; ?></th>
                                                        <th><?php print $estadoCasoUsuarioC; ?></th>
                                                        <th><?php print $idEquipoUsuarioC; ?></th>
                                                        <th><?php print $marcaEquipoUsuarioC; ?></th>
                                                        <th>
                                                    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalCasoUsuarioCompletado<?php print $varControlUsu; ?>" class="modal fade">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                                    <h4 class="modal-title">Calificar Caso</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <form name="CalificarCaso" action="../../BUSINESS_CAP/HelpDesk/ProcRevisionCasoUsuario.php" method="POST">
                                                                        <div class="form-group">
                                                                            <label class="">Escriba un Comentario del caso y si finalización:</label>
                                                                            <textarea name="txtDescCalificar" class="form-control" cols="60" rows="2" maxlength="400" required></textarea>
                                                                            <br/>
                                                                            <input type="text" name="txtIdCasoCalificar" value="<?php print $idCasoUsuarioC; ?>" readonly hidden>
                                                                            <br/>
                                                                            <label class="">Seleccione una calificación:</label>
                                                                            <br/>
                                                                            <div class="col-lg-3">
                                                                                <select class="form-control m-bot15" name="cbxCalificacion">
                                                                                    <option>10</option>
                                                                                    <option>9</option>
                                                                                    <option>8</option>
                                                                                    <option>7</option>
                                                                                    <option>6</option>
                                                                                    <option>5</option>
                                                                                    <option>4</option>
                                                                                    <option>3</option>
                                                                                    <option>2</option>
                                                                                    <option>1</option>
                                                                                </select>
                                                                            </div>
                                                                            <br/><br/>

                                                                        </div>
                                                                        <center>
                                                                            <button type="submit" name="CalificarCaso" class="btn btn-success">Calificar Caso</button>
                                                                        </center>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <center>
                                                    <a href="#myModalCasoUsuarioCompletado<?php print $varControlUsu; ?>" data-toggle="modal" class="btn btn-info">
                                                        Calificar Caso
                                                    </a>
                                                </center>
                                                </th>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                    <!-- page end-->
                </section>
            </section>
            <!--main content end-->
            <!--footer start-->
            <div class="site-footer">
                <div class="text-center">
                    2015 &copy; Alcaldia Municipal de San Salvador.
                </div>
            </div>
            <!--footer end-->
        </section>
        <?php include '../import_js.php'; ?>
    </body>
</html>
