
<!DOCTYPE html>
<html lang="en">

<!-- head -->
    <?php include '../import_css.php';?>
<!-- /End head -->

  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
            <a class="logo" href="index.html"><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <a class="sublogo" href="index.html"><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            </div>
            <?php include '../Session.php' ?>
            <!--Finaliza logo-->
        </div>
      <!--header end-->

       <!-- Main -->
          <?php include '../main.php';?>
       <!-- /End Main -->

      <!--Comienza contenido principal-->

      <section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              <div class="row">
              <div class="col-lg-12">
                <section class="panel">
                  <header class="panel-heading">
                    Casos en proceso
                  </header>
                  <div class="panel-body">
                    <div class="adv-table">
                      <table  class="display table table-bordered table-striped" id="">
                        <thead>
                          <tr>
                            <th>Codigo de Caso</th>
                            <th>Codigo de Bitacora</th> <!-- id_solicitud_caso -->
                            <th>Descripcion de Seguimiento deCaso</th> <!-- descripcion_caso-->
                            <th>Fecha de Seguimiento</th> <!-- fecha_caso -->
                            <th>Tecnico Asignado</th> <!-- id_deta_persona-->
                            <th>Tipo de Caso</th>
                            <th>Escalabilidad</th>
                          </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <?php
                                include '../../DAO_CAP/HelpDesk/model.historial_caso_usuario.php';
                                $objRevCasoUsuario = new HistorialCasoUsuario();
                                $rsRevCasoUsuario = $objRevCasoUsuario->SelectHistorialCasosUsuario();

                                while ($rowRevCasoUsuario = pg_fetch_assoc($rsRevCasoUsuario)) {
                                    ?>

                                <th><?php print $rowRevCasoUsuario['id_solicitud_caso'] ?> </th>
                                <th><?php print $rowRevCasoUsuario['id_bitacora_caso'] ?></th>
                                <th><?php print $rowRevCasoUsuario['descripcion_seguimiento'] ?></th>
                                <th><?php print $rowRevCasoUsuario['fecha_seguimiento'] ?></th>
                                <th><?php print $rowRevCasoUsuario['tecnico'] ?></th>
                                <th><?php print $rowRevCasoUsuario['id_tipo_caso'] ?></th>
                                <th><?php print $rowRevCasoUsuario['escalabilidad'] ?></th>
                          </tr>
                          </tbody>
                          <?php  } ?>
                      </table>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </section>
      </section>
      <!--main content end-->
          <!--footer start-->
      <div class="site-footer">
          <div class="text-center">
              2015 &copy; Alcaldia Municipal de San Salvador.
          </div>
      </div>
      <!--footer end-->
  </section>

    <?php include '../import_js.php';?>
  </body>
</html>
