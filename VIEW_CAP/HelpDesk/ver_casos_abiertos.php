<!DOCTYPE html>
<html lang="en">

<!-- head -->
    <?php include '../import_css.php';?>
<!-- /End head -->


  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
                <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>

                <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->

       <!-- Main -->
          <?php include '../main.php';?>
       <!-- /End Main -->


      <!--Comienza contenido principal-->

      <section id="main-content">
        <section class="wrapper">
            <!-- page start-->
            <div class="row">
              <div class="col-lg-12">
                <section class="panel">
                   
                  <header class="panel-heading">
                      <center><h3>GESTION DE CASOS</h3></center>
                  </header>
                  <div class="panel-body">
                    <h4>Caso Seleccionado</h4>
                      <table  class="display table table-bordered table-striped" id="example">
                        <thead>
                          <tr>
                            <th>Codigo de Caso</th> <!-- id_solicitud_caso -->
                            <th>Descripción de Caso</th> <!-- fecha_caso -->
                            <th>Fecha Solicitud</th> <!-- descripcion_seguimiento-->
                            <th>Solicitante</th> <!-- id_estado_caso-->
                            <th>Equipo en gestion</th> <!-- EQUIPO-->
                            <th>Estado Caso</th> <!-- id_equipo_tecnologico-->
                            <th>Calificación del Caso</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                            <?php
                                include("../../DAO_CAP/HelpDesk/model.gestion_casos.php");
                                $idTCasoLog = $_SESSION['IdCasoAbiertosAdmin'];
                                $objMisCasos = new GestionBitacoras();
                                $rsMisCasos = $objMisCasos->SelectCasoVista($idTCasoLog);
                                while ($filaMisCasos = pg_fetch_assoc($rsMisCasos)){
                                    $idEmpleadoEquipo = $filaMisCasos['id_deta_empleado_solicitante'];
                                ?> 
                                  <th>
                                      <?php print $filaMisCasos['id_solicitud_caso']; ?>
                                      
                                      
                                  </th>
                              <th><?php print $filaMisCasos['descripcion_caso'];?></th>
                              <th><?php print $filaMisCasos['fecha'];?></th>
                              <th><?php print $filaMisCasos['solicitante']; ?></th>
                              <th><?php print $filaMisCasos['codigo_equipo']; ?></th>
                              <th><?php print $filaMisCasos['estado_caso']; ?></th>
                              <th><?php print $filaMisCasos['calificacion_caso']; ?></th>
                          </tr>
                          <?php } ?>
                          </tbody>
                      </table>
                  </div>
           
                </section>
                  <section class="panel">
                      <header class="panel-heading"></header>
                  <header class="panel-heading">
                      <center><h3>Técnicos Asignados al Caso</h3></center>
                  </header> 
                  <center>
                      <button type="submit" onclick="location='AgregarQuitarTecnicosCasos.php'" class="btn btn-success">Agregar o Quitar Técnicos</button>
                      <a href="#myModalBitacoraTecnicos" data-toggle="modal" class="btn btn-info">Ver Bitacoras de Técnicos Asignados</a>
                      <a href="#myModalBitacoraEstadosCaso" data-toggle="modal" class="btn btn-warning">Ver Bitacoras de Estados del Caso</a>
                      <a href="#myModalPriorizarCaso" data-toggle="modal" class="btn btn-default">Priorizar Caso</a>
                  </center>
                  <div class="panel-body">
                      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalBitacoraTecnicos" class="modal fade">
                          <div class="modal-dialog">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                      <h4 class="modal-title">Bitacora de técnicos asignados al caso</h4>
                                  </div>
                                  <div class="modal-body">

                                      <form role="form">
                                          <div class="form-group">
                                              <table class="table">
                                                  <thead>
                                                      <tr>
                                                          <th>Código de Empleado</th>
                                                          <th>Especialidad Técnico</th>
                                                          <th>Estado en el caso</th>
                                                          <th>Fecha Operación</th>
                                                          <th>Realizado Por</th>
                                                      </tr>
                                                  </thead>
                                                  <tbody>
                                                      <tr>
                                                          <?php
                                                          $rsBitacoraTecnicos = $objMisCasos->SelectBitacoraTecnicosCasos($idTCasoLog);
                                                          while ($filaBitacoraTecnicos = pg_fetch_assoc($rsBitacoraTecnicos)){
                                                          ?>
                                                          <th><?php print $filaBitacoraTecnicos['codigo_empleado']; ?></th>
                                                          <th><?php print $filaBitacoraTecnicos['especialidad_tecnico']; ?></th>
                                                          <th><?php print $filaBitacoraTecnicos['activo']; ?></th>
                                                          <th><?php print $filaBitacoraTecnicos['fecha_operacion']; ?></th>
                                                          <th><?php print $filaBitacoraTecnicos['realizadopor']; ?></th>                                                          
                                                      </tr>
                                                      <?php
                                                          }
                                                          ?>
                                                  </tbody>

                                              </table>
                                      </form>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalBitacoraEstadosCaso" class="modal fade">
                          <div class="modal-dialog">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                      <h4 class="modal-title">Bitacora de estados del caso</h4>
                                  </div>
                                  <div class="modal-body">

                                      <form role="form">
                                          <div class="form-group">
                                             <table class="table">
                                                  <thead>
                                                      <tr>
                                                          <th>Estado del Caso</th>
                                                          <th>Fecha de Gestión</th>
                                                          <th>Empleado gestionó</th>
                                                      </tr>
                                                  </thead>
                                                  <tbody>
                                                      <tr>
                                                          <?php
                                                          $rsBitacoraEstadosCaso = $objMisCasos->SelectEstadosDelCaso($idTCasoLog);
                                                          while ($filaBitacoraEstados = pg_fetch_assoc($rsBitacoraEstadosCaso)){
                                                          ?>
                                                          <th><?php print $filaBitacoraEstados['estado_caso']; ?></th>
                                                          <th><?php print $filaBitacoraEstados['fecha_estado']; ?></th>    
                                                          <th><?php print $filaBitacoraEstados['empleado']; ?></th> 
                                                      </tr>
                                                      <?php
                                                          }
                                                          ?>
                                                  </tbody>

                                              </table>
                                      </form>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalPriorizarCaso" class="modal fade">
                      <div class="modal-dialog">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                  <h4 class="modal-title">Priorizar Casos</h4>
                              </div>
                              <div class="modal-body">
                                      <div class="form-group">
                                          <?php require_once '../../DAO_CAP/HelpDesk/cbx_AsignarCaso.php'; ?>
                                          <form action="../../BUSINESS_CAP/HelpDesk/ProcGestionCasos.php" method="POST">
                                              <label class="">Descripción de la priorización:</label>
                                              <textarea name="txtDescPrio" class="form-control" cols="60" rows="2" maxlength="400" required></textarea>
                                              <br/>
                                              <label class="">Tipo Caso:</label>
                                              <?php echo combo_escalabilidad(); ?>
                                              <input type="text" name="txtIdCasoGestionar3" value="<?php print $idTCasoLog; ?>" readonly hidden>
                                              <br/>
                                              
                                              <center>
                                              <button type="submit" name="PriorizarCaso" class="btn btn-send">Priorizar Caso</button>
                                              </center>
                                          </form>
                                      </div>
                              </div>
                          </div>
                      </div>
                  </div>
                      <table  class="display table table-bordered table-striped" id="">
                        <thead>
                          <tr>
                            <th>Código de Tecnico</th>
                            <th>Nombre</th> <!-- fecha_caso -->
                            <th>Especialidad</th> <!-- descripcion_seguimiento-->
                            
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                            <?php
                                $rsBitacoTec = $objMisCasos->SelectTecnicosEnCasoActivo($idTCasoLog);
                                while ($filaMisCasosBitaTec = pg_fetch_assoc($rsBitacoTec)){
                                ?> 
                              <th><?php print $filaMisCasosBitaTec['codigo_empleado']; ?></th>
                              <th><?php print $filaMisCasosBitaTec['nombre'];?></th>
                              <th><?php print $filaMisCasosBitaTec['especialidad_tecnico'];?></th>
                          </tr>
                          <?php } ?>
                          </tbody>
                      </table>
                      
                  </section>
                  <section class="panel">
                      <header class="panel-heading"></header>
                  <header class="panel-heading">
                      <center><h3>BITACORAS DEL CASO</h3></center>
                  </header>
                  <div class="panel-body">
                      <table  class="display table table-bordered table-striped" id="example">
                        <thead>
                          <tr>
                            <th>Id Bitacora</th>
                            <th>Descripción de Seguimiento</th> <!-- fecha_caso -->
                            <th>Fecha Seguimiento</th> <!-- descripcion_seguimiento-->
                            <th>Tipo Caso</th> <!-- id_estado_caso-->
                            <th>Escalabilidad</th> <!-- id_equipo_tecnologico-->
                            <th>Creado por:</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                            <?php
                                $rsBitaco = $objMisCasos->SelectBitacorasGestionCaso($idTCasoLog);
                                while ($filaMisCasosBita = pg_fetch_assoc($rsBitaco)){
                                ?> 
                              <th><?php print $filaMisCasosBita['id_bitacora_caso']; ?></th>
                              <th><?php print $filaMisCasosBita['descripcion_seguimiento'];?></th>
                              <th><?php print $filaMisCasosBita['fecha_seguimiento'];?></th>
                              <th><?php print $filaMisCasosBita['tipo_caso']; ?></th>
                              <th><?php print $filaMisCasosBita['escalabilidad']; ?></th>
                              <th><?php print $filaMisCasosBita['empleado']; ?></th>
                          </tr>
                          <?php } ?>
                          </tbody>
                      </table>
                      
                  </section>
              </div>
            </div>
          <!-- page end-->
        </section>
      </section>
      <!--main content end-->
      <!--footer start-->
      <div class="site-footer">
        <div class="text-center">
          2015 &copy; Alcaldia Municipal de San Salvador.
        </div>
      </div>
      <!--footer end-->
  </section>

    <?php include '../import_js.php';?>
  </body>
</html>


