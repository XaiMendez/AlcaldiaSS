
<!DOCTYPE html>
<html lang="en">

<!-- head -->
    <?php include '../import_css.php';?>
<!-- /End head -->


  <body>

    <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
                <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>

                <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->

      <!-- Main -->
          <?php include '../main.php';?>
      <!-- /End Main -->


        <!--Comienza contenido principal-->
      <section id="main-content">
        <section class="wrapper">
          <!-- page start-->
          <div class="row">
            <div class="col-lg-12">
              <section class="panel">
                <header class="panel-heading">
                    <H4>Todos los Casos.</H4>
                </header>
                <div class="panel-body">
                  <div class="adv-table">
                    <table  class="display table table-bordered table-striped" id="">
                      <thead>
                        <tr>
                          <th>Codigo de Caso</th> <!-- id_solicitud_caso -->
                          <th>Descripcion de Caso</th> <!-- descripcion_caso-->
                          <th>Fecha de Creación</th> <!-- fecha_caso -->
                          <th>Solicitante</th> <!-- id_deta_persona-->
                          <th>Ubicación del Solicitante</th>
                          <th>Equipo Seleccionado</th>
                          <th>Estado del Caso</th> <!-- id_estado_caso-->
                          <th>Calificación del Caso</th>
                          <th> </th> <!-- boton para asignar caso -->
                        </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <?php
                              include '../../DAO_CAP/HelpDesk/model.revision_caso.php';
                              $objTodosCasos = new RevisionCaso();
                              $rsTodosCasos = $objTodosCasos->SelectTodosCasos();

                              while ($rowTodosCaso = pg_fetch_assoc($rsTodosCasos)) {
                                  ?>

                             <form action="../../BUSINESS_CAP/HelpDesk/ProcRevisionCaso.php" method="POST">
                              <th>
                                <?php print $rowTodosCaso['id_solicitud_caso'] ?>
                                  <input name="txtIdCaso" value="<?php print $rowTodosCaso['id_solicitud_caso'] ?>"
                                         readonly hidden>
                              </th>
                              <th><?php print $rowTodosCaso['descripcion_caso'] ?></th>
                              <th><?php print $rowTodosCaso['fecha'] ?></th>
                              <th><?php print $rowTodosCaso['solicitante'] ?></th>
                              <th><?php print $rowTodosCaso['ubicacionsoli'] ?></th>
                              <th><?php print $rowTodosCaso['codigo_equipo'] ?></th>
                              <th><?php print $rowTodosCaso['estado_caso'] ?></th>
                              <th><?php print $rowTodosCaso['calificacion_caso'] ?></th>
                              <th>
                              <center>
                                  <button type="submit" name="VerCaso" class="btn btn-info">Ver Detalles</button>
                              </center>
                              </th>
                          </form>
                        </tr>
                        </tbody>
                        <?php  } ?>
                    </table>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </section>
      </section>
        <!--main content end-->
        <!--footer start-->
      <div class="site-footer">
        <div class="text-center">
          2015 &copy; Alcaldia Municipal de San Salvador.
        </div>
      </div>
        <!--footer end-->
    </section>

    <?php include '../import_js.php';?>
  </body>
</html>

