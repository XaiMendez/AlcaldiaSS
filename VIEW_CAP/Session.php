<!DOCTYPE html>
<html>
<head>
        <?php
        include '../../DAO_CAP/Conexion/admon_conexion.php';
        session_start(); 
        if ($_SESSION['IngresoSistema'] == NULL){
           header( 'Location: ../../index.php' ) ;
        } 
        ?>
</head>
<body>
<?php 
$nombreRol = $_SESSION['RolEmpIngresoSistema'];

if ($nombreRol == 'ADMINISTRADOR'){
?>
 <div class="nav notify-row" id="top_menu">
            <!--  notification start -->
            <ul class="nav top-menu">
              <!-- settings start -->
              <li class="dropdown">
                  <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                      <i class="icon-tasks"></i>
                      <?php
                      $rsCasosPendFinAdmin = pg_query("SELECT COUNT(ID_SOLICITUD_CASO) FROM SOLICITUD_CASO WHERE ID_ESTADO_CASO = 1 --PENDIENTES
                                                    UNION ALL
                                                    SELECT COUNT(ID_SOLICITUD_CASO) FROM SOLICITUD_CASO WHERE ID_ESTADO_CASO = 4 --CALIFICADOS;");
                      $numCasosPendAdmin = pg_fetch_result($rsCasosPendFinAdmin, 0,'count');
                      $numCasosFinAdmin = pg_fetch_result($rsCasosPendFinAdmin, 1,'count');
                      //$numCasosPendAdmin = 9;
                      //$numCasosFinAdmin = 8;
                      $sumCasosAdmin = $numCasosPendAdmin + $numCasosFinAdmin;
                      if ($sumCasosAdmin > 0){
                      ?>
                      <span class="badge bg-success"><?php print $sumCasosAdmin; ?></span>
                      <?php
                      }
                      ?>
                  </a>
                  <ul class="dropdown-menu extended tasks-bar">
                      <div class="notify-arrow notify-arrow-green"></div>
                      <li>
                          <p class="green">Hay <?php print $numCasosPendAdmin; ?> casos pendientes de asignar</p>
                      </li>
                      <li>
                          <p class="green">Hay <?php print $numCasosFinAdmin; ?> casos calificados pendientes de finalizar</p>
                      </li>
                      <li class="external">
                          <a href="../HelpDesk/revision_casos.php">Ir a Casos de HelpDesk</a>
                      </li>
                  </ul>
              </li>
              <li class="dropdown">
                  <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                      <i class="icon-book"></i>
                      <?php
                      $idLogueado = $_SESSION['IngresoSistema'];
                      $rsCasosCompletados = pg_query("SELECT COUNT(ID_SOLICITUD_CASO) FROM SOLICITUD_CASO WHERE ID_ESTADO_CASO = 3 AND ID_DETA_EMPLEADO_SOLICITANTE = $idLogueado");
                      $numCasosComp = pg_fetch_result($rsCasosCompletados, 0,'count');
                      //$numCasosPendAdmin = 9;
                      //$numCasosFinAdmin = 8;
                      if ($numCasosComp > 0){
                      ?>
                      <span class="badge bg-success"><?php print $numCasosComp; ?></span>
                      <?php
                      }
                      ?>
                  </a>
                  <ul class="dropdown-menu extended tasks-bar">
                      <div class="notify-arrow notify-arrow-green"></div>
                      <li>
                          <p class="green">Tienes <?php print $numCasosComp; ?> casos completados esperando que los califiques.</p>
                      </li>
                      <li class="external">
                          <a href="../HelpDesk/vista_casos_usuario.php">Ir a mis casos</a>
                      </li>
                  </ul>
              </li>
              <!-- settings end -->
              <!-- inbox dropdown start-->
              
              <!-- inbox dropdown end -->
              <!-- notification dropdown start-->
              <li id="header_notification_bar" class="dropdown">
                  <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                      <i class="icon-bell-alt"></i>
                          <?php
                            $rsSolicitudesPendAdmin = pg_query("SELECT COUNT(ID_SOLICITUD_TRASLADO_EQUIPO) FROM SOLICITUD_TRASLADO_EQUIPO WHERE ID_ESTADO_SOLICITUD_TRASLADO = 1");
                            $numSolicitudesPendAdmin = pg_fetch_result($rsSolicitudesPendAdmin, 0);
                            if ($numSolicitudesPendAdmin > 0) {
                            ?>
                            <span class="badge bg-warning"><?php print $numSolicitudesPendAdmin ?></span>
                            <?php
                            }
                            ?>
                  </a>
                  <ul class="dropdown-menu extended notification">
                      <div class="notify-arrow notify-arrow-yellow"></div>
                      <li>
                         
                          <p class="yellow">Hay <?php print $numSolicitudesPendAdmin;?> Solicitud de traslado que revisar</p>
                      </li>
                      <li>
                          <a href="../AdmonEquipo/VerSolicitudTrasladoEquipo.php">Ir a Solicitudes de Traslado de Equipos</a>
                      </li>
                  </ul>
              </li>
              <!-- notification dropdown end -->
            </ul>
        </div>
<?php 
} else if ($nombreRol == 'TECNICO'){
?>
<div class="nav notify-row" id="top_menu">
            <!--  notification start -->
            <ul class="nav top-menu">
              <!-- settings start -->
              <!-- settings end -->
              <!-- inbox dropdown start-->
              <li class="dropdown">
                  <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                      <i class="icon-book"></i>
                      <?php
                      $idLogueado = $_SESSION['IngresoSistema'];
                      $rsCasosCompletados = pg_query("SELECT COUNT(ID_SOLICITUD_CASO) FROM SOLICITUD_CASO WHERE ID_ESTADO_CASO = 3 AND ID_DETA_EMPLEADO_SOLICITANTE = $idLogueado");
                      $numCasosComp = pg_fetch_result($rsCasosCompletados, 0,'count');
                      //$numCasosPendAdmin = 9;
                      //$numCasosFinAdmin = 8;
                      if ($numCasosComp > 0){
                      ?>
                      <span class="badge bg-success"><?php print $numCasosComp; ?></span>
                      <?php
                      }
                      ?>
                  </a>
                  <ul class="dropdown-menu extended tasks-bar">
                      <div class="notify-arrow notify-arrow-green"></div>
                      <li>
                          <p class="green">Tienes <?php print $numCasosComp; ?> casos completados esperando que los califiques.</p>
                      </li>
                      <li class="external">
                          <a href="../HelpDesk/vista_casos_usuario.php">Ir a mis casos</a>
                      </li>
                  </ul>
              </li>
              <!-- inbox dropdown end -->
              <li id="header_inbox_bar" class="dropdown">
                  <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                      <i class="icon-lightbulb"></i>
                      <?php
                      $rsCasosGestion = pg_query("SELECT 
                                                COUNT(B.ID_SOLICITUD_CASO)
                                                FROM CASO_TECNICO A
                                                INNER JOIN SOLICITUD_CASO B ON (A.ID_SOLICITUD_CASO = B.ID_SOLICITUD_CASO)
                                                WHERE ID_DETA_EMPLEADO_TECNICO = $idLogueado
                                                AND ACTIVIO = TRUE
                                                AND B.ID_ESTADO_CASO = 2;");
                      $numCasosGestion = pg_fetch_result($rsCasosGestion, 0,'count');
                      if ($numCasosGestion > 0){
                      ?>
                      <span class="badge bg-important"><?php print $numCasosGestion; ?></span>
                      <?php
                      }
                      ?>
                  </a>
                  <ul class="dropdown-menu extended inbox">
                      <div class="notify-arrow notify-arrow-red"></div>
                      <li>
                          <p class="red">Tienes <?php print $numCasosGestion; ?> Casos Abiertos</p>
                      </li>
                      <li>
                          <a href="../HelpDesk/Gestion_casos.php">Ir a mis casos abiertos</a>
                      </li>
                  </ul>
              </li>
            </ul>
        </div>
<?php
} else if ($nombreRol == 'USUARIO'){
?>
  <div class="nav notify-row" id="top_menu">
            <!--  notification start -->
            <ul class="nav top-menu">
              <!-- settings start -->
              <li class="dropdown">
                  <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                      <i class="icon-book"></i>
                      <?php
                      $idLogueado = $_SESSION['IngresoSistema'];
                      $rsCasosCompletados = pg_query("SELECT COUNT(ID_SOLICITUD_CASO) FROM SOLICITUD_CASO WHERE ID_ESTADO_CASO = 3 AND ID_DETA_EMPLEADO_SOLICITANTE = $idLogueado");
                      $numCasosComp = pg_fetch_result($rsCasosCompletados, 0,'count');
                      //$numCasosPendAdmin = 9;
                      //$numCasosFinAdmin = 8;
                      if ($numCasosComp > 0){
                      ?>
                      <span class="badge bg-success"><?php print $numCasosComp; ?></span>
                      <?php
                      }
                      ?>
                  </a>
                  <ul class="dropdown-menu extended tasks-bar">
                      <div class="notify-arrow notify-arrow-green"></div>
                      <li>
                          <p class="green">Tienes <?php print $numCasosComp; ?> casos completados esperando que los califiques.</p>
                      </li>
                      <li class="external">
                          <a href="../HelpDesk/vista_casos_usuario.php">Ir a mis casos</a>
                      </li>
                  </ul>
              </li>
              <!-- settings end -->
              <!-- inbox dropdown start-->
            </ul>
        </div>
<?php
}
?>
    

 <div class="top-nav ">
              <ul class="nav pull-right top-menu">
                  <!-- user login dropdown start-->
                  <li class="dropdown">
                      <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                          <span class="username">
                          <?php 
                          echo "Bienvenido/a:<br/>";               
                          echo $_SESSION['NombreEmpIngresoSistema'];
                          ?>
                          </span>
                          <b class="caret"></b>
                      </a>
                      <ul class="dropdown-menu extended logout">
                          <div class="log-arrow-up"></div>
                          <center><li><a href="#"><i class=""></i>ROL: <?php print $_SESSION['RolEmpIngresoSistema']; ?></a></li></center>
                          <li><a href="../LogOut.php"><i class="icon-key"></i> Cerrar Sesión </a></li>
                      </ul>
                  </li>
                  <!-- user login dropdown end -->
              </ul>

          </div>

</body>
</html>	