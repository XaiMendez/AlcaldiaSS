<!DOCTYPE html>
<html>
<?php
require_once '../../DAO_CAP/AdmonEquipo/variables_VerificacionPermisos.php';
$objVerificacionPermisos = new VerificacionPermisos();
$Rol = $_SESSION['RolEmpIngresoSistema'];
$idEmpleadoPermisos = $_SESSION['IngresoSistema'];
$rsVerificacionPermisos = $objVerificacionPermisos->SelectVerificacionPermisosPorUsuario($idEmpleadoPermisos);
//inicia verificacion de los permisos
$permisoIngresoEquiposTecnico = pg_fetch_result($rsVerificacionPermisos, 0, 'tiene_permiso');
$permisoModificarEquiposTecnico = pg_fetch_result($rsVerificacionPermisos, 1, 'tiene_permiso');
$permisoDetalleCpu = pg_fetch_result($rsVerificacionPermisos, 2, 'tiene_permiso');
$permisoMantenimientoSoftware = pg_fetch_result($rsVerificacionPermisos, 3, 'tiene_permiso');
$permisoRelacionarEquipos = pg_fetch_result($rsVerificacionPermisos, 4, 'tiene_permiso');
$permisoActivarDesactivarEquipos = pg_fetch_result($rsVerificacionPermisos, 5, 'tiene_permiso');
$permisosRepararEquipo = pg_fetch_result($rsVerificacionPermisos, 6, 'tiene_permiso');

if ($Rol == 'ADMINISTRADOR'){

?>
<head>
	<title></title>
</head>
<body>

<!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
          <ul class="sidebar-menu" id="nav-accordion">

          <li>
                  <a href="../AdmonEquipo/index.php">
                      <i class="icon-bar-chart"></i>
                      <span>Inicio</span>
                  </a>
          </li>

          <li class="sub-menu">
              <a  href="javascript:;" >
                  <i class="icon-book"></i>
                  <span>Solicitudes de Compra</span>
              </a>
              <ul class="sub">
                  <li><a  href="../AdmonEquipo/SolicitudCompra.php">Crear Nueva Solicitud</a></li>
                  <li class="sub-menu">
                      <a  href="../AdmonEquipo/VerSolicitudesCompra.php">Ver Solicitudes</a>
                  </li>
              </ul>
          </li>

          <li class="sub-menu">
              <a href="javascript:;" >
                  <i class="icon-shopping-cart"></i>
                  <span>Compras</span>
              </a>
              <ul class="sub">
                  <li><a  href="../AdmonEquipo/PreRegistrarCompra.php">Registrar Compra</a>
                  <li class="sub-menu">
                      <a  href="../AdmonEquipo/VerCompras.php">Ver Compras</a>
                  </li>
              </ul>
          </li>
          <!-- * * * HELP DESK * * * -->
          <li class="sub-menu">
            <a href="javascript:;" >
              <i class="icon-table"></i>
                <span>Casos HelpDesk</span>
            </a>
            <ul class="sub">
              <li class="sub-menu">
                <a  href="../HelpDesk/ingresoCaso.php">Crear Caso</a>
              </li>
            </ul>
            <ul class="sub">
              <li class="sub-menu">
                <a  href="../HelpDesk/vista_casos_abiertos.php">Gestion de Casos</a>
                  <ul class="sub">
                    <li>
                      <a  href="../HelpDesk/casos_abiertos.php">Casos Abiertos</a>
                    </li>
                    <li>
                      <a  href="../HelpDesk/revision_casos.php">Asignación y Finalización</a>
                    </li>
                    <li>
                      <a  href="../HelpDesk/VerTodosCasos.php">Todos los Casos</a>
                    </li>
                  <li class="sub-menu">
                      <a  href="../HelpDesk/vista_casos_usuario.php">Mis Casos</a>
                  </li>
                  </ul>
              </li>
            </ul>
          </li>
          <!-- * * * END HELP DESK * * * -->

          <!-- * * * TECNICOS * * * -->
          <li class="sub-menu">
              <a href="javascript:;" >
                <i class="icon-user"></i>
                  <span>Técnicos</span>
              </a>
							<ul class="sub">
                <li class="sub-menu">
                  <a  href="../HelpDesk/listTecnicos.php">Lista Técnicos</a>
                </li>
              </ul>
              <ul class="sub">
                <li class="sub-menu">
                  <a  href="../HelpDesk/ingresoTecnico.php">Agregar Técnico</a>
                </li>
              </ul>
              <ul class="sub">
                <li class="sub-menu">
                  <a  href="../HelpDesk/modificar_tecnico.php">Modificar Datos</a>
                </li>
              </ul>
              <ul class="sub">
                <li class="sub-menu">
                  <a  href="../HelpDesk/reset_password.php">Reset Password</a>
                </li>
                <li class="sub-menu">
                  <a  href="../AdmonEquipo/configurar_permisos.php">Configurar Permisos</a>
                </li>
              </ul>
          </li>
          <!-- * * * END TECNICOS * * * -->

          <!-- * * * EQUIPOS TECNOLOGICOS * * * -->

          <li class="sub-menu">
                      <a class="" href="javascript:;" >
                          <i class="icon-desktop"></i>
                          <span>Equipos tecnológicos</span>
                      </a>
                      <ul class="sub">
                          <li>
                              <a  href="../AdmonEquipo/IngresarEquipoTec.php">Ingresar un Equipo</a>
                          </li>
                          <li class="sub-menu">
                              <a href="javascript:;">Mantenimiento de Equipos</a>
                          <ul class="sub">
                              <a  href="../AdmonEquipo/IngresarDetaCPU.php">Ingresar Detalle de Computadora</a>
                          </ul>
                          <ul class="sub">
                              <a  href="../AdmonEquipo/IngresarSoftwareEquipo.php">Ingresar Software a Equipo</a>
                          </ul>
                          <ul class="sub">
                              <a  href="../AdmonEquipo/RelacionarEquipos.php">Relacionar Equipos</a>
                          </ul>
                          <ul class="sub">
                              <a  href="../AdmonEquipo/VerEquiposTec.php">Ver Equipos</a>
                          </ul>
                              </li>
                      </ul>
          </li>
           <!-- * * * TRANSACCIONES EQUIPO * * * -->
           <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="icon-truck"></i>
                          <span>Transacciones de Equipo</span>

                      </a>
                      <ul class="sub">
                          <!--
                          <li>
                              <a  href="../AdmonEquipo/TransAsignacionEq.php">Asignar un Equipo</a>
                          </li>
                          -->
                          <li class="sub-menu">
                              <a  href="javascript:;">Solicitud de Traslado de Equipo</a>

                              <ul class="sub">
                                 <li><a  href="../AdmonEquipo/SolicitudTrasladoEquipo.php">Asignar un Equipo</a></li>
                              </ul>
                              <ul class="sub">
                                 <li><a  href="../AdmonEquipo/SolicitudTrasladoEquipoBodega.php">Mover a Bodega</a></li>
                              </ul>
                          </li>
                          <li class="sub-menu">
                              <a  href="../AdmonEquipo/VerSolicitudTrasladoEquipo.php">Ver Solicitudes de Traslado</a>
                          </li>
                          <!--
                          <li class="sub-menu">
                              <a  href="../AdmonEquipo/TransReAsignacionEq.php">Mover un Equipo</a>
                          </li>
                          -->
                      </ul>
           </li>

            <!-- * * * CATALOGOS * * * -->
          <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="icon-gears"></i>
                          <span>Mantenimiento de Catalogos</span>
                      </a>
                      <ul class="sub">
                              <li class="sub-menu">
                                      <a  href="javascript:;">Solicitud de Compra</a>
                                      <ul class="sub">
                                          <li><a  href="../catalogos/Mantenimiento_Estado_Compra.php">Estado</a></li>
                                      </ul>
                              </li>
                              <li class="sub-menu">
                                      <a  href="javascript:;">Compra</a>
                                      <ul class="sub">
                                          <li><a  href="../catalogos/Mantenimiento_Tipo_doc.php">Tipo Documento</a></li>
                                      </ul>
                              </li>
                              <li class="sub-menu">
                                      <a  href="javascript:;">Transacciones de Equipo</a>
                                      <ul class="sub">
                                          <li><a  href="../catalogos/Mantenimiento_Tipo_trans.php">Tipo Transacción</a></li>
                                      </ul>
                              </li>
                              <li class="sub-menu"> <a href="">Equipos Tecnológicos</a>
                                <ul class="sub">
                                  <li><a  href="../catalogos/Mantenimiento_Tipo_Equipo.php">Tipo de Equipo</a></li>
                                  <li><a  href="../catalogos/Mantenimiento_Estado_Equipo.php">Estado de Equipo</a></li>
                                  <li><a  href="../catalogos/Mantenimiento_Tipo_Adq.php">Tipo de Adquisición</a></li>
                                  <li><a  href="../catalogos/Mantenimiento_Marca_Equipo.php">Marca de Equipos</a></li>
                                  <li><a  href="../catalogos/Mantenimiento_Modelo_Equipo.php">Modelo de Equipos</a></li>
                                  <li class="sub-menu">
                                      <a >Software</a>
                                      <ul class="sub">
                                          <li><a  href="../Catalogos/Mantenimiento_software.php">Detalles</a></li>
                                          <li><a  href="../catalogos/Mantenimiento_Tipo_Software.php">Tipo Software</a></li>
                                      </ul>
                                  </li>
                              </ul>
                            </li>
                            <li class="sub-menu"> <a href="">Help-Desk</a>
                                <ul class="sub">
                                  <li><a  href="../catalogos/estado_caso.php">Estado de Caso</a></li>
                                  <li><a  href="../catalogos/escalabilidad.php">Escalabilidad</a></li>
                                  <li><a  href="../catalogos/tipo_caso.php">Tipo Caso</a></li>
                              </ul>
                            </li>
                             <li class="sub-menu"> <a href="">Empleados</a>
                                <ul class="sub">
                                  <li><a  href="../catalogos/estado_empleado.php">Estado Empleado</a></li>
                                  <li><a  href="../catalogos/acceso_empleado.php">Accesos de Empleado</a></li>
                                  <li><a  href="../catalogos/empleado_subsistema.php">Empleado_Subsistema</a></li>
                                  <li><a  href="../catalogos/permisos.php">Permisos de Empleado</a></li>
                                  <li><a  href="../catalogos/rol.php">Rol</a></li>
                                  <li><a  href="../catalogos/usuario_permiso.php">Usuario Permiso</a></li>
                              </ul>
                            </li>
                            <li class="sub-menu"> <a href="">Sub-sistemas</a>
                                <ul class="sub">
                                  <li><a  href="../catalogos/codigo_subsistema.php">Sub-Sistemas</a></li>
                              </ul>
                            </li>
                        </li>
                      </ul>
          </li>
          <!-- * * * END CATALOGOS * * * -->
          <li class="sub-menu">
              <a  href="javascript:;" >
                  <i class="icon-book"></i>
                  <span>Reportes</span>
              </a>
              <ul class="sub-menu">
                  
			<li class="sub-menu">
										<a  href="../HelpDesk/reportes_help_desk.php">Reporte Help Desk</a>
                					<ul>
														<a  href="../HelpDesk/report2_helpdesk.php">Historial de casos finalizados  </a>
														<a  href="../HelpDesk/report1_helpdesk.php">Historial de casos Resueltos por Tecnico   </a>
													</ul>
									</li>

                  <li class="sub-menu">
                      <a  href="../HelpDesk/reportes_adm_equipo.php">Reporte Administracion Equipo</a>
											<ul>
					<a  href="../AdmonEquipo/report1_adm_equipo.php">Reporte de Especificacion Tecnica  </a>
					<a  href="../AdmonEquipo/report2_adm_equipo.php">Reporte de Movimiento de Equipo   </a>
			 </ul>
                  </li>
                  <li class="sub-menu">
                      <a  href="../AdmonEquipo/reportes_adm_equipo_ubicacion_equipos.php">Ubicación de Equipos</a>
                  </li>
              </ul>
          </li>
          <li class="sub-menu">
                <a  href="../Carnetizacion/home.php" >
              <a  href="../Carnetizacion/home.php" >
                  <i class="icon-book"></i>
                  <span>Ir a Carnetizacion</span>
              </a>
          </li>

          <!--multi level menu end-->
          </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->

</body>

<?php
}

 else if ($Rol == 'TECNICO'){

?>
<head>
	<title></title>
</head>
<body>

<!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
          <ul class="sidebar-menu" id="nav-accordion">

          <li>
                  <a href="../AdmonEquipo/index.php">
                      <i class="icon-bar-chart"></i>
                      <span>Inicio</span>
                  </a>
          </li>

          <li class="sub-menu">
              <a  href="javascript:;" >
                  <i class="icon-book"></i>
                  <span>Solicitudes de Compra</span>
              </a>
              <ul class="sub">
                  <li><a  href="../AdmonEquipo/SolicitudCompra.php">Crear Nueva Solicitud</a></li>
                  <li class="sub-menu">
                      <a  href="../AdmonEquipo/VerSolicitudesCompra.php">Ver Solicitudes</a>
                  </li>
              </ul>
          </li>


          <!-- * * * HELP DESK * * * -->
          <li class="sub-menu">
            <a href="javascript:;" >
              <i class="icon-table"></i>
                <span>Casos HelpDesk</span>
            </a>
            <ul class="sub">
              <li class="sub-menu">
                <a  href="../HelpDesk/ingresoCaso.php">Crear Caso</a>
              </li>
            </ul>
            <ul class="sub">
              <li class="sub-menu">
                <a  href="../HelpDesk/vista_casos_tenicos.php">Mis Casos Asignados</a>
                  <ul class="sub">
                    <li>
                      <a  href="../HelpDesk/Gestion_casos.php">Seguimientos de Casos</a>
                    </li>
                    <li>
                      <a  href="../HelpDesk/VerCasosCompFinalizados.php">Completados y Finalizados</a>
                    </li>
                    <li class="sub-menu">
                        <a  href="../HelpDesk/vista_casos_usuario.php">Mis Casos</a>
                    </li>
                  </ul>
              </li>
            </ul>
          </li>
          <!-- * * * END HELP DESK * * * -->

          <!-- * * * EQUIPOS TECNOLOGICOS * * * -->

          <li class="sub-menu">
                      <a class="" href="javascript:;" >
                          <i class="icon-desktop"></i>
                          <span>Equipos tecnológicos</span>
                      </a>
                      <ul class="sub">
                          <?php
                          if ($permisoIngresoEquiposTecnico == 'SI'){
                          ?>
                          <li>
                              <a  href="../AdmonEquipo/IngresarEquipoTec.php">Ingresar un Equipo</a>
                          </li>
                          <?php
                          }
                          ?>
                          <li class="sub-menu">
                              <a href="javascript:;">Mantenimiento de Equipos</a>
                          <?php
                          if ($permisoDetalleCpu == 'SI'){
                          ?>
                          <ul class="sub">
                              <a  href="../AdmonEquipo/IngresarDetaCPU.php">Ingresar Detalle de Computadora</a>
                          </ul>
                          <?php
                          }
                          ?>
                          <?php
                          if ($permisoMantenimientoSoftware == 'SI'){
                          ?>
                          <ul class="sub">
                              <a  href="../AdmonEquipo/IngresarSoftwareEquipo.php">Ingresar Software a Equipo</a>
                          </ul>
                          <?php
                          }
                          ?>
                          <?php
                          if ($permisoRelacionarEquipos == 'SI'){
                          ?>
                          <ul class="sub">
                              <a  href="../AdmonEquipo/RelacionarEquipos.php">Relacionar Equipos</a>
                          </ul>
                          <?php
                          }
                          ?>
                          <ul class="sub">
                              <a  href="../AdmonEquipo/VerEquiposTec.php">Ver Equipos</a>
                          </ul>
                              </li>
                      </ul>
          </li>
           <!-- * * * TRANSACCIONES EQUIPO * * * -->
           <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="icon-truck"></i>
                          <span>Transacciones de Equipo</span>
                      </a>
                      <ul class="sub">
                          <!--
                          <li>
                              <a  href="../AdmonEquipo/TransAsignacionEq.php">Asignar un Equipo</a>
                          </li>
                          -->
                          <li class="sub-menu">
                              <a  href="javascript:;">Solicitud de Traslado de Equipo</a>
                              <ul class="sub">
                                 <li><a  href="../AdmonEquipo/SolicitudTrasladoEquipo.php">Asignar un Equipo</a></li>
                              </ul>
                              <ul class="sub">
                                 <li><a  href="../AdmonEquipo/SolicitudTrasladoEquipoBodega.php">Mover a Bodega</a></li>
                              </ul>
                          </li>
                          <li>
                              <a  href="../AdmonEquipo/VerSolicitudTrasladoEquipoTecnico.php">Mis Solicitudes</a>
                          </li>
                      </ul>
           </li>
          <li class="sub-menu">
              <a  href="../Carnetizacion/home.php" >
                  <i class="icon-book"></i>
                  <span>Ir a Carnetizacion</span>
              </a>
          </li>

          <!--multi level menu end-->
          </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->

</body>
<?php
 } else if ($Rol == 'USUARIO') {
?>

<head>
	<title></title>
</head>
<body>

<!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
          <ul class="sidebar-menu" id="nav-accordion">

          <li>
                  <a href="../AdmonEquipo/index.php">
                      <i class="icon-bar-chart"></i>
                      <span>Inicio</span>
                  </a>
          </li>



          <!-- * * * HELP DESK * * * -->
          <li class="sub-menu">
            <a href="javascript:;" >
              <i class="icon-table"></i>
                <span>Casos HelpDesk</span>
            </a>
            <ul class="sub">
              <li class="sub-menu">
                <a  href="../HelpDesk/ingresoCaso.php">Crear Caso</a>
              </li>
            </ul>
            <ul class="sub">
              <li class="sub-menu">
                <a  href="../HelpDesk/vista_casos_usuario.php">Mis Casos</a>
              </li>
            </ul>
          </li>
          <!-- * * * END HELP DESK * * * -->
          <li class="sub-menu">
              <a  href="../Carnetizacion/home.php" >
                  <i class="icon-book"></i>
                  <span>Ir a Carnetizacion</span>
              </a>
          </li>

          <!--multi level menu end-->
          </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->

</body>

<?php
 }
?>
</html>
