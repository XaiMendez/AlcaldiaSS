<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Mosaddek">
  <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <link rel="shortcut icon" href="../../Resources/img/favicon.png">

  <title>Alcaldia de San Salvador</title>

  <!-- Bootstrap core CSS -->
  <link href="../../Resources/css/bootstrap.min.css" rel="stylesheet">
  <link href="../../Resources/css/bootstrap-reset.css" rel="stylesheet">
  <!--external css-->
  <link href="../../Resources/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link href="../../Resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
  <link rel="stylesheet" href="../../Resources/css/owl.carousel.css" type="text/css">
  <link rel="stylesheet" href="../../Resources/assets/data-tables/DT_bootstrap.css" />
  <!-- Custom styles for this template -->
  <link href="../../Resources/css/style.css" rel="stylesheet">
  <link href="../../Resources/css/style-responsive.css" rel="stylesheet" />

  <!-- TABLA DINAMICA -->
  <link href="../assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
  <link href="../assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
  <![endif]-->
</head>
