
<!DOCTYPE html>
<html lang="en">
 <?php include '../import_css.php'; ?>
    
  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
                <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                
                <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->
       <!-- Main -->
          <?php include '../main.php';?>
       <!-- /End Main -->

      <!--Comienza contenido principal-->
      <section id="main-content">
          <section class="wrapper">
          <div class="row"> <!-- div 1-->
                  <div class="col-lg-12"> <!-- div 2-->
                      <section class="panel">
                          <header class="panel-heading">
                              <center><h2>SOLICITUDES DE TRASLADO DE EQUIPO TECNOLÓGICO</h2></center>
                          </header>
                          <div class="panel-body"> <!-- div 3-->
                               <div class="adv-table">
                                   <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th> Fecha Solicitud </th>
                                                            <th> Tipo de Solicitud </th>
                                                            <th> Solicitud Enviada por: </th>
                                                            <th> Estado </th>
                                                            <th> Distrito Destino</th>
                                                            <th> Area Distrito Destino </th>
                                                            <th> </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <?php
                                                            require ("../../DAO_CAP/AdmonEquipo/variables_asignarEquipo.php");
                                                            $objVerSolicitudesPend = new SolicitudTrasladoEquipo();
                                                            $rsSoliTras = $objVerSolicitudesPend->SelectSolicitudesPend();
                                                            $numRowsSoliTras = pg_num_rows($rsSoliTras);
                                                            //while ($rowEquipoUbicacion = pg_fetch_assoc($rsBitacoraUbicacion)) {
                                                            for ($varControl2 = 0 ; $varControl2< $numRowsSoliTras ; $varControl2++) {
                                                                $IdSoliTras = pg_fetch_result($rsSoliTras, $varControl2, 'id_soli');
                                                                $detaSoliTras = pg_fetch_result($rsSoliTras, $varControl2, 'deta');
                                                                $detaEqSoliTras = pg_fetch_result($rsSoliTras, $varControl2, 'deta_eq');
                                                                $fechaSoliTras = pg_fetch_result($rsSoliTras, $varControl2, 'fecha');
                                                                $codEmpSoliTras = pg_fetch_result($rsSoliTras, $varControl2, 'cod_emp');
                                                                $estadoSoliTras = pg_fetch_result($rsSoliTras, $varControl2, 'estado');
                                                                $distritoSoliTras = pg_fetch_result($rsSoliTras, $varControl2, 'distrito');
                                                                $areaDistSoliTras = pg_fetch_result($rsSoliTras, $varControl2, 'area_dist');
                                                                $casoSoliTras = pg_fetch_result($rsSoliTras, $varControl2, 'caso');
                                                                $bitaCasoSoliTras = pg_fetch_result($rsSoliTras, $varControl2, 'bitacaso');
                                                                $tipoSolicitudTraslado = pg_fetch_result($rsSoliTras, $varControl2, 'tipo_solicitud');
                                                                $idEquipoSolicitudMoverBodega = pg_fetch_result($rsSoliTras, $varControl2, 'id_eq_mov_bodega');
                                                                ?>
                                                                <th><?php print $fechaSoliTras; ?></th>
                                                                <th><?php print $tipoSolicitudTraslado; ?></th>
                                                                <th><?php print $codEmpSoliTras; ?></th>
                                                                <th><?php print $estadoSoliTras; ?></th>
                                                                <th><?php print $distritoSoliTras; ?></th>
                                                                <th><?php print $areaDistSoliTras; ?></th>
                                                                <th>
                                                                    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalSoliTras<?php print $varControl2; ?>" class="modal fade">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                                                    <h4 class="modal-title">Detalle de Solicitud de traslado de equipo</h4>
                                                                                </div>
                                                                                <div class="modal-body">
                                                                                    <form action="../../BUSINESS_CAP/AdmonEquipo/ProcPreMoverEquiposTec.php" method="POST">
                                                                                    <div class="form-group">
                                                                                         <article class="media">
                                                                                            <div class="media-body">
                                                                                                <label class=" p-head">Descripción Solicitud: </label>
                                                                                                <input name="txtIdSoliTras" value="<?php print $IdSoliTras; ?>" readonly hidden />
                                                                                                <?php print $detaSoliTras; ?>
                                                                                            </div>
                                                                                        </article>
                                                                                        <article class="media">
                                                                                            <div class="media-body">
                                                                                                <label class=" p-head">Descripción del Equipo: </label>
                                                                                                <?php print $detaEqSoliTras; ?>
                                                                                            </div>
                                                                                        </article>
                                                                                        <article class="media">
                                                                                            <div class="media-body">
                                                                                                <label class=" p-head">Tipo de la Solicitud: </label>
                                                                                                <input name="txtTipoTransaccionMovimientoEquipo" value="<?php print $tipoSolicitudTraslado; ?>" readonly hidden>
                                                                                                <?php print $tipoSolicitudTraslado; ?>
                                                                                            </div>
                                                                                        </article>
                                                                                        <article class="media">
                                                                                            <div class="media-body">
                                                                                                <label class=" p-head">Fecha Solicitud: </label>
                                                                                                <?php print $fechaSoliTras; ?>
                                                                                            </div>
                                                                                        </article>
                                                                                        <article class="media">
                                                                                            <div class="media-body">
                                                                                                <label class=" p-head">Solicitud Enviada Por: </label>
                                                                                                <?php print $codEmpSoliTras; ?>
                                                                                            </div>
                                                                                        </article>
                                                                                        <article class="media">
                                                                                            <div class="media-body">
                                                                                                <label class=" p-head">Estado: </label>
                                                                                                <?php print $estadoSoliTras; ?>
                                                                                            </div>
                                                                                        </article>
                                                                                        <article class="media">
                                                                                            <div class="media-body">
                                                                                                <label class=" p-head">Distrito Destino: </label>
                                                                                                <?php print $distritoSoliTras; ?>
                                                                                            </div>
                                                                                        </article>
                                                                                        <article class="media">
                                                                                            <div class="media-body">
                                                                                                <label class=" p-head">Area u oficina Destino: </label>
                                                                                                <?php print $areaDistSoliTras; ?>
                                                                                            </div>
                                                                                        </article>
                                                                                        <article class="media">
                                                                                            <div class="media-body">
                                                                                                <label class=" p-head">ID Caso: </label>
                                                                                                <?php print $casoSoliTras; ?>
                                                                                            </div>
                                                                                        </article>
                                                                                        <article class="media">
                                                                                            <div class="media-body">
                                                                                                <label class=" p-head">Bitacora Caso: </label>
                                                                                                <?php print $bitaCasoSoliTras; ?>
                                                                                            </div>
                                                                                        </article>
                                                                                        <article class="media">
                                                                                            <div class="media-body">
                                                                                                <label class=" p-head">Id Equipo Solicitado: </label>
                                                                                                <input name="txtIdEquipoTransaccionMovimientoEquipo" value="<?php print $idEquipoSolicitudMoverBodega; ?>" readonly hidden>
                                                                                                <?php print $idEquipoSolicitudMoverBodega; ?>
                                                                                            </div>
                                                                                        </article>
                                                                                    </div>
                                                                                        <center>
                                                                                        <button type="submit" name="GenerarMovimiento" class="btn btn-default">Generar Movimiento de Equipo</button>
                                                                                        <button type="submit" name="DenegarSolicitud" class="btn btn-danger">Denegar Solicitud</button>
                                                                                        </center>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <a href="#myModalSoliTras<?php print $varControl2; ?>" data-toggle="modal" class="btn btn-info">
                                                                    Ver Solicitud
                                                                </a>
                                                                </th>
                                                            </tr>
                                                        </tbody>
                                                        <?php 
                                                            }
                                                        
                                                        ?>
                                                </table>
                                </div>
                          </div> <!-- div 3-->

                      </section>
                  </div> <!-- div 2-->
              </div>  <!-- div 1-->
          </section>
      </section>

      
      <!--Finaliza contenido principal-->

          <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2015 &copy; Alcaldia Municipal de San Salvador.
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="../../Resources/js/jquery.js"></script>
    <script src="../../Resources/js/jquery-1.8.3.min.js"></script>
    <script src="../../Resources/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../../Resources/js/jquery.scrollTo.min.js"></script>
    <script src="../../Resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../../Resources/js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="../../Resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="../../Resources/js/owl.carousel.js" ></script>
    <script src="../../Resources/js/jquery.customSelect.min.js" ></script>
    <script src="../../Resources/js/respond.min.js" ></script>

    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>

    <!--common script for all pages-->
    <script src="../../Resources/js/common-scripts.js"></script>

    <!--script for this page-->
    <script src="../../Resources/js/sparkline-chart.js"></script>
    <script src="../../Resources/js/easy-pie-chart.js"></script>
    <script src="../../Resources/js/count.js"></script>



    <!--SCRIPT DE TABLA DINAMICA -->
    <script type="text/javascript" language="javascript" src="../../Resources/assets/advanced-datatable/media/js/jquery.dataTables.js"></script>

        <script type="text/javascript" charset="utf-8">
          $(document).ready(function() {
              $('#example').dataTable( {
                  "aaSorting": [[ 4, "desc" ]]
              } );
          } );
      </script>


    <!--script for this page only-->

  

  <script>

      //owl carousel

      $(document).ready(function() {
          $("#owl-demo").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true,
			  autoPlay:true

          });
      });

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>

  </body>
</html>


