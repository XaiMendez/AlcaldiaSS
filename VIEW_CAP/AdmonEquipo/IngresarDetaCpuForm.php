<!DOCTYPE html>
<html lang="en">
 <?php include '../import_css.php'; ?>
 
    <!--SCRIPT PARA COMBOBOX DEPENDIENTES-->
    <script type="text/javascript" language="javascript" src="../../Resources/js/ajax_cbx.js"></script>	
    <script> 
    window.onload=function(){
    from(document.formEquipoTec.cbxMarcaEquipo.value,'divModelo','cbx_ModeloEquipo.php'); 
    from(document.formEquipoTec.cbxMTipoEquipo.value,'divTipo','cbx_TipoEquipo.php'); 
    } 
    </script> 
    
    <body>
        <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
                <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                
            <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->
     
      <!-- Main -->
          <?php include '../main.php';?>
      <!-- /End Main -->

      <!--Comienza contenido principal-->
      <section id="main-content">
          <section class="wrapper">
              <form name="formDetaCpu" action="../../BUSINESS_CAP/AdmonEquipo/ProcDetaCpu.php" method="POST"> <!-- FORM -->
             <?php 
             include '../../DAO_CAP/AdmonEquipo/cbx_equipoTecnologico.php' ;
             ?>
          <div class="row"> <!-- div 1-->
                  <div class="col-lg-12"> <!-- div 2-->
                      <section class="panel">
                          <header class="panel-heading">
                              <center><h2>INGRESO DE DETALLE DE COMPUTADORA</h2></center>
                          </header>
                          <div class="panel-body"> <!-- div 3-->
                              <header class="panel-heading">
                              <center><h4>Procesador</h4></center>
                          </header>
                             <div class="col-lg-6">
                      <section class="panel">
                        <header class="panel-heading"><h5>Marca</h5></header>
                        <div class="panel-body">
                            <select name="cbxMarcaProcesador" class="form-control m-bot15" id="" 
                                    onchange="from(document.formDetaCpu.cbxMarcaProcesador.value,'divModeloPro','cbx_ModeloProcesador.php')">
                            <option value="0">Seleccione un Procesador</option>
                            <?php 
                            $queryProcesador = "SELECT
                                                A.ID_MARCA_EQUIPO,
                                                A.MARCA_EQUIPO
                                                FROM
                                                MARCA_EQUIPO A
                                                INNER JOIN M_TIPO_EQUIPO B ON (A.ID_M_TIPO_EQUIPO = B.ID_M_TIPO_EQUIPO)
                                                WHERE B.ID_M_TIPO_EQUIPO=5";
                            $rsProcesador = pg_query($queryProcesador); 
                            while ($pro = pg_fetch_array($rsProcesador)) {
                            ?>
                                <option value="<?php echo $pro['id_marca_equipo'] ?>"><?php echo $pro['marca_equipo'] ?></option>   
                            <?php
                            }
                            ?>
                        </select>
                        </div>
                      </section>
                   </div>
                  <div class="col-lg-6" onload="">
                      <section class="panel">
                        <header class="panel-heading"><h5>Modelo</h5></header>
                        <div class="panel-body" id="divModeloPro">
                        </div>
                      </section>
                   </div>
                          </div> <!-- div 3-->                          
                             <div class="panel-body"> <!-- div 3-->
                              <center><h4>Memoria Ram</h4></center>
                          
                   <div class="col-lg-4">
                      <section class="panel">
                        <header class="panel-heading"><h5>Marca</h5></header>
                        <div class="panel-body">
                            <select name="cbxMarcaMemoria" class="form-control m-bot15" id="" 
                                    onchange="from(document.formDetaCpu.cbxMarcaMemoria.value,'divModeloRam','cbx_MemoriaRam.php')">
                            <option value="0">Seleccione un Memoria</option>
                            <?php 
                            $queryMemoria = "SELECT
                                                A.ID_MARCA_EQUIPO,
                                                A.MARCA_EQUIPO
                                                FROM
                                                MARCA_EQUIPO A
                                                INNER JOIN M_TIPO_EQUIPO B ON (A.ID_M_TIPO_EQUIPO = B.ID_M_TIPO_EQUIPO)
                                                WHERE B.ID_M_TIPO_EQUIPO=1";
                            $rsMemoria = pg_query($queryMemoria); 
                            while ($Memoria = pg_fetch_array($rsMemoria)) {
                            ?>
                                <option value="<?php echo $Memoria['id_marca_equipo'] ?>"><?php echo $Memoria['marca_equipo'] ?></option>   
                            <?php
                            }
                            ?>
                        </select>
                        </div>
                      </section>
                   </div>
                  <div class="col-lg-4" onload="">
                      <section class="panel">
                        <header class="panel-heading"><h5>Modelo</h5></header>
                        <div class="panel-body" id="divModeloRam">
                        </div>
                      </section>
                   </div>
                 <div class="col-lg-4" onload="">
                      <section class="panel">
                        <header class="panel-heading"><h5>Numero de memorias</h5></header>
                        <div class="panel-body" >
                            <input type="number" name="txtNumeroMemorias" class="form-control m-bot15" 
                                   placeholder="Digite la cantidad de almacenamiento" required min="1" step="1">
                        </div>
                      </section>
                   </div>
                          </div> <!-- div fin de memoria ram-->
                          <center><h4></h4></center>  
                    <div class="col-lg-6" onload="">
                      <section class="panel">
                           <header class="panel-heading"><h4>Usuario de Red</h4></header>
                        <div class="panel-body" id="">
                            <input type="text" name="txtRed" class="form-control m-bot15" 
                                   placeholder="Digite Usuario de Red" required>
                        </div>
                      </section>
                   </div> 
                   <div class="col-lg-6" onload="">
                      <section class="panel">
                           <header class="panel-heading"><h4>Nombre de Equipo</h4></header>
                        <div class="panel-body" id="">
                            <input type="text" name="txtNombre" class="form-control m-bot15" 
                                   placeholder="Digite Nombre de Equipo" required>
                        </div>
                      </section>
                   </div> 
                   <div class="col-lg-6">
                      <section class="panel">
                        <header class="panel-heading"><h4>Almacenamiento</h4></header>
                        <div class="panel-body">
                            <input type="number" name="txtAlmacenamiento" class="form-control m-bot15" 
                                   placeholder="Digite la cantidad de almacenamiento" required min="1" step="1">
                            <select name="cbxAlmacenamiento" class="form-control m-bot15" id="" >
                            <option value="0">Seleccione Unidad de Almacenamiento</option>
                            <?php 
                            $queryUnidad = "SELECT ID_UNIDAD_ALMACENAMIENTO , (CODE_UNIDAD ||' '||DESCRIPCION_UNIDAD)AS UNIDAD FROM UNIDAD_ALMACENAMIENTO";
                            $rsUnidad = pg_query($queryUnidad); 
                            while ($Unidad = pg_fetch_array($rsUnidad)) {
                            ?>
                                <option value="<?php echo $Unidad['id_unidad_almacenamiento'] ?>"><?php echo $Unidad['unidad'] ?></option>   
                            <?php
                            }
                            ?>
                        </select>
                        </div>
                      </section>
                   </div>
                  <div class="col-lg-6" onload="">
                      <section class="panel">
                           <header class="panel-heading"><h4>Sistema Operativo Predeterminado</h4></header>
                        <div class="panel-body" id="">
                            <select name="cbxSO" class="form-control m-bot15" id="" >
                            <option value="0">Seleccione Sistema operativo</option>
                            <?php 
                            $querySistema = "SELECT ID_SOFTWARE , DESCRIPCION_SOFTWARE FROM SOFTWARE WHERE ID_TIPO_SOFTWARE = 5;";
                            $rsSistema = pg_query($querySistema); 
                            while ($Sistema = pg_fetch_array($rsSistema)) {
                            ?>
                                <option value="<?php echo $Sistema['id_software'] ?>"><?php echo $Sistema['descripcion_software'] ?></option>   
                            <?php
                            }
                            ?>
                            </select><br/><br/>
                        </div>
                      </section>
                   </div>
                   
                          </div> <!-- div fin de ALMACENAMIENTO-->

                      </section>
                   </section>
                  </div> <!-- div 2-->

                  <div class="col-lg-12">
                       <div class="panel-body">
                    <center>
                        <button type="submit" name="IngresarDetalleCpu" class="btn btn-success">Ingresar Detalle Cpu</button>
                        <button type="button" onclick = "location='index.php'" class="btn btn-danger">Regresar</button>
                   </center>
                   <br/>
                      </div>
                   </div>
              </div>  <!-- div 1-->
              </form>
          </section>
      </section>

      
      <!--Finaliza contenido principal-->

          <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2015 &copy; Alcaldia Municipal de San Salvador.
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
     <script src="../../Resources/js/jquery.js"></script>
    <script src="../../Resources/js/jquery-1.8.3.min.js"></script>
    <script src="../../Resources/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../../Resources/js/jquery.scrollTo.min.js"></script>
    <script src="../../Resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../../Resources/js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="../../Resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="../../Resources/js/owl.carousel.js" ></script>
    <script src="../../Resources/js/jquery.customSelect.min.js" ></script>
    <script src="../../Resources/js/respond.min.js" ></script>

    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>

    <!--common script for all pages-->
    <script src="../../Resources/js/common-scripts.js"></script>

    <!--script for this page-->
    <script src="../../Resources/js/sparkline-chart.js"></script>
    <script src="../../Resources/js/easy-pie-chart.js"></script>
    <script src="../../Resources/js/count.js"></script>
    
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.js" type="text/javascript"></script>


  <script>

      //owl carousel

      $(document).ready(function() {
          $("#owl-demo").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true,
        autoPlay:true

          });
      });

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>
  
  
	
  </body>
</html>

