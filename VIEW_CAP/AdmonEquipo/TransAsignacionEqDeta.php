
<!DOCTYPE html>
<html lang="en">
<?php include '../import_css.php'; ?>
    
    <!--SCRIPT PARA COMBOBOX DEPENDIENTES-->
    <script type="text/javascript" language="javascript" src="../../Resources/js/ajax_cbx.js"></script>	
    <script>
    window.onload=function(){
    from(document.formAsignarEquipo.cbxDistrito.value,'divAreaDistrito','cbx_AreaDistrito.php');
    } 
    </script>
  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
                <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                
                <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->
      
       <!-- Main -->
          <?php include '../main.php';?>
       <!-- /End Main -->

      <!--Comienza contenido principal-->
      <section id="main-content">
          <section class="wrapper">
          <form name="formAsignarEquipo" action="../../BUSINESS_CAP/AdmonEquipo/ProcAsignarEquipoTec.php" method="POST"> <!-- FORM -->
          <div class="row"> <!-- div 1-->
                  <div class="col-lg-12"> <!-- div 2-->
                      <section class="panel">
                          <header class="panel-heading">
                              <center><h2>ASIGNACIÓN DE EQUIPO TECNOLÓGICO</h2></center>
                          </header>
                          <div class="panel-body"> <!-- div 3-->
                              <div class="form-group">
                                      <label>Numero de Caso:</label><br>
                                      <p>Si realizas esta asignación por seguimiento de un caso escribe el # de este</p>
                                      <div class="col-sm-3">
                                          <input type="text" name="txtEsIdCaso" placeholder="# de bitacora de caso" class="form-control" >
                                      </div>
                              </div><br><br>
                                <div class="form-group">
                                  <label><h3>Detalle de Transacción</h3></label>
                                  <input type="text" name="txtDescTransAsig"class="form-control" size="150" maxlength="200" 
                                         placeholder="Descripción de la asignación." required>
                                  <label><h3>Equipo a asignar</h3></label>
                                  <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Codigo de Equipo</th>
                                                    <th>Descripción</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php //llenado de tabla encabezado de solicitud de compra
                                            include '../../DAO_CAP/Conexion/admon_conexion.php';
                                            $idEquipoAsignar = $_SESSION['IdEquipoParaAsignar'];
                                            $queryEncaEqAsig = "SELECT CODIGO_EQUIPO , DESCRIPCION_EQUIPO FROM EQUIPO_TECNOLOGICO WHERE ID_EQUIPO = $idEquipoAsignar"; //Funcion sql
                                            $rsEncaEqAsig = pg_query($queryEncaEqAsig);
                                            $rowEncaEqAsig = pg_fetch_array($rsEncaEqAsig);
                                            ?>
                                            <th>
                                                <input type="text" name="txtEquipoTransAsig" value="<?php echo $idEquipoAsignar; ?>" hidden readonly>
                                                <?php echo $rowEncaEqAsig['codigo_equipo']; ?>
                                            </th>
                                            <th><?php echo $rowEncaEqAsig['descripcion_equipo']; ?></th>
                                            </tbody>
                                        </table>
                                </div>
                          </div> <!-- div 3-->

                      </section>
                  </div> <!-- div 2-->
                   <div class="col-lg-4">
                      <section class="panel">
                        <header class="panel-heading"><h5>Distrito u oficina de destino</h5></header>
                        <div class="panel-body">
                         <select name="cbxDistrito" class="form-control m-bot15" id="" 
                                    onchange="from(document.formAsignarEquipo.cbxDistrito.value,'divAreaDistrito','cbx_AreaDistrito.php')">
                            <?php 
                            $queryDistrito = "SELECT ID_DISTRITO,DETALLE_DISTRITO FROM DISTRITO;";
                            $rsDistrito = pg_query($queryDistrito); 
                            while ($Dist = pg_fetch_array($rsDistrito)) {
                            ?>
                                <option value="<?php echo $Dist['id_distrito'] ?>"><?php echo $Dist['detalle_distrito'] ?></option>   
                            <?php
                            }
                            ?>
                        </select>
                        </div>
                      </section>
                   </div>
                   <div class="col-lg-4">
                      <section class="panel">
                        <header class="panel-heading"><h5>Area del Distrito u oficina</h5></header>
                        <div class="panel-body" id="divAreaDistrito">

                        </div>
                      </section>
                   </div>
                   <div class="col-lg-4">
                      <section class="panel">
                        <header class="panel-heading"><h5>Empleado</h5></header>
                        <div class="panel-body" id="divDetalleEmpleado">
                        
                        </div>
                      </section>
                   </div>
                   <div>
                    <center>
                        <button type="submit" name="AsignarEquipoTec" class="btn btn-success">Asignar equipo</button>
                        <button type="button" onclick = "location='TransAsignacionEq.php'" class="btn btn-danger">Regresar</button>
                   </center>
                   <br/>
                   </div>
              </div>  <!-- div 1-->
              </form>
          </section>
      </section>

      
      <!--Finaliza contenido principal-->

          <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2015 &copy; Alcaldia Municipal de San Salvador.
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="../../Resources/js/jquery.js"></script>
    <script src="../../Resources/js/jquery-1.8.3.min.js"></script>
    <script src="../../Resources/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../../Resources/js/jquery.scrollTo.min.js"></script>
    <script src="../../Resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../../Resources/js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="../../Resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="../../Resources/js/owl.carousel.js" ></script>
    <script src="../../Resources/js/jquery.customSelect.min.js" ></script>
    <script src="../../Resources/js/respond.min.js" ></script>

    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>

    <!--common script for all pages-->
    <script src="../../Resources/js/common-scripts.js"></script>

    <!--script for this page-->
    <script src="../../Resources/js/sparkline-chart.js"></script>
    <script src="../../Resources/js/easy-pie-chart.js"></script>
    <script src="../../Resources/js/count.js"></script>


  <script>

      //owl carousel

      $(document).ready(function() {
          $("#owl-demo").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true,
        autoPlay:true

          });
      });

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>

  </body>
</html>
