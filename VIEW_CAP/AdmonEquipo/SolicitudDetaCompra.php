 
<!DOCTYPE html>
<html lang="en">
    <?php include '../import_css.php'; ?>
    
  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
                <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            
                    
                     <?php include '../Session.php' ?>
                </div>
                <!--Finaliza logo-->
            </div>
            <!--header end-->

            <!-- Main -->
            <?php include '../main.php'; ?>
            <!-- /End Main -->

            <!--Comienza contenido principal-->
            <section id="main-content">
                <section class="wrapper">
                    <div class="row"> <!-- div 1-->
                        <div class="col-lg-12"> <!-- div 2-->
                            <section class="panel">
                                <header class="panel-heading">
                                    <center><h2>DETALLE SOLICITUD DE COMPRA</h2></center>
                                </header>
                                <div class="panel-body"> <!-- div 3-->
                                <form name="formDetaSoliCompra" action="../../BUSINESS_CAP/AdmonEquipo/ProcDetaSolicitudCompra.php" method="POST">
                                    <div class="form-group">
                                        <label><h3>Encabezado de Solicitud</h3></label>
                                        <header class="panel-heading">
                                        </header>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Fecha</th>
                                                    <th>Descripción</th>
                                                    <th>Estado</th>
                                                    <th>Cod Solicitante</th>
                                                    <th>id Bitacora Caso</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php //llenado de tabla encabezado de solicitud de compra
                                            include '../../DAO_CAP/Conexion/admon_conexion.php';
                                            $idSoliSess = $_SESSION['IdSoliCompra'];
                                            $queryEnca = "SELECT * FROM Admon_Select_Enca_Soli_Compra($idSoliSess)"; //Funcion sql
                                            $rsEnca = pg_query($queryEnca);
                                            $rowEnca = pg_fetch_array($rsEnca);
                                            ?>
                                            <th>
                                            <?php echo $rowEnca['id_soli']; ?>
                                            <input name="txtIdSoliCompra" value="<?php echo $rowEnca['id_soli']; ?>" readonly hidden>
                                            </th>
                                            <th><?php echo $rowEnca['fecha_soli']; ?></th>
                                            <th><?php echo $rowEnca['descripcion']; ?></th>
                                            <th><?php echo $rowEnca['descripcion_estado']; ?></th>
                                            <th><?php echo $rowEnca['cod_emp']; ?></th>
                                            <th><?php echo $rowEnca['id_caso']; ?></th>
                                            </tbody>
                                        </table>
                                    </div>
                                  
                                    
                                    <div class="form-group">
                                                <label><h3>Agregar Detalle de Solicitud</h3></label>
                                           
                                            <div class="col-lg-12">
                                                <section class="panel">
                                                    <header class="panel-heading"><h5>Descripción Detalle</h5></header>
                                                    <div class="panel-body">
                                                        <input type="text" name="txtDescDetaSoliCompra" placeholder="Describa el Articulo Solicitado" class="form-control" required>
                                                    </div>
                                                </section>
                                            </div>
                                            <div class="col-lg-6">
                                                <section class="panel">
                                                    <header class="panel-heading"><h5>Articulo Solicitado</h5></header>
                                                    <div class="panel-body">
                                                        <input type="text" name="txtDescArticulo" placeholder="Describa el Articulo Solicitado" class="form-control" required>
                                                    </div>
                                                </section>
                                            </div>
                                            <div class="col-lg-6">
                                                <section class="panel">
                                                    <header class="panel-heading"><h5>Cantidad</h5></header>
                                                    <div class="panel-body">
                                                        <input type="number" name="txtCantidadDesSoli" 
                                                               placeholder="Cantidad Solicitada" class="form-control" 
                                                               required min="1" step="1">
                                                    </div>
                                                </section>
                                            </div>
                                    <div>
                                        <br>
                                        <center>
                                            <button type="submit" name="AgregarDetaSoli" class="btn btn-success">Agregar Detalle</button>
                                            
                                        </center>
                                    </div>
                                    </div>
                                      </form>
                                    <form name="quitarLineaSoli" action="../../BUSINESS_CAP/AdmonEquipo/ProcDetaSolicitudCompra.php" method="POST">
                                      <div class="form-group">
                                        <label><h3>Detalle de Solicitud</h3></label>
                                        <header class="panel-heading">
                                        </header>
                                        
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Descipción Detalle</th>
                                                    <th>Descripción Item</th>
                                                    <th>Cantidad</th>
                                                    <th>Operación</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                             <?php 
                                             $idEncaSolicitud = $rowEnca['id_soli'];
                                             //$idEncaSolicitud = 1; pruebas
                                             $queryDeta = "SELECT * FROM Admon_Select_detalle_soli_compra($idEncaSolicitud)";
                                             $rsDeta = pg_query($queryDeta);
                                             ?>
                                            
                                            <?php while($rowDeta = pg_fetch_assoc($rsDeta)) { ?>
                                                <th><?php echo $rowDeta['id_deta_soli']; ?>
                                                    <input name="txtIdSoliDetaCompra" value="<?php echo $rowDeta['id_deta_soli']; ?>" 
                                                           readonly hidden>
                                                </th>
                                                <th><?php echo $rowDeta['desc_soli']; ?></th>
                                                <th><?php echo $rowDeta['desc_item']; ?></th>
                                                <th><?php echo $rowDeta['cantidad']; ?> </th>
                                                <th>
                                                    <button type="submit" name="QuitarLineaDetaSoli" class="btn btn-info">Quitar</button>
                                                </th>
                                                </tbody>
                                             <?php } ?>
                                                
                                        </table>
                                    </div>  
                                    </form>
                                    <form name="FinalizarSoliCompra" action="../../BUSINESS_CAP/AdmonEquipo/ProcDetaSolicitudCompra.php" method="POST">
                                    <div class="form-group">
                                        <header class="panel-heading">
                                            <center>
                                            <input name="txtIdSoliCompraCanc" value="<?php echo $rowEnca['id_soli']; ?>" readonly hidden>
                                            <button type="submit" name="FinalizarSoli" class="btn btn-info">Finalizar Solicitud</button>
                                            <button type="submit" name="CancelarSoli" class="btn btn-danger">Cancelar Solicitud</button>
                                            </center>                                            
                                        </header>
                                    </div> 
                                    </form>
                                    </div>
                            </section>
                        </div> <!-- div 2-->
                    </div>  <!-- div 1-->
                </section>
            </section>


            <!--Finaliza contenido principal-->

            <!--footer start-->
            <footer class="site-footer">
                <div class="text-center">
                    2015 &copy; Alcaldia Municipal de San Salvador.
                </div>
            </footer>
            <!--footer end-->
        </section>

        <!-- js placed at the end of the document so the pages load faster -->
        <script src="../../Resources/js/jquery.js"></script>
    <script src="../../Resources/js/jquery-1.8.3.min.js"></script>
    <script src="../../Resources/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../../Resources/js/jquery.scrollTo.min.js"></script>
    <script src="../../Resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../../Resources/js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="../../Resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="../../Resources/js/owl.carousel.js" ></script>
    <script src="../../Resources/js/jquery.customSelect.min.js" ></script>
    <script src="../../Resources/js/respond.min.js" ></script>

    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>

    <!--common script for all pages-->
    <script src="../../Resources/js/common-scripts.js"></script>

    <!--script for this page-->
    <script src="../../Resources/js/sparkline-chart.js"></script>
    <script src="../../Resources/js/easy-pie-chart.js"></script>
    <script src="../../Resources/js/count.js"></script>

        <!-- SCRIPTS PARA LA DATA TABLE EDITABLE -->
        <script type="text/javascript" src="../../Resources/assets/data-tables/jquery.dataTables.js"></script>
        <script type="text/javascript" src="../../Resources/assets/data-tables/DT_bootstrap.js"></script>
        <script>
                                                    jQuery(document).ready(function() {
                                                        EditableTable.init();
                                                    });
        </script>

        <script>

            //owl carousel

            $(document).ready(function() {
                $("#owl-demo").owlCarousel({
                    navigation: true,
                    slideSpeed: 300,
                    paginationSpeed: 400,
                    singleItem: true,
                    autoPlay: true

                });
            });

            //custom select box

            $(function() {
                $('select.styled').customSelect();
            });
        </script>


    </body>
</html>
