
<!DOCTYPE html>
<html lang="en">
 <?php include '../import_css.php'; ?>

  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
                <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>

                <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->
       <!-- Main -->
          <?php include '../main.php';?>
       <!-- /End Main -->

      <!--Comienza contenido principal-->
      <section id="main-content">
          <section class="wrapper">
          <div class="row"> <!-- div 1-->
                  <div class="col-lg-12"> <!-- div 2-->
                      <section class="panel">
                          <header class="panel-heading">
                              <center><h2>VER DETALLES DE EQUIPO TECNOLÓGICO</h2></center>
                          </header>
                          <div class="panel-body">
                                <h2>Seleccione un equipo</h2>
                               <table class="table">
                                      <thead>
                                      <tr>
                                          <th> Id Equipo </th>
                                          <th> Codigo Equipo </th>
                                          <th> Tipo Equipo </th>
                                          <th> Detalle Tipo Equipo</th>
                                          <th> Equipo </th>
                                          <th> Estado </th>
                                          <th> </th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                          <tr>
                                             <?php
                                             include '../../DAO_CAP/AdmonEquipo/variables_equipoTecnologico.php';
                                             $objVerPreEquipos = new EquipoTecnologico();
                                             $rsEqV = $objVerPreEquipos->SelectVerEquipos();

                                             while ($rowEqV = pg_fetch_assoc($rsEqV)) { ?>

                                             <form action="../../BUSINESS_CAP/AdmonEquipo/ProcPreVerEquipos.php" method="POST">
                                                <th>
                                                    <?php print $rowEqV['id_eq']?>
                                                    <input name="txtIdEquipoV" value="<?php print $rowEqV['id_eq']?>"
                                                    readonly hidden>
                                                </th>
                                                <th><?php print $rowEqV['cod_eq']?></th>
                                                <th><?php print $rowEqV['m_tipo']?></th>
                                                <th><?php print $rowEqV['tipo']?></th>
                                                <th><?php print $rowEqV['m2_tipo']?></th>
                                                <th><?php print $rowEqV['estado']?></th>
                                                <th>
                                                    <center>
                                                    <button type="submit" name="VerEquipo" class="btn btn-info">Ver Detalles</button>
                                                    </center>
                                                </th>
                                            </form>
                                          </tr>
                                      </tbody>
                                      <?php  } ?>
                                    </table>
                          </div> <!-- div 3-->

                      </section>
                  </div> <!-- div 2-->
              </div>  <!-- div 1-->
          </section>
      </section>


      <!--Finaliza contenido principal-->

          <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2015 &copy; Alcaldia Municipal de San Salvador.
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="../../Resources/js/jquery.js"></script>
    <script src="../../Resources/js/jquery-1.8.3.min.js"></script>
    <script src="../../Resources/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../../Resources/js/jquery.scrollTo.min.js"></script>
    <script src="../../Resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../../Resources/js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="../../Resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="../../Resources/js/owl.carousel.js" ></script>
    <script src="../../Resources/js/jquery.customSelect.min.js" ></script>
    <script src="../../Resources/js/respond.min.js" ></script>

    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>

    <!--common script for all pages-->
    <script src="../../Resources/js/common-scripts.js"></script>

    <!--script for this page-->
    <script src="../../Resources/js/sparkline-chart.js"></script>
    <script src="../../Resources/js/easy-pie-chart.js"></script>
    <script src="../../Resources/js/count.js"></script>



    <!--SCRIPT DE TABLA DINAMICA -->
    <script type="text/javascript" language="javascript" src="../../Resources/assets/advanced-datatable/media/js/jquery.dataTables.js"></script>

        <script type="text/javascript" charset="utf-8">
          $(document).ready(function() {
              $('#example').dataTable( {
                  "aaSorting": [[ 4, "desc" ]]
              } );
          } );
      </script>


    <!--script for this page only-->



  <script>

      //owl carousel

      $(document).ready(function() {
          $("#owl-demo").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true,
			  autoPlay:true

          });
      });

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>

  </body>
</html>
