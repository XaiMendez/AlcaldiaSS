<!DOCTYPE html>
<html lang="en">
 <?php include '../import_css.php'; ?>
 
    <!--SCRIPT PARA COMBOBOX DEPENDIENTES-->
    <script type="text/javascript" language="javascript" src="../../Resources/js/ajax_cbx.js"></script>	
    
    <body>
        <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
                <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                
            <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->
     
      <!-- Main -->
          <?php include '../main.php';?>
      <!-- /End Main -->

      <!--Comienza contenido principal-->
      <section id="main-content">
          <section class="wrapper">
              <form name="formRptEquipos" action="../../BUSINESS_CAP/AdmonEquipo/Rpt_UbicacionEquipo.php" method="POST"> <!-- FORM -->
          <div class="row"> <!-- div 1-->
                  <div class="col-lg-12"> <!-- div 2-->
                      <section class="panel">
                          <header class="panel-heading">
                              <center><h2>Reporte de Ubicación actual de equipos</h2></center>
                          </header>
                          <div class="panel-body"> <!-- div 3-->
                              <header class="panel-heading">
                              
                          </header>
                             <div class="col-lg-12">
                      
                        <div class="panel-body">
                            <select name="cbxIdEquipos" class="form-control m-bot15" id="" 
                                    onchange="from(document.formRptEquipos.cbxIdEquipos.value,'divUbicacionEquipos','view_ReportUbicacionEquipos.php')">
                            <option>Seleccione un equipo</option>
                            <?php 
                            $queryIdEquipo = "SELECT ID_EQUIPO, CODIGO_EQUIPO FROM EQUIPO_TECNOLOGICO
                                              UNION ALL
                                              SELECT 0,'TODOS LOS EQUIPOS'";
                            $rsIdEquipo = pg_query($queryIdEquipo); 
                            while ($idEq = pg_fetch_array($rsIdEquipo)) {
                            ?>
                                <option value="<?php echo $idEq['id_equipo'] ?>"><?php echo $idEq['codigo_equipo'] ?></option>   
                            <?php
                            }
                            ?>
                        </select>
                        </div>
                   </div>
                  <div class="col-lg-12" onload="">
                      <button type="submit" name="GenerarPdf" class="btn btn-danger">Generar PDF</button>
                        <header class="panel-heading"><h5>Resultados:</h5></header>
                        <div class="panel-body" id="divUbicacionEquipos">
                        </div>
                   </div>
                          </div> <!-- div 3-->                            
                      </section>
                   </section>
              </div>  <!-- div 1-->
              </form>
          </section>
      </section>

      
      <!--Finaliza contenido principal-->

          <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2015 &copy; Alcaldia Municipal de San Salvador.
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
     <script src="../../Resources/js/jquery.js"></script>
    <script src="../../Resources/js/jquery-1.8.3.min.js"></script>
    <script src="../../Resources/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../../Resources/js/jquery.scrollTo.min.js"></script>
    <script src="../../Resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../../Resources/js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="../../Resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="../../Resources/js/owl.carousel.js" ></script>
    <script src="../../Resources/js/jquery.customSelect.min.js" ></script>
    <script src="../../Resources/js/respond.min.js" ></script>

    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>

    <!--common script for all pages-->
    <script src="../../Resources/js/common-scripts.js"></script>

    <!--script for this page-->
    <script src="../../Resources/js/sparkline-chart.js"></script>
    <script src="../../Resources/js/easy-pie-chart.js"></script>
    <script src="../../Resources/js/count.js"></script>
    
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.js" type="text/javascript"></script>


  <script>

      //owl carousel

      $(document).ready(function() {
          $("#owl-demo").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true,
        autoPlay:true

          });
      });

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>
  
  
	
  </body>
</html>

