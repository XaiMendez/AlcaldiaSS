
<!DOCTYPE html>
<html lang="en">
<?php include '../import_css.php'; ?>
    
    <!--SCRIPT PARA COMBOBOX DEPENDIENTES-->
    <script type="text/javascript" language="javascript" src="../../Resources/js/ajax_cbx.js"></script>	
    <script>
    window.onload=function(){
    from(document.formAsignarEquipo.cbxDistrito.value,'divAreaDistrito','cbx_AreaDistrito.php');
    } 
    </script>
  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
                <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                
                <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->
      
       <!-- Main -->
          <?php include '../main.php';?>
       <!-- /End Main -->

      <!--Comienza contenido principal-->
      <section id="main-content">
          <section class="wrapper">
          <form name="formAsignarEquipo" action="../../BUSINESS_CAP/AdmonEquipo/ProcMoverEquipoTec.php" method="POST"> <!-- FORM -->
          <div class="row"> <!-- div 1-->
                  <div class="col-lg-12"> <!-- div 2-->
                      <section class="panel">
                          <header class="panel-heading">
                              <center><h2>MOVER EQUIPO DE EQUIPO TECNOLÓGICO</h2></center>
                          </header>
                          <div class="panel-body"> <!-- div 3-->
                              
                                <div class="form-group">
                                    <label><h3>Datos de Ubicacion Actual</h3></label>
                                  <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>ID bitacora</th>
                                                    <th>Codigo de Equipo</th>
                                                    <th>Fecha de Ubicación</th>
                                                    <th>Distrito</th>
                                                    <th>Area del Distrito u oficina</th>
                                                    <th>Empleado Asignado</th>
                                                    <th>Empelado que realizo la asignación</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php 
                                            require_once '../../DAO_CAP/AdmonEquipo/variables_asignarEquipo.php';
                                            $idEquipoMover = $_SESSION['IdEquipoParaMover'];
                                            $objEquipoMover = new MoverEquipoTecnologico();
                                            $objEquipoMover->setIdEquipo($idEquipoMover);
                                            $rsEncaEqMov = $objEquipoMover->SelectUbicacionActualParaMover();
                                            $rowEncaEqMover = pg_fetch_array($rsEncaEqMov);
                                            ?>
                                            <th>
                                                <?php echo $rowEncaEqMover['id_bita']; ?>
                                                <input type="text" name="txtIdBitacoraActualizar" value="<?php echo $rowEncaEqMover['id_bita']; ?>"  readonly hidden>
                                            </th>
                                            <th>
                                                <input type="text" name="txtEquipoTransMov" value="<?php echo $idEquipoMover; ?>"  readonly hidden>
                                                <?php echo $rowEncaEqMover['cod_eq']; ?>
                                            </th>
                                            <th><?php echo $rowEncaEqMover['fecha_asig']; ?></th>
                                            <th><?php echo $rowEncaEqMover['distrito']; ?></th>
                                            <th>
                                                <input type="text" name="txtIdAreaDistritoActualMov" value="<?php echo $rowEncaEqMover['id_area']; ?>"  readonly hidden>
                                                <?php echo $rowEncaEqMover['area_dist']; ?>
                                            </th>
                                            <th>
                                                <input type="text" name="txtIdEmpleadoAsignadoMov" value="<?php echo $rowEncaEqMover['id_emp_asignado']; ?>"  readonly hidden>
                                                <?php echo $rowEncaEqMover['emp_asignado']; ?>
                                            </th>
                                            <th><?php echo $rowEncaEqMover['emp_autorizo']; ?></th>
                                            </tbody>
                                        </table>
                                    <label><h3>Datos de Solicitud de Traslado</h3></label>
                                    <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Id Solicitud</th>
                                                    <th>Fecha</th>
                                                    <th>Descripción</th>
                                                    <th>Distrito Solicitado</th>
                                                    <th>Area del Distrito u oficina Solicitada</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php 
                                            $idSolicitudTrasladoSelec  = $_SESSION['IdSolicitudTrasladoParaProcesar'];
                                            $objSolicitudSelec = new SolicitudTrasladoEquipo();
                                            $rsEncaSoliTras = $objSolicitudSelec->SelectSolicitudesPendId($idSolicitudTrasladoSelec);
                                            $rowEncaSoliTraslado = pg_fetch_array($rsEncaSoliTras);
                                            ?>
                                            <th> <?php echo $rowEncaSoliTraslado['id_soli']; ?> </th>
                                            <th><?php echo $rowEncaSoliTraslado['fecha']; ?></th>
                                            <th><?php echo $rowEncaSoliTraslado['deta']; ?></th>
                                            <th><?php echo $rowEncaSoliTraslado['distrito']; ?></th>
                                            <th><?php echo $rowEncaSoliTraslado['area_dist']; ?></th>
                                            </tbody>
                                        </table>
                                    <br/>
                                   
                                  <label><h3>Detalle de Transacción</h3></label>
                                  <input type="text" name="txtDescTransMov"class="form-control" size="150" maxlength="200" 
                                         placeholder="Descripción del Movimiento." required>
                                </div>
                          </div> <!-- div 3-->
                          

                      </section>
                  </div> <!-- div 2-->
                   <div class="col-lg-4">
                      <section class="panel">
                        <header class="panel-heading"><h5>Distrito u oficina de destino</h5></header>
                        <div class="panel-body">
                         <select name="cbxDistrito" class="form-control m-bot15" id="" 
                                    onchange="from(document.formAsignarEquipo.cbxDistrito.value,'divAreaDistrito','cbx_AreaDistrito.php')">
                            <?php 
                            $queryDistrito = "SELECT ID_DISTRITO,DETALLE_DISTRITO FROM DISTRITO;";
                            $rsDistrito = pg_query($queryDistrito); 
                            while ($Dist = pg_fetch_array($rsDistrito)) {
                            ?>
                                <option value="<?php echo $Dist['id_distrito'] ?>"><?php echo $Dist['detalle_distrito'] ?></option>   
                            <?php
                            }
                            ?>
                        </select>
                        </div>
                      </section>
                   </div>
                   <div class="col-lg-4">
                      <section class="panel">
                        <header class="panel-heading"><h5>Area del Distrito u oficina</h5></header>
                        <div class="panel-body" id="divAreaDistrito">

                        </div>
                      </section>
                   </div>
                   <div class="col-lg-4">
                      <section class="panel">
                        <header class="panel-heading"><h5>Empleado</h5></header>
                        <div class="panel-body" id="divDetalleEmpleado">
                        
                        </div>
                      </section>
                   </div>
                   <div>
                    <center>
                        <button type="submit" name="MoverEquipoTec" class="btn btn-success">Mover equipo</button>
                        <button type="button" onclick = "location='TransReAsignacionEq.php'" class="btn btn-danger">Regresar</button>
                   </center>
                   <br/>
                   </div>
              </div>  <!-- div 1-->
              </form>
          </section>
      </section>

      
      <!--Finaliza contenido principal-->

          <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2015 &copy; Alcaldia Municipal de San Salvador.
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="../../Resources/js/jquery.js"></script>
    <script src="../../Resources/js/jquery-1.8.3.min.js"></script>
    <script src="../../Resources/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../../Resources/js/jquery.scrollTo.min.js"></script>
    <script src="../../Resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../../Resources/js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="../../Resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="../../Resources/js/owl.carousel.js" ></script>
    <script src="../../Resources/js/jquery.customSelect.min.js" ></script>
    <script src="../../Resources/js/respond.min.js" ></script>

    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>

    <!--common script for all pages-->
    <script src="../../Resources/js/common-scripts.js"></script>

    <!--script for this page-->
    <script src="../../Resources/js/sparkline-chart.js"></script>
    <script src="../../Resources/js/easy-pie-chart.js"></script>
    <script src="../../Resources/js/count.js"></script>


  <script>

      //owl carousel

      $(document).ready(function() {
          $("#owl-demo").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true,
        autoPlay:true

          });
      });

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>

  </body>
</html>
