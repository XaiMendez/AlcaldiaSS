
<!DOCTYPE html>
<html lang="en">
  <?php include '../import_css.php'; ?>
    
  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
                <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->
      
      <!-- Main -->
          <?php include '../main.php';?>
      <!-- /End Main -->

      <!--Comienza contenido principal-->
      <section id="main-content">
          <section class="wrapper">
          <div class="row"> <!-- div 1-->
                  <div class="col-lg-12"> <!-- div 2-->
                      <section class="panel">
                          <header class="panel-heading">
                              <center><h2>REGISTRO DE COMPRA</h2></center>
                          </header>
                          <div class="panel-body"> <!-- div 3-->
                          <h4>Seleccione una solicitud de compra para registrarla</h4>
                              <form action="RegistrarCompra.php" method="GET"> <!-- FORM -->
                                
                               <div class="adv-table">
                                    <table  class="display table table-bordered table-striped" id="example">
                                      <thead>
                                      <tr>
                                          <th> Id </th>
                                          <th> Fecha </th>
                                          <th> Estado </th>
                                          <th> Cod Solicitante</th>
                                          <th> Caso </th>
                                          <th> </th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      <tr>
                                          <td>
                                              <input type="text" name="txtIdCompra" value="1">
                                            </td>  <!-- PARA EL FORM PONER INPUT EN LOS ID PARA JALARLOS -->
                                          <td>2015-08-03</td>
                                          <td>APROVADA</td>
                                          <td>AB123456</td>
                                          <td>  </td>
                                          <td>
                                              <center>
                                              <button type="submit" class="btn btn-success">Registrar Compra</button>
                                              </center>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              <input type="text" name="txtIdCompra" value="2">
                                            </td>  
                                          <td>2015-08-03</td>
                                          <td>APROVADA</td>
                                          <td>AB123456</td>
                                          <td>  </td>
                                          <td>
                                              <center>
                                              <button type="submit" class="btn btn-success">Registrar Compra</button>
                                              </center>
                                          </td>
                                      </tr>
                                      
                                    </table>
                                </div>
                              </form>
                          </div> <!-- div 3-->
                      </section>
                  </div> <!-- div 2-->
              </div>  <!-- div 1-->
          </section>
      </section>

      
      <!--Finaliza contenido principal-->

          <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2015 &copy; Alcaldia Municipal de San Salvador.
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="../../Resources/js/jquery.js"></script>
    <script src="../../Resources/js/jquery-1.8.3.min.js"></script>
    <script src="../../Resources/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../../Resources/js/jquery.scrollTo.min.js"></script>
    <script src="../../Resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../../Resources/js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="../../Resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="../../Resources/js/owl.carousel.js" ></script>
    <script src="../../Resources/js/jquery.customSelect.min.js" ></script>
    <script src="../../Resources/js/respond.min.js" ></script>

    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>

    <!--common script for all pages-->
    <script src="../../Resources/js/common-scripts.js"></script>

    <!--script for this page-->
    <script src="../../Resources/js/sparkline-chart.js"></script>
    <script src="../../Resources/js/easy-pie-chart.js"></script>
    <script src="../../Resources/js/count.js"></script>



    <!--SCRIPT DE TABLA DINAMICA -->
    <script type="text/javascript" language="javascript" src="../../Resources/assets/advanced-datatable/media/js/jquery.dataTables.js"></script>

        <script type="text/javascript" charset="utf-8">
          $(document).ready(function() {
              $('#example').dataTable( {
                  "aaSorting": [[ 4, "desc" ]]
              } );
          } );
      </script>


    <!--script for this page only-->

  

  <script>

      //owl carousel

      $(document).ready(function() {
          $("#owl-demo").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true,
			  autoPlay:true

          });
      });

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>

  </body>
</html>
