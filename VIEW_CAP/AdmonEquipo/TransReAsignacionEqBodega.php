
<!DOCTYPE html>
<html lang="en">
  <?php include '../import_css.php'; ?>
    
  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
                <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            
            <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->
      
      <!-- Main -->
          <?php include '../main.php';?>
      <!-- /End Main -->

      <!--Comienza contenido principal-->
      <section id="main-content">
          <section class="wrapper">
          <div class="row"> <!-- div 1-->
                  <div class="col-lg-12"> <!-- div 2-->
                      <section class="panel">
                          <header class="panel-heading">
                              <center><h2>APROBAR SOLICITUD DE TRASLADO DE EQUIPO A BODEGA</h2></center>
                          </header>
                          <div class="panel-body"> <!-- div 3-->
                              <form name="AprovarSolicitudTrasladoEquipoBodega" action="../../BUSINESS_CAP/AdmonEquipo/ProcMoverEquipoTec.php" method="POST"> <!-- FORM -->
                                <div class="form-group">
                                    <label><h3>Datos de Ubicacion Actual</h3></label>
                                  <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>ID bitacora</th>
                                                    <th>Codigo de Equipo</th>
                                                    <th>Fecha de Ubicación</th>
                                                    <th>Distrito</th>
                                                    <th>Area del Distrito u oficina</th>
                                                    <th>Empleado Asignado</th>
                                                    <th>Empelado que realizo la asignación</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php 
                                            require ("../../DAO_CAP/AdmonEquipo/variables_asignarEquipo.php");
                                            $idEquipoMoverBodega = $_SESSION['IdEquipoSolicitadoMoverBodega'];
                                            $objEquipoMoverBodega = new MoverEquipoTecnologico();
                                            $objEquipoMoverBodega->setIdEquipo($idEquipoMoverBodega);
                                            $rsEncaEqMovBodega = $objEquipoMoverBodega->SelectUbicacionActualParaMover();
                                            $rowEncaEqMoverBod = pg_fetch_array($rsEncaEqMovBodega);
                                            ?>
                                            <th>
                                                <?php echo $rowEncaEqMoverBod['id_bita']; ?>
                                                <input type="text" name="txtIdBitacoraActualizarBodega" value="<?php echo $rowEncaEqMoverBod['id_bita']; ?>"  readonly hidden>
                                            </th>
                                            <th>
                                                <input type="text" name="txtEquipoTransMovBodega" value="<?php echo $idEquipoMoverBodega; ?>"  readonly hidden>
                                                <?php echo $rowEncaEqMoverBod['cod_eq']; ?>
                                            </th>
                                            <th><?php echo $rowEncaEqMoverBod['fecha_asig']; ?></th>
                                            <th><?php echo $rowEncaEqMoverBod['distrito']; ?></th>
                                            <th>
                                                <input type="text" name="txtIdAreaDistritoActualMovBodega" value="<?php echo $rowEncaEqMoverBod['id_area']; ?>"  readonly hidden>
                                                <?php echo $rowEncaEqMoverBod['area_dist']; ?>
                                            </th>
                                            <th>
                                                <?php echo $rowEncaEqMoverBod['emp_asignado']; ?>
                                            </th>
                                            <th><?php echo $rowEncaEqMoverBod['emp_autorizo']; ?></th>
                                            </tbody>
                                        </table>
                                  <label><h3>Datos de Solicitud</h3></label>
                                  <header class="panel-heading">
                          </header>
                          <table class="table">
                              <thead>
                              <tr>
                                  <th>Id Solicitud</th>
                                  <th>Detalle</th>
                                  <th>Equipo</th>
                                  <th>Fecha</th>
                                  <th>Empleado Solicitante</th>
                                  <th>Estado Solicitud</th>
                                  <th>Distrito</th>
                                  <th>Area Distrito Destino</th>
                                  <th># Caso</th>
                                  <th>id Bitacora Caso</th>
                              </tr>
                              </thead>
                              <tbody>
                              <tr>
                              <?php 
                              
                              $idSolicitudTrasBodega = $_SESSION['IdSolicitudTrasladoParaProcesar'];
                              $objMoverEquBodega = new SolicitudTrasladoEquipo();
                              $rsDataTrasBodega = $objMoverEquBodega->SelectSolicitudesPendId($idSolicitudTrasBodega);
                              $rsSoliTrasBodega = pg_fetch_assoc($rsDataTrasBodega);
                              ?>
                                  <th>
                                      <?php print $rsSoliTrasBodega['id_soli']; ?>
                                      <input type="text" name="txtIdSolicitudTrasladoBodega" value="<?php print $rsSoliTrasBodega['id_soli']; ?>" readonly hidden>
                                  </th>
                                  <th><?php print $rsSoliTrasBodega['deta']; ?></th>
                                  <th><?php print $rsSoliTrasBodega['deta_eq']; ?></th>
                                  <th><?php print $rsSoliTrasBodega['fecha']; ?></th>
                                  <th><?php print $rsSoliTrasBodega['cod_emp']; ?></th>
                                  <th><?php print $rsSoliTrasBodega['estado']; ?></th>
                                  <th><?php print $rsSoliTrasBodega['distrito']; ?></th>
                                  <th><?php print $rsSoliTrasBodega['area_dist']; ?></th>
                                  <th><?php print $rsSoliTrasBodega['caso']; ?></th>
                                  <th><?php print $rsSoliTrasBodega['bitacaso']; ?></th>
                              </tr>
                              </tbody>
                              </table>
                                  </div>
                              <div class="form-group">
                                  </div>
                                  <div>
                                  <br>
                                  <center>
                                      <button type="submit" name="AprovarSoliTrasladoBodega" class="btn btn-success">Aprovar Solicitud</button>
                                      <button type="submit" name="DenegarSoliTrasladoBodega" class="btn btn-danger">Denegar Solicitud</button>
                                          <button type="button" class="btn btn-info"
                                          onclick = "location='VerSolicitudTrasladoEquipo.php'">Regresar</button>
                                  </center>
                                  </div>
                                  <!--Cierra el cuble -->
                              </form>
                          </div> <!-- div 3-->

                      </section>
                  </div> <!-- div 2-->
              </div>  <!-- div 1-->
          </section>
      </section>

      
      <!--Finaliza contenido principal-->

          <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2015 &copy; Alcaldia Municipal de San Salvador.
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
   <script src="../../Resources/js/jquery.js"></script>
    <script src="../../Resources/js/jquery-1.8.3.min.js"></script>
    <script src="../../Resources/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../../Resources/js/jquery.scrollTo.min.js"></script>
    <script src="../../Resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../../Resources/js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="../../Resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="../../Resources/js/owl.carousel.js" ></script>
    <script src="../../Resources/js/jquery.customSelect.min.js" ></script>
    <script src="../../Resources/js/respond.min.js" ></script>

    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>

    <!--common script for all pages-->
    <script src="../../Resources/js/common-scripts.js"></script>

    <!--script for this page-->
    <script src="../../Resources/js/sparkline-chart.js"></script>
    <script src="../../Resources/js/easy-pie-chart.js"></script>
    <script src="../../Resources/js/count.js"></script>


    <!-- SCRIPTS PARA LA DATA TABLE EDITABLE -->
    <script type="text/javascript" src="../../Resources/assets/data-tables/jquery.dataTables.js"></script>
    <script type="text/javascript" src="../../Resources/assets/data-tables/DT_bootstrap.js"></script>
    <script>
          jQuery(document).ready(function() {
              EditableTable.init();
          });
    </script>

  <script>

      //owl carousel

      $(document).ready(function() {
          $("#owl-demo").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true,
			  autoPlay:true

          });
      });

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>

  </body>
</html>
