
<!DOCTYPE html>
<html lang="en">
  <?php include '../import_css.php'; ?>
    
  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
                <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            
            <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->
      
      <!-- Main -->
          <?php include '../main.php';?>
      <!-- /End Main -->

      <!--Comienza contenido principal-->
      <section id="main-content">
          <section class="wrapper">
          <div class="row"> <!-- div 1-->
                  <div class="col-lg-12"> <!-- div 2-->
                      <section class="panel">
                          <header class="panel-heading">
                              <center><h2>APROBAR SOLICITUD DE COMPRA</h2></center>
                          </header>
                          <div class="panel-body"> <!-- div 3-->
                              <form name="AprovarSolicitudCompra" action="../../BUSINESS_CAP/AdmonEquipo/ProcAprovarSolicitudCompra.php" method="POST"> <!-- FORM -->
                                <div class="form-group">
                                  <label><h3>Datos de Solicitud</h3></label>
                                  <header class="panel-heading">
                          </header>
                          <table class="table">
                              <thead>
                              <tr>
                                  <th>Id</th>
                                  <th>Fecha</th>
                                  <th>Descripción</th>
                                  <th>Estado</th>
                                  <th>Cod Solicitante</th>
                                  <th>id Bitacora Caso</th>
                                  <th>Aprovado o Denegado por:</th>
                              </tr>
                              </thead>
                              <tbody>
                              <tr>
                                <?php 
                                if(isset($_POST['VerSoliCompra'])){
                                include '../../DAO_CAP/Conexion/admon_conexion.php';
                                $idSoliCompraS = $_POST['txtIdSoliCmpra'];
                                $queryEncaS = "SELECT * FROM Admon_Select_Enca_Soli_Compra($idSoliCompraS)";
                                $rsEncaS = pg_query($queryEncaS);
                                $rowEncaS = pg_fetch_array($rsEncaS);
                                ?>
                                <th><?php print $rowEncaS['id_soli']; ?></th>
                                <th><?php print $rowEncaS['fecha_soli']; ?></th>
                                <th><?php print $rowEncaS['descripcion']; ?></th>
                                <th><?php print $rowEncaS['descripcion_estado']; ?></th>
                                <th><?php print $rowEncaS['cod_emp']; ?></th>
                                <th><?php print $rowEncaS['id_caso']; ?></th>
                                <th><?php print $rowEncaS['cod_emp_aprov']; ?></th>
                              </tr>
                              </tbody>
                              </table>
                                  </div>
                              <div class="form-group">
                                  <label><h3>Detalle de Solicitud</h3></label>
                                  <header class="panel-heading">
                          </header>
                          <table class="table">
                              <thead>
                              <tr>
                                  <th>Id</th>
                                  <th>Descipción Detalle</th>
                                  <th>Descripción Item</th>
                                  <th>Cantidad</th>
                              </tr>
                              </thead>
                              <tbody>
                                <?php 
                                $queryDetaS = "SELECT * FROM Admon_Select_detalle_soli_compra($idSoliCompraS)";
                                $rsDetaS = pg_query($queryDetaS);
                                    while($rowDetaS = pg_fetch_assoc($rsDetaS)) { ?>
                                       <th><?php echo $rowDetaS['id_deta_soli']; ?></th>
                                       <th><?php echo $rowDetaS['desc_soli']; ?></th>
                                       <th><?php echo $rowDetaS['desc_item']; ?></th>
                                       <th><?php echo $rowDetaS['cantidad']; ?> </th>
                              </tbody>
                              <?php } ?>
                              </tbody>
                              </table>
                                  </div>
                                  <div>
                                  <br>
                                  <input type="text" name="txtAprovSoliCompra" hidden value="<?php print $idSoliCompraS; ?>">
                                  <input type="text" name="txtIdEmpAprov" hidden value="<?php print $_SESSION['IngresoSistema']; ?>">
                                  <center>
                                      <button type="submit" name="AprovarSoliCompra" class="btn btn-success">Aprovar Solicitud</button>
                                      <button type="submit" name="DenegarSoliCompra" class="btn btn-danger">Denegar Solicitud</button>
                                          <button type="button" class="btn btn-info"
                                          onclick = "location='VerSolicitudesCompra.php'">Regresar</button>
                                  </center>
                                  </div>
                              <?php } ?>
                              </form>
                          </div> <!-- div 3-->

                      </section>
                  </div> <!-- div 2-->
              </div>  <!-- div 1-->
          </section>
      </section>

      
      <!--Finaliza contenido principal-->

          <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2015 &copy; Alcaldia Municipal de San Salvador.
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
   <script src="../../Resources/js/jquery.js"></script>
    <script src="../../Resources/js/jquery-1.8.3.min.js"></script>
    <script src="../../Resources/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../../Resources/js/jquery.scrollTo.min.js"></script>
    <script src="../../Resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../../Resources/js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="../../Resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="../../Resources/js/owl.carousel.js" ></script>
    <script src="../../Resources/js/jquery.customSelect.min.js" ></script>
    <script src="../../Resources/js/respond.min.js" ></script>

    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>

    <!--common script for all pages-->
    <script src="../../Resources/js/common-scripts.js"></script>

    <!--script for this page-->
    <script src="../../Resources/js/sparkline-chart.js"></script>
    <script src="../../Resources/js/easy-pie-chart.js"></script>
    <script src="../../Resources/js/count.js"></script>


    <!-- SCRIPTS PARA LA DATA TABLE EDITABLE -->
    <script type="text/javascript" src="../../Resources/assets/data-tables/jquery.dataTables.js"></script>
    <script type="text/javascript" src="../../Resources/assets/data-tables/DT_bootstrap.js"></script>
    <script>
          jQuery(document).ready(function() {
              EditableTable.init();
          });
    </script>

  <script>

      //owl carousel

      $(document).ready(function() {
          $("#owl-demo").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true,
			  autoPlay:true

          });
      });

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>

  </body>
</html>
