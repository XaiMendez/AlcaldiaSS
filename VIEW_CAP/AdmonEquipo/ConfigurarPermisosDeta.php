<!DOCTYPE html>
<html lang="en">
    <?php include '../import_css.php'; ?>

    <body>

        <section id="container" >
            <!--Comienza el Header-->
            <div class="header white-bg">
                <!--Inicio del Logo-->
                <div class="header">
                    <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                    <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>

                    <?php include '../Session.php' ?>
                </div>
                <!--Finaliza logo-->
            </div>
            <!--header end-->

            <!-- Main -->
            <?php include '../main.php'; ?>
            <!-- /End Main -->

            <!--Comienza contenido principal-->
            <section id="main-content">
                <section class="wrapper">
                    <div class="row"> <!-- div 1-->
                        <div class="col-lg-12"> <!-- div 2-->
                            <section class="panel">
                                <header class="panel-heading">
                                    <center><h2>CONFIGURACION DE PERMISOS</h2></center>
                                </header>
                                <div class="panel-body"> <!-- div 3-->
                                    <center><button type="button" onclick="location = 'configurar_permisos.php'" class="btn btn-info">Regresar</button></center>
                                    <div class="form-group">
                                        <label><h3>Técnicos Seleccionado</h3></label>
                                        <header class="panel-heading">
                                        </header>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Id Técnico</th>
                                                    <th>Codigo Técnico</th>
                                                    <th>Nombre</th>
                                                    <th>Especialidad</th>
                                                    <th>Estado</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <?php
                                                    include '../../DAO_CAP/AdmonEquipo/variables_permisos.php';
                                                    $idTecnicoSelecPermiso = $_SESSION['IdTecnicoPermisos'];
                                                    $objPermisosTecnicos = new Permisos();
                                                    $rsTecnicoSelec = $objPermisosTecnicos->SelectTecnicosPermisosSeleccionado($idTecnicoSelecPermiso);
                                                    while ($filaTecnicoSel = pg_fetch_assoc($rsTecnicoSelec)) {
                                                        ?>

                                                        <th><?php print $filaTecnicoSel['id_deta_empleado']; ?></th>
                                                        <th><?php print $filaTecnicoSel['codigo_empleado']; ?></th>
                                                        <th><?php print $filaTecnicoSel['nombretec']; ?></th>
                                                        <th><?php print $filaTecnicoSel['especialidad_tecnico']; ?></th>
                                                        <th><?php print $filaTecnicoSel['estado_empleado']; ?></th>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>

                            </section>
                            <section class="panel">

                                <div class="col-lg-6">

                                    <section class="panel">
                                        <header class="panel-heading"><h3>Permisos</h3></header>
                                        <div class="panel-body">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Id Permiso</th>
                                                        <th>Descripción de Permiso</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <?php
                                                        $rsPermisos1 = $objPermisosTecnicos->SelectPermisosParaUsuario($idTecnicoSelecPermiso);
                                                        while ($filaPermiso1 = pg_fetch_assoc($rsPermisos1)) {
                                                            ?>
                                                        <form action="../../BUSINESS_CAP/AdmonEquipo/ProcPreVerPermisos.php" method="POST">
                                                        <th>
                                                            <?php print $filaPermiso1['id_permiso']; ?>
                                                            <input type="text" name="txtIdPermisoAgregar" value="<?php print $filaPermiso1['id_permiso']; ?>" readonly hidden>
                                                                <input type="text" name="txtIdEmpAgregar" value="<?php print $idTecnicoSelecPermiso; ?>" readonly hidden>
                                                        </th>
                                                        <th><?php print $filaPermiso1['detalle_permiso']; ?></th>
                                                        <th>
                                                            <button type="submit" name="AgregarPermiso" class="btn btn-success">Agregar</button></center>
                                                        </th>
                                                    </form>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </section>
                                </div>

                            </section>
                            <section class="panel">

                                <div class="col-lg-6">

                                    <section class="panel">
                                        <header class="panel-heading"><h3>Permisos Actuales</h3></header>
                                        <div class="panel-body">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Id Permiso</th>
                                                        <th>Descripción de Permiso</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <?php
                                                        $rsPermisos2 = $objPermisosTecnicos->SelectPermisosActuales($idTecnicoSelecPermiso);
                                                        while ($filaPermiso2 = pg_fetch_assoc($rsPermisos2)) {
                                                            ?>
                                                    <form action="../../BUSINESS_CAP/AdmonEquipo/ProcPreVerPermisos.php" method="POST">
                                                        <th>
                                                            <?php print $filaPermiso2['id_permiso']; ?>
                                                            <input type="text" name="txtIdUsuarioPermisoQuitar" value="<?php print $filaPermiso2['id_usuario_permiso']; ?>" readonly hidden>
                                                        </th>
                                                        <th><?php print $filaPermiso2['detalle_permiso']; ?></th>
                                                        <th>
                                                            <button type="submit" name="QuitarPermiso" class="btn btn-danger">Quitar</button></center>
                                                        </th>
                                                    </form>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </section>
                                </div>

                            </section>


                        </div> <!-- div 2-->
                    </div>  <!-- div 1-->
                </section>
            </section>


            <!--Finaliza contenido principal-->

            <!--footer start-->
            <footer class="site-footer">
                <div class="text-center">
                    2015 &copy; Alcaldia Municipal de San Salvador.
                </div>
            </footer>
            <!--footer end-->
        </section>

        <!-- js placed at the end of the document so the pages load faster -->
        <script src="../../Resources/js/jquery.js"></script>
        <script src="../../Resources/js/jquery-1.8.3.min.js"></script>
        <script src="../../Resources/js/bootstrap.min.js"></script>
        <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>
        <script src="../../Resources/js/jquery.scrollTo.min.js"></script>
        <script src="../../Resources/js/jquery.nicescroll.js" type="text/javascript"></script>
        <script src="../../Resources/js/jquery.sparkline.js" type="text/javascript"></script>
        <script src="../../Resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
        <script src="../../Resources/js/owl.carousel.js" ></script>
        <script src="../../Resources/js/jquery.customSelect.min.js" ></script>
        <script src="../../Resources/js/respond.min.js" ></script>

        <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>

        <!--common script for all pages-->
        <script src="../../Resources/js/common-scripts.js"></script>

        <!--script for this page-->
        <script src="../../Resources/js/sparkline-chart.js"></script>
        <script src="../../Resources/js/easy-pie-chart.js"></script>
        <script src="../../Resources/js/count.js"></script>

        <!-- SCRIPTS PARA LA DATA TABLE EDITABLE -->
        <script type="text/javascript" src="../../Resources/assets/data-tables/jquery.dataTables.js"></script>
        <script type="text/javascript" src="../../Resources/assets/data-tables/DT_bootstrap.js"></script>
        <script>
                                        jQuery(document).ready(function() {
                                            EditableTable.init();
                                        });
        </script>

        <!--SCRIPT PARA TABLA FILTRADA -->
        <script type="text/javascript" language="javascript" src="../assets/advanced-datatable/media/js/jquery.dataTables.js"></script>

        <script type="text/javascript" charset="utf-8">
                                        $(document).ready(function() {
                                            $('#example').dataTable({
                                                "aaSorting": [[4, "desc"]]
                                            });
                                        });
        </script>

        <script>

            //owl carousel

            $(document).ready(function() {
                $("#owl-demo").owlCarousel({
                    navigation: true,
                    slideSpeed: 300,
                    paginationSpeed: 400,
                    singleItem: true,
                    autoPlay: true

                });
            });

            //custom select box

            $(function() {
                $('select.styled').customSelect();
            });

        </script>

    </body>
</html>
