
<!DOCTYPE html>
<html lang="en">
    <?php include '../import_css.php'; ?>

    <!--SCRIPT PARA COMBOBOX DEPENDIENTES-->
    <script type="text/javascript" language="javascript" src="../../Resources/js/ajax_cbx.js"></script>

    <body onload="from(document.IngreSoftware.cbxTipoSoftware.value, 'divSoftware', 'cbx_Software.php');">

        <section id="container" >
            <!--Comienza el Header-->
            <div class="header white-bg">
                <!--Inicio del Logo-->
                <div class="header">
                    <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                    <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>

                    <?php include '../Session.php' ?>
                </div>
                <!--Finaliza logo-->
            </div>
            <!--header end-->

            <!-- Main -->
            <?php include '../main.php'; ?>
            <!-- /End Main -->

            <!--Comienza contenido principal-->
            <section id="main-content">
                <section class="wrapper">
                    <div class="row"> <!-- div 1-->
                        <div class="col-lg-12"> <!-- div 2-->
                            <section class="panel">
                                <header class="panel-heading">
                                    <center><h2>REGISTRAR DETALLES A EQUIPO</h2></center>
                                    <header class="panel-heading"><center><h3>INGRESE NUEVO SOFTWARE</h3></center></header>
                                </header>
                                <div class="row">
                                        <div class="col-lg-6">
                                            <?php include '../../DAO_CAP/Conexion/admon_conexion.php'; ?>
                                            <section class="panel"><header class="panel-heading"><h5>Equipo Seleccionado</h5></header>
                                                <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Codigo Equipo</th>
                                                    <th>Descripción</th>
                                                    <th> </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                    <tr>
                                                    <?php 
                                                    //capturamos la session con el id del equipo
                                                    $idEquipoSelec = $_SESSION['IdEquipoParaSoftware'];
                                                    
                                                    $queryEqSoft = "SELECT CODIGO_EQUIPO,DESCRIPCION_EQUIPO FROM EQUIPO_TECNOLOGICO WHERE ID_EQUIPO = $idEquipoSelec ";
                                                    $rsEquipoSele= pg_query($queryEqSoft);
                                                    $rowEqSelec = pg_fetch_array($rsEquipoSele);
                                                    ?>
                                                    <th><?php print $rowEqSelec['codigo_equipo']; ?></th>
                                                    <th><?php print $rowEqSelec['descripcion_equipo']; ?></th>
                                                  </tr>
                                                </tbody>
                                        </table>
                                            </section>
                                        </div>
                                    </div>
                                <form action="../../BUSINESS_CAP/AdmonEquipo/ProcIngresarSoftware.php" name="IngreSoftware" method="POST"> <!-- FORM -->
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <section class="panel">
                                                <header class="panel-heading"><h5>Tipo Software</h5></header>
                                                <!-- NO BORRAR ESTE INPUT DE ABAJO, ESTE MANDA EL ID DEL EQUIPO A INSTALAR EL SOFTWARE -->
                                                <input name="txtIdEquipo" value="<?php print $_SESSION['IdEquipoParaSoftware']; ?>" readonly hidden> 
                                                <div class="panel-body">
                                                    <select name="cbxTipoSoftware" class="form-control m-bot15"
                                                            onchange="from(document.IngreSoftware.cbxTipoSoftware.value, 'divSoftware', 'cbx_Software.php')">
                                                                <?php
                                                                $queryTipoSoft = "SELECT ID_TIPO_SOFTWARE,TIPO_SOFTWARE FROM TIPO_SOFTWARE WHERE ID_TIPO_SOFTWARE NOT IN (5)";
                                                                $rsTipoSoft = pg_query($queryTipoSoft);
                                                                while ($filaTs = pg_fetch_assoc($rsTipoSoft)) {
                                                                    ?>

                                                            <option value="<?php print $filaTs['id_tipo_software'] ?>"><?php print $filaTs['tipo_software'] ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </section>
                                        </div>
                                        <div class="col-lg-6">
                                            <section class="panel">
                                                <header class="panel-heading"><h5>Software</h5></header>
                                                <div class="panel-body" id="divSoftware">
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                    <div>
                                        <center>
                                            <button type="submit" name="IngresarSoft" class="btn btn-success">Registrar Detalle Software</button>
                                        </center>
                                    </div>
                                </form>
                                <br>
                                <div class="panel-body"> <!-- div 3-->
                                    <div class="form-group">
                                        <label><h3>Detalles actuales</h3></label>
                                        <header class="panel-heading">
                                        </header>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Descripción</th>
                                                    <th>Tipo</th>
                                                    <th>Activo</th>
                                                    <th>Fecha Activación</th>
                                                    <th>Fecha Desactivación</th>
                                                    <th>Id Empleado activó</th>
                                                    <th>Id Empleado Desactivó</th>
                                                    <th> </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <?php
                                                    $queryTabla = "SELECT * FROM Admon_Select_SoftwareInstalado_Eq($idEquipoSelec)";
                                                    $rsTabla = pg_query($queryTabla);

                                                    while ($rowTabla = pg_fetch_assoc($rsTabla)) {
                                                        ?>
                                                <form action="../../BUSINESS_CAP/AdmonEquipo/ProcIngresarSoftware.php" method="POST"> <!-- FORM -->
                                                    <th>
                                                        <?php print $rowTabla['id_eq_soft'] ?>
                                                        <input name="txtIdEqSoft" value="<?php echo $rowTabla['id_eq_soft'] ?>"
                                                               readonly hidden>
                                                    </th>
                                                    <th><?php print $rowTabla['desc_soft'] ?></th>
                                                    <th><?php print $rowTabla['tipo_soft'] ?></th>
                                                    <th><?php print $rowTabla['is_active'] ?></th>
                                                    <th><?php print $rowTabla['fecha_ins'] ?></th>
                                                    <th><?php print $rowTabla['fecha_desins'] ?></th>
                                                    <th><?php print $rowTabla['id_emp_ins'] ?></th>
                                                    <th><?php print $rowTabla['id_emp_desins'] ?></th>
                                                    <input name="txtIdEmpDesactivo" value="<?php print $rowTabla['id_emp_desins'] ?>"
                                                               readonly hidden>
                                                    <th>
                                                    <center>
                                                        <button type="submit" name="desactivarSoftware" class="btn btn-info">Desactivar</button>
                                                    </center>
                                                    </th>
                                                </form>
                                                </tr>
                                                </tbody>
                                                <?php } ?>
                                        </table>
                                    </div>
                                </div> <!-- div 3-->
                            </section>
                        </div> <!-- div 2-->
                    </div>  <!-- div 1-->


                </section>
            </section>


            <!--Finaliza contenido principal-->

            <!--footer start-->
            <footer class="site-footer">
                <div class="text-center">
                    2015 &copy; Alcaldia Municipal de San Salvador.
                </div>
            </footer>
            <!--footer end-->
        </section>

        <!-- js placed at the end of the document so the pages load faster -->
        <script src="../../Resources/js/jquery.js"></script>
        <script src="../../Resources/js/jquery-1.8.3.min.js"></script>
        <script src="../../Resources/js/bootstrap.min.js"></script>
        <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>
        <script src="../../Resources/js/jquery.scrollTo.min.js"></script>
        <script src="../../Resources/js/jquery.nicescroll.js" type="text/javascript"></script>
        <script src="../../Resources/js/jquery.sparkline.js" type="text/javascript"></script>
        <script src="../../Resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
        <script src="../../Resources/js/owl.carousel.js" ></script>
        <script src="../../Resources/js/jquery.customSelect.min.js" ></script>
        <script src="../../Resources/js/respond.min.js" ></script>

        <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>

        <!--common script for all pages-->
        <script src="../../Resources/js/common-scripts.js"></script>

        <!--script for this page-->
        <script src="../../Resources/js/sparkline-chart.js"></script>
        <script src="../../Resources/js/easy-pie-chart.js"></script>
        <script src="../../Resources/js/count.js"></script>

        <!-- SCRIPTS PARA LA DATA TABLE EDITABLE -->
        <script type="text/javascript" src="../../Resources/assets/data-tables/jquery.dataTables.js"></script>
        <script type="text/javascript" src="../../Resources/assets/data-tables/DT_bootstrap.js"></script>
        <script src="../js/editable-table-RegistrarCompra.js"></script>
        <script>
                                                      jQuery(document).ready(function() {
                                                          EditableTable.init();
                                                      });
        </script>

        <script>

            //owl carousel

            $(document).ready(function() {
                $("#owl-demo").owlCarousel({
                    navigation: true,
                    slideSpeed: 300,
                    paginationSpeed: 400,
                    singleItem: true,
                    autoPlay: true

                });
            });

            //custom select box

            $(function() {
                $('select.styled').customSelect();
            });

        </script>

    </body>
</html>
