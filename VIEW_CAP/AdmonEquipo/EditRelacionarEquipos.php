
<!DOCTYPE html>
<html lang="en">
 <?php include '../import_css.php'; ?>
    
  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
                <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                
                <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->
      
       <!-- Main -->
          <?php include '../main.php';?>
       <!-- /End Main -->

      <!--Comienza contenido principal-->
      <section id="main-content">
          <section class="wrapper">
          <div class="row"> <!-- div 1-->
                  <div class="col-lg-12"> <!-- div 2-->
                      <section class="panel">
                          <header class="panel-heading">
                              <center><h2>RELACIONAR EQUIPOS</h2></center>
                          </header>
                          <div class="panel-body"> <!-- div 3-->
                         
                              <form action="AgregarEquipoRelacionado.php" method="POST">
                                <div class="form-group">
                                  <label><h3>Datos de Equipo Seleccionado</h3></label>
                                  <header class="panel-heading">
                          </header>
                          <table class="table">
                              <thead>
                              <tr>
                                  <th>Id Equipo</th>
                                  <th>Codigo Equipo</th>
                                  <th>Descripción Equipo</th>
                                  <th>Tipo Equipo</th>
                                  <th>Detalle Tipo Equipo</th>
                                  <th>Estado</th>
                                  <th>Equipo Relacionado</th>
                              </tr>
                              </thead>
                              <tbody>
                              <tr>
                                  <?php
                                  include '../../DAO_CAP/Conexion/admon_conexion.php';
                                  $IdEquipoSeleccionado = $_SESSION['IdEquipoParaRelacionar'];
                                  $queryRela1 = "SELECT * FROM Admon_Select_Equipo_Relacionado_1($IdEquipoSeleccionado)";
                                  $rsEquipo1 = pg_query($queryRela1);
                                  $rowEquipo1 = pg_fetch_array($rsEquipo1);
                                  ?>
                                  <th><?php print $rowEquipo1['id_eq']; ?></th>
                                  <th><?php print $rowEquipo1['cod_eq']; ?></th>
                                  <th><?php print $rowEquipo1['desc_eq']; ?></th>
                                  <th><?php print $rowEquipo1['m_tipo_eq']; ?></th>
                                  <th><?php print $rowEquipo1['tipo_eq']; ?></th>
                                  <th><?php print $rowEquipo1['estado_eq']; ?></th>
                                  <th><?php print $rowEquipo1['id_eq_rela']; ?></th>
                              </tr>
                              <tr>
                              </tr>
                              </tbody>
                              </table>
                                  </div>
                                        <center>
                                        <button type="submit" class="btn btn-success">RELACIONAR MAS EQUIPOS</button>
                                        <button type="button" onclick="location='RelacionarEquipos.php'" class="btn btn-info">REGRESAR</button>
                                        </center>
                          </form>
                              <div class="panel-body">
                              <a href="#myModal" data-toggle="modal" class="btn btn-xs btn-success">
                                 VER BITACORAS DE EQUIPOS RELACIONADOS
                              </a>

                              <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
                                  <div class="modal-dialog">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                              <h4 class="modal-title">Bitacora de equipos relacionados</h4>
                                          </div>
                                          <div class="modal-body">

                                              <form role="form">
                                                  <div class="form-group">
                                                    <table class="table">
                                                        <thead>
                                                        <tr>
                                                            <th>Fecha</th>
                                                            <th>Codigo Equipo Relacionado</th>
                                                            <th>Realizado por</th>
                                                            <th>Operacion</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <?php
                                                            $queryBita = "SELECT * FROM Admon_Select_Bitacora_Relacion_Equipo($IdEquipoSeleccionado)";
                                                            $rsBita = pg_query($queryBita);
                                                            
                                                            while ($rowBita = pg_fetch_array($rsBita)){ ?>
                                                                <th><?php print $rowBita['fecha']; ?></th>
                                                                <th><?php print $rowBita['equipo_asig']; ?></th>
                                                                <th><?php print $rowBita['codigo_emp']; ?></th>
                                                                <th><?php print $rowBita['operation']; ?></th>
                                                        </tr>
                                                        <tr>
                                                        </tr>
                                                        </tbody>
                                                        <?php } ?>
                                                        </table>
                                              </form>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                             
                          </div>
                        <div class="form-group">
                                <label><h3>Detalle de Equipos Relacionados</h3></label>
                                <header class="panel-heading"></header>
                          <table class="table">
                              <thead>
                              <tr>
                                  <th>Id Equipo</th>
                                  <th>Codigo Equipo</th>
                                  <th>Descripción Equipo</th>
                                  <th>Tipo Equipo</th>
                                  <th>Detalle Tipo Equipo</th>
                                  <th>Estado</th>
                                  <th>Equipo Relacionado</th>
                                  <th>  </th>
                              </tr>
                              </thead>
                              <tbody>
                                  <tr>
                                <?php 
                                 $queryRela2 = "SELECT * FROM Admon_Select_Equipo_Relacionado_2($IdEquipoSeleccionado)";
                                 $rsEquipo2 = pg_query($queryRela2);
                                 
                                 while ($rowEquipo2 = pg_fetch_assoc($rsEquipo2)){ ?>
                                    <form action="../../BUSINESS_CAP/AdmonEquipo/ProcQuitarEquipoRelacionado.php" method="POST">
                                        <th>
                                            <?php print $rowEquipo2['id_eq']; ?>
                                            <input name="txtIdEquipoQuitarRelacion" value="<?php print $rowEquipo2['id_eq']; ?>" readonly hidden>
                                            <input name="txtIdEquipoSeleccionadoQ" value="<?php print $IdEquipoSeleccionado; ?>" readonly hidden>
                                        </th>
                                        <th><?php print $rowEquipo2['cod_eq']; ?></th>
                                        <th><?php print $rowEquipo2['desc_eq']; ?></th>
                                        <th><?php print $rowEquipo2['m_tipo_eq']; ?></th>
                                        <th><?php print $rowEquipo2['tipo_eq']; ?></th>
                                        <th><?php print $rowEquipo2['estado_eq']; ?></th>
                                        <th><?php print $rowEquipo2['id_eq_rela']; ?></th>
                                        <th>
                                        <center>
                                           <button type="submit" name="QuitarRelacionEquipos" class="btn btn-info">Quitar Relación</button>
                                        </center>
                                        </th>
                                    </form>
                                  </tr>
                              </tbody>
                              <?php } ?>
                              </table>
                          </div>
                          </div>
                      </section>
                  </div> <!-- div 2-->
              </div>  <!-- div 1-->
          </section>
      </section>

      
      <!--Finaliza contenido principal-->

          <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2015 &copy; Alcaldia Municipal de San Salvador.
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="../../Resources/js/jquery.js"></script>
    <script src="../../Resources/js/jquery-1.8.3.min.js"></script>
    <script src="../../Resources/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../../Resources/js/jquery.scrollTo.min.js"></script>
    <script src="../../Resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../../Resources/js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="../../Resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="../../Resources/js/owl.carousel.js" ></script>
    <script src="../../Resources/js/jquery.customSelect.min.js" ></script>
    <script src="../../Resources/js/respond.min.js" ></script>

    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>

    <!--common script for all pages-->
    <script src="../../Resources/js/common-scripts.js"></script>

    <!--script for this page-->
    <script src="../../Resources/js/sparkline-chart.js"></script>
    <script src="../../Resources/js/easy-pie-chart.js"></script>
    <script src="../../Resources/js/count.js"></script>

    <!-- SCRIPTS PARA LA DATA TABLE EDITABLE -->
    <script type="text/javascript" src="../../Resources/assets/data-tables/jquery.dataTables.js"></script>
    <script type="text/javascript" src="../../Resources/assets/data-tables/DT_bootstrap.js"></script>
    <script>
          jQuery(document).ready(function() {
              EditableTable.init();
          });
    </script>

    <!--SCRIPT PARA TABLA FILTRADA -->
     <script type="text/javascript" language="javascript" src="../assets/advanced-datatable/media/js/jquery.dataTables.js"></script>

        <script type="text/javascript" charset="utf-8">
          $(document).ready(function() {
              $('#example').dataTable( {
                  "aaSorting": [[ 4, "desc" ]]
              } );
          } );
      </script>

  <script>

      //owl carousel

      $(document).ready(function() {
          $("#owl-demo").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true,
			  autoPlay:true

          });
      });

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>

  </body>
</html>
