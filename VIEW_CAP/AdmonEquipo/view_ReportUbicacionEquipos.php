<table class="table">
    <thead>
        <tr>
            <th>Id Equipo</th>
            <th>Codigo Equipo</th>
            <th>Descripción Equipo</th>
            <th>Relacionado Con:</th>
            <th>Fecha Asignación</th>
            <th>Distrito Actual</th>
            <th>Area Distrito</th>
            <th>Empleado Asignado:</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <?php
            include("../../DAO_CAP/AdmonEquipo/variables_equipoTecnologico.php");
            $objReporteUbicacionEquipos = new EquipoTecnologico();
            $idEqRptUbicacion = $_GET['id'];
            $objReporteUbicacionEquipos->setIdEquipo($idEqRptUbicacion);
            $rsRpt = $objReporteUbicacionEquipos->ReporteSelectUbicacionesEquipos();
            while ($filaRpt = pg_fetch_assoc($rsRpt)) {
                ?> 
                <th><?php print $filaRpt['id_eq']; ?></th>
                <th><?php print $filaRpt['cod_eq']; ?></th>
                <th><?php print $filaRpt['desc_eq']; ?></th>
                <th><?php print $filaRpt['id_eq_rel']; ?></th>
                <th><?php print $filaRpt['fecha_asig']; ?></th>
                <th><?php print $filaRpt['distrito']; ?></th>
                <th><?php print $filaRpt['area_dist']; ?></th>
                <th><?php print $filaRpt['empasig']; ?></th>
            </tr>
        <?php } ?>
    </tbody>
</table>
