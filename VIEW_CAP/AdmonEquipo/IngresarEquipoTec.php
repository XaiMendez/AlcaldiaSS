
<!DOCTYPE html>
<html lang="en">
 <?php include '../import_css.php'; ?>
 
    <!--SCRIPT PARA COMBOBOX DEPENDIENTES-->
    <script type="text/javascript" language="javascript" src="../../Resources/js/ajax_cbx.js"></script>	
    <script> 
    window.onload=function(){
    from(document.formEquipoTec.cbxMTipoEquipo.value,'divTipo','cbx_TipoEquipo.php'); 
    } 
    </script> 
    
    <body>
        <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
                <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                
            <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->
     
      <!-- Main -->
          <?php include '../main.php';?>
      <!-- /End Main -->

      <!--Comienza contenido principal-->
      <section id="main-content">
          <section class="wrapper">
              <form name="formEquipoTec" action="../../BUSINESS_CAP/AdmonEquipo/ProcEquipoTec.php" method="POST"> <!-- FORM -->
             <?php 
             include '../../DAO_CAP/AdmonEquipo/cbx_equipoTecnologico.php' ;
             ?>
          <div class="row"> <!-- div 1-->
                  <div class="col-lg-12"> <!-- div 2-->
                      <section class="panel">
                          <header class="panel-heading">
                              <center><h2>INGRESO DE EQUIPOS TECNOLÓGICOS</h2></center>
                          </header>
                          <div class="panel-body"> <!-- div 3-->
                              
                                <div class="form-group">
                                  <label><h3>Descripción</h3></label>
                                  <textarea class="form-control" cols="30" rows="3" 
                                            placeholder="Descripción de Equipo" maxlength="200"
                                            required name="txtDescEquipoTec"></textarea>
                                  
                                </div>
                          </div> <!-- div 3-->
                      </section>
                  </div> <!-- div 2-->
                   <div class="col-lg-4">
                      
                      <section class="panel">
                        <header class="panel-heading"><h5>Codigo de equipo</h5></header>
                        <div class="panel-body">
                            <input class="form-control" type="text" maxlength="50" size="50" placeholder="Codigo de equipo" 
                                   name="txtCodigoEquipo" required>
                        </div>
                      </section>
                   </div>
                   <div class="col-lg-4">
                      <section class="panel">
                        <header class="panel-heading"><h5>Estado de Equipo</h5></header>
                        <div class="panel-body">
                            <?php echo combo_estado_equipo(); ?>
                        </div>
                      </section>
                   </div>
                   <div class="col-lg-4">
                      <section class="panel">
                        <header class="panel-heading"><h5>Tipo de Aquisición</h5></header>
                        <div class="panel-body">
                          <?php echo combo_tipo_adquisicion(); ?>
                        </div>
                      </section>
                   </div>
                  <div class="col-lg-6">
                      <section class="panel">
                        <header class="panel-heading"><h5>Tipo Equipo</h5></header>
                        <div class="panel-body">
                            <select name="cbxMTipoEquipo" class="form-control m-bot15" id="" 
                                    onchange="from(document.formEquipoTec.cbxMTipoEquipo.value,'divTipo','cbx_TipoEquipo.php')">
                            <option value="0">Seleccione un tipo</option> 
                            <?php 
                            $queryMTipo = "SELECT ID_M_TIPO_EQUIPO,M_TIPO_EQUIPO FROM M_TIPO_EQUIPO;";
                            $rsMtipo = pg_query($queryMTipo); 
                            while ($Mtipo = pg_fetch_array($rsMtipo)) {
                            ?>
                                <option value="<?php echo $Mtipo['id_m_tipo_equipo'] ?>"><?php echo $Mtipo['m_tipo_equipo'] ?></option>   
                            <?php
                            }
                            ?>
                        </select>
                        </div>
                      </section>
                   </div>
                  <div class="col-lg-6" onload="">
                      <section class="panel">
                        <header class="panel-heading"><h5>Detalle Tipo Equipo</h5></header>
                        <div class="panel-body" id="divTipo">
                        </div>
                      </section>
                   </div>
                   <div class="col-lg-6">
                      <section class="panel">
                        <header class="panel-heading"><h5>Marca Equipo</h5></header>
                        <div class="panel-body" id="divMarca">
                            
                        </div>
                      </section>
                   </div>
                   <div class="col-lg-6">
                      <section class="panel">
                        <header class="panel-heading"><h5>Modelo Equipo</h5></header>
                        <div class="panel-body" id="divModelo">
                        </div>
                      </section>
                   </div>
                   <div>
                       <div class="panel-body">
                    <center>
                        <button type="submit" name="IngresarEquipo" class="btn btn-success">Ingresar equipo</button>
                        <button type="button" onclick = "location='index.php'" class="btn btn-danger">Regresar</button>
                   </center>
                   <br/>
                      </div>
                   </div>
              </div>  <!-- div 1-->
              </form>
          </section>
      </section>

      
      <!--Finaliza contenido principal-->

          <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2015 &copy; Alcaldia Municipal de San Salvador.
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
     <script src="../../Resources/js/jquery.js"></script>
    <script src="../../Resources/js/jquery-1.8.3.min.js"></script>
    <script src="../../Resources/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../../Resources/js/jquery.scrollTo.min.js"></script>
    <script src="../../Resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../../Resources/js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="../../Resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="../../Resources/js/owl.carousel.js" ></script>
    <script src="../../Resources/js/jquery.customSelect.min.js" ></script>
    <script src="../../Resources/js/respond.min.js" ></script>

    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>

    <!--common script for all pages-->
    <script src="../../Resources/js/common-scripts.js"></script>

    <!--script for this page-->
    <script src="../../Resources/js/sparkline-chart.js"></script>
    <script src="../../Resources/js/easy-pie-chart.js"></script>
    <script src="../../Resources/js/count.js"></script>
    
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.js" type="text/javascript"></script>


  <script>

      //owl carousel

      $(document).ready(function() {
          $("#owl-demo").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true,
        autoPlay:true

          });
      });

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>
  
  
	
  </body>
</html>
