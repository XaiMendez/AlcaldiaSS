<!DOCTYPE html>
<html lang="en">

<!-- head -->
<?php include '../import_css.php';?>
<!-- /End head -->
<!--SCRIPT PARA COMBOBOX DEPENDIENTES-->
<script type="text/javascript" language="javascript" src="../../Resources/js/ajax_cbx.js"></script>

<body>

  <section id="container" >
    <!--Comienza el Header-->
    <div class="header white-bg">
      <!--Inicio del Logo-->
      <div class="header">
        <a class="logo" href="index.html"><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
        <a class="sublogo" href="index.html"><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
        <?php include '../Session.php' ?>
      </div>
      <!--Finaliza logo-->
    </div>
    <!--header end-->

    <!-- Main -->
    <?php include '../main.php';?>
    <!-- /End Main -->


    <!--Comienza contenido principal-->
    <section id="main-content">
      <section class="wrapper">
        <form name="formRptEquipos" action="../../BUSINESS_CAP/AdmonEquipo/Rpt_report2_adm_equipo.php" method="POST">
        <!-- page start-->

        <div class="col-lg-9">
          <section class="panel">
            <header class="panel-heading">
              <center><h2>Reporte Detallado de Equipos </h2></center>
            </header>
            <div class="panel-body col-lg-4">
              <label>Ingrese el codigo de su equipo:</label>
            </div>
            <div class="panel-body col-lg-3">
              <label>Fecha Inicio:</label>
            </div>
            <div class="panel-body col-lg-3">
              <label>Fecha Final:</label>
            </div>
            <div class="panel-body col-lg-4">
              <input type="text" name="txt_codigo_equipo" class="form-control">
            </div>
            <div class="panel-body col-lg-3">
              <input type="date" name="fecha1">
            </div>
            <div class="panel-body col-lg-4">
              <input type="date" name="fecha2">
            </div>
            <div class="panel-body col-lg-4">
            </br><button type="submit" name="GenerarPdf" class="btn btn-danger">Generar PDF</button>
            </div>
              <div class="panel-body " id="divUbicacionEquipos">
          </div>

        </div>
    <!-- page end-->
  </section>
</section>
<!--main content end-->
<!--footer start-->
<footer class="site-footer">
      <div class="text-center">
          2015 &copy; Alcaldia Municipal de San Salvador.
      </div>
  </footer>
<!--footer end-->
</section>
<!-- js placed at the end of the document so the pages load faster -->
 <script src="../../Resources/js/jquery.js"></script>
<script src="../../Resources/js/jquery-1.8.3.min.js"></script>
<script src="../../Resources/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="../../Resources/js/jquery.scrollTo.min.js"></script>
<script src="../../Resources/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="../../Resources/js/jquery.sparkline.js" type="text/javascript"></script>
<script src="../../Resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
<script src="../../Resources/js/owl.carousel.js" ></script>
<script src="../../Resources/js/jquery.customSelect.min.js" ></script>
<script src="../../Resources/js/respond.min.js" ></script>

<script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>

<!--common script for all pages-->
<script src="../../Resources/js/common-scripts.js"></script>

<!--script for this page-->
<script src="../../Resources/js/sparkline-chart.js"></script>
<script src="../../Resources/js/easy-pie-chart.js"></script>
<script src="../../Resources/js/count.js"></script>

<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.js" type="text/javascript"></script>


<script>

  //owl carousel

  $(document).ready(function() {
      $("#owl-demo").owlCarousel({
          navigation : true,
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem : true,
    autoPlay:true

      });
  });

  //custom select box

  $(function(){
      $('select.styled').customSelect();
  });

</script>
</body>
</html>
