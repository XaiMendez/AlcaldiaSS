
<!DOCTYPE html>
<html lang="en">
<?php include '../import_css.php'; ?>
    
  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
                <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
                <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->
      
      <!-- Main -->
          <?php include '../main.php';?>
      <!-- /End Main -->

      <!--Comienza contenido principal-->
      <section id="main-content">
          <section class="wrapper">
          <div class="row"> <!-- div 1-->
                  <div class="col-lg-12"> <!-- div 2-->
                      <section class="panel">
                          <header class="panel-heading">
                              <center><h2>BITACORA DE UBICACIONES</h2></center>
                          </header>
                          <div class="panel-body"> <!-- div 3-->
                              <form action="TransBitacoraAsigDeta.php" method="POST"> <!-- FORM -->
                                
                               <div class="adv-table">
                                    <table  class="display table table-bordered table-striped" id="example">
                                      <thead>
                                      <tr>
                                          <th> Correlativo </th>
                                          <th> Fecha Asignación </th>
                                          <th> Detalle Asignación </th>
                                          <th> Autorizado por </th>
                                          <th> Empleado Asignado </th>
                                          <th> Distrito</th>
                                          <th> Area del Distrito </th>
                                          <th> Ubicación actual</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      <tr>
                                          <th> 1 </th>
                                          <th> 2015-08-12 08:25:12.145 </th>
                                          <th> Texto...1 </th>
                                          <th> GT100211 </th>
                                          <th> Empleado123 </th>
                                          <th> Distrito 1</th>
                                          <th> Oficinas Administrativas 1234 </th>
                                          <th> true </th>
                                      </tr>
                                      <tr>
                                          <th> 2 </th>
                                          <th> 2015-08-05 08:25:12.145 </th>
                                          <th> Texto1234...11.. </th>
                                          <th> AB123456 </th>
                                          <th> gt100211 </th>
                                          <th> Distrito 2 </th>
                                          <th> Bodega </th>
                                          <th> false </th>
                                      </tr>
                                      <tr>
                                         <th> 3 </th>
                                          <th> 2015-08-01 08:25:12.145 </th>
                                          <th> Deta..... </th>
                                          <th> AM12345 </th>
                                          <th> XMBA456 </th>
                                          <th> Distrito 4</th>
                                          <th> Caja 2 </th>
                                          <th> false </th>
                                      </tr>
                                    </table>
                                </div>
                              </form>
                          </div> <!-- div 3-->

                      </section>
                  </div> <!-- div 2-->
              </div>  <!-- div 1-->
          </section>
      </section>

      
      <!--Finaliza contenido principal-->

          <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2015 &copy; Alcaldia Municipal de San Salvador.
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="../../Resources/js/jquery.js"></script>
    <script src="../../Resources/js/jquery-1.8.3.min.js"></script>
    <script src="../../Resources/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../../Resources/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../../Resources/js/jquery.scrollTo.min.js"></script>
    <script src="../../Resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../../Resources/js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="../../Resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="../../Resources/js/owl.carousel.js" ></script>
    <script src="../../Resources/js/jquery.customSelect.min.js" ></script>
    <script src="../../Resources/js/respond.min.js" ></script>

    <!--common script for all pages-->
    <script src="../../Resources/js/common-scripts.js"></script>

    <!--script for this page-->
    <script src="../../Resources/js/sparkline-chart.js"></script>
    <script src="../../Resources/js/easy-pie-chart.js"></script>
    <script src="../../Resources/js/count.js"></script>



    <!--SCRIPT DE TABLA DINAMICA -->
    <script type="text/javascript" language="javascript" src="../../Resources/assets/advanced-datatable/media/js/jquery.dataTables.js"></script>

        <script type="text/javascript" charset="utf-8">
          $(document).ready(function() {
              $('#example').dataTable( {
                  "aaSorting": [[ 4, "desc" ]]
              } );
          } );
      </script>


    <!--script for this page only-->

  

  <script>

      //owl carousel

      $(document).ready(function() {
          $("#owl-demo").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true,
        autoPlay:true

          });
      });

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>

  </body>
</html>
