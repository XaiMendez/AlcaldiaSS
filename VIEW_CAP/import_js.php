<!-- JavaScripts de todas las paginas -->

<!--js colocado al final de la pagina para que cargue mas rapido-->
    <script src="../../Resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="../../Resources/js/jquery.js"></script>
    <script src="../../Resources/js/jquery-1.8.3.min.js"></script>
    <script src="../../Resources/js/bootstrap.min.js"></script>
    <script src="../../Resources/js/jquery.dcjqaccordion.2.7.js"class="include" type="text/javascript" ></script>
    <script src="../../Resources/js/jquery.scrollTo.min.js"></script>
    <script src="../../Resources/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../../Resources/js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="../../Resources/js/owl.carousel.js" ></script>
    <script src="../../Resources/js/jquery.customSelect.min.js" ></script>
    <script src="../../Resources/js/respond.min.js" ></script>

<!-- Script comun de todas las paginas -->
	  <script src="../../Resources/js/common-scripts.js"></script>

<!-- Script para las paginas de catalogo -->
	  <script src="../../Resources/js/sparkline-chart.js"></script>
    <script src="../../Resources/js/easy-pie-chart.js"></script>
    <script src="../../Resources/js/count.js"></script>

<!-- Script para las editable -->
    <script src="../../Resources/assets/data-tables/jquery.dataTables.js"type="text/javascript" ></script>
    <script src="../../Resources/assets/data-tables/DT_bootstrap.js" type="text/javascript"></script>
    <script src="../../Resources/js/editable-table-solicitudCompra.js"></script>

<!--Script para esta las tablas-->
    <script src="../../Resources/js/editable-table2.js"></script>
    <script src="../../Resources/js/editable-table.js"></script>
      
<!--script para carnetizacion-->
    <script src="../../Resourcesjs/jquery.stepy.js"></script>

<!-- Finaliza javascript -->

    <script>
        //owl carousel
        $(document).ready(function() {
            $("#owl-demo").owlCarousel({
                navigation : true,
                slideSpeed : 300,
                paginationSpeed : 400,
                singleItem : true,
          autoPlay:true
            });
        });

        //custom select box
        $(function(){
            $('select.styled').customSelect();
        });
    </script>
    <!-- Funciones de editable -->
    <script>
        jQuery(document).ready(function() {
            EditableTable.init();
        });
    </script>
    <script>
          jQuery(document).ready(function() {
              Escalabilidad.init();
          });
    </script>
    <script>
      $(function() {
        $('#default').stepy({
            backLabel: 'Previous',
            block: true,
            nextLabel: 'Next',
            titleClick: true,
            titleTarget: '.stepy-tab'
          });
      });
  </script>
  <!--script para tabla asignacion-->

      <script type="text/javascript" charset="utf-8">
          $(document).ready(function() {
              $('#asignacion').dataTable( {
                  "aaSorting": [[ 0, "asc" ]]
              } );
          } );
      </script>
    <!--script para tabla asignacion-->

      <script type="text/javascript" charset="utf-8">
          $(document).ready(function() {
              $('#finalizar').dataTable( {
                  "aaSorting": [[ 0, "asc" ]]
              } );
          } );
      </script>

      <!--<script src="js/jquery.js"></script>-->
    
  <!--common script for all pages-->
    <script src="../../Resources/assets/advanced-datatable/media/js/jquery.js"type="text/javascript" language="javascript" ></script>
    <script src="../../Resources/assets/advanced-datatable/media/js/jquery.dataTables.js"type="text/javascript" language="javascript" ></script>
    <script src="../../Resources/js/jquery.dcjqaccordion.2.7.js"class="include" type="text/javascript" ></script>


    <!--script para tabla asignacion-->

      <script type="text/javascript" charset="utf-8">
          $(document).ready(function() {
              $('#asignacion').dataTable( {
                  "aaSorting": [[ 0, "asc" ]]
              } );
          } );
      </script>
    <!--script para tabla asignacion-->

      <script type="text/javascript" charset="utf-8">
          $(document).ready(function() {
              $('#finalizar').dataTable( {
                  "aaSorting": [[ 0, "asc" ]]
              } );
          } );
      </script>
      