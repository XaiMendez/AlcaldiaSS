<!DOCTYPE html>
<html lang="en">
  <?php include '../import_css.php'; ?>

  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
            <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            
            <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->
      
      <!-- Main -->
          <?php include '../main.php';?>
      <!-- /End Main -->

       <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              <section class="panel">
                  <header class="panel-heading">
                      <center><h3>Detalle de Tipo Equipo</h3></center>
                  </header>
                  <div class="panel-body">
                      <center>
                      <a href="#myModalInsertarMTipoEquipo" data-toggle="modal" class="btn btn-success">
                                  Insertar Nuevo M Tipo Equipo
                      </a> 
                      </center>
                      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalInsertarMTipoEquipo" class="modal fade">
                                  <div class="modal-dialog">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                              <h4 class="modal-title">Insertar Nuevo Tipo Equipo</h4>
                                          </div>
                                          <div class="modal-body">
                                              <form action="../../BUSINESS_CAP/AdmonEquipo/ProcMTipoEquipo.php" method="POST">
                                                  <div class="form-group">
                                                      <label class=" p-head">M Tipo Equipo</label>
                                                      <input type="text" name="txtMTipoEquipoInsertar" maxlength="100" class="form-control" 
                                                             placeholder="Describa el Tipo Equipo" required>                                                    
                                                   <br/>
                                                      <label class=" p-head">M2 Tipo Equipo</label>
                                                      <select name="cbxMTipoEquipoInsertar" class="form-control m-bot15">
                                                          <?php
                                                          require ("../../DAO_CAP/AdmonEquipo/Mantenimiento_M_Tipo_Equipo.php");
                                                          $objtipoEquipoM = new MantenimientoM_TipoEquipo();
                                                          $rsMTipoEquipoIns = $objtipoEquipoM->SelectMTipoEquipoParaInsertar();
                                                          while ($mtipoEqVarIns = pg_fetch_array( $rsMTipoEquipoIns)) {
                                                              ?>
                                                              <option value="<?php echo $mtipoEqVarIns['id_m2_tipo_equipo']; ?>"><?php echo $mtipoEqVarIns['m2_tipo_equipo']; ?></option>  
                                                              <?php
                                                          }
                                                          ?> 
                                                      </select>
                                                      <br/>
                                                      <center><button type="submit" class="btn btn-default" name="InsertarMEquipo">Insertar Tipo Equipo</button></center>
                                                  </div>
                                              </form>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                 
                      <table class="table">
                          <thead>
                              <tr>
                                  <th> Id Maestro Tipo Equipo </th>
                                  <th> Maestro Tipo Equipo </th>
                                  <th> Maestro2 Tipo Equipo </th>
                                  <th> </th>
                              </tr>
                          </thead>
                           <tbody>
                              <tr>
                                  <?php
                                  
                                  $rsTEquipoMan = $objtipoEquipoM->SelectMTipoEquipo();
                                  $rsRowsmtipoEquipoMan = pg_num_rows($rsTEquipoMan);
                                  //while ($rowEquipoUbicacion = pg_fetch_assoc($rsBitacoraUbicacion)) {
                                  for ($varTipoEquipo = 0; $varTipoEquipo < $rsRowsmtipoEquipoMan; $varTipoEquipo++) {
                                      $idmEquipoModal = pg_fetch_result($rsTEquipoMan, $varTipoEquipo, 'id_m_tipo_equipo');
                                      $mtipoEquipoModal = pg_fetch_result($rsTEquipoMan, $varTipoEquipo, 'm_tipo_equipo');
                                      $idm2TipoEquipoActual = pg_fetch_result($rsTEquipoMan, $varTipoEquipo, 'id_m2_tipo_equipo');
                                      $m2TipoEquipoModal = pg_fetch_result($rsTEquipoMan, $varTipoEquipo, 'm2_tipo_equipo');
                                      ?>
                                      <th><?php print $idmEquipoModal; ?></th>
                                      <th><?php print $mtipoEquipoModal; ?></th>                                     
                                      <th><?php print $m2TipoEquipoModal; ?></th>
                                      <th>
                                        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalmTipo<?php print $varTipoEquipo; ?>" class="modal fade">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                            <h4 class="modal-title">Detalle de Tipo Equipo</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form action="../../BUSINESS_CAP/AdmonEquipo/ProcMTipoEquipo.php" method="POST">
                                                                <div class="form-group">
                                                                    <input type="text" name="idmTipoEquipoModificar" value="<?php print $idmEquipoModal; ?>"readonly hidden>
                                                                    <label class=" p-head">M Tipo Equipo</label>
                                                                    <input type="text" name="mtipoEquipo" value="<?php print $mtipoEquipoModal; ?>" class="form-control" required>
                                                                    <br/>      
                                                                    <label class=" p-head">M2 Tipo de Equipo</label>
                                                                    <select name="cbxM2TipoEquipoMan" class="form-control m-bot15">
                                                                        <option value="<?php print $idm2TipoEquipoActual; ?>"><?php print $m2TipoEquipoModal; ?></option> 
                                                                      <?php
                                                                        $rsTEquipo = $objtipoEquipoM->SelectMEquipo($idm2TipoEquipoActual);
                                                                        while ($tipoEquipoVar = pg_fetch_array($rsTEquipo)){
                                                                     ?>
                                                                        <option value="<?php echo $tipoEquipoVar['id_m2_tipo_equipo']; ?>"><?php echo $tipoEquipoVar['m2_tipo_equipo']; ?></option>  
                                                                     <?php
                                                                        }
                                                                      ?> 
                                                                        
                                                                    </select>
                                                                </div>
                                                                <center>
                                                                    <button type="submit" name="ModificarMEquipo" class="btn btn-default">Modificar Registro</button>
                                                                </center>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#myModalmTipo<?php print $varTipoEquipo; ?>" data-toggle="modal" class="btn btn-info">
                                                Modificar
                                           </a>
                                    </th>
                                  </tr>
                              </tbody>
                              <?php
                          }
                          ?>
                      </table>
                  </div>
              </section>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
    
    
  </section>

    <?php include '../import_js.php'; ?>

  </body>
</html>
