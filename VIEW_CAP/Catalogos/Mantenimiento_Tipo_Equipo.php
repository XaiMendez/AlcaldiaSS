<!DOCTYPE html>
<html lang="en">
  <?php include '../import_css.php'; ?>

  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
            <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            
            <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->
      
      <!-- Main -->
          <?php include '../main.php';?>
      <!-- /End Main -->

       <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              <section class="panel">
                  <header class="panel-heading">
                      <center><h3>Detalle de Tipo Equipo</h3></center>
                  </header>
                  <div class="panel-body">
                      <center>
                      <a href="#myModalInsertarEquipo" data-toggle="modal" class="btn btn-success">
                                  Insertar Nuevo Tipo Equipo
                      </a> 
                      </center>
                      <div aria-hidden="true" aria-labelledby="myModalLabel1" role="dialog" tabindex="-1" id="myModalInsertarEquipo" class="modal fade">
                                  <div class="modal-dialog">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                              <h4 class="modal-title">Insertar Nuevo Tipo Equipo</h4>
                                          </div>
                                          <div class="modal-body">
                                              <form action="../../BUSINESS_CAP/AdmonEquipo/ProcTipoEquipo.php" method="POST">
                                                  <div class="form-group">
                                                      <label class=" p-head">Tipo Equipo</label>
                                                      <input type="text" name="txtTipoEquipoInsertar" maxlength="45" class="form-control" 
                                                             placeholder="Describa el Tipo Equipo" required>                                                    
                                                   <br/>
                                                      <label class=" p-head">M Tipo Equipo</label>
                                                      <select name="cbxTipoEquipoInsertar" class="form-control m-bot15">
                                                          <?php
                                                          require ("../../DAO_CAP/AdmonEquipo/Mantenimiento_Tipo_Equipo.php");
                                                          $objtipoEquipo = new MantenimientoTipoEquipo();
                                                          $rsTipoEquipoIns = $objtipoEquipo->SelectTipoEquipoParaInsertar();
                                                          while ($tipoEqVarIns = pg_fetch_array( $rsTipoEquipoIns)) {
                                                              ?>
                                                              <option value="<?php echo $tipoEqVarIns['id_m_tipo_equipo']; ?>"><?php echo $tipoEqVarIns['m_tipo_equipo']; ?></option>  
                                                              <?php
                                                          }
                                                          ?> 
                                                      </select>
                                                      <br/>
                                                      <center><button type="submit" class="btn btn-default" name="InsertarEquipo">Insertar Tipo Equipo</button></center>
                                                  </div>
                                              </form>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                 
                      <table class="table">
                          <thead>
                              <tr>
                                  <th> Id Tipo Equipo </th>
                                  <th> Tipo Equipo </th>
                                  <th> M Tipo Equipo </th>
                                  <th> </th>
                              </tr>
                          </thead>
                          <tbody>
                              <tr>
                                  <?php
                                  
                                  $rsEquipoMan = $objtipoEquipo->SelectTipoEquipo();
                                  $rsRowstipoEquipoMan = pg_num_rows($rsEquipoMan);
                                  //while ($rowEquipoUbicacion = pg_fetch_assoc($rsBitacoraUbicacion)) {
                                  for ($varTipoEq = 0; $varTipoEq < $rsRowstipoEquipoMan; $varTipoEq++) {
                                      $idEquipoModal = pg_fetch_result($rsEquipoMan, $varTipoEq, 'id_tipo_equipo');
                                      $tipoEquipoModal = pg_fetch_result($rsEquipoMan, $varTipoEq, 'tipo_equipo');
                                      $idmTipoEquipoActual = pg_fetch_result($rsEquipoMan, $varTipoEq, 'id_m_tipo_equipo');
                                      $mTipoEquipoModal = pg_fetch_result($rsEquipoMan, $varTipoEq, 'm_tipo_equipo');
                                      ?>
                                      <th><?php print $idEquipoModal; ?></th>
                                      <th><?php print $tipoEquipoModal; ?></th>                                     
                                      <th><?php print $mTipoEquipoModal; ?></th>
                                      <th>
                                        <div aria-hidden="true" aria-labelledby="myModalLabel1" role="dialog" tabindex="-1" id="myModalTipo<?php print $varTipoEq; ?>" class="modal fade">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                            <h4 class="modal-title">Detalle de Tipo Equipo</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form action="../../BUSINESS_CAP/AdmonEquipo/ProcTipoEquipo.php" method="POST">
                                                                <div class="form-group">
                                                                    <input type="text" name="idTipoEquipoModificar" value="<?php print $idEquipoModal; ?>"readonly hidden>
                                                                    <label class=" p-head">Tipo Equipo</label>
                                                                    <input type="text" name="tipoEquipo" value="<?php print $tipoEquipoModal; ?>" class="form-control" required>
                                                                    <br/>      
                                                                    <label class=" p-head">M Tipo de Equipo</label>
                                                                    <select name="cbxTipoEquipoMan" class="form-control m-bot15">
                                                                        <option value="<?php print $idmTipoEquipoActual; ?>"><?php print $mTipoEquipoModal; ?></option> 
                                                                      <?php
                                                                        $rsEquipo = $objtipoEquipo->SelectEquipo($idmTipoEquipoActual);
                                                                        while ($tipoEqVar = pg_fetch_array($rsEquipo)){
                                                                     ?>
                                                                        <option value="<?php echo $tipoEqVar['id_m_tipo_equipo']; ?>"><?php echo $tipoEqVar['m_tipo_equipo']; ?></option>  
                                                                     <?php
                                                                        }
                                                                      ?> 
                                                                        
                                                                    </select>
                                                                </div>
                                                                <center>
                                                                    <button type="submit" name="ModificarEquipo" class="btn btn-default">Modificar Registro</button>
                                                                </center>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#myModalTipo<?php print $varTipoEq; ?>" data-toggle="modal" class="btn btn-info">
                                                Modificar
                                           </a>
                                    </th>
                                  </tr>
                              </tbody>
                              <?php
                          }
                          ?>
                      </table>
                  </div>
              </section>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
    
    
  </section>

    <?php include '../import_js.php'; ?>

  </body>
</html>
