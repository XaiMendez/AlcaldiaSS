
<!DOCTYPE html>
<html lang="en">

<!-- head -->
    <?php include '../import_css.php'; ?>
<!-- /End head -->

  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
            <a class="logo" href="index.html"><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <a class="sublogo" href="index.html"><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
              <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
      </div>
      <!--header end-->

       <!-- Main -->
          <?php include '../main.php';?>
       <!-- /End Main -->

      <!--Comienza contenido principal-->
      <!--*********************************************************************** -->
         <!--main content start-->
      <section id="main-content">
        <section class="wrapper">
          <!-- inicio de pagina-->
          <section class="panel">
            <header class="panel-heading">
              <center><h3>Escalabilidad de Casos</h3></center>
            </header>
            <div class="panel-body">
              <center>
                <a href="#myModalInsertarEscalabilidad" data-toggle="modal" class="btn btn-success">
                  Insertar Nueva Escalabilidad
                </a>
              </center>
              <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalInsertarEscalabilidad" class="modal fade">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                        <h4 class="modal-title">Insertar Nueva Escalabilidad</h4>
                    </div>
                    <div class="modal-body">
                      <form action="../../BUSINESS_CAP/HelpDesk/ProcEscalabilidadCat.php" method="POST">
                        <div class="form-group">
                          <label class=" p-head">Descripción de escalabilidad</label>
                            <input type="text" name="txtDescEscalabilidadInsertar" maxlength="100" class="form-control" placeholder="Describa el software" required>
                            <br/>
                            <center><button type="submit" class="btn btn-default" name="InsertarEscalabilidad">Insertar escalabilidad</button></center>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <table class="table">
                <thead>
                  <tr>
                    <th>Id Escalabilidad</th>
                    <th>Escalabilidad</th>
                    <th> </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <?php
                  //  error_reporting(E_ALL ^ E_NOTICE);
                    require ("../../DAO_CAP/HelpDesk/model.escalabilidad.php");
                    $objEscalabilidad = new MantenimientoEscalabilidad();
                    $rsEscalabilidadMan = $objEscalabilidad->SelectEscalabilidad();
                    $rsRowsEscalabilidadMan = pg_num_rows($rsEscalabilidadMan);

                    for ($varControEsca = 0; $varControEsca < $rsRowsEscalabilidadMan; $varControEsca++){
                      $idEscalabilidadModal = pg_fetch_result($rsEscalabilidadMan, $varControEsca, 'id_escalabilidad');
                      $descEscalabilidadModal = pg_fetch_result($rsEscalabilidadMan, $varControEsca, 'escalabilidad');

                     ?>
                     <th><?php print $idEscalabilidadModal; ?></th>
                     <th><?php print $descEscalabilidadModal; ?></th>
                     <th>
                       <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalEscalabilidad<?php print $varControEsca; ?>" class="modal fade">
                         <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                <h4 class="modal-title">Detalle de escalabilidad</h4>
                            </div>
                            <div class="modal-body">
                               <form action="../../BUSINESS_CAP/HelpDesk/ProcEscalabilidadCat.php" method="POST">
                                 <div class="form-group">
                                   <input type="text" name="idEscaModificar" value="<?php print $idEscalabilidadModal; ?>" readonly hidden>
                                   <label class="p-head"> Descripcion de Escalabilidad</label>
                                   <input type="text" name="DescEscalabilidad" value="<?php print $descEscalabilidadModal; ?>" class="form-control" required>
                                 </div>
                                 <center>
                                   <button type="submit" name="ModificarEscalabilidad" class="btn btn-default">Modificar Registro</button>
                                 </center>
                               </form>
                             </div>
                           </div>
                         </div>
                       </div>
                       <a href="#myModalEscalabilidad<?php print $varControEsca; ?>" data-toggle="modal" class="btn btn-info">
                          Modificar
                       </a>
                     </th>
                   </tr>
                 </tbody>
                 <?php
                          }
                          ?>
               </table>
            </div>
          </section>
            <!-- finaliza pag-->
        </section>
      </section>
      <!--main content end-->
      <!-- ********************************************************************** -->
      <!--Finaliza contenido principal-->

          <!--footer start-->
      <footer class="site-footer">
        <div class="text-center">
          2015 &copy; Alcaldia Municipal de San Salvador.
        </div>
      </footer>
      <!--footer end-->
    </section>
      <?php include '../import_js.php';?>
  </body>
</html>
