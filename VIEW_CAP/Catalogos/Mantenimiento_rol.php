<!DOCTYPE html>
<html lang="en">

<!-- head -->
    <?php include '../import_css.php';?>
<!-- /End head -->


  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
            <a class="logo" href="index.html"><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <a class="sublogo" href="index.html"><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->

      <!-- Main -->
          <?php include '../main.php';?>
      <!-- /End Main -->

      <!--Comienza contenido principal-->

      <section id="main-content">
        <section class="wrapper">
            <!-- page start-->
            <section class="panel">
              <header class="panel-heading">
                <center><h3>Rol</h3></center>
              </header>
              <div class="panel-body">
                <center>
                <a href="#myModalInsertarRol" data-toggle="modal" class="btn btn-success">
                  Insertar Nuevo Rol
                </a>
                </center>
                <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalInsertarRol" class="modal fade">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                        <h4 class="modal-title">Insertar Nuevo Rol</h4>
                    </div>
                    <div class="modal-body">
                      <form action="../../BUSINESS_CAP/HelpDesk/ProcRolCat.php" method="POST">
                        <div class="form-group">
                          <label class=" p-head">Descripción de Rol</label>
                            <input type="text" name="txtDescRolInsertar" maxlength="100" class="form-control" placeholder="Describa el software" required>
                            <br/>
                            <center><button type="submit" class="btn btn-default" name="InsertarRol">Insertar Rol</button></center>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <table class="table">
                <thead>
                  <tr>
                    <th>Id Rol</th>
                    <th>Rol</th>
                    <th> </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <?php
                    require ("../../DAO_CAP/HelpDesk/model.Rol.php");
                    $objRol = new MantenimientoRol();
                    $rsRolMan = $objRol->SelectRol();
                    $rsRowsRolMan = pg_num_rows($rsRolMan);

                    for ($varControRol = 0; $varControRol < $rsRowsRolMan; $varControRol++){
                        $idRolModal = pg_fetch_result($rsRolMan, $varControRol, 'id_rol');
                        $descRolModal = pg_fetch_result($rsRolMan, $varControRol, 'nombre_rol');

                    ?>
                    <th><?php print $idRolModal; ?></th>
                    <th><?php print $descRolModal; ?></th>
                    <th>
                      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalRol<?php print $varControRol; ?>" class="modal fade">
                         <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                <h4 class="modal-title">Modificar Rol</h4>
                            </div>
                            <div class="modal-body">
                               <form action="../../BUSINESS_CAP/HelpDesk/ProcRolCat.php" method="POST">
                                 <div class="form-group">
                                   <input type="text" name="idRolModificar" value="<?php print $idRolModal; ?>" readonly hidden>
                                   <label class="p-head"> Nombre de Rol</label>
                                   <input type="text" name="DescRol" value="<?php print $descRolModal; ?>" class="form-control" required>
                                 </div>
                                 <center>
                                   <button type="submit" name="ModificarRol" class="btn btn-default">Modificar Rol</button>
                                 </center>
                               </form>
                             </div>
                           </div>
                         </div>
                       </div>
                       <a href="#myModalRol<?php print $varControRol; ?>" data-toggle="modal" class="btn btn-info">
                          Modificar
                       </a>
                     </th>
                  </tr>
                </tbody>
                <?php
                    }
                ?>
              </table>
              </div>
            </section>
        </section>

          <!-- page end-->
      </section>
  </section>
      <!--main content end-->
      <!--footer start-->
      <div class="site-footer">
        <div class="text-center">
          2015 &copy; Alcaldia Municipal de San Salvador.
        </div>
      </div>
      <!--footer end-->
  </section>

    <?php include '../import_js.php';?>

  </body>
</html>
