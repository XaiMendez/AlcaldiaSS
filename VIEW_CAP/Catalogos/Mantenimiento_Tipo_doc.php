<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Mantenimiento Tipo Documento</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="../assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="../assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <link rel="stylesheet" href="../css/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="../assets/data-tables/DT_bootstrap.css" />
    <!-- Custom styles for this template -->
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/style-responsive.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
            <a class="logo" href="index.html"><img src="../img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <a class="sublogo" href="index.html"><img src="../img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->
      
       <!-- Main -->
          <?php include '../main.php';?>
       <!-- /End Main -->

	    <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              <section class="panel">
                  <header class="panel-heading">
                      Tipo Documento de Compra
                  </header>
                  <div class="panel-body">
                    <div style="width:50%;">
                      <div class="adv-table editable-table-m ">
                          <div class="clearfix">
                              
                                  <button id="editable-sample_new" class="btn green">
                                      Agregar Nuevo <i class="icon-plus"></i>
                                  </button>
                              </div>
                              <div class="btn-group pull-right">
                                  <button class="btn dropdown-toggle" data-toggle="dropdown">Herramientas <i class="icon-angle-down"></i>
                                  </button>
                                  <ul class="dropdown-menu pull-right">
                                      <li><a href="#">Print</a></li>
                                      <li><a href="#">Save as PDF</a></li>
                                      <li><a href="#">Export to Excel</a></li>
                                  </ul>
                              </div>
                          </div>
                          <div class="space5"></div>
                          <table class="table table-striped table-hover  table-bordered" id="editable-sample">
                              <thead>
                              <tr>
                                  
                                  <th>Detalle</th>
                                  <th>Editar</th>
                                  <th>Eliminar</th>
                              </tr>
                              </thead>
                              <tbody>
                              <tr class="">
                                  <td class="center">Credito Fiscal</td>
                                  <td>
                                  <center>
                                    <a class="edit" href="javascript:;">
                                       <button class="btn btn-primary btn-sm btn-block">
                                        <i class="icon-pencil"></i>&nbsp&nbspEditar
                                      </button>
                                    </a>
                                  </td>
                                  <td>
                                  <a class="delete" href="javascript:;">
                                    <button class="btn btn-danger btn-sm btn-block">
                                      <i class="icon-trash"></i>&nbsp&nbspEliminar
                                    </button>
                                  </a>
                                  </center>
                                  </td>
                              </tr>
                              <tr class="">
                                 
                                  <td class="center">Consumidor Final</td>
                                  <td>
                                  <center>
                                    <a class="edit" href="javascript:;">
                                       <button class="btn btn-primary btn-sm btn-block">
                                        <i class="icon-pencil"></i>&nbsp&nbspEditar
                                      </button>
                                    </a>
                                  </td>
                                  <td>
                                  <a class="delete" href="javascript:;">
                                    <button class="btn btn-danger btn-sm btn-block">
                                      <i class="icon-trash"></i>&nbsp&nbspEliminar
                                    </button>
                                  </a>
                                  </center>
                                  </td>
                              </tr>
                              <tr class="">
                                  <td class="center">Ticket</td>
                                   <td>
                                  <center>
                                    <a class="edit" href="javascript:;">
                                       <button class="btn btn-primary btn-sm btn-block">
                                        <i class="icon-pencil"></i>&nbsp&nbspEditar
                                      </button>
                                    </a>
                                  </td>
                                  <td>
                                  <a class="delete" href="javascript:;">
                                    <button class="btn btn-danger btn-sm btn-block">
                                      <i class="icon-trash"></i>&nbsp&nbspEliminar
                                    </button>
                                  </a>
                                  </center>
                                  </td>
                              </tr>                                       
                              </tbody>
                          </table>
                      </div>
                  </div>
              </section>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
    
    
  </section>

     <!-- js placed at the end of the document so the pages load faster -->
    <script src="../js/jquery-1.8.3.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../js/jquery.scrollTo.min.js"></script>
    <script src="../js/jquery.nicescroll.js" type="text/javascript"></script>
    <script type="text/javascript" src="../assets/data-tables/jquery.dataTables.js"></script>
    <script type="text/javascript" src="../assets/data-tables/DT_bootstrap.js"></script>
    <script src="../js/respond.min.js" ></script>

    <!--common script for all pages-->
    <script src="../js/common-scripts.js"></script>

      <!--script for this page only-->
      <script src="../js/editable-table-m.js"></script>

    <!-- SCRIPTS PARA LA DATA TABLE EDITABLE -->
    <script type="text/javascript" src="../assets/data-tables/jquery.dataTables.js"></script>
    <script type="text/javascript" src="../assets/data-tables/DT_bootstrap.js"></script>
    <script src="../js/editable-table-m.js"></script>
    <script>
          jQuery(document).ready(function() {
              EditableTable.init();
          });
    </script>

  <script>

      //owl carousel

      $(document).ready(function() {
          $("#owl-demo").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true,
			  autoPlay:true

          });
      });

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>

  </body>
</html>
