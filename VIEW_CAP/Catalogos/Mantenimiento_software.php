
<!DOCTYPE html>
<html lang="en">
  <?php include '../import_css.php'; ?>

  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
            <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            
            <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->
      
      <!-- Main -->
          <?php include '../main.php';?>
      <!-- /End Main -->

       <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              <section class="panel">
                  <header class="panel-heading">
                      <center><h3>Detalle de Software</h3></center>
                  </header>
                  <div class="panel-body">
                      <center>
                      <a href="#myModalInsertarSoftware" data-toggle="modal" class="btn btn-success">
                                  Insertar Nuevo Software
                      </a> 
                      </center>
                      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalInsertarSoftware" class="modal fade">
                                  <div class="modal-dialog">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                              <h4 class="modal-title">Insertar Nuevo Software</h4>
                                          </div>
                                          <div class="modal-body">
                                              <form action="../../BUSINESS_CAP/AdmonEquipo/ProcSoftwareCat.php" method="POST">
                                                  <div class="form-group">
                                                      <label class=" p-head">Descripción de Software</label>
                                                      <input type="text" name="txtDescSoftwareInsertar" maxlength="100" class="form-control" 
                                                             placeholder="Describa el software" required>
                                                      <br/>
                                                      <label class=" p-head">Licencia de Software</label>
                                                      <input type="text" name="txtLicenciaSoftwareInsertar" maxlength="100" class="form-control" 
                                                             placeholder="Describa la licencia" required>
                                                      <br/>
                                                      <label class=" p-head">Tipo de Software</label>
                                                      <select name="cbxTipoSoftwareInsertar" class="form-control m-bot15">
                                                          <?php
                                                          require ("../../DAO_CAP/AdmonEquipo/mantenimiento_software.php");
                                                          $objSoftware = new MantenimientoSoftware();
                                                          $rsTipoSoftwareIns = $objSoftware->SelectTipoSoftwareParaInsertar();
                                                          while ($tipoSoftVarIns = pg_fetch_array($rsTipoSoftwareIns)) {
                                                              ?>
                                                              <option value="<?php echo $tipoSoftVarIns['id_tipo_software']; ?>"><?php echo $tipoSoftVarIns['tipo_software']; ?></option>  
                                                              <?php
                                                          }
                                                          ?> 
                                                      </select>
                                                      <br/>
                                                      <center><button type="submit" class="btn btn-default" name="InsertarSofware">Insertar Software</button></center>
                                                  </div>
                                              </form>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                      <table class="table">
                          <thead>
                              <tr>
                                  <th> Id Software </th>
                                  <th> Descripción Software </th>
                                  <th> Licencia </th>
                                  <th> Tipo Software </th>
                                  <th> </th>
                              </tr>
                          </thead>
                          <tbody>
                              <tr>
                                  <?php
                                  
                                  $rsSoftwareMan = $objSoftware->SelectSoftware();
                                  $rsRowsSoftwareMan = pg_num_rows($rsSoftwareMan);
                                  //while ($rowEquipoUbicacion = pg_fetch_assoc($rsBitacoraUbicacion)) {
                                  for ($varControSoft = 0; $varControSoft < $rsRowsSoftwareMan; $varControSoft++) {
                                      $idSoftwareModal = pg_fetch_result($rsSoftwareMan, $varControSoft, 'id_software');
                                      $descSoftwareModal = pg_fetch_result($rsSoftwareMan, $varControSoft, 'descripcion_software');
                                      $licenciaModal = pg_fetch_result($rsSoftwareMan, $varControSoft, 'licencia_software');
                                      $idTipoSoftwareActual = pg_fetch_result($rsSoftwareMan, $varControSoft, 'id_tipo_software');
                                      $TipoSoftModal = pg_fetch_result($rsSoftwareMan, $varControSoft, 'tipo_software');
                                      ?>
                                      <th><?php print $idSoftwareModal; ?></th>
                                      <th><?php print $descSoftwareModal; ?></th>
                                      <th><?php print $licenciaModal; ?></th>
                                      <th><?php print $TipoSoftModal; ?></th>
                                      <th>
                                        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalSoftware<?php print $varControSoft; ?>" class="modal fade">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                            <h4 class="modal-title">Detalle de Software</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form action="../../BUSINESS_CAP/AdmonEquipo/ProcSoftwareCat.php" method="POST">
                                                                <div class="form-group">
                                                                    <input type="text" name="idSoftModificar" value="<?php print $idSoftwareModal; ?>"readonly hidden>
                                                                    <label class=" p-head">Descripción de Software</label>
                                                                    <input type="text" name="DescSoftware" value="<?php print $descSoftwareModal; ?>" class="form-control" required>
                                                                    <br/>
                                                                    <label class=" p-head">Licencia de Software</label>
                                                                    <input type="text" name="LicenciaSoftware" value="<?php print $licenciaModal; ?>" class="form-control">
                                                                    <br/>
                                                                    <label class=" p-head">Tipo de Software</label>
                                                                    <select name="cbxTipoSoftwareMan" class="form-control m-bot15">
                                                                        <option value="<?php print $idTipoSoftwareActual; ?>"><?php print $TipoSoftModal; ?></option> 
                                                                      <?php
                                                                        $rsTipoSoftware = $objSoftware->SelectTipoSoftware($idTipoSoftwareActual);
                                                                        while ($tipoSoftVar = pg_fetch_array($rsTipoSoftware)){
                                                                     ?>
                                                                        <option value="<?php echo $tipoSoftVar['id_tipo_software']; ?>"><?php echo $tipoSoftVar['tipo_software']; ?></option>  
                                                                     <?php
                                                                        }
                                                                      ?> 
                                                                        
                                                                    </select>
                                                                </div>
                                                                <center>
                                                                    <button type="submit" name="ModificarSoftware" class="btn btn-default">Modificar Registro</button>
                                                                </center>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#myModalSoftware<?php print $varControSoft; ?>" data-toggle="modal" class="btn btn-info">
                                                Modificar
                                           </a>
                                    </th>
                                  </tr>
                              </tbody>
                              <?php
                          }
                          ?>
                      </table>
                  </div>
              </section>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
    
    
  </section>

    <?php include '../import_js.php'; ?>

  </body>
</html>
