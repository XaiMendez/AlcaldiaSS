<!DOCTYPE html>
<html lang="en">

<!-- head -->
    <?php include '../import_css.php';?>
<!-- /End head -->


  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
            <a class="logo" href="index.html"><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <a class="sublogo" href="index.html"><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->

      <!-- Main -->
          <?php include '../main.php';?>
      <!-- /End Main -->

      <!--Comienza contenido principal-->

      <section id="main-content">
        <section class="wrapper">
            <!-- page start-->
            <section class="panel">
              <header class="panel-heading">
                <center><h3>Codigo Subsistema</h3></center>
              </header>
              <div class="panel-body">
                <center>
                <a href="#myModalInsertarSubsistema" data-toggle="modal" class="btn btn-success">
                  Insertar Nuevo Subsistema
                </a>
                </center>
                <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalInsertarSubsistema" class="modal fade">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                        <h4 class="modal-title">Insertar Nuevo Subsistema</h4>
                    </div>
                    <div class="modal-body">
                      <form action="../../BUSINESS_CAP/HelpDesk/ProcSubsistemaCat.php" method="POST">
                        <div class="form-group">
                          <label class=" p-head">Descripción de Subsistema</label>
                            <input type="text" name="txtDescSubsistemaInsertar" maxlength="100" class="form-control" placeholder="Describa el software" required>
                            <br/>
                            <center><button type="submit" class="btn btn-default" name="InsertarSubsistema">Insertar Subsistema</button></center>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <table class="table">
                <thead>
                  <tr>
                    <th>Id Subsistema</th>
                    <th>Subsistema</th>
                    <th> </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <?php
                    require ("../../DAO_CAP/HelpDesk/model.codigo_subsistema.php");
                    $objSubsistema = new MantenimientoSubsistema();
                    $rsSubsistemaMan = $objSubsistema->SelectSubsistema();
                    $rsRowsSubsistemaMan = pg_num_rows($rsSubsistemaMan);

                    for ($varControSubsistema = 0; $varControSubsistema < $rsRowsSubsistemaMan; $varControSubsistema++){
                        $idSubsistemaModal = pg_fetch_result($rsSubsistemaMan, $varControSubsistema, 'id_codigo_subsistema');
                        $descSubsistemaModal = pg_fetch_result($rsSubsistemaMan, $varControSubsistema, 'descripcion_subsistema');

                    ?>
                    <th><?php print $idSubsistemaModal; ?></th>
                    <th><?php print $descSubsistemaModal; ?></th>
                    <th>
                      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalSubsistema<?php print $varControSubsistema; ?>" class="modal fade">
                         <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                <h4 class="modal-title">Detalle de Subsistema</h4>
                            </div>
                            <div class="modal-body">
                               <form action="../../BUSINESS_CAP/HelpDesk/ProcSubsistemaCat.php" method="POST">
                                 <div class="form-group">
                                   <input type="text" name="idSubsistemaModificar" value="<?php print $idSubsistemaModal; ?>" readonly hidden>
                                   <label class="p-head"> Descripcion de Subsistema</label>
                                   <input type="text" name="DescSubsistema" value="<?php print $descSubsistemaModal; ?>" class="form-control" required>
                                 </div>
                                 <center>
                                   <button type="submit" name="ModificarSubsistema" class="btn btn-default">Modificar Subsistema</button>
                                 </center>
                               </form>
                             </div>
                           </div>
                         </div>
                       </div>
                       <a href="#myModalSubsistema<?php print $varControSubsistema; ?>" data-toggle="modal" class="btn btn-info">
                          Modificar
                       </a>
                     </th>
                  </tr>
                </tbody>
                <?php
                    }
                ?>
              </table>
              </div>
            </section>
        </section>

          <!-- page end-->
      </section>
  </section>
      <!--main content end-->
      <!--footer start-->
      <div class="site-footer">
        <div class="text-center">
          2015 &copy; Alcaldia Municipal de San Salvador.
        </div>
      </div>
      <!--footer end-->
  </section>

    <?php include '../import_js.php';?>

  </body>
</html>
