<!DOCTYPE html>
<html lang="en">
  <?php include '../import_css.php'; ?>

  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
            <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            
            <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->
      
      <!-- Main -->
          <?php include '../main.php';?>
      <!-- /End Main -->

       <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              <section class="panel">
                  <header class="panel-heading">
                      <center><h3>Detalle de Estado Compra</h3></center>
                  </header>
                  <div class="panel-body">
                      <center>
                      <a href="#myModalInsertarEstadoCompra" data-toggle="modal" class="btn btn-success">
                                  Insertar Nuevo Estado de Compra.
                      </a> 
                      </center>
                      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalInsertarEstadoCompra" class="modal fade">
                                  <div class="modal-dialog">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                              <h4 class="modal-title">Insertar Nuevo Estado de Compra.</h4>
                                          </div>
                                          <div class="modal-body">
                                              <form action="../../BUSINESS_CAP/AdmonEquipo/ProceEstadoCompra.php" method="POST">
                                                  <div class="form-group">
                                                      <label class=" p-head">Descripción de Estado</label>
                                                      <input type="text" name="txtDescEstadoComInsertar" maxlength="200" class="form-control" 
                                                             placeholder="Describa el Estado de Compra" required>
                                                      <br/>                                                  
                                                      <center><button type="submit" class="btn btn-default" name="InsertarEstado_Compra">Insertar Estado Compra</button></center>
                                                  </div>
                                              </form>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                      <table class="table">
                          <thead>
                              <tr>
                                  <th> Id Estado Compra </th>
                                  <th> Descripción </th>
                                  <th> </th>
                              </tr>
                          </thead>
                          <tbody>
                              <tr>
                                <?php
                                 require ("../../DAO_CAP/AdmonEquipo/Mantenimiento_Estado_Compra.php");
                                  $objEstado = new MantenimientoEstado();
                                  $rsEstadoCoMan = $objEstado->SelectEstado();
                                  $rsRowsEstadoMan = pg_num_rows($rsEstadoCoMan);
                                
                                  for ($varCompra = 0; $varCompra < $rsRowsEstadoMan; $varCompra++) {
                                      $idEstadoModal = pg_fetch_result($rsEstadoCoMan, $varCompra, 'id_estado_compra');
                                      $descripcionModal = pg_fetch_result($rsEstadoCoMan, $varCompra, 'descripcion');
                                      ?>
                                      <th><?php print $idEstadoModal; ?></th>
                                      <th><?php print $descripcionModal; ?></th>
                                      <th>
                                        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalEstado<?php print $varCompra; ?>" class="modal fade">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                            <h4 class="modal-title">Detalle de Estado Compra</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form action="../../BUSINESS_CAP/AdmonEquipo/ProceEstadoCompra.php" method="POST">
                                                                <div class="form-group">
                                                                    <input type="text" name="idEstadoComModificar" value="<?php print $idEstadoModal; ?>"readonly hidden>
                                                                    <label class=" p-head">Descripción</label>
                                                                    <input type="text" name="Descripcion" value="<?php print $descripcionModal; ?>" class="form-control" required>
                                                                    <br/>
                                                                     </select>
                                                                </div>
                                                                <center>
                                                                    <button type="submit" name="ModificarEstado_Compra" class="btn btn-default">Modificar Registro</button>
                                                                </center>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#myModalEstado<?php print $varCompra; ?>" data-toggle="modal" class="btn btn-info">
                                                Modificar
                                           </a>
                                    </th>
                                  </tr>
                              </tbody>
                              <?php
                          }
                          ?>
                      </table>
                  </div>
              </section>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
    
    
  </section>

    <?php include '../import_js.php'; ?>

  </body>
</html>
