<!DOCTYPE html>
<html lang="en">
  <?php include '../import_css.php'; ?>

  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
            <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            
            <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->
      
      <!-- Main -->
          <?php include '../main.php';?>
      <!-- /End Main -->

       <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              <section class="panel">
                  <header class="panel-heading">
                      <center><h3>Detalle de M2 Tipo Equipo</h3></center>
                  </header>
                  <div class="panel-body">
                      <center>
                      <a href="#myModalInsertarM2TipoEquipo" data-toggle="modal" class="btn btn-success">
                                  Insertar Nuevo Estado de Compra.
                      </a> 
                      </center>
                      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalInsertarM2TipoEquipo" class="modal fade">
                                  <div class="modal-dialog">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                              <h4 class="modal-title">Insertar Nuevo M2 Tipo Equipo.</h4>
                                          </div>
                                          <div class="modal-body">
                                              <form action="../../BUSINESS_CAP/AdmonEquipo/ProcM2_Tipo_Equipo.php" method="POST">
                                                  <div class="form-group">
                                                      <label class=" p-head">Descripción de M2 Tipo Equipo</label>
                                                      <input type="text" name="txtM2tipoEquipoInsertar" maxlength="50" class="form-control" 
                                                             placeholder="Describa el M2 Tipo Equipo" required>
                                                      <br/>                                                  
                                                      <center><button type="submit" class="btn btn-default" name="Insertarm2_tipo_equipo">Insertar M2 Tipo Equipo</button></center>
                                                  </div>
                                              </form>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                      <table class="table">
                          <thead>
                              <tr>
                                  <th> Id M2 Tipo Equipo </th>
                                  <th> M2 Tipo Equipo </th>
                                  <th> </th>
                              </tr>
                               </thead>
                          <tbody>
                              <tr>
                                <?php
                                 require ("../../DAO_CAP/AdmonEquipo/Mantenimiento_M2_Tipo_Equipo.php");
                                  $objM2Tipo = new Mantenimiento_M2_tipo_Equipo();
                                  $rsM2EquipoMan = $objM2Tipo->SelectM2_Tipo_Equipo();
                                  $rsRowsM2Man = pg_num_rows($rsM2EquipoMan);
                                
                                  for ($varM2TipoEquipo = 0; $varM2TipoEquipo < $rsRowsM2Man; $varM2TipoEquipo++) {
                                      $idm2TipoEquipoModal = pg_fetch_result($rsM2EquipoMan, $varM2TipoEquipo, 'id_m2_tipo_equipo');
                                      $m2TipoEquipoModal = pg_fetch_result($rsM2EquipoMan, $varM2TipoEquipo, 'm2_tipo_equipo');
                                      ?>
                                      <th><?php print  $idm2TipoEquipoModal; ?></th>
                                      <th><?php print $m2TipoEquipoModal; ?></th>
                                      <th>
                                        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalM2<?php print $varM2TipoEquipo; ?>" class="modal fade">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                            <h4 class="modal-title">Detalle de M2 Tipo Equipo</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form action="../../BUSINESS_CAP/AdmonEquipo/ProcM2_Tipo_Equipo.php" method="POST">
                                                                <div class="form-group">
                                                                    <input type="text" name="idM2_tipoEquipoModificar" value="<?php print $idm2TipoEquipoModal; ?>"readonly hidden>
                                                                    <label class=" p-head">M2 Tipo Equipo</label>
                                                                    <input type="text" name="M2_tipoEquipo" value="<?php print $m2TipoEquipoModal; ?>" class="form-control" required>
                                                                    <br/>
                                                                     </select>
                                                                </div>
                                                                <center>
                                                                    <button type="submit" name="Modificarm2_tipo_equipo" class="btn btn-default">Modificar Registro</button>
                                                                </center>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#myModalM2<?php print $varM2TipoEquipo; ?>" data-toggle="modal" class="btn btn-info">
                                                Modificar
                                           </a>
                                    </th>
                                  </tr>
                              </tbody>
                              <?php
                          }
                          ?>
                      </table>
                  </div>
              </section>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
    
    
  </section>

    <?php include '../import_js.php'; ?>

  </body>
</html>
