<!DOCTYPE html>
<html lang="en">
  <?php include '../import_css.php'; ?>

  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
            <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            
            <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->
      
      <!-- Main -->
          <?php include '../main.php';?>
      <!-- /End Main -->

       <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              <section class="panel">
                  <header class="panel-heading">
                      <center><h3>Detalle Unidad de Almacenamiento</h3></center>
                  </header>
                  <div class="panel-body">
                      <center>
                      <a href="#myModalInsertarUnidad" data-toggle="modal" class="btn btn-success">
                                  Insertar Nuevo Unidad de Almacenamiento.
                      </a> 
                      </center>
                      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalInsertarUnidad" class="modal fade">
                                  <div class="modal-dialog">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                              <h4 class="modal-title">Insertar Nueva Unidad de Almacenamiento.</h4>
                                          </div>
                                          <div class="modal-body">
                                              <form action="../../BUSINESS_CAP/AdmonEquipo/ProcUnidadAlmacenamiento.php" method="POST">
                                                  <div class="form-group">
                                                      <label class=" p-head">Code Unidad</label>
                                                      <input type="text" name="txtcodeUnidadInsertar" maxlength="40" class="form-control" 
                                                             placeholder="Describa el Code Almacenamiento" required>
                                                      <br/>    
                                                      <label class="p-head">Descripcion Unidad</label> 
                                                      <input type="text" name="txtdescUnidadInsertar" maxlength="40" class="form-control"
                                                              placeholder="Describa la Unidad" required>   
                                                              </br>                                          
                                                      <center><button type="submit" class="btn btn-default" name="InsertarUnidad_Almacenamiento">Insertar Unidad de Almacenamiento</button></center>
                                                  </div>
                                              </form>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                      <table class="table">
                          <thead>
                              <tr>
                                  <th> ID UNIDAD ALMACENAMIENTO </th>
                                  <th> CODE UNIDAD </th>
                                  <th> DESCRIPCION DE UNIDAD</th>
                                  <th> </th>
                              </tr>
                          </thead>
                           <tbody>
                              <tr>
                                <?php
                                 require ("../../DAO_CAP/AdmonEquipo/Mantenimiento_Unidad_Almacenamiento.php");
                                  $objUnidad = new MantenimientoUnidad_Almacenamiento();
                                  $rsUnidadMan = $objUnidad->SelectUnidad();
                                  $rsRowsUnidadMan = pg_num_rows($rsUnidadMan);
                                
                                  for ($varUnidad = 0; $varUnidad < $rsRowsUnidadMan; $varUnidad++) {
                                      $idUnidadAlmModal = pg_fetch_result($rsUnidadMan, $varUnidad, 'id_unidad_almacenamiento');
                                      $codeUnidadModal = pg_fetch_result($rsUnidadMan, $varUnidad, 'code_unidad');
                                      $descUnidadModal = pg_fetch_result($rsUnidadMan, $varUnidad, 'descripcion_unidad');
                                      ?>
                                      <th><?php print $idUnidadAlmModal; ?></th>
                                      <th><?php print $codeUnidadModal; ?></th>
                                      <th><?php print $descUnidadModal; ?></th>
                                      <th>
                                        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalUnidad<?php print $varUnidad; ?>" class="modal fade">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                            <h4 class="modal-title">Detalle de Unidad Almacenamiento</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form action="../../BUSINESS_CAP/AdmonEquipo/ProcUnidadAlmacenamiento.php" method="POST">
                                                                <div class="form-group">
                                                                    <input type="text" name="idUnidadModificar" value="<?php print $idUnidadAlmModal; ?>"readonly hidden>
                                                                    <label class=" p-head">Code Unidad</label>
                                                                    <input type="text" name="codeUnidad" value="<?php print $codeUnidadModal; ?>" class="form-control" required>
                                                                    <br/>
                                                                     <label class=" p-head">Descripcion Unidad</label>
                                                                     <input type="text" name="descUnidad" value="<?php print $descUnidadModal; ?>" class="form-control" required>
                                                                    <br/>
                                                                     </select>
                                                                </div>
                                                                <center>
                                                                    <button type="submit" name="ModificarUnidad_Almacenamiento" class="btn btn-default">Modificar Registro</button>
                                                                </center>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#myModalUnidad<?php print $varUnidad; ?>" data-toggle="modal" class="btn btn-info">
                                                Modificar
                                           </a>
                                    </th>
                                  </tr>
                              </tbody>
                              <?php
                          }
                          ?>
                      </table>
                  </div>
              </section>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
    
    
  </section>

    <?php include '../import_js.php'; ?>

  </body>
</html>
