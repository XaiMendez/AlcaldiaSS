
<!DOCTYPE html>
<html lang="en">
  <?php include '../import_css.php'; ?>

  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
            <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            
            <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->
      
      <!-- Main -->
          <?php include '../main.php';?>
      <!-- /End Main -->

       <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              <section class="panel">
                  <header class="panel-heading">
                      <center><h3>Detalle de Marca Equipo</h3></center>
                  </header>
                  <div class="panel-body">
                      <center>
                      <a href="#myModalInsertarMarcaEquipo" data-toggle="modal" class="btn btn-success">
                                  Insertar Nueva Marca Equipo
                      </a> 
                      </center>
                      <div aria-hidden="true" aria-labelledby="myModalLabel1" role="dialog" tabindex="-1" id="myModalInsertarMarcaEquipo" class="modal fade">
                                  <div class="modal-dialog">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                              <h4 class="modal-title">Insertar Nueva Marca Equipo</h4>
                                          </div>
                                          <div class="modal-body">
                                              <form action="../../BUSINESS_CAP/AdmonEquipo/ProcMarcaEquipo.php" method="POST">
                                                  <div class="form-group">
                                                      <label class=" p-head">Marca Equipo</label>
                                                      <input type="text" name="txtMarcaEquipoInsertar" maxlength="50" class="form-control" 
                                                             placeholder="Describa la Marca Equipo" required>                                                    
                                                   <br/>
                                                      <label class=" p-head">M Tipo Equipo</label>
                                                      <select name="cbxEquipoInsertar" class="form-control m-bot15">
                                                          <?php
                                                          require ("../../DAO_CAP/AdmonEquipo/Mantenimiento_Marca_Equipo.php");
                                                          $objmarcaEquipo = new MantenimientoMarcaEquipo();
                                                          $rsMarcaEquipoIns = $objmarcaEquipo->SelectMarcaEquipoParaInsertar();
                                                          while ($marcaEqVarIns = pg_fetch_array( $rsMarcaEquipoIns)) {
                                                              ?>
                                                              <option value="<?php echo $marcaEqVarIns['id_m_tipo_equipo']; ?>"><?php echo $marcaEqVarIns['m_tipo_equipo']; ?></option>  
                                                              <?php
                                                          }
                                                          ?> 
                                                      </select>
                                                      <br/>
                                                      <center><button type="submit" class="btn btn-default" name="InsertarMarcaEquipo">Insertar Marca Equipo</button></center>
                                                  </div>
                                              </form>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                 
                      <table class="table">
                          <thead>
                              <tr>
                                  <th> Id Marca Equipo </th>
                                  <th> Marca Equipo </th>
                                  <th> Maestro Tipo Equipo </th>
                                  <th> </th>
                              </tr>
                          </thead>
                          <tbody>
                             <tr>
                                  <?php
                                  
                                  $rsMarEquipoMan = $objmarcaEquipo->SelectMarca_Equipo();            
                                  $rsRowsmarcaEquipoMan = pg_num_rows($rsMarEquipoMan);
                                  //while ($rowEquipoUbicacion = pg_fetch_assoc($rsBitacoraUbicacion)) {
                                  for ($varMarcaEq = 0; $varMarcaEq < $rsRowsmarcaEquipoMan; $varMarcaEq++) {
                                      $idMarcaEquipoModal = pg_fetch_result($rsMarEquipoMan, $varMarcaEq, 'id_marca_equipo');
                                      $marcaEquipoModal = pg_fetch_result($rsMarEquipoMan, $varMarcaEq, 'marca_equipo');
                                      $idmTipoEquipoActual = pg_fetch_result($rsMarEquipoMan, $varMarcaEq, 'id_m_tipo_equipo');
                                      $mTipoEquipoModal = pg_fetch_result($rsMarEquipoMan, $varMarcaEq, 'm_tipo_equipo');
                                      ?>
                                      <th><?php print $idMarcaEquipoModal; ?></th>
                                      <th><?php print $marcaEquipoModal; ?></th>                                     
                                      <th><?php print $mTipoEquipoModal; ?></th>
                                      <th>
                                         <div aria-hidden="true" aria-labelledby="myModalLabel1" role="dialog" tabindex="-1" id="myModalMarca<?php print $varMarcaEq; ?>" class="modal fade">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                            <h4 class="modal-title">Detalle Marca Equipo</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form action="../../BUSINESS_CAP/AdmonEquipo/ProcMarcaEquipo.php" method="POST">
                                                                <div class="form-group">
                                                                    <input type="text" name="idmarcaEquipoModificar" value="<?php print $idMarcaEquipoModal; ?>"readonly hidden>
                                                                    <label class=" p-head">Marca Equipo</label>
                                                                    <input type="text" name="MarcaEquipo" value="<?php print $marcaEquipoModal; ?>" class="form-control" required>
                                                                    <br/>      
                                                                    <label class=" p-head">M Tipo de Equipo</label>
                                                                    <select name="cbxMarcaEquipoMan" class="form-control m-bot15">
                                                                        <option value="<?php print $idmTipoEquipoActual; ?>"><?php print $mTipoEquipoModal; ?></option> 
                                                                      <?php
                                                                        $rsMarEquipo = $objmarcaEquipo->SelectMarEquipo($idmTipoEquipoActual);
                                                                        while ($MarcaEqVar = pg_fetch_array($rsMarEquipo)){
                                                                     ?>
                                                                        <option value="<?php echo $MarcaEqVar['id_m_tipo_equipo']; ?>"><?php echo $MarcaEqVar['m_tipo_equipo']; ?></option>  
                                                                     <?php
                                                                        }
                                                                      ?> 
                                                                        
                                                                    </select>
                                                                </div>
                                                                <center>
                                                                    <button type="submit" name="ModificarMarcaEquipo" class="btn btn-default">Modificar Registro</button>
                                                                </center>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#myModalMarca<?php print $varMarcaEq; ?>" data-toggle="modal" class="btn btn-info">
                                                Modificar
                                           </a>
                                    </th>
                                  </tr>
                              </tbody>
                              <?php
                          }
                          ?>
                      </table>
                  </div>
              </section>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
    
    
  </section>

    <?php include '../import_js.php'; ?>

  </body>
</html>
