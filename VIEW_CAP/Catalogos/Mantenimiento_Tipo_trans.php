<!DOCTYPE html>
<html lang="en">
  <?php include '../import_css.php'; ?>

  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
            <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            
            <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->
      
      <!-- Main -->

          <?php include '../main.php';?>
      <!-- /End Main -->

       <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              <section class="panel">
                  <header class="panel-heading">
                      <center><h3>Detalle Tipo de Transaccion</h3></center>
                  </header>
                  <div class="panel-body">
                      <center>
                      <a href="#myModalInsertarTipoTransaccion" data-toggle="modal" class="btn btn-success">
                                  Insertar Nuevo Tipo de Transaccion.
                      </a> 
                      </center>
                      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalInsertarTipoTransaccion" class="modal fade">
                                  <div class="modal-dialog">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                              <h4 class="modal-title">Insertar Nuevo Tipo de Transaccion.</h4>
                                          </div>
                                          <div class="modal-body">
                                              <form action="../../BUSINESS_CAP/AdmonEquipo/ProcTipoTransaccion.php" method="POST">
                                                  <div class="form-group">
                                                      <label class=" p-head">Descripción de Tipo Transaccion</label>
                                                      <input type="text" name="txtTipoTransaccionInsertar" maxlength="50" class="form-control" 
                                                             placeholder="Describa el Tipo de Transaccion" required>
                                                      <br/>                                                  
                                                      <center><button type="submit" class="btn btn-default" name="InsertarTipo_Transaccion">Insertar Tipo Transaccion</button></center>
                                                  </div>
                                              </form>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                      <table class="table">
                          <thead>
                              <tr>
                                  <th> Id Tipo Transaccion </th>
                                  <th> Tipo Transaccion </th>
                                  <th> </th>
                              </tr>
                          </thead>
                          <tbody>
                              <tr>
                                <?php
                                 require ("../../DAO_CAP/AdmonEquipo/Mantenimiento_Tipo_Transaccion.php");
                                  $objTransaccion = new MantenimientoTipoTransaccion();
                                  $rsTipoTransMan = $objTransaccion->SelectTipo_Transaccion();
                                  $rsRowsTipoTransMan = pg_num_rows($rsTipoTransMan);
                                
                                  for ($varTipoTrans = 0; $varTipoTrans < $rsRowsTipoTransMan; $varTipoTrans++) {
                                      $idTipoTransaccionModal = pg_fetch_result($rsTipoTransMan, $varTipoTrans, 'id_tipo_transaccion');
                                      $TipoTransaccionModal = pg_fetch_result($rsTipoTransMan, $varTipoTrans, 'tipo_transaccion');
                                      ?>
                                      <th><?php print  $idTipoTransaccionModal; ?></th>
                                      <th><?php print  $TipoTransaccionModal; ?></th>
                                      <th>
                                        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalTrans<?php print $varTipoTrans; ?>" class="modal fade">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                            <h4 class="modal-title">Detalle de Tipo Compra</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form action="../../BUSINESS_CAP/AdmonEquipo/ProcTipoTransaccion.php" method="POST">
                                                                <div class="form-group">
                                                                    <input type="text" name="idTipoTransaccionModificar" value="<?php print  $idTipoTransaccionModal; ?>"readonly hidden>
                                                                    <label class=" p-head">Descripción</label>
                                                                    <input type="text" name="Tipo_Transaccion" value="<?php print  $TipoTransaccionModal; ?>" class="form-control" required>
                                                                    <br/>
                                                                     </select>
                                                                </div>
                                                                <center>
                                                                    <button type="submit" name="ModificarTipo_Transaccion" class="btn btn-default">Modificar Registro</button>
                                                                </center>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#myModalTrans<?php print $varTipoTrans; ?>" data-toggle="modal" class="btn btn-info">
                                                Modificar
                                           </a>
                                    </th>
                                  </tr>
                              </tbody>
                              <?php
                          }
                          ?>
                      </table>
                  </div>
              </section>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
    
    
  </section>

    <?php include '../import_js.php'; ?>

  </body>
</html>
