<!DOCTYPE html>
<html lang="en">
  <?php include '../import_css.php'; ?>

  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
            <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            
            <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->
      
      <!-- Main -->
          <?php include '../main.php';?>
      <!-- /End Main -->

       <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              <section class="panel">
                  <header class="panel-heading">
                      <center><h3>Tipo de Adquisicion</h3></center>
                  </header>
                  <div class="panel-body">
                      <center>
                      <a href="#myModalInsertarAdquisicion" data-toggle="modal" class="btn btn-success">
                                  Insertar Nuevo Tipo Adquisicion.
                      </a> 
                      </center>
                      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalInsertarAdquisicion" class="modal fade">
                                  <div class="modal-dialog">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                              <h4 class="modal-title">Insertar Nuevo Tipo Adquisicion.</h4>
                                          </div>
                                          <div class="modal-body">
                                              <form action="../../BUSINESS_CAP/AdmonEquipo/ProcTipoAdquisicion.php" method="POST">
                                                  <div class="form-group">
                                                      <label class=" p-head">Tipo Adquisicion</label>
                                                      <input type="text" name="txtTipoAdquisicionInsertar" maxlength="50" class="form-control" 
                                                             placeholder="Describa el Tipo Adquisicion" required>
                                                      <br/>                                                  
                                                      <center><button type="submit" class="btn btn-default" name="InsertarTipo_Adquisicion">Insertar Tipo Adquisicion</button></center>
                                                  </div>
                                              </form>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                      <table class="table">
                          <thead>
                              <tr>
                                  <th> ID TIPO ADQUISICION </th>
                                  <th> TIPO ADQUISICION </th>
                                  <th> </th>
                              </tr>
                          </thead>
                          <tbody>
                              <tr>
                                <?php
                                 require ("../../DAO_CAP/AdmonEquipo/Mantenimiento_Tipo_Adquisicion.php");
                                  $objTipoAdq = new Mantenimientotipo_Adquisicion();
                                  $rsAdquisicionMan = $objTipoAdq->SelectTipo_Adquisicion();
                                  $rsRowsTipoAdqMan = pg_num_rows($rsAdquisicionMan);
                                
                                  for ($varTipoAdq = 0; $varTipoAdq < $rsRowsTipoAdqMan; $varTipoAdq++) {
                                      $idTipoAdqModal = pg_fetch_result($rsAdquisicionMan, $varTipoAdq, 'id_tipo_adquisicion');
                                      $tipoAdqModal = pg_fetch_result($rsAdquisicionMan, $varTipoAdq, 'tipo_adquisicion');
                                      ?>
                                      <th><?php print $idTipoAdqModal; ?></th>
                                      <th><?php print $tipoAdqModal; ?></th>
                                      <th>
                                        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalAdquisicion<?php print $varTipoAdq; ?>" class="modal fade">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                            <h4 class="modal-title">Tipo de Adquisicion</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form action="../../BUSINESS_CAP/AdmonEquipo/ProcTipoAdquisicion.php" method="POST">
                                                                <div class="form-group">
                                                                    <input type="text" name="idTipoAdquisicionModificar" value="<?php print $idTipoAdqModal; ?>"readonly hidden>
                                                                    <label class=" p-head">Tipo de Adquisicion</label>
                                                                    <input type="text" name="tipoAdquisicion" value="<?php print $tipoAdqModal; ?>" class="form-control" required>
                                                                    <br/>
                                                                     </select>
                                                                </div>
                                                                <center>
                                                                    <button type="submit" name="ModificarTipo_Adquisicion" class="btn btn-default">Modificar Registro</button>
                                                                </center>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#myModalAdquisicion<?php print $varTipoAdq; ?>" data-toggle="modal" class="btn btn-info">
                                                Modificar
                                           </a>
                                    </th>
                                  </tr>
                              </tbody>
                              <?php
                          }
                          ?>
                      </table>
                  </div>
              </section>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
    
    
  </section>

    <?php include '../import_js.php'; ?>

  </body>
</html>
