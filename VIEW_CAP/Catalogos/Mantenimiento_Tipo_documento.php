<!DOCTYPE html>
<html lang="en">
  <?php include '../import_css.php'; ?>

  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
            <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            
            <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->
      
      <!-- Main -->
          <?php include '../main.php';?>
      <!-- /End Main -->

       <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              <section class="panel">
                  <header class="panel-heading">
                      <center><h3>Detalle de Tipo Documento de Compra</h3></center>
                  </header>
                  <div class="panel-body">
                      <center>
                      <a href="#myModalInsertarDocCompra" data-toggle="modal" class="btn btn-success">
                                  Insertar Nuevo Tipo Documento de Compra.
                      </a> 
                      </center>
                      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalInsertarDocCompra" class="modal fade">
                                  <div class="modal-dialog">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                              <h4 class="modal-title">Insertar Nuevo Tipo Documento de Compra.</h4>
                                          </div>
                                          <div class="modal-body">
                                              <form action="../../BUSINESS_CAP/AdmonEquipo/ProcTipoDocCompra.php" method="POST">
                                                  <div class="form-group">
                                                      <label class=" p-head">Tipo Documento Compra</label>
                                                      <input type="text" name="txttipoDocComManInsert" maxlength="60" class="form-control" 
                                                             placeholder="Detalle el Tipo de documento" required>
                                                      <br/>                                                  
                                                      <center><button type="submit" class="btn btn-default" name="InsertarTipo_Doc_Compra">Insertar Tipo Documento de Compra</button></center>
                                                  </div>
                                              </form>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                      <table class="table">
                          <thead>
                              <tr>
                                  <th> Id Tipo Documento </th>
                                  <th> Tipo Documento </th>
                                  <th> </th>
                              </tr>
                          </thead>
                          <tbody>
                              <tr>
                                <?php
                                 require ("../../DAO_CAP/AdmonEquipo/Mantenimiento_Tipo_Documento.php");
                                  $objDocumento = new MantenimientoDocumento();
                                  $rsDocumentoCoMan = $objDocumento->SelectDocumento();
                                  $rsRowsDocumentoMan = pg_num_rows($rsDocumentoCoMan);
                                
                                  for ($varTipo = 0; $varTipo < $rsRowsDocumentoMan; $varTipo++) {
                                      $idDocumentoModal = pg_fetch_result($rsDocumentoCoMan, $varTipo, 'id_tipo_documento');
                                      $documentoModal = pg_fetch_result($rsDocumentoCoMan, $varTipo, 'tipo_documento');
                                      ?>
                                      <th><?php print $idDocumentoModal; ?></th>
                                      <th><?php print $documentoModal; ?></th>
                                      <th>
                                        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalTipo<?php print $varTipo; ?>" class="modal fade">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                            <h4 class="modal-title">Detalle de Estado Compra</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form action="../../BUSINESS_CAP/AdmonEquipo/ProcTipoDocCompra.php" method="POST">
                                                                <div class="form-group">
                                                                    <input type="text" name="idDocCompraModificar" value="<?php print $idDocumentoModal; ?>"readonly hidden>
                                                                    <label class=" p-head">Descripción</label>
                                                                    <input type="text" name="Tipo_Documento" value="<?php print  $documentoModal; ?>" class="form-control" required>
                                                                    <br/>
                                                                     </select>
                                                                </div>
                                                                <center>
                                                                    <button type="submit" name="ModificarTipo_Doc_Compra" class="btn btn-default">Modificar Registro</button>
                                                                </center>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#myModalTipo<?php print $varTipo; ?>" data-toggle="modal" class="btn btn-info">
                                                Modificar
                                           </a>
                                    </th>
                                  </tr>
                              </tbody>
                              <?php
                          }
                          ?>
                      </table>
                  </div>
              </section>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
    
    
  </section>

    <?php include '../import_js.php'; ?>

  </body>
</html>
