<!DOCTYPE html>
<html lang="en">
  <?php include '../import_css.php'; ?>

  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
            <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            
            <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->
      
      <!-- Main -->
          <?php include '../main.php';?>
      <!-- /End Main -->

       <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              <section class="panel">
                  <header class="panel-heading">
                      <center><h3>Detalle de Estado Equipo</h3></center>
                  </header>
                  <div class="panel-body">
                      <center>
                      <a href="#myModalInsertarEstadoEquipo" data-toggle="modal" class="btn btn-success">
                                  Insertar Nuevo Estado de Equipo.
                      </a> 
                      </center>
                      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalInsertarEstadoEquipo" class="modal fade">
                                  <div class="modal-dialog">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                              <h4 class="modal-title">Insertar Nuevo Estado de Equipo.</h4>
                                          </div>
                                          <div class="modal-body">
                                              <form action="../../BUSINESS_CAP/AdmonEquipo/ProcEstadoEquipo.php" method="POST">
                                                  <div class="form-group">
                                                      <label class=" p-head">Estado Equipo</label>
                                                      <input type="text" name="txtEstadoEquipoInsertar" maxlength="40" class="form-control" 
                                                             placeholder="Describa el Estado de Equipo" required>
                                                      <br/>                                                  
                                                      <center><button type="submit" class="btn btn-default" name="InsertarEstado_Equipo">Insertar Estado Equipo</button></center>
                                                  </div>
                                              </form>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                      <table class="table">
                          <thead>
                              <tr>
                                  <th> Id Estado Equipo </th>
                                  <th> Estado Equipo </th>
                                  <th> </th>
                              </tr>
                          </thead>
                          <tbody>
                              <tr>
                                <?php
                                 require ("../../DAO_CAP/AdmonEquipo/Mantenimiento_Estado_Equipo.php");
                                  $objEstadoEquipo = new MantenimientoEstadoEquipo();
                                  $rsEstadoEqMan = $objEstadoEquipo->SelectEstado_Equipo();
                                  $rsRowsEstadoEquipoMan = pg_num_rows($rsEstadoEqMan);
                                
                                  for ($varEquipo = 0; $varEquipo < $rsRowsEstadoEquipoMan; $varEquipo++) {
                                      $idEstadoEquipoModal = pg_fetch_result($rsEstadoEqMan, $varEquipo, 'id_estado');
                                      $estadoEquipoModal = pg_fetch_result($rsEstadoEqMan, $varEquipo, 'estado_equipo');
                                      ?>
                                      <th><?php print $idEstadoEquipoModal; ?></th>
                                      <th><?php print $estadoEquipoModal; ?></th>
                                      <th>
                                        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalEquipo<?php print $varEquipo; ?>" class="modal fade">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                            <h4 class="modal-title">Detalle de Estado Equipo</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form action="../../BUSINESS_CAP/AdmonEquipo/ProcEstadoEquipo.php" method="POST">
                                                                <div class="form-group">
                                                                    <input type="text" name="idEstadoEquipoModificar" value="<?php print $idEstadoEquipoModal; ?>"readonly hidden>
                                                                    <label class=" p-head">Estado Equipo</label>
                                                                    <input type="text" name="EstadoEquipo" value="<?php print $estadoEquipoModal; ?>" class="form-control" required>
                                                                    <br/>
                                                                     </select>
                                                                </div>
                                                                <center>
                                                                    <button type="submit" name="ModificarEstado_Equipo" class="btn btn-default">Modificar Registro</button>
                                                                </center>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#myModalEquipo<?php print $varEquipo; ?>" data-toggle="modal" class="btn btn-info">
                                                Modificar
                                           </a>
                                    </th>
                                  </tr>
                              </tbody>
                              <?php
                          }
                          ?>
                      </table>
                  </div>
              </section>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
    
    
  </section>

    <?php include '../import_js.php'; ?>

  </body>
</html>
