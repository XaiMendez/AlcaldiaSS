<!DOCTYPE html>
<html lang="en">
  <?php include '../import_css.php'; ?>

  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
            <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            
            <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->
      
      <!-- Main -->
          <?php include '../main.php';?>
      <!-- /End Main -->

       <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              <section class="panel">
                  <header class="panel-heading">
                      <center><h3>Detalle de Tipo de Software</h3></center>
                  </header>
                  <div class="panel-body">
                      <center>
                      <a href="#myModalInsertarTipoSoftware" data-toggle="modal" class="btn btn-success">
                                  Insertar Nuevo Estado de Compra.
                      </a> 
                      </center>
                      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalInsertarTipoSoftware" class="modal fade">
                                  <div class="modal-dialog">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                              <h4 class="modal-title">Insertar Nuevo Tipo de Software.</h4>
                                          </div>
                                          <div class="modal-body">
                                              <form action="../../BUSINESS_CAP/AdmonEquipo/ProcTipoSoftware.php" method="POST">
                                                  <div class="form-group">
                                                      <label class=" p-head">Tipo Software</label>
                                                      <input type="text" name="txttipoSoftwareInsertar" maxlength="45" class="form-control" 
                                                             placeholder="Describa el Tipo de Software" required>
                                                      <br/>                                                  
                                                      <center><button type="submit" class="btn btn-default" name="InsertarTipo_Software">Insertar Tipo Software</button></center>
                                                  </div>
                                              </form>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                      <table class="table">
                          <thead>
                              <tr>
                                  <th> Id Tipo Software </th>
                                  <th> Tipo Software </th>
                                  <th> </th>
                              </tr>
                          </thead>
                           <tbody>
                              <tr>
                                <?php
                                 require ("../../DAO_CAP/AdmonEquipo/Mantenimiento_Tipo_Software.php");
                                  $objTiposoft = new MantenimientoTipo_Software();
                                  $rsTipoSoftMan = $objTiposoft->SelectTipo_Software();
                                  $rsRowsTipoSoftMan = pg_num_rows($rsTipoSoftMan);
                                
                                  for ($varTipoSoft = 0; $varTipoSoft < $rsRowsTipoSoftMan; $varTipoSoft++) {
                                      $idTipoSoftwareModal = pg_fetch_result($rsTipoSoftMan, $varTipoSoft, 'id_tipo_software');
                                      $tipoSoftwareModal = pg_fetch_result($rsTipoSoftMan, $varTipoSoft, 'tipo_software');
                                      ?>
                                      <th><?php print $idTipoSoftwareModal; ?></th>
                                      <th><?php print $tipoSoftwareModal; ?></th>
                                      <th>
                                        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModalTipoSoft<?php print $varTipoSoft; ?>" class="modal fade">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                            <h4 class="modal-title">Detalle de Tipo Software</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form action="../../BUSINESS_CAP/AdmonEquipo/ProcTipoSoftware.php" method="POST">
                                                                <div class="form-group">
                                                                    <input type="text" name="idTipoSoftModificar" value="<?php print $idTipoSoftwareModal; ?>"readonly hidden>
                                                                    <label class=" p-head">Tipo de Software</label>
                                                                    <input type="text" name="Tipo_Software" value="<?php print $tipoSoftwareModal; ?>" class="form-control" required>
                                                                    <br/>
                                                                     </select>
                                                                </div>
                                                                <center>
                                                                    <button type="submit" name="ModificarTipo_Software" class="btn btn-default">Modificar Registro</button>
                                                                </center>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#myModalTipoSoft<?php print $varTipoSoft; ?>" data-toggle="modal" class="btn btn-info">
                                                Modificar
                                           </a>
                                    </th>
                                  </tr>
                              </tbody>
                              <?php
                          }
                          ?>
                      </table>
                  </div>
              </section>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
    
    
  </section>

    <?php include '../import_js.php'; ?>

  </body>
</html>
