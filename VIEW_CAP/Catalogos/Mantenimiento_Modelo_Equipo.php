
<!DOCTYPE html>
<html lang="en">
  <?php include '../import_css.php'; ?>

  <body>

  <section id="container" >
      <!--Comienza el Header-->
      <div class="header white-bg">
            <!--Inicio del Logo-->
            <div class="header">
            <a class="logo" href=""><img src="../../Resources/img/logo.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            <a class="sublogo" href=""><img src="../../Resources/img/alcaldia.png" alt="AlcaldiaSS" title="AlcaldiaSS"/></a>
            
            <?php include '../Session.php' ?>
            </div>
            <!--Finaliza logo-->
        </div>
      <!--header end-->
      
      <!-- Main -->
          <?php include '../main.php';?>
      <!-- /End Main -->

       <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              <section class="panel">
                  <header class="panel-heading">
                      <center><h3>Detalle de Modelo Equipo</h3></center>
                  </header>
                  <div class="panel-body">
                      <center>
                      <a href="#myModalInsertarModeloEquipo" data-toggle="modal" class="btn btn-success">
                                  Insertar Nuevo Modelo Equipo
                      </a> 
                      </center>
                      <div aria-hidden="true" aria-labelledby="myModalLabel1" role="dialog" tabindex="-1" id="myModalInsertarModeloEquipo" class="modal fade">
                                  <div class="modal-dialog">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                              <h4 class="modal-title">Insertar Nuevo Modelo Equipo</h4>
                                          </div>
                                          <div class="modal-body">
                                              <form action="../../BUSINESS_CAP/AdmonEquipo/ProcModeloEquipo.php" method="POST">
                                                  <div class="form-group">
                                                      <label class=" p-head">Modelo Equipo</label>
                                                      <input type="text" name="txtModeloEquipoInsertar" maxlength="50" class="form-control" 
                                                             placeholder="Describa el Modelo Equipo" required>                                                    
                                                   <br/>
                                                      <label class=" p-head">Marca Equipo</label>
                                                      <select name="cbxMarcaEquipoInsertar" class="form-control m-bot15">
                                                          <?php
                                                          require ("../../DAO_CAP/AdmonEquipo/Mantenimiento_Modelo_Equipo.php");
                                                          $objmodeloEquipo = new MantenimientoModeloEquipo();
                                                          $rsModeloEquipoIns = $objmodeloEquipo->SelectModeloEquipoParaInsertar();
                                                          while ($modeloEqVarIns = pg_fetch_array( $rsModeloEquipoIns)) {
                                                              ?>
                                                              <option value="<?php echo $modeloEqVarIns['id_marca_equipo']; ?>"><?php echo $modeloEqVarIns['marca_equipo']; ?></option>  
                                                              <?php
                                                          }
                                                          ?> 
                                                      </select>
                                                      <br/>
                                                      <center><button type="submit" class="btn btn-default" name="InsertarModeloEquipo">Insertar Modelo Equipo</button></center>
                                                  </div>
                                              </form>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                 
                      <table class="table">
                          <thead>
                              <tr>
                                  <th> Id Modelo Equipo </th>
                                  <th> Nombre del Modelo </th>
                                  <th> Marca Equipo </th>
                                  <th> </th>
                              </tr>
                          </thead>
                           <tbody>
                             <tr>
                                  <?php
                                  
                                  $rsModEquipoMan = $objmodeloEquipo->SelectModelo_Equipo();            
                                  $rsRowsmodeloEquipoMan = pg_num_rows($rsModEquipoMan);
                                  //while ($rowEquipoUbicacion = pg_fetch_assoc($rsBitacoraUbicacion)) {
                                  for ($varModeloEq = 0; $varModeloEq < $rsRowsmodeloEquipoMan; $varModeloEq++) {
                                      $idModeloEquipoModal = pg_fetch_result($rsModEquipoMan, $varModeloEq, 'id_modelo_equipo');
                                      $nombreModeloModal = pg_fetch_result($rsModEquipoMan, $varModeloEq, 'nombre_modelo');
                                      $idmarcaEquipoActual = pg_fetch_result($rsModEquipoMan, $varModeloEq, 'id_marca_equipo');
                                      $marcaEquipoModal = pg_fetch_result($rsModEquipoMan, $varModeloEq, 'marca_equipo');
                                      ?>
                                      <th><?php print $idModeloEquipoModal; ?></th>
                                      <th><?php print $nombreModeloModal; ?></th>                                     
                                      <th><?php print $marcaEquipoModal; ?></th>
                                      <th>
                                         <div aria-hidden="true" aria-labelledby="myModalLabel1" role="dialog" tabindex="-1" id="myModalModelo<?php print $varModeloEq; ?>" class="modal fade">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                            <h4 class="modal-title">Detalle Modelo Equipo</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form action="../../BUSINESS_CAP/AdmonEquipo/ProcModeloEquipo.php" method="POST">
                                                                <div class="form-group">
                                                                    <input type="text" name="idmodeloEquipoModificar" value="<?php print $idModeloEquipoModal; ?>"readonly hidden>
                                                                    <label class=" p-head">Nombre Modelo</label>
                                                                    <input type="text" name="NombreModelo" value="<?php print $nombreModeloModal; ?>" class="form-control" required>
                                                                    <br/>      
                                                                    <label class=" p-head">Marca Equipo</label>
                                                                    <select name="cbxModeloEquipoMan" class="form-control m-bot15">
                                                                        <option value="<?php print $idmarcaEquipoActual; ?>"><?php print $marcaEquipoModal; ?></option> 
                                                                      <?php
                                                                        $rsModEquipo = $objmodeloEquipo->SelectModEquipo($idmarcaEquipoActual);
                                                                        while ($ModeloEqVar = pg_fetch_array($rsModEquipo)){
                                                                     ?>
                                                                        <option value="<?php echo $ModeloEqVar['id_marca_equipo']; ?>"><?php echo $ModeloEqVar['marca_equipo']; ?></option>  
                                                                     <?php
                                                                        }
                                                                      ?> 
                                                                        
                                                                    </select>
                                                                </div>
                                                                <center>
                                                                    <button type="submit" name="ModificarModeloEquipo" class="btn btn-default">Modificar Registro</button>
                                                                </center>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#myModalModelo<?php print $varModeloEq; ?>" data-toggle="modal" class="btn btn-info">
                                                Modificar
                                           </a>
                                    </th>
                                  </tr>
                              </tbody>
                              <?php
                          }
                          ?>
                      </table>
                  </div>
              </section>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
    
    
  </section>

    <?php include '../import_js.php'; ?>

  </body>
</html>
