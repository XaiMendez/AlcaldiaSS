<?php

include ("../../DAO_CAP/HelpDesk/model.revision_caso.php");

session_start();

if(isset($_POST['AbrirCasoProc'])){

    $DescAsigMan = $_POST['txtDescAsignacion'];
    $idSolicitudCasoMan = $_POST['idSoliCaso'];
    $idTipoCasoMan = $_POST['cbxTipoCaso'];
    //$idTecnicoAsigMan = $_POST['cbxTecnico'];
    $idEscalabilidadMan = $_POST['cbxEscalabilidad'];
    $idEmpleadoAsignaCasoMan = $_SESSION['IngresoSistema'];

    $objAsignarCasoTec = new AsignarFinalizarCaso();
    $objAsignarCasoTec->setDescAsignacion($DescAsigMan);
    $objAsignarCasoTec->setIdSolicitudCaso($idSolicitudCasoMan);
    $objAsignarCasoTec->setIdTipoCaso($idTipoCasoMan);
    //$objAsignarCasoTec->setIdTecnicoAsig($idTecnicoAsigMan);
    $objAsignarCasoTec->setIdEscabilidad($idEscalabilidadMan);
    $objAsignarCasoTec->setIdEmpAsignaCaso($idEmpleadoAsignaCasoMan);
    $objAsignarCasoTec->AsignarCaso();

    echo "<script>";
    echo "alert('Caso Abierto Satisfactoriamente, Proceda a Asignar el técnico que lo gestionara');";
    echo "window.location = '../../VIEW_CAP/HelpDesk/casos_abiertos.php';";
    echo "</script>";

}

if (isset($_POST['DenegarCasoProc'])){
    $DescAsigManD = $_POST['txtDescAsignacion'];
    $idSolicitudCasoManD = $_POST['idSoliCaso'];
    $idTipoCasoManD = $_POST['cbxTipoCaso'];
    //$idTecnicoAsigMan = $_POST['cbxTecnico'];
    $idEscalabilidadManD = $_POST['cbxEscalabilidad'];
    $idEmpleadoAsignaCasoManD = $_SESSION['IngresoSistema'];

    $objAsignarCasoTec = new AsignarFinalizarCaso();
    $objAsignarCasoTec->setDescAsignacion($DescAsigManD);
    $objAsignarCasoTec->setIdSolicitudCaso($idSolicitudCasoManD);
    $objAsignarCasoTec->setIdTipoCaso($idTipoCasoManD);
    //$objAsignarCasoTec->setIdTecnicoAsig($idTecnicoAsigMan);
    $objAsignarCasoTec->setIdEscabilidad($idEscalabilidadManD);
    $objAsignarCasoTec->setIdEmpAsignaCaso($idEmpleadoAsignaCasoManD);
    $objAsignarCasoTec->DenegarCaso();

    echo "<script>";
    echo "alert('Caso Denegado');";
    echo "window.location = '../../VIEW_CAP/HelpDesk/casos_abiertos.php';";
    echo "</script>";
}

if(isset($_POST['FinalizarCaso'])){
    $IdFinalizarCasoMan = $_SESSION['IdFinalizarCaso'];
    $DescFinalizarCasoMan = $_POST['txtDescFinalizarCaso'];
    $idEmpleadoAsignaCasoManD = $_SESSION['IngresoSistema'];

    $objAsignarCasoTec = new AsignarFinalizarCaso();
    $objAsignarCasoTec->setIdSolicitudCaso($IdFinalizarCasoMan);
    $objAsignarCasoTec->setDescAsignacion($DescFinalizarCasoMan);
    $objAsignarCasoTec->setIdEmpAsignaCaso($idEmpleadoAsignaCasoManD);
    $objAsignarCasoTec->FinalizarCaso();

    echo "<script>";
    echo "alert('Caso Finalizado');";
    echo "window.location = '../../VIEW_CAP/HelpDesk/revision_casos.php';";
    echo "</script>";
}
if(isset($_POST['ReAbrirCaso'])){
    $IdFinalizarCasoMan = $_SESSION['IdFinalizarCaso'];
    $DescAsigMan = $_POST['txtDescAsignacion'];
    //$idSolicitudCasoMan = $_POST['idSoliCaso'];
    $idTipoCasoMan = $_POST['cbxTipoCaso'];
    //$idTecnicoAsigMan = $_POST['cbxTecnico'];
    $idEscalabilidadMan = $_POST['cbxEscalabilidad'];
    $idEmpleadoAsignaCasoMan = $_SESSION['IngresoSistema'];

    $objAsignarCasoTec = new AsignarFinalizarCaso();
    $objAsignarCasoTec->setDescAsignacion($DescAsigMan);
    $objAsignarCasoTec->setIdSolicitudCaso($IdFinalizarCasoMan);
    $objAsignarCasoTec->setIdTipoCaso($idTipoCasoMan);
    //$objAsignarCasoTec->setIdTecnicoAsig($idTecnicoAsigMan);
    $objAsignarCasoTec->setIdEscabilidad($idEscalabilidadMan);
    $objAsignarCasoTec->setIdEmpAsignaCaso($idEmpleadoAsignaCasoMan);
    $objAsignarCasoTec->AsignarCaso();

    echo "<script>";
    echo "alert('Caso Abierto Satisfactoriamente, Proceda a Asignar el técnico que lo gestionara');";
    echo "window.location = '../../VIEW_CAP/HelpDesk/casos_abiertos.php';";
    echo "</script>";

}
