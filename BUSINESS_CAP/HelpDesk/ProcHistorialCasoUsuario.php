<?php

include("../../DAO_CAP/HelpDesk/model.revision_caso.php");

if(isset($_POST['AsignarCaso'])){
    //CREAMOS LA SESSION CON EL ID DE EQUIPO PARA EL PROCESO DE RELACIONAR EQUIPOS
    session_start();
    $_SESSION['IdAsignarCaso'] = $_POST['txtIdCasoAsignar'];

    echo '<script>';
    echo "window.location = '../../VIEW_CAP/HelpDesk/asignacion_casos.php';";
    echo '</script>';
}

if(isset($_POST['VerAsigCaso'])){
    //CREAMOS LA SESSION CON EL ID DE EQUIPO PARA EL PROCESO DE RELACIONAR EQUIPOS
    session_start();
    $_SESSION['IdAsignarCaso'] = $_POST['txtIdSoliCaso'];

    echo '<script>';
    echo "window.location = '../../VIEW_CAP/HelpDesk/asignacion_caso.php';";
    echo '</script>';
}

if(isset($_POST['FinalizarCaso'])){
    //CREAMOS LA SESSION CON EL ID DE EQUIPO PARA EL PROCESO DE RELACIONAR EQUIPOS
    session_start();
    $_SESSION['IdAsignarCaso'] = $_POST['txtIdCasoFinalizar'];
/*
    echo '<script>';
    echo "window.location = '../../VIEW_CAP/HelpDesk/asignacion_casos.php';";
    echo '</script>';*/

    $RevisionCaso= new RevisionCaso();
    $RevisionCaso->SelectFinalizarCasoById($_SESSION['IdAsignarCaso']);
    header('Location:../../VIEW_CAP/HelpDesk/casos_finalizados.php');

    //echo $RevisionCaso->getDescripcion_caso();

}
?>
