<?php

include("../../DAO_CAP/HelpDesk/model.revision_caso.php");

if(isset($_POST['AsignarCaso'])){
    //CREAMOS LA SESSION CON EL ID DE EQUIPO PARA EL PROCESO DE RELACIONAR EQUIPOS
    session_start();
    $_SESSION['IdAsignarCaso'] = $_POST['txtIdCasoAsignar'];

    echo '<script>';
    echo "window.location = '../../VIEW_CAP/HelpDesk/asignacion_casos.php';";
    echo '</script>';
}

if(isset($_POST['VerAsigCaso'])){
    //CREAMOS LA SESSION CON EL ID DE EQUIPO PARA EL PROCESO DE RELACIONAR EQUIPOS
    session_start();
    $_SESSION['IdAsignarCaso'] = $_POST['txtIdSoliCaso'];

    echo '<script>';
    echo "window.location = '../../VIEW_CAP/HelpDesk/asignacion_caso.php';";
    echo '</script>';
}

if(isset($_POST['FinalizarCaso'])){

    session_start();
    $_SESSION['IdFinalizarCaso'] = $_POST['txtIdCasoFinalizar'];
    echo '<script>';
    echo "window.location = '../../VIEW_CAP/HelpDesk/casos_cierre.php';";
    echo '</script>';

    //echo $RevisionCaso->getDescripcion_caso();

}



if(isset($_POST['GestionarCasoAdmin'])){
    //CREAMOS LA SESSION CON EL ID DE EQUIPO PARA EL PROCESO DE RELACIONAR EQUIPOS
    session_start();
    $_SESSION['IdCasoAbiertosAdmin'] = $_POST['txtIdCasoVerAdmin'];

    echo '<script>';
    echo "window.location = '../../VIEW_CAP/HelpDesk/ver_casos_abiertos.php';";
    echo '</script>';

}

if (isset($_POST['VerCaso'])){
    session_start();
    $_SESSION['IdVerTodosCasos'] = $_POST['txtIdCaso'];

    echo '<script>';
    echo "window.location = '../../VIEW_CAP/HelpDesk/ver_todos_casos.php';";
    echo '</script>';
}
?>
