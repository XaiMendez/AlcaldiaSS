<?php

include ("../../DAO_CAP/HelpDesk/model.codigo_subsistema.php");

if (isset($_POST['ModificarSubsistema'])) {
    $id_SubsistemaMantenimiento = $_POST['idSubsistemaModificar'];
    $descSubsistemaMan = $_POST['DescSubsistema'];

    $objModEscalabilidad = new MantenimientoSubsistema();
    $objModEscalabilidad->setid_codigo_subsistema($id_SubsistemaMantenimiento);
    $objModEscalabilidad->setdescripcion_subsistema($descSubsistemaMan);
    $objModEscalabilidad->ModificarSubsistema();

    echo '<script>';
    echo "alert('Subsistema Moificada Satisfactoriamente');";
    echo "window.location = '../../VIEW_CAP/Catalogos/Mantenimiento_codigo_subsistema.php';";
    echo '</script>';

}
if (isset($_POST['InsertarSubsistema'])) {
    $descSubsistemaManInsert = $_POST['txtDescSubsistemaInsertar'];

    $objModSubsistema = new MantenimientoSubsistema();
    $objModSubsistema->setdescripcion_subsistema($descSubsistemaManInsert);
    $objModSubsistema->InsertarSubsistema();

    echo '<script>';
    echo "alert('Campo Moificada Satisfactoriamente');";
    echo "window.location = '../../VIEW_CAP/Catalogos/Mantenimiento_codigo_subsistema.php';";
    echo '</script>';
}

 ?>
