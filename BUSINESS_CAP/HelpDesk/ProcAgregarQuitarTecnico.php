<?php

include("../../DAO_CAP/HelpDesk/model.gestion_casos.php");

session_start();

if (isset($_POST['QuitarTecnicoCaso'])){
    $idCasoTecnicoMan = $_POST['txtIdTecnicoQuitar'];
    $idEmpQuitaCasoMan = $_SESSION['IngresoSistema'];
    $objQuitarTecnico = new GestionBitacoras();
    $objQuitarTecnico->setIdEmpAgregaQuita($idEmpQuitaCasoMan);
    $objQuitarTecnico->setIdCasoTecnico($idCasoTecnicoMan);
    $objQuitarTecnico->QuitarTecnicoCaso();
    
    echo "<script>";
    echo "alert('El técnico fue removido del caso');";  
    echo "window.location = '../../VIEW_CAP/HelpDesk/AgregarQuitarTecnicosCasos.php';";
    echo "</script>";
}

if (isset($_POST['AgregarTecnicoCaso'])){
    $idTecnicoAgregarMan = $_POST['txtIdTecnicoAsignar'];
    $idEmpAgregaMan = $_SESSION['IngresoSistema'];
    $idCasoManAgregar = $_POST['txtIdCasoAgregarTecnico'];
    
    $objQuitarTecnico = new GestionBitacoras();
    $objQuitarTecnico->setIdEmpAgregaQuita($idEmpAgregaMan); //EMPLEADO QUE CREA EL REGISTRO
    $objQuitarTecnico->setIdTecnicoAsignado($idTecnicoAgregarMan);//TECNICO AGREGADO AL CASO
    $objQuitarTecnico->setIdSolicitudCaso($idCasoManAgregar); //ID DEL CASO EN GESTION
    $objQuitarTecnico->AgregarTecnicoCaso();
    
    echo "<script>";
    echo "alert('El técnico fue agregado del caso');";  
    echo "window.location = '../../VIEW_CAP/HelpDesk/AgregarQuitarTecnicosCasos.php';";
    echo "</script>";
}

?>

