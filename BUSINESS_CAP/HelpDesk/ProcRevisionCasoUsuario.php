<?php
    
include ("../../DAO_CAP/HelpDesk/model.usuariosCasos.php");
session_start();

if (isset($_POST['CancelarCaso'])){
    $idCasoManCancel = $_POST['txtIdCasoCancelar'];
    $idSolicitanteManCancel = $_SESSION['IngresoSistema'];
    $descCancel = $_POST['txtDescCancelar'];
    
    $objCancelarCaso = new CasosUsuarios();
    $objCancelarCaso->setIdCasoSeleccionado($idCasoManCancel);
    $objCancelarCaso->setDescCancelarCalificarCaso($descCancel);
    $objCancelarCaso->setIdSolicitante($idSolicitanteManCancel);
    $objCancelarCaso->CancelarCasoUsuario();
    
    echo '<script>';
    echo "alert('Su Caso fue Cancelado');";
    echo "window.location = '../../VIEW_CAP/HelpDesk/vista_casos_usuario.php';";
    echo '</script>';
    
}

if (isset($_POST['CalificarCaso'])){
    $descCalificacionMan = $_POST['txtDescCalificar'];
    $idCasoCalificar = $_POST['txtIdCasoCalificar'];
    $idCalificador = $_SESSION['IngresoSistema'];
    $calificacionMan = $_POST['cbxCalificacion'];
    
    $objCalificarCaso = new CasosUsuarios();
    $objCalificarCaso->setDescCancelarCalificarCaso($descCalificacionMan);
    $objCalificarCaso->setIdCasoSeleccionado($idCasoCalificar);
    $objCalificarCaso->setIdSolicitante($idCalificador);
    $objCalificarCaso->setCalificacion($calificacionMan);
    $objCalificarCaso->CalificarCaso();
    
    echo '<script>';
    echo "alert('Su Caso fue calificado satisfactoriamenente');";
    echo "window.location = '../../VIEW_CAP/HelpDesk/vista_casos_usuario.php';";
    echo '</script>';
    
    
    
}

?>

