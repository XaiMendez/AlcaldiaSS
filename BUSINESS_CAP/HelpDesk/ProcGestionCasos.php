<?php

session_start();

if (isset($_POST['GestionarCasos'])){
    $_SESSION['IdCasoEnGestion'] = $_POST['txtIdCasoGestionar'];
    
    echo '<script>';
    echo "window.location = '../../VIEW_CAP/HelpDesk/historial_bitacora_tecnico.php';";
    echo '</script>';
}

if (isset($_POST['IngresarBiatcoraCasp'])){
    
    include("../../DAO_CAP/HelpDesk/model.gestion_casos.php");
        
    $DescBitacoraMan = $_POST['txtDescAsignacion'];
    $idSoliBitacora = $_POST['txtIdCasoGestionar2'];
    $idTipoCasoBitaCaso = $_POST['cbxTipoCaso'];
    $idEmpCreaBitacoraCaso = $_SESSION['IngresoSistema'];
    
    $objBitacoraCaso = new GestionBitacoras();
    $objBitacoraCaso->setDescAsignacion($DescBitacoraMan);
    $objBitacoraCaso->setIdSolicitudCaso($idSoliBitacora);
    $objBitacoraCaso->setIdTipoCaso($idTipoCasoBitaCaso);
    $objBitacoraCaso->setIdEmpAsignaCaso($idEmpCreaBitacoraCaso);
    $objBitacoraCaso->IngresarBitacora();
    
    echo '<script>';
    echo "alert('Bitacora Ingresada Satisfactoriamente');";
    echo "window.location = '../../VIEW_CAP/HelpDesk/historial_bitacora_tecnico.php';";
    echo '</script>';
    
}

if (isset($_POST['CompletarCaso'])){
    
    include("../../DAO_CAP/HelpDesk/model.gestion_casos.php");
        
    $DescBitacoraMan = $_POST['txtDescAsignacion'];
    $idSoliBitacora = $_POST['txtIdCasoGestionar2'];
    $idTipoCasoBitaCaso = $_POST['cbxTipoCaso'];
    $idEmpCreaBitacoraCaso = $_SESSION['IngresoSistema'];
    
    $objBitacoraCaso = new GestionBitacoras();
    $objBitacoraCaso->setDescAsignacion($DescBitacoraMan);
    $objBitacoraCaso->setIdSolicitudCaso($idSoliBitacora);
    $objBitacoraCaso->setIdTipoCaso($idTipoCasoBitaCaso);
    $objBitacoraCaso->setIdEmpAsignaCaso($idEmpCreaBitacoraCaso);
    $objBitacoraCaso->CompletarCaso();
    
    echo '<script>';
    echo "alert('Caso Completado Satisfactoriamente');";
    echo "window.location = '../../VIEW_CAP/HelpDesk/Gestion_casos.php';";
    echo '</script>';
    
}

if (isset($_POST['PriorizarCaso'])){
    
    include("../../DAO_CAP/HelpDesk/model.gestion_casos.php");
        
    $DescBitacoraMan = $_POST['txtDescPrio'];
    $idSoliBitacora = $_POST['txtIdCasoGestionar3'];
    $idEscalabilidadBitaCaso = $_POST['cbxEscalabilidad'];
    $idEmpCreaBitacoraCaso = $_SESSION['IngresoSistema'];
    
    $objBitacoraCaso = new GestionBitacoras();
    $objBitacoraCaso->setDescAsignacion($DescBitacoraMan);
    $objBitacoraCaso->setIdSolicitudCaso($idSoliBitacora);
    $objBitacoraCaso->setIdEscabilidad($idEscalabilidadBitaCaso);
    $objBitacoraCaso->setIdEmpAsignaCaso($idEmpCreaBitacoraCaso);
    $objBitacoraCaso->PriorizarCaso();
    
    echo '<script>';
    echo "alert('Caso Completado Satisfactoriamente');";
    echo "window.location = '../../VIEW_CAP/HelpDesk/ver_casos_abiertos.php';";
    echo '</script>';
    
}