<?php

require ("../../DAO_CAP/HelpDesk/model.report1_helpdesk.php");
require '../../Resources/FPDF/fpdf.php';

$rpt_ubicacionEquipoPdf = new FPDF('L','mm','A4');
$rpt_ubicacionEquipoPdf->AddPage();
$rpt_ubicacionEquipoPdf->SetFont('Arial','',10); //Tipo letra, Efecto, Tamaño
$rpt_ubicacionEquipoPdf->Image('../../Resources/img/logo.png',30,10,38,35,'PNG'); //ancho, alto , eje x , eje y
$rpt_ubicacionEquipoPdf->Cell(18,10,'',0); //Cell() configuracion para celdas (largo,alto,'(texto o algo)',grosor de celda o borde)
$rpt_ubicacionEquipoPdf->Cell(230,10,'',0);
$rpt_ubicacionEquipoPdf->SetFont('Arial','',8);
$rpt_ubicacionEquipoPdf->Cell(250,10,'FECHA: '.date('d-m-y').'',0);
$rpt_ubicacionEquipoPdf->Ln(15); //SALTO DE LINEA (CANTIDAD DE SALTO)
$rpt_ubicacionEquipoPdf->SetFont('Arial','B',15);
$rpt_ubicacionEquipoPdf->Cell(88,8,'',0);
$rpt_ubicacionEquipoPdf->Cell(200,8,'REPORTE DE HISTORIAL DE CASO POR TECNICO',0);
$rpt_ubicacionEquipoPdf->Ln(20);
$rpt_ubicacionEquipoPdf->SetFont('Arial','B',6);
//inicia encabezado de las columnas del reporte. totalizar 190 para una buena distribucion de tabla.

$rpt_ubicacionEquipoPdf->Cell(38,9,'NUMERO DE SOLICITUD',0);
$rpt_ubicacionEquipoPdf->Cell(58,9,'DESCRIPCION DEL CASO',0);
$rpt_ubicacionEquipoPdf->Cell(25,9,'FECHA',0);
$rpt_ubicacionEquipoPdf->Cell(35,9,'CODIGO DE EMPLEADO',0);
$rpt_ubicacionEquipoPdf->Cell(25,9,'ESTADO DE CASO',0);
$rpt_ubicacionEquipoPdf->Cell(28,9,'CODIGO DE EQUIPO',0);
$rpt_ubicacionEquipoPdf->Line(8, 52, 220, 52);
//fin de encabezados
$rpt_ubicacionEquipoPdf->Ln(8);
$rpt_ubicacionEquipoPdf->SetFont('Arial','',9);

//Hacemos la Consulta de nuestra capa DAO.
$idEquipoRpt = $_POST['cbxtecnico']; //capturamos el ID que esta en el combobox
$objRptUbicacionEquipo = new reportes1_helpdesk();
$objRptUbicacionEquipo->setIdhistorial($idEquipoRpt);
$rsRptPdf = $objRptUbicacionEquipo->SelectIdbitacora2();
while ($filaRptPdf = pg_fetch_assoc($rsRptPdf)){
    $rpt_ubicacionEquipoPdf->Cell(30,7,$filaRptPdf['id_solicitud_caso'],0);
    $rpt_ubicacionEquipoPdf->Cell(55,7,$filaRptPdf['descripcion_caso'],0);
    $rpt_ubicacionEquipoPdf->Cell(40,7,$filaRptPdf['fecha'],0);
    $rpt_ubicacionEquipoPdf->Cell(30,7,$filaRptPdf['codigo_empleado'],0);
    $rpt_ubicacionEquipoPdf->Cell(30,7,$filaRptPdf['estado_caso'],0);
    $rpt_ubicacionEquipoPdf->Cell(22,7,$filaRptPdf['codigo_equipo'],0);
    $rpt_ubicacionEquipoPdf->Ln(5);
}
$rpt_ubicacionEquipoPdf->Ln(18);
$rpt_ubicacionEquipoPdf->SetFont('Arial','B',6);
$rpt_ubicacionEquipoPdf->Cell(25,9,'NUM SOLICITUD',0);
$rpt_ubicacionEquipoPdf->Cell(45,9,'CODIGO DE EQUIPO',0);
$rpt_ubicacionEquipoPdf->Cell(75,9,'DESCRIPCION DE SEGUIMIENTO',0);
$rpt_ubicacionEquipoPdf->Cell(32,9,'FECHA DE SEGUIMIENTO',0);
$rpt_ubicacionEquipoPdf->Cell(35,9,'CODIGO SOLICITANTE',0);
$rpt_ubicacionEquipoPdf->Cell(25,9,'TIPO CASO',0);
$rpt_ubicacionEquipoPdf->Cell(25,9,'ESCALABILIDAD',0);
$rpt_ubicacionEquipoPdf->Cell(25,9,'CODIGO TECNICO',0);

//fin de encabezados
$rpt_ubicacionEquipoPdf->Ln(8);
$rpt_ubicacionEquipoPdf->SetFont('Arial','',9);

//Hacemos la Consulta de nuestra capa DAO.
$idEquipoRpt = $_POST['cbxtecnico']; //capturamos el ID que esta en el combobox
$objRptUbicacionEquipo = new reportes1_helpdesk();
$objRptUbicacionEquipo->setIdhistorial2($idEquipoRpt);
$rsRptPdf = $objRptUbicacionEquipo->SelectIddetalle_bitacora2();
while ($filaRptPdf = pg_fetch_assoc($rsRptPdf)){
    $rpt_ubicacionEquipoPdf->Cell(30,7,$filaRptPdf['codigo_equipo'],0);
    $rpt_ubicacionEquipoPdf->Cell(100,7,$filaRptPdf['descripcion_seguimiento'],0);
    $rpt_ubicacionEquipoPdf->Cell(45,7,$filaRptPdf['fecha_seguimiento'],0);
    $rpt_ubicacionEquipoPdf->Cell(30,7,$filaRptPdf['usuario'],0);
    $rpt_ubicacionEquipoPdf->Cell(30,7,$filaRptPdf['tipo_caso'],0);
    $rpt_ubicacionEquipoPdf->Cell(22,7,$filaRptPdf['escalabilidad'],0);
    $rpt_ubicacionEquipoPdf->Cell(22,7,$filaRptPdf['codigo_empleado'],0);
    $rpt_ubicacionEquipoPdf->Ln(5);
}

ob_end_clean(); //limpiamos el bufer de salida por la sobreescritura de |la variable $rpt_ubicacionEquipoPdf, aveces da error...
$rpt_ubicacionEquipoPdf->Output(); //GENERAMOS EL REPORTE
?>
