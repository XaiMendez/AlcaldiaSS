<?php

include ("../../DAO_CAP/HelpDesk/model.ingreso_tecnico.php");
  if (isset($_POST['InsertarTecnico'])){

    $codTecInsert = $_POST['txtCodTec'];
    $idPersonaInsert = $_POST['cbxPersona'];
    $idEstadoEmpleadoInsert = $_POST['cbxEstadoEmpleado'];
    $idAreaDistritoInsert = $_POST['cbxAreaDistrito'];
    $usuarioDetaPersonaInsert = $_POST['txtUsuario'];
    $passDetaPersonaInsert = $_POST['txtPass'];
    $passConfPersonaInsert = $_POST['txtPassConf'];
    $idEspecialidadTec = $_POST['cbxEspecialidad'];
    $hashedPass = crypt($passDetaPersonaInsert , '$5$');
    $hashedConfPass = crypt($passConfPersonaInsert , '$5$');

    $objIngresoTec = new IngresoTecnico();
    $objIngresoTec->setCodigoEmpleado($codTecInsert);
    $objIngresoTec->setIdPersona($idPersonaInsert);
    $objIngresoTec->setIdEstadoEmpleado($idEstadoEmpleadoInsert);
    $objIngresoTec->setIdAreaDistrito($idAreaDistritoInsert);
    $objIngresoTec->setUsuarioDetaPersona($usuarioDetaPersonaInsert);
    $objIngresoTec->setPassDetaPersona($hashedPass);
    $objIngresoTec->SetIdEspecialidadTec($idEspecialidadTec);
    $objIngresoTec->setConfPassDetaPersona($hashedConfPass);
    $objIngresoTec->InsertarTecnico();

}
elseif (isset($_POST['ActualizarTecnico'])){

  $codTecInsert = $_POST['txtCodTec'];
  $idEstadoEmpleadoInsert = $_POST['cbxEstadoEmpleado'];
  $idAreaDistritoInsert = $_POST['cbxAreaDistrito'];
  $idEspecialidadTec = $_POST['cbxEspecialidad'];

  $objIngresoTec = new IngresoTecnico();
  $objIngresoTec->setIdEstadoEmpleado($idEstadoEmpleadoInsert);
  $objIngresoTec->setIdAreaDistrito($idAreaDistritoInsert);
  $objIngresoTec->SetIdEspecialidadTec($idEspecialidadTec);


//echo $codTecInsert;
//echo $idEstadoEmpleadoInsert;
//echo $idAreaDistritoInsert;
//echo $idEspecialidadTec;
$objIngresoTec->update($codTecInsert);

}
elseif (isset($_POST['changePass'])){

  $codTecInsert = $_POST['txtCodTec'];
  $txtPass = $_POST['txtPass'];
  $txtPassConf = $_POST['txtPassConf'];
  $hashedPass = crypt($txtPass , '$5$');


  $objIngresoTec = new IngresoTecnico();
  $objIngresoTec->setPassDetaPersona($hashedPass);



//echo $codTecInsert;
//echo $txtPass;

$objIngresoTec->updatePassword($codTecInsert);
header('Location: ../../VIEW_CAP/HelpDesk/listTecnicos.php');
}
?>
