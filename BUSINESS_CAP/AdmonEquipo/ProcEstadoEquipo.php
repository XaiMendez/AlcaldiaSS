<?php

include ("../../DAO_CAP/AdmonEquipo/Mantenimiento_Estado_Equipo.php");

if (isset($_POST['ModificarEstado_Equipo'])){
    $idEstadoEquipoMantenimiento = $_POST['idEstadoEquipoModificar'];
    $estadoEquipoMan = $_POST['EstadoEquipo'];

    $objModEstadoEq = new MantenimientoEstadoEquipo();
    $objModEstadoEq->setIdEstado($idEstadoEquipoMantenimiento);
    $objModEstadoEq->setEstadoEquipo($estadoEquipoMan);
    $objModEstadoEq->ModificarEstado_Equipo();
    
     echo '<script>';
    echo "alert('Estado Equipo Modificado Satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/Catalogos/Mantenimiento_Estado_Equipo.php';";
    echo '</script>';

    }

if (isset($_POST['InsertarEstado_Equipo'])){
    $EstadoEquipoManInsert = $_POST['txtEstadoEquipoInsertar'];
    
    $objModEstadoEq = new MantenimientoEstadoEquipo();
    $objModEstadoEq->setEstadoEquipo($EstadoEquipoManInsert);
    $objModEstadoEq->InsertarEstado_Equipo();
    
    echo '<script>';
    echo "alert('Estado Equipo Insertado Satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/Catalogos/Mantenimiento_Estado_Equipo.php';";
    echo '</script>';
}
