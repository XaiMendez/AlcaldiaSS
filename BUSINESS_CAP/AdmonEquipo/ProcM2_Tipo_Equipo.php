<?php

include ("../../DAO_CAP/AdmonEquipo/Mantenimiento_M2_Tipo_Equipo.php");

if (isset($_POST['Modificarm2_tipo_equipo'])){
    $idm2_tipoequipoMantenimiento = $_POST['idM2_tipoEquipoModificar'];
    $m2_tipoequipoMan = $_POST['M2_tipoEquipo'];

    $objModM2Equipo = new Mantenimiento_M2_tipo_Equipo();
    $objModM2Equipo->setId_M2_Tipo_Equipo($idm2_tipoequipoMantenimiento);
    $objModM2Equipo->setM2_Tipo_Equipo($m2_tipoequipoMan);
    $objModM2Equipo->Modificarm2_tipo_equipo();
    
     echo '<script>';
    echo "alert('Modificado Satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/Catalogos/Mantenimiento_Estado_Compra.php';";
    echo '</script>';
    
}
if (isset($_POST['Insertarm2_tipo_equipo'])){
    $m2_tipoequipoInsert = $_POST['txtM2tipoEquipoInsertar'];
    
    $objModM2Equipo = new Mantenimiento_M2_tipo_Equipo();
    $objModM2Equipo->setM2_Tipo_Equipo($m2_tipoequipoInsert);
    $objModM2Equipo->Insertarm2_tipo_equipo();
    
    echo '<script>';
    echo "alert('Insertado Satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/Catalogos/Mantenimiento_Estado_Compra.php';";
    echo '</script>';
}