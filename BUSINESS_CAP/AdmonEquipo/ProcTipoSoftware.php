<?php

include ("../../DAO_CAP/AdmonEquipo/Mantenimiento_Tipo_Software.php");

if (isset($_POST['ModificarTipo_Software'])){
    $idTipoSoftwareMantenimiento = $_POST['idTipoSoftModificar'];
    $TipoSoftwareMan = $_POST['Tipo_Software'];

    $objModTipoSoftCom = new MantenimientoTipo_Software();
    $objModTipoSoftCom->setIdTipo_Software($idTipoSoftwareMantenimiento);
    $objModTipoSoftCom->setTipo_Software($TipoSoftwareMan);
    $objModTipoSoftCom->ModificarTipo_Software();

     echo '<script>';
    echo "alert('Tipo de Software Modificado Satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/Catalogos/Mantenimiento_Tipo_Software.php';";
    echo '</script>';
    
}
if (isset($_POST['InsertarTipo_Software'])){
    $tipoSoftwareManInsert = $_POST['txttipoSoftwareInsertar'];
    
    $objModTipoSoftCom = new MantenimientoTipo_Software();
    $objModTipoSoftCom->setTipo_Software($tipoSoftwareManInsert);
    $objModTipoSoftCom->InsertarTipo_Software();
    
    echo '<script>';
    echo "alert('Software Insertado Satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/Catalogos/Mantenimiento_Tipo_Software.php';";
    echo '</script>';
}
