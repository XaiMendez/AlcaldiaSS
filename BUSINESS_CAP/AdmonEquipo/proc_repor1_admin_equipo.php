<?php

require ("../../DAO_CAP/AdmonEquipo/model.report1_adm_equipo.php");
require '../../Resources/FPDF/fpdf.php';

$rpt_ubicacionEquipoPdf = new FPDF('L','mm','A3');
$rpt_ubicacionEquipoPdf->AddPage();
$rpt_ubicacionEquipoPdf->SetFont('Arial','',10); //Tipo letra, Efecto, Tamaño
$rpt_ubicacionEquipoPdf->Image('../../Resources/img/logo.png',30,10,38,35,'PNG'); //ancho, alto , eje x , eje y
$rpt_ubicacionEquipoPdf->Cell(18,10,'',0); //Cell() configuracion para celdas (largo,alto,'(texto o algo)',grosor de celda o borde)
$rpt_ubicacionEquipoPdf->Cell(230,10,'',0);
$rpt_ubicacionEquipoPdf->SetFont('Arial','',8);
$rpt_ubicacionEquipoPdf->Cell(250,10,'FECHA: '.date('d-m-y').'',0);
$rpt_ubicacionEquipoPdf->Ln(15); //SALTO DE LINEA (CANTIDAD DE SALTO)
$rpt_ubicacionEquipoPdf->SetFont('Arial','B',15);
$rpt_ubicacionEquipoPdf->Cell(88,8,'',0);
$rpt_ubicacionEquipoPdf->Cell(200,8,'REPORTE DETALLADO DE EQUIPO INFORMATICO',0);
$rpt_ubicacionEquipoPdf->Ln(20);
$rpt_ubicacionEquipoPdf->SetFont('Arial','B',6);
//inicia encabezado de las columnas del reporte. totalizar 190 para una buena distribucion de tabla.
$rpt_ubicacionEquipoPdf->Cell(50,9,'DESCRIPCION EQUIPO',0);
$rpt_ubicacionEquipoPdf->Cell(25,9,'CODIGO EQUIPO',0);
$rpt_ubicacionEquipoPdf->Cell(20,9,'EQUIPO',0);
$rpt_ubicacionEquipoPdf->Cell(22,9,'ESTADO EQUIPO',0);
$rpt_ubicacionEquipoPdf->Cell(18,9,'TIPO EQUIPO',0);
$rpt_ubicacionEquipoPdf->Cell(28,9,'ALMACENAMIENTO',0);
$rpt_ubicacionEquipoPdf->Cell(25,9,'USUARIO RED',0);
$rpt_ubicacionEquipoPdf->Cell(40,9,'NOMBRE EQUIPO',0);
$rpt_ubicacionEquipoPdf->Cell(50,9,'DESCRIPCION SOFTWARE',0);
$rpt_ubicacionEquipoPdf->Cell(40,9,'PROCESADOR',0);
$rpt_ubicacionEquipoPdf->Cell(30,9,'MEMORIA RAM',0);
$rpt_ubicacionEquipoPdf->Line(8, 52, 380, 52);
//fin de encabezados
$rpt_ubicacionEquipoPdf->Ln(8);
$rpt_ubicacionEquipoPdf->SetFont('Arial','',9);

//Hacemos la Consulta de nuestra capa DAO.
$idEquipoRpt = $_POST['txt_codigo_equipo']; //capturamos el ID que esta en el combobox
$objRptUbicacionEquipo = new reportes1_adm_equipo();
$objRptUbicacionEquipo->setIdcodigo($idEquipoRpt);
$rsRptPdf = $objRptUbicacionEquipo->Select_espec_tecnica();
while ($filaRptPdf = pg_fetch_assoc($rsRptPdf)){
    $rpt_ubicacionEquipoPdf->Cell(55,7,$filaRptPdf['descripcion_equipo'],0);
    $rpt_ubicacionEquipoPdf->Cell(15,7,$filaRptPdf['codigo_equipo'],0);
    $rpt_ubicacionEquipoPdf->Cell(28,7,$filaRptPdf['equipo'],0);
    $rpt_ubicacionEquipoPdf->Cell(20,7,$filaRptPdf['estado_equipo'],0);
    $rpt_ubicacionEquipoPdf->Cell(22,7,$filaRptPdf['tipo_equipo'],0);
    $rpt_ubicacionEquipoPdf->Cell(22,7,$filaRptPdf['almacenamiento'],0);
    $rpt_ubicacionEquipoPdf->Cell(28,7,$filaRptPdf['usuario_red'],0);
    $rpt_ubicacionEquipoPdf->Cell(30,7,$filaRptPdf['nombre_equipo'],0);
    $rpt_ubicacionEquipoPdf->Cell(50,7,$filaRptPdf['descripcion_software'],0);
    $rpt_ubicacionEquipoPdf->Cell(38,7,$filaRptPdf['procesador'],0);
    $rpt_ubicacionEquipoPdf->Cell(60,7,$filaRptPdf['memoria_ram'],0);
    $rpt_ubicacionEquipoPdf->Ln(5);
}
ob_end_clean(); //limpiamos el bufer de salida por la sobreescritura de |la variable $rpt_ubicacionEquipoPdf, aveces da error...
$rpt_ubicacionEquipoPdf->Output(); //GENERAMOS EL REPORTE
?>
