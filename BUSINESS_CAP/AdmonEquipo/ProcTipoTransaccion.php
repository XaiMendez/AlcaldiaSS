<?php

include ("../../DAO_CAP/AdmonEquipo/Mantenimiento_Tipo_Transaccion.php");

if (isset($_POST['ModificarTipo_Transaccion'])){
    $idTipoTransaccionMantenimiento = $_POST['idTipoTransaccionModificar'];
    $tipoTransaccionMan = $_POST['Tipo_Transaccion'];

    $objModTipoTransaccion = new MantenimientoTipoTransaccion();
    $objModTipoTransaccion->setIdTipo_Transaccion($idTipoTransaccionMantenimiento);
    $objModTipoTransaccion->setTipo_Transaccion($tipoTransaccionMan);
    $objModTipoTransaccion->ModificarTipo_Transaccion();
    
     echo '<script>';
    echo "alert('Tipo Transaccion Modificado Satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/Catalogos/Mantenimiento_Tipo_Trans.php';";
    echo '</script>';
    
}

if (isset($_POST['InsertarTipo_Transaccion'])){
    $tipoTransaccionManInsert = $_POST['txtTipoTransaccionInsertar'];
    
    $objModTipoTransaccion = new MantenimientoTipoTransaccion();
    $objModTipoTransaccion->setTipo_Transaccion($tipoTransaccionManInsert);
    $objModTipoTransaccion->InsertarTipo_Transaccion();
    
    echo '<script>';
    echo "alert('Tipo Transaccion Insertado Satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/Catalogos/Mantenimiento_Tipo_Trans.php';";
    echo '</script>';
}
