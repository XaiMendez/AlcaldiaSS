<?php

include ("../../DAO_CAP/AdmonEquipo/Mantenimiento_Tipo_Adquisicion.php");

if (isset($_POST['ModificarTipo_Adquisicion'])){
    $idTipoAdquisicionMantenimiento = $_POST['idTipoAdquisicionModificar'];
    $tipoAdquisicionMan = $_POST['tipoAdquisicion'];

    $objModTipoAdq = new Mantenimientotipo_Adquisicion();
    $objModTipoAdq->setIdTipo_Adquisicion($idTipoAdquisicionMantenimiento);
    $objModTipoAdq->setTipo_Adquisicion($tipoAdquisicionMan);
    $objModTipoAdq->ModificarTipo_Adquisicion();
    
     echo '<script>';
    echo "alert('Tipo Adquisicion Modificado Satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/Catalogos/Mantenimiento_Tipo_Adquisicion.php';";
    echo '</script>';
    
}
if (isset($_POST['InsertarTipo_Adquisicion'])){
    $tipoAdquisicionManInsert = $_POST['txtTipoAdquisicionInsertar'];
    
    $objModTipoAdq = new Mantenimientotipo_Adquisicion();
    $objModTipoAdq->setTipo_Adquisicion($tipoAdquisicionManInsert);
    $objModTipoAdq->InsertarTipo_Adquisicion();
    
    echo '<script>';
    echo "alert('Tipo Adquisicion Insertado Satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/Catalogos/Mantenimiento_Tipo_Adquisicion.php';";
    echo '</script>';
}