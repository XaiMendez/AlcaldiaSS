<?php

include ("../../DAO_CAP/AdmonEquipo/variables_asignarEquipo.php");

if(isset($_POST['AsignarEquipoTec'])){
    session_start();
    $DetaTransMan = $_POST['txtDescTransAsig'];
    $IdEquipoAsigMan = $_POST['txtEquipoTransAsig'];
    $idEmpleadoAutorizaMan = $_SESSION['IngresoSistema'];
    $idAreaDistritoMan = $_POST['cbxAreaDistrito'];
    $idEmpleadoAsignado = $_POST['cbxDetaEmpleadoAsig'];
    $idEsCasoMan = $_POST['txtEsIdCaso'];
    
    $objAsignarEquipo = new AsignarEquipoTecnologico();
    $objAsignarEquipo->setDetalleTransaccion($DetaTransMan);
    $objAsignarEquipo->setIdEquipo($IdEquipoAsigMan);
    $objAsignarEquipo->setIdEmpleadoAutoriza($idEmpleadoAutorizaMan);
    $objAsignarEquipo->setIdAreaDistritoDestino($idAreaDistritoMan);
    $objAsignarEquipo->setIdEmpleadoAsignado($idEmpleadoAsignado);
    
    
    if ($idEsCasoMan != ''){
        //hago herencia a la clase de enca solicitud compra para validar el login
        $objAsignarEquipo->setIdCaso($idEsCasoMan); // hago set a la funcion del enca solicitud compra
        $ExisteIdCasoAsignar = $objAsignarEquipo->ValidarIdCaso();
        
        if ($ExisteIdCasoAsignar!=NULL){
            //si devuelve id de caso inserta todo.
            $objAsignarEquipo->AsignarEquipo();
             echo "<script>";
             echo "alert('Equipo Asignado Satisfactoriamente');";  
             echo "window.location = '../../VIEW_CAP/AdmonEquipo/TransAsignacionEq.php';";
             echo "</script>";
        }else {
            //si no encuentra id de caso lanza error
             echo "<script>";
             echo "alert('Error en la bitacora de caso, favor verificar el numero.');";  
             echo "window.location = '../../VIEW_CAP/AdmonEquipo/TransAsignacionEqDeta.php';";
             echo "</script>";
        }
    }else {
        //si no ingresan ID de caso se asigna sin insertarle nada
        $idEsCasoMan = 'NULL';
        $objAsignarEquipo->setIdCaso($idEsCasoMan);
        $objAsignarEquipo->AsignarEquipo();
        echo "<script>";
        echo "alert('Asignacion Realizada Satisfactoriamente.');";  
        echo "window.location = '../../VIEW_CAP/AdmonEquipo/TransAsignacionEq.php';";
        echo "</script>";
    }
    
    
    
    
    
   
}

?>