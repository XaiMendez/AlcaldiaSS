<?php

include ("../../DAO_CAP/AdmonEquipo/Mantenimiento_Tipo_Equipo.php");

if (isset($_POST['ModificarEquipo'])){
    $idTipoEquipoMan = $_POST['idTipoEquipoModificar'];
    $tipoEquipoMan = $_POST['tipoEquipo'];
    $idmTipoEquipoMan = $_POST['cbxTipoEquipoMan'];
    
    $objModEquipo = new MantenimientoTipoEquipo();
    $objModEquipo->setIdTipo_Equipo($idTipoEquipoMan);
    $objModEquipo->setTipo_Equipo($tipoEquipoMan);
    $objModEquipo->setIdmTipo_Equipo($idmTipoEquipoMan);
    $objModEquipo->ModificarEquipo();
    
    echo '<script>';
    echo "alert('Tipo de Equipo Modificado Satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/Catalogos/Mantenimiento_Tipo_Equipo.php';";
    echo '</script>';
    
}
if (isset($_POST['InsertarEquipo'])){
    $tipoEquipoManInsert = $_POST['txtTipoEquipoInsertar'];
    $idmTipoEquipoManInsert = $_POST['cbxTipoEquipoInsertar'];
    
    $objModEquipo = new MantenimientoTipoEquipo();
    $objModEquipo->setTipo_Equipo($tipoEquipoManInsert);
    $objModEquipo->setIdmTipo_Equipo($idmTipoEquipoManInsert);
    $objModEquipo->InsertarEquipo();
    
    echo '<script>';
    echo "alert('Tipo de Equipo Insertado Satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/Catalogos/Mantenimiento_Tipo_Equipo.php';";
    echo '</script>';
}