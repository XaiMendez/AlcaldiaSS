<?php

include ("../../DAO_CAP/AdmonEquipo/variables_equipoRelacionado.php");

if (isset($_POST['QuitarRelacionEquipos'])){
    
    $idEquipoRelacionarMan = $_POST['txtIdEquipoQuitarRelacion'];
    $idEquipoSeleccionadoMan = $_POST['txtIdEquipoSeleccionadoQ'];
    session_start();
    $idEmpleadoCreaRelacion = $_SESSION['IngresoSistema'];
    
    $objRelacionarEquipo = new ProcesoEquipoRelacionado();
    $objRelacionarEquipo->setIdEquipoRelacionar($idEquipoSeleccionadoMan); // ej id de computadora
    $objRelacionarEquipo->setIdEquipoSeleccionado($idEquipoRelacionarMan); // ej id de ram
    $objRelacionarEquipo->setIdEmpleadoCrea($idEmpleadoCreaRelacion);
    
    $objRelacionarEquipo->QuitarEquipoRelacionado();
    
    echo "<script>";
    echo "alert('Equipo Removido Satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/AdmonEquipo/EditRelacionarEquipos.php';";
    echo "</script>";
    
}

?>

