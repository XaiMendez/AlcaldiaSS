<?php

include ("../../DAO_CAP/AdmonEquipo/variables_ingresoDetalleCpu.php");
session_start();
$objDetalleCpu = new IngresoDetalleCpu();

if (isset($_POST['IngresarDetalleCpu'])){
    
    $AlmacenamientoDefectoMn = $_POST['txtAlmacenamiento'];
    $UsuarioRedMn = $_POST['txtRed'];
    $NombreEquipoMn = $_POST['txtNombre'];
    $IdEquipoMn = $_SESSION['VarSessionDetaCpu'];
    $IdSoftwareSoMn = $_POST['cbxSO'];
    $UnidadAlmacenamientoHarddrive = $_POST['cbxAlmacenamiento'];
    $IdDetaEmpleadoCrea = $_SESSION['IngresoSistema'];
    $IdMarcaProcesador = $_POST['cbxModeloProcesador'];
    $IdMarcaRam = $_POST['cbxMemoriaRam'];
    $CantidadRam = $_POST['txtNumeroMemorias'];
   
    $objDetalleCpu ->setAlmacenamiento($AlmacenamientoDefectoMn);
    $objDetalleCpu ->setUsuarioDeRed($UsuarioRedMn);
    $objDetalleCpu ->setNombreDeEquipo($NombreEquipoMn);
    $objDetalleCpu ->setIdEquipo($IdEquipoMn);
    $objDetalleCpu ->setIdSoftware($IdSoftwareSoMn);
    $objDetalleCpu ->setIdUndAlmac($UnidadAlmacenamientoHarddrive);
    $objDetalleCpu ->setIdEmpleado($IdDetaEmpleadoCrea);
    $objDetalleCpu ->setIdProcesador($IdMarcaProcesador);
    $objDetalleCpu ->setIdRam($IdMarcaRam);
    $objDetalleCpu ->setCantidadMemoria($CantidadRam);
    
    $objDetalleCpu ->IngresarDetalleCpu();
    
    echo '<script>';
    echo "alert('Detalle de computadora ingresado exitosamente');";
    echo "window.location = '../../VIEW_CAP/AdmonEquipo/index.php';";
    echo '</script>';
     
}

if (isset($_POST['ModificarDetalleCpu'])){
    
    $AlmacenamientoDefectoMn = $_POST['txtAlmacenamiento'];
    $UsuarioRedMn = $_POST['txtRed'];
    $NombreEquipoMn = $_POST['txtNombre'];
    $IdEquipoMn = $_SESSION['IdEquipoParaVer'];
    $IdSoftwareSoMn = $_POST['cbxSO'];
    $UnidadAlmacenamientoHarddrive = $_POST['cbxAlmacenamiento'];
    $IdDetaEmpleadoCrea = $_SESSION['IngresoSistema'];
    $IdMarcaProcesador = $_POST['cbxModeloProcesador'];
    $IdMarcaRam = $_POST['cbxMemoriaRam'];
    $CantidadRam = $_POST['txtNumeroMemorias'];
   
    $objDetalleCpu ->setAlmacenamiento($AlmacenamientoDefectoMn);
    $objDetalleCpu ->setUsuarioDeRed($UsuarioRedMn);
    $objDetalleCpu ->setNombreDeEquipo($NombreEquipoMn);
    $objDetalleCpu ->setIdEquipo($IdEquipoMn);
    $objDetalleCpu ->setIdSoftware($IdSoftwareSoMn);
    $objDetalleCpu ->setIdUndAlmac($UnidadAlmacenamientoHarddrive);
    $objDetalleCpu ->setIdEmpleado($IdDetaEmpleadoCrea);
    $objDetalleCpu ->setIdProcesador($IdMarcaProcesador);
    $objDetalleCpu ->setIdRam($IdMarcaRam);
    $objDetalleCpu ->setCantidadMemoria($CantidadRam);
    
    $objDetalleCpu ->IngresarDetalleCpu();
    
    echo '<script>';
    echo "alert('Detalle de computadora agregado');";
    echo "window.location = '../../VIEW_CAP/AdmonEquipo/index.php';";
    echo '</script>';
}


