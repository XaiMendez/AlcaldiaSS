
<?php

include ("../../DAO_CAP/AdmonEquipo/mantenimiento_software.php");

if (isset($_POST['ModificarSoftware'])){
    $idSoftwareMantenimiento = $_POST['idSoftModificar'];
    $descSoftwareMan = $_POST['DescSoftware'];
    $licenciaSoftMan = $_POST['LicenciaSoftware'];
    $idTipoSoftwareMan = $_POST['cbxTipoSoftwareMan'];
    
    $objModSoftware = new MantenimientoSoftware();
    $objModSoftware->setIdSoftware($idSoftwareMantenimiento);
    $objModSoftware->setDescSoftware($descSoftwareMan);
    $objModSoftware->setLicencia($licenciaSoftMan);
    $objModSoftware->setIdTipoSoftware($idTipoSoftwareMan);
    $objModSoftware->ModificarSoftware();
    
    echo '<script>';
    echo "alert('Software Modificado Satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/Catalogos/Mantenimiento_software.php';";
    echo '</script>';
    
}

if (isset($_POST['InsertarSofware'])){
    $descSoftwareManInsert = $_POST['txtDescSoftwareInsertar'];
    $licenciaSoftManInsert = $_POST['txtLicenciaSoftwareInsertar'];
    $idTipoSoftwareManInsert = $_POST['cbxTipoSoftwareInsertar'];
    
    $objModSoftware = new MantenimientoSoftware();
    $objModSoftware->setDescSoftware($descSoftwareManInsert);
    $objModSoftware->setLicencia($licenciaSoftManInsert);
    $objModSoftware->setIdTipoSoftware($idTipoSoftwareManInsert);
    $objModSoftware->InsertarSoftware();
    
    echo '<script>';
    echo "alert('Software Insertado Satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/Catalogos/Mantenimiento_software.php';";
    echo '</script>';
}
