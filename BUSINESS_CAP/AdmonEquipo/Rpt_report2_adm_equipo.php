<?php

require ("../../DAO_CAP/AdmonEquipo/model.report1_adm_equipo.php");
require '../../Resources/FPDF/fpdf.php';

$rpt_ubicacionEquipoPdf = new FPDF('L','mm','A3');
$rpt_ubicacionEquipoPdf->AddPage();
$rpt_ubicacionEquipoPdf->SetFont('Arial','',10); //Tipo letra, Efecto, Tamaño
$rpt_ubicacionEquipoPdf->Image('../../Resources/img/logo.png',30,10,32,28,'PNG'); //ancho, alto , eje x , eje y
$rpt_ubicacionEquipoPdf->Cell(18,10,'',0); //Cell() configuracion para celdas (largo,alto,'(texto o algo)',grosor de celda o borde)
$rpt_ubicacionEquipoPdf->Cell(230,10,'',0);
$rpt_ubicacionEquipoPdf->SetFont('Arial','',8);
$rpt_ubicacionEquipoPdf->Cell(250,10,'FECHA: '.date('d-m-y').'',0);
$rpt_ubicacionEquipoPdf->Ln(15); //SALTO DE LINEA (CANTIDAD DE SALTO)
$rpt_ubicacionEquipoPdf->SetFont('Arial','B',15);
$rpt_ubicacionEquipoPdf->Cell(88,8,'',0);
$rpt_ubicacionEquipoPdf->Cell(200,8,'REPORTE DE MOVIMIENTO DE EQUIPO',0);
$rpt_ubicacionEquipoPdf->Ln(20);
$rpt_ubicacionEquipoPdf->SetFont('Arial','B',6);
//inicia encabezado de las columnas del reporte. totalizar 190 para una buena distribucion de tabla.
$rpt_ubicacionEquipoPdf->Cell(20,9,'CODIGO' ,0);
$rpt_ubicacionEquipoPdf->Cell(18,9,'EQUIPO',0);
$rpt_ubicacionEquipoPdf->Cell(30,9,'ALMACENAMIENTO',0);
$rpt_ubicacionEquipoPdf->Cell(40,9,'PROCESADOR',0);
$rpt_ubicacionEquipoPdf->Cell(55,9,'MEMORIA RAM',0);
$rpt_ubicacionEquipoPdf->Cell(70,9,'DETALLE TRANSACCION',0);
$rpt_ubicacionEquipoPdf->Cell(40,9,'FECHA TRANSACCION',0);
$rpt_ubicacionEquipoPdf->Cell(62,9,'TIPO TRANSACCION',0);
$rpt_ubicacionEquipoPdf->Cell(40,9,'DISTRITO ORIGEN',0);
$rpt_ubicacionEquipoPdf->Cell(30,9,'DISTRITO DESTINO',0);
$rpt_ubicacionEquipoPdf->Line(8, 52, 450, 52);
//fin de encabezados
$rpt_ubicacionEquipoPdf->Ln(8);
$rpt_ubicacionEquipoPdf->SetFont('Arial','',9);

//Hacemos la Consulta de nuestra capa DAO.
$idEquipoRpt = $_POST['txt_codigo_equipo'];
$prm1 = $_POST['fecha1'];
$prm2 = $_POST['fecha2'];
$objRptUbicacionEquipo = new reportes1_adm_equipo();
$objRptUbicacionEquipo->setIdcodigo($idEquipoRpt);
$objRptUbicacionEquipo->setparametro1fecha($prm1);
$objRptUbicacionEquipo->setparametro2fecha($prm2);
$rsRptPdf = $objRptUbicacionEquipo->Select_movimiento();
while ($filaRptPdf = pg_fetch_assoc($rsRptPdf)){
    $rpt_ubicacionEquipoPdf->Cell(15,7,$filaRptPdf['codigo_equipo'],0);
    $rpt_ubicacionEquipoPdf->Cell(25,7,$filaRptPdf['equipo'],0);
    $rpt_ubicacionEquipoPdf->Cell(20,7,$filaRptPdf['almacenamiento'],0);
    $rpt_ubicacionEquipoPdf->Cell(35,7,$filaRptPdf['procesador'],0);
    $rpt_ubicacionEquipoPdf->Cell(55,7,$filaRptPdf['memoria_ram'],0);
    $rpt_ubicacionEquipoPdf->Cell(75,7,$filaRptPdf['detalle_transaccion'],0);
    $rpt_ubicacionEquipoPdf->Cell(40,7,$filaRptPdf['fecha_transaccion'],0);
    $rpt_ubicacionEquipoPdf->Cell(63,7,$filaRptPdf['tipo_transaccion'],0);
    $rpt_ubicacionEquipoPdf->Cell(40,7,$filaRptPdf['distrito_origen'],0);
    $rpt_ubicacionEquipoPdf->Cell(60,7,$filaRptPdf['distrito_destino'],0);
    $rpt_ubicacionEquipoPdf->Ln(5);
}

$rpt_ubicacionEquipoPdf->Ln(18);

$rpt_ubicacionEquipoPdf->SetFont('Arial','B',6);
$rpt_ubicacionEquipoPdf->Cell(25,9,'CODIGO DE EQUIPO',0);
$rpt_ubicacionEquipoPdf->Cell(35,9,'CODIGO DE EMPLEADO',0);
$rpt_ubicacionEquipoPdf->Cell(50,9,'FECHA DE TRANSACCION',0);
$rpt_ubicacionEquipoPdf->Cell(55,9,'FECHA DE TRANSACCION',0);
$rpt_ubicacionEquipoPdf->Cell(40,9,'DISTRITO',0);
$rpt_ubicacionEquipoPdf->Cell(25,9,'AREA',0);
//fin de encabezados
$rpt_ubicacionEquipoPdf->Ln(8);
$rpt_ubicacionEquipoPdf->SetFont('Arial','',9);

//Hacemos la Consulta de nuestra capa DAO.
$idEquipoRpt = $_POST['txt_codigo_equipo'];
$prm1 = $_POST['fecha1'];
$prm2 = $_POST['fecha2'];
$objRptUbicacionEquipo = new reportes1_adm_equipo();
$objRptUbicacionEquipo->setIdcodigo($idEquipoRpt);
$objRptUbicacionEquipo->setparametro1fecha($prm1);
$objRptUbicacionEquipo->setparametro2fecha($prm2);
$rsRptPdf = $objRptUbicacionEquipo->Select_detalle();
while ($filaRptPdf = pg_fetch_assoc($rsRptPdf)){
    $rpt_ubicacionEquipoPdf->Cell(30,7,$filaRptPdf['codigo_equipo'],0);
    $rpt_ubicacionEquipoPdf->Cell(30,7,$filaRptPdf['codigo_empleado'],0);
    $rpt_ubicacionEquipoPdf->Cell(45,7,$filaRptPdf['fecha_solicitud'],0);
    $rpt_ubicacionEquipoPdf->Cell(45,7,$filaRptPdf['fecha_transaccion'],0);
    $rpt_ubicacionEquipoPdf->Cell(45,7,$filaRptPdf['distrito_destino'],0);
    $rpt_ubicacionEquipoPdf->Cell(30,7,$filaRptPdf['area'],0);

    $rpt_ubicacionEquipoPdf->Ln(5);

}
ob_end_clean(); //limpiamos el bufer de salida por la sobreescritura de |la variable $rpt_ubicacionEquipoPdf, aveces da error...
$rpt_ubicacionEquipoPdf->Output(); //GENERAMOS EL REPORTE
?>
