<?php

include ("../../DAO_CAP/AdmonEquipo/variables_equipoTecnologico.php");
$objEquipotec = new EquipoTecnologico(); //clase EquipoTecnologico()
session_start();

if(isset($_POST['IngresarEquipo'])){
    $descEquipoMan = $_POST['txtDescEquipoTec'];
    $codEquipoMan = $_POST['txtCodigoEquipo'];
    $idTipoEquipoMan = $_POST['cbxTipoEquipo'];
    $idEstadoEquipoMan = $_POST['cbxEstadoEquipo'];
    $idTipoAdquiMan = $_POST['cbxTipoAdquisicion'];
    $idMarcaMan = $_POST['cbxMarcaEquipo']; //dato de marca de equipo , solo usaremos el modelo
    $idModeloMan = $_POST['cbxModeloEquipo'];
    $idEmpleadoCrea = $_SESSION['IngresoSistema']; //id de empleado que esta realizando el proceso
    
    
    
    $objEquipotec->setCodEquipo($codEquipoMan);
    $ExisteCodigo = $objEquipotec->ValidarCodEquipo();  //veremos si existe codigo para no repetirlo
    
    if ($ExisteCodigo!=NULL){
        echo "<script>";
        echo "alert('Codigo de equipo ya existe');";  
        echo "window.location = '../../VIEW_CAP/AdmonEquipo/IngresarEquipoTec.php';";
        echo "</script>";
    } else {
        $objEquipotec->setDescEquipo($descEquipoMan);
        $objEquipotec->setIdTipoEquipo($idTipoEquipoMan);
        $objEquipotec->setIdEstadoEquipo($idEstadoEquipoMan);
        $objEquipotec->setIdTipoAdquisicion($idTipoAdquiMan);
        $objEquipotec->setIdModelo($idModeloMan);
        $objEquipotec->setIdEmpCreaModif($idEmpleadoCrea);
        
        $objEquipotec->IngresarEquipoTec();
    
        echo "<script>";
        echo "alert('Equipo Almacenado Satisfactoriamente');";  
        echo "window.location = '../../VIEW_CAP/AdmonEquipo/IngresarEquipoTec.php';";
        echo "</script>";
    }
    
}

if(isset($_POST['ModificarEquipo'])){
    $descEquipoMan = $_POST['txtDescEquipoTec'];
    $idTipoEquipoMan = $_POST['cbxTipoEquipo'];
    $idTipoAdquiMan = $_POST['cbxTipoAdquisicion'];
    $idModeloMan = $_POST['cbxModeloEquipo'];
    $idEmpleadoCrea = $_SESSION['IngresoSistema']; //id de empleado que esta realizando el proceso
    $idEquipoModif = $_SESSION['IdEquipoParaVer'];

    $objEquipotec->setDescEquipo($descEquipoMan);
    $objEquipotec->setIdTipoEquipo($idTipoEquipoMan);
    $objEquipotec->setIdTipoAdquisicion($idTipoAdquiMan);
    $objEquipotec->setIdModelo($idModeloMan);
    $objEquipotec->setIdEmpCreaModif($idEmpleadoCrea);
    $objEquipotec->setIdEquipo($idEquipoModif);

    $objEquipotec->ModificarEquipoTec();

    echo "<script>";
    echo "alert('Equipo Modificado Satisfactoriamente');";
    echo "window.location = '../../VIEW_CAP/AdmonEquipo/VerEquiposTecDeta.php';";
    echo "</script>";
}

?>

