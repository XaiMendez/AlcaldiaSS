<?php
include ("../../DAO_CAP/AdmonEquipo/variables_equipoTecnologico.php");

session_start();
$objDesactivarActivarEquipo = new EquipoTecnologico();

if(isset($_POST['DesactivarEquipo'])){
    
    $idEquipoDesactivar = $_SESSION['IdEquipoParaVer'];
    
    $idArea = $objDesactivarActivarEquipo->SelectValidarUbicacionEquipoCambioEstado($idEquipoDesactivar);
    
    if ($idArea == 1){
        $idEmpDesactiva = $_SESSION['IngresoSistema'];
        $objDesactivarActivarEquipo->setIdEquipo($idEquipoDesactivar);
        $objDesactivarActivarEquipo->setIdEmpCreaModif($idEmpDesactiva);
        $objDesactivarActivarEquipo->DesactivarEquipo();

         echo "<script>";
         echo "alert('Equipo Desactivado');";  
         echo "window.location = '../../VIEW_CAP/AdmonEquipo/VerEquiposTecDeta.php';";
         echo "</script>";
    }else {
         echo "<script>";
         echo "alert('El Equipo debe estar ubicado en bodega para cambiar su estado, Realice la solicitud de traslado a bodega para actualizar los estados');";  
         echo "window.location = '../../VIEW_CAP/AdmonEquipo/VerEquiposTecDeta.php';";
         echo "</script>";
    }
    
    
}

if(isset($_POST['ActivarEquipo'])){
    $idEquipoDesactivar = $_SESSION['IdEquipoParaVer'];
    
    $idArea = $objDesactivarActivarEquipo->SelectValidarUbicacionEquipoCambioEstado($idEquipoDesactivar);
    
    if ($idArea == 1){
        $idEmpDesactiva = $_SESSION['IngresoSistema'];
        $objDesactivarActivarEquipo->setIdEquipo($idEquipoDesactivar);
        $objDesactivarActivarEquipo->setIdEmpCreaModif($idEmpDesactiva);
        $objDesactivarActivarEquipo->ActivarEquipo();

         echo "<script>";
         echo "alert('Equipo Activado');";  
         echo "window.location = '../../VIEW_CAP/AdmonEquipo/VerEquiposTecDeta.php';";
         echo "</script>";
    }else {
         echo "<script>";
         echo "alert('El Equipo debe estar ubicado en bodega para cambiar su estado, Realice la solicitud de traslado a bodega para actualizar los estados');";  
         echo "window.location = '../../VIEW_CAP/AdmonEquipo/VerEquiposTecDeta.php';";
         echo "</script>";
    }
}

if(isset($_POST['RepararEquipo'])){
    $idEquipoDesactivar = $_SESSION['IdEquipoParaVer'];
    
    $idArea = $objDesactivarActivarEquipo->SelectValidarUbicacionEquipoCambioEstado($idEquipoDesactivar);
    
    if ($idArea == 1){
        $idEmpDesactiva = $_SESSION['IngresoSistema'];
        $objDesactivarActivarEquipo->setIdEquipo($idEquipoDesactivar);
        $objDesactivarActivarEquipo->setIdEmpCreaModif($idEmpDesactiva);
        $objDesactivarActivarEquipo->RepararEquipo();

         echo "<script>";
         echo "alert('Equipo en Reparación');";  
         echo "window.location = '../../VIEW_CAP/AdmonEquipo/VerEquiposTecDeta.php';";
         echo "</script>";
    }else {
         echo "<script>";
         echo "alert('El Equipo debe estar ubicado en bodega para cambiar su estado, Realice la solicitud de traslado a bodega para actualizar los estados');";  
         echo "window.location = '../../VIEW_CAP/AdmonEquipo/VerEquiposTecDeta.php';";
         echo "</script>";
    }
}

if(isset($_POST['ModificarEquipo'])){
    echo "<script>"; 
    echo "window.location = '../../VIEW_CAP/AdmonEquipo/ModificarEquipoTec.php';";
    echo "</script>";
}

if (isset($_POST['ModificarDetaCpu'])){
    echo "<script>"; 
    echo "window.location = '../../VIEW_CAP/AdmonEquipo/ModificarDetalleCpu.php';";
    echo "</script>";
}

?>