<?php

include ("../../DAO_CAP/AdmonEquipo/Mantenimiento_Tipo_Documento.php");

if (isset($_POST['ModificarTipo_Doc_Compra'])){
    $idDocCompraMantenimiento = $_POST['idDocCompraModificar'];
    $TipoDocMan = $_POST['Tipo_Documento'];

    $objModDocumentoCom = new MantenimientoDocumento();
    $objModDocumentoCom->setIdTipo_Documento_Compra($idDocCompraMantenimiento);
    $objModDocumentoCom->setTipoDocumento_Compra($TipoDocMan);
    $objModDocumentoCom->ModificarTipo_Doc_Compra();
    
     echo '<script>';
    echo "alert('Tipo de Documento Modificado Satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/Catalogos/Mantenimiento_Tipo_documento.php';";
    echo '</script>';
    
}

if (isset($_POST['InsertarTipo_Doc_Compra'])){
    $tipoDocManInsert = $_POST['txttipoDocComManInsert'];
    
    $objModDocumentoCom = new MantenimientoDocumento();
    $objModDocumentoCom->setTipoDocumento_Compra( $tipoDocManInsert);
    $objModDocumentoCom->InsertarTipo_Doc_Compra();
    
    echo '<script>';
    echo "alert('Tipo Documento Insertado Satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/Catalogos/Mantenimiento_Tipo_documento.php';";
    echo '</script>';
}