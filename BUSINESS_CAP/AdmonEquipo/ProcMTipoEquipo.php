<?php

include ("../../DAO_CAP/AdmonEquipo/Mantenimiento_M_Tipo_Equipo.php");

if (isset($_POST['ModificarMEquipo'])){
    $idmTipoEquipoMan = $_POST['idmTipoEquipoModificar'];
    $mtipoEquipoMan = $_POST['mtipoEquipo'];
    $idm2TipoEquipoMan = $_POST['cbxM2TipoEquipoMan'];
    
    $objModMEquipo = new MantenimientoM_TipoEquipo();
    $objModMEquipo->setId_m_Tipo_Equipo($idmTipoEquipoMan);
    $objModMEquipo->setM_tipo_Equipo($mtipoEquipoMan);
    $objModMEquipo->setIdm2_Tipo_EquipO($idm2TipoEquipoMan);
    $objModMEquipo->ModificarMEquipo();
    
    echo '<script>';
    echo "alert('Tipo de Equipo Modificado Satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/Catalogos/Mantenimiento_Tipo_Equipo.php';";
    echo '</script>';


}
if (isset($_POST['InsertarMEquipo'])){
    $mtipoEquipoManInsert = $_POST['txtMTipoEquipoInsertar'];
    $idm2TipoEquipoManInsert = $_POST['cbxMTipoEquipoInsertar'];
    
    $objModMEquipo = new MantenimientoM_TipoEquipo();
    $objModMEquipo->setM_tipo_Equipo($mtipoEquipoManInsert);
    $objModMEquipo->setIdm2_Tipo_Equipo($idm2TipoEquipoManInsert);
    $objModMEquipo->InsertarMEquipo();
    
    echo '<script>';
    echo "alert('Tipo de Equipo Insertado Satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/Catalogos/Mantenimiento_Tipo_Equipo.php';";
    echo '</script>';
}