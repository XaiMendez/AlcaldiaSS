<?php

include ("../../DAO_CAP/AdmonEquipo/variables_softwareEquipo.php");

session_start();

if (isset($_POST['IngresarSoft'])){
    $IdEquipoMan = $_POST['txtIdEquipo'];
    $idSoftwareMan = $_POST['cbxSoftware'];
    
    $idEmpleadoMan = $_SESSION['IngresoSistema'];
    
    $objIngresoSoftware = new IngresarSoftwareEquipo();
    $objIngresoSoftware->setIdEquipo($IdEquipoMan);
    $objIngresoSoftware->setIdSoftware($idSoftwareMan);
    $objIngresoSoftware->setIdEmpleadoIns($idEmpleadoMan);
    
    $softwareActivo = $objIngresoSoftware->validarSoftwareActivo();
    
    if($softwareActivo == 'SI'){
        echo "<script>";
        echo "alert('No se pudo Activar el Software debido a que ya se encuentra Activado para este Equipo');";  
        echo "window.location = '../../VIEW_CAP/AdmonEquipo/EditIngresarSoftwareEq.php';";
        echo "</script>";
    } else {   
        $objIngresoSoftware->IngresarSoftware();

        echo "<script>";
        echo "alert('Software Activado Satisfactoriamente');";  
        echo "window.location = '../../VIEW_CAP/AdmonEquipo/EditIngresarSoftwareEq.php';";
        echo "</script>";
    }

}

if(isset($_POST['desactivarSoftware'])){
    $idSsoftEquipoMan = $_POST['txtIdEqSoft'];
    $idEmpleadoMan = $_SESSION['IngresoSistema'];
    
    $objDesactivarSoft = new IngresarSoftwareEquipo();
    $objDesactivarSoft->setIdDesact($idSsoftEquipoMan);
    $objDesactivarSoft->setIdEmpleadoIns($idEmpleadoMan);
    
    $estaDesactivado = $objDesactivarSoft->validarDesactivarSoftware();
    
    if ($estaDesactivado!=NULL){
        echo "<script>";
        echo "alert('El Software ya fue Desactivado');";  
        echo "window.location = '../../VIEW_CAP/AdmonEquipo/EditIngresarSoftwareEq.php';";
        echo "</script>";
        
    } else {
        $objDesactivarSoft->EditarSoftware();
        
        echo "<script>";
        echo "alert('Software Desactivado Satisfactoriamente');";  
        echo "window.location = '../../VIEW_CAP/AdmonEquipo/EditIngresarSoftwareEq.php';";
        echo "</script>";
        
    }
    
}
