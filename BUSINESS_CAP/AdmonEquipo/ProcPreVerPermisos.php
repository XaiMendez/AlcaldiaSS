<?php
include ("../../DAO_CAP/AdmonEquipo/variables_permisos.php");

session_start();
$objPermisosMan = new Permisos();

if (isset($_POST['VerPermisios'])){
    $_SESSION['IdTecnicoPermisos'] = $_POST['txtIdTecPermiso'];
    echo '<script>';
    echo "window.location = '../../VIEW_CAP/AdmonEquipo/ConfigurarPermisosDeta.php';";
    echo '</script>';
}

if (isset($_POST['AgregarPermiso'])){
    $idPermisoMan = $_POST['txtIdPermisoAgregar'];
    $idEmpleadoMan = $_POST['txtIdEmpAgregar'];
    $idEmpAsignaPermiso = $_SESSION['IngresoSistema'];
    $objPermisosMan->setIdPermiso($idPermisoMan);
    $objPermisosMan->setIdTecnico($idEmpleadoMan);
    $objPermisosMan->setIdEmpAsignaPermiso($idEmpAsignaPermiso);
    $alerta = $objPermisosMan->AgregarPermisoTecnico();
    if ($alerta == 1){
        echo '<script>';
        echo "alert('Permiso Agregado Satisfactoriamente');"; 
        echo "window.location = '../../VIEW_CAP/AdmonEquipo/ConfigurarPermisosDeta.php';";
        echo '</script>';
    }else {
        echo '<script>';
        echo "alert('Error al Configurar los permisos');"; 
        echo "window.location = '../../VIEW_CAP/AdmonEquipo/ConfigurarPermisosDeta.php';";
        echo '</script>';
    }
    
}

if (isset($_POST['QuitarPermiso'])){
    $idUsuarioPermisoMan = $_POST['txtIdUsuarioPermisoQuitar'];
    $idEmpAsignaPermiso = $_SESSION['IngresoSistema'];
    
    $objPermisosMan->setIdPermiso($idUsuarioPermisoMan);
    $objPermisosMan->setIdEmpAsignaPermiso($idEmpAsignaPermiso);
    $alerta = $objPermisosMan->QuitarPermisoTecnico();
    if ($alerta == 1){
        echo '<script>';
        echo "alert('Permiso quitado Satisfactoriamente');"; 
        echo "window.location = '../../VIEW_CAP/AdmonEquipo/ConfigurarPermisosDeta.php';";
        echo '</script>';
    }else {
        echo '<script>';
        echo "alert('Error al Configurar los permisos');"; 
        echo "window.location = '../../VIEW_CAP/AdmonEquipo/ConfigurarPermisosDeta.php';";
        echo '</script>';
    }
    
}
