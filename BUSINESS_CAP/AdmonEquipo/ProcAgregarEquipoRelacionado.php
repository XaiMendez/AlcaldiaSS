<?php

include ("../../DAO_CAP/AdmonEquipo/variables_equipoRelacionado.php");

if (isset($_POST['AgregarRelacionEquipos'])){
    
    $idEquipoRelacionarMan = $_POST['txtIdEquipoAgregarRelacion'];
    $idEquipoSeleccionadoMan = $_POST['txtIdEquipoSelecR'];
    session_start();
    $idEmpleadoCreaRelacion = $_SESSION['IngresoSistema'];
    
    $objRelacionarEquipo = new ProcesoEquipoRelacionado();
    $objRelacionarEquipo->setIdEquipoRelacionar($idEquipoSeleccionadoMan);
    $objRelacionarEquipo->setIdEquipoSeleccionado($idEquipoRelacionarMan);
    $objRelacionarEquipo->setIdEmpleadoCrea($idEmpleadoCreaRelacion);
    
    $objRelacionarEquipo->AgregarEquipoRelacionado();
    
    echo "<script>";
    echo "alert('Equipo Relacionado Satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/AdmonEquipo/EditRelacionarEquipos.php';";
    echo "</script>";
    
}

?>