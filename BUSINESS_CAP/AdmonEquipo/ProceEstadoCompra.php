
<?php

include ("../../DAO_CAP/AdmonEquipo/Mantenimiento_Estado_Compra.php");

if (isset($_POST['ModificarEstado_Compra'])){
    $idEstadoCompraMantenimiento = $_POST['idEstadoComModificar'];
    $descripcionMan = $_POST['Descripcion'];

    $objModEstadoCom = new MantenimientoEstado();
    $objModEstadoCom->setIdEstado_Compra($idEstadoCompraMantenimiento);
    $objModEstadoCom->setDescEstado_Compra($descripcionMan);
    $objModEstadoCom->ModificarEstado_Compra();
    
     echo '<script>';
    echo "alert('Estado Modificado Satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/Catalogos/Mantenimiento_Estado_Compra.php';";
    echo '</script>';
    
}

if (isset($_POST['InsertarEstado_Compra'])){
    $descEstadoComManInsert = $_POST['txtDescEstadoComInsertar'];
    
    $objModEstadoCom = new MantenimientoEstado();
    $objModEstadoCom->setDescEstado_Compra($descEstadoComManInsert);
    $objModEstadoCom->InsertarEstado_Compra();
    
    echo '<script>';
    echo "alert('Software Insertado Satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/Catalogos/Mantenimiento_Estado_Compra.php';";
    echo '</script>';
}
