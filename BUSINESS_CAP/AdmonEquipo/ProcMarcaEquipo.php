<?php

include ("../../DAO_CAP/AdmonEquipo/Mantenimiento_Marca_Equipo.php");

if (isset($_POST['ModificarMarcaEquipo'])){
    $idMarcaEquipoMan = $_POST['idmarcaEquipoModificar'];
    $marcaEquipoMan = $_POST['MarcaEquipo'];
    $idmTipoEquipoMan = $_POST['cbxMarcaEquipoMan'];
    
    $objModMarcaEquipo = new MantenimientoMarcaEquipo();
    $objModMarcaEquipo->setId_Marca_Equipo($idMarcaEquipoMan);
    $objModMarcaEquipo->setMarca_Equipo($marcaEquipoMan);
    $objModMarcaEquipo->setIdm_Tipo_Equipo($idmTipoEquipoMan);
    $objModMarcaEquipo->ModificarMarcaEquipo();
    
    echo '<script>';
    echo "alert('Marca de Equipo Modificado Satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/Catalogos/Mantenimiento_Marca_Equipo.php';";                     
    echo '</script>';
    }
if (isset($_POST['InsertarMarcaEquipo'])){
    $marcaEquipoManInsert = $_POST['txtMarcaEquipoInsertar'];
    $idmTipoEquipoManInsert = $_POST['cbxEquipoInsertar'];
    
    $objModMarcaEquipo = new MantenimientoMarcaEquipo();
    $objModMarcaEquipo->setMarca_Equipo($marcaEquipoManInsert);
    $objModMarcaEquipo->setIdm_Tipo_Equipo($idmTipoEquipoManInsert);
    $objModMarcaEquipo->InsertarMarcaEquipo();
    
    echo '<script>';
    echo "alert('Marca de Equipo Insertado Satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/Catalogos/Mantenimiento_Marca_Equipo.php';";                  
    echo '</script>';
}