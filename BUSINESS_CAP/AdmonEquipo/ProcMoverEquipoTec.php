<?php

include ("../../DAO_CAP/AdmonEquipo/variables_asignarEquipo.php");

if (isset($_POST['MoverEquipoTec'])){ // CONDICION DE MOVER EQUIPO A USUARIO
    
    session_start();
    $DetaTransMoverMan = $_POST['txtDescTransMov'];
    $IdEquipoMovMan = $_POST['txtEquipoTransMov'];
    $IdEmpleadoAutorizaMovMan = $_SESSION['IngresoSistema'];
    $IdAreaDistritoActualMovMan = $_POST['txtIdAreaDistritoActualMov'];
    $idAreaDistritoDestinoMov = $_POST['cbxAreaDistrito'];
    $idEmpleadoAsignadoMov = $_POST['cbxDetaEmpleadoAsig'];
    $idBitacoraActualizarMan = $_POST['txtIdBitacoraActualizar'];
    // ID DE EMPLEADO A COMPARAR PARA NO MOVER EL EQUIPO AL MISMO LUGAR EN EL QUE SE ENCUENTRA
    $idEmpleadoComparar = $_POST['txtIdEmpleadoAsignadoMov'];
    $idSolicitudTrasladoMan = $_SESSION['IdSolicitudTrasladoParaProcesar']; //id de solicitud de traslado si lo hubiese
    

    
    $objMoverEquipoTrans = new MoverEquipoTecnologico();
    
    $objMoverEquipoTrans->setDetalleTransaccion($DetaTransMoverMan);
    $objMoverEquipoTrans->setIdEquipo($IdEquipoMovMan);
    $objMoverEquipoTrans->setIdEmpleadoAutoriza($IdEmpleadoAutorizaMovMan);
    
    if ($IdAreaDistritoActualMovMan != ''){
        $objMoverEquipoTrans->setIdAreaDistritoOrigen($IdAreaDistritoActualMovMan);
    } else {
        $IdAreaDistritoActualMovManNull = 'NULL';
        $objMoverEquipoTrans->setIdAreaDistritoOrigen($IdAreaDistritoActualMovManNull);
    }
    
    if ($idSolicitudTrasladoMan != '') {
        $objMoverEquipoTrans->setIdSolicitudTrasladoEquipo($idSolicitudTrasladoMan);
    } else {
        $idSolicitudTrasladoManNull = 'NULL';
        $objMoverEquipoTrans->setIdSolicitudTrasladoEquipo($idSolicitudTrasladoEquipo);
    }
    
    $objMoverEquipoTrans->setIdAreaDistritoDestino($idAreaDistritoDestinoMov);
    $objMoverEquipoTrans->setIdEmpleadoAsignado($idEmpleadoAsignadoMov);
    $objMoverEquipoTrans->setIdBitacoraActualizar($idBitacoraActualizarMan);
    
    
    if ($idEmpleadoComparar == $idEmpleadoAsignadoMov){
        echo "<script>";
        echo "alert('Error, El empleado seleccionado ya posee el equipo, debe seleccionar un destino distinto');";  
        echo "window.location = '../../VIEW_CAP/AdmonEquipo/TransReAsignacionEdit.php';";
        echo "</script>";
    } else {

            $objMoverEquipoTrans->MoverEquipo();
            
            unset($_SESSION['IdSolicitudTrasladoParaProcesar']);
            echo "<script>";
            echo "alert('Equipo Fue Movido Satisfactoriamente');";  
            echo "window.location = '../../VIEW_CAP/AdmonEquipo/index.php';";
            echo "</script>";
        }
}

// INICIA CAPA DE NEGOCIO PARA MOVER EQUIPO A BODEGA, SEA APROVAR LA SOLICITUD O DENEGARLA DESDE ACA

if (isset($_POST['AprovarSoliTrasladoBodega'])){
    session_start();
    $idEquipoManBodegaMan = $_POST['txtEquipoTransMovBodega'];
    $idAreaOrigenMan = $_POST['txtIdAreaDistritoActualMovBodega'];
    $idBitacoraActualizarBodegaMan = $_POST['txtIdBitacoraActualizarBodega'];
    $idSolicitudTrasladoBodegaMan = $_POST['txtIdSolicitudTrasladoBodega'];
    $idEmpleadoAutorizaMovBodegaMan = $_SESSION['IngresoSistema'];
    
    $objMoverEquipoBodega = new MoverEquipoTecnologico();
    $objMoverEquipoBodega->setIdEquipo($idEquipoManBodegaMan);
    $objMoverEquipoBodega->setIdAreaDistritoOrigen($idAreaOrigenMan);
    $objMoverEquipoBodega->setIdBitacoraActualizar($idBitacoraActualizarBodegaMan);
    $objMoverEquipoBodega->setIdSolicitudTrasladoEquipo($idSolicitudTrasladoBodegaMan);
    $objMoverEquipoBodega->setIdEmpleadoAutoriza($idEmpleadoAutorizaMovBodegaMan);
    $objMoverEquipoBodega->MoverEquipoBodega();
    
    unset($_SESSION['IdSolicitudTrasladoParaProcesar']); //ELIMINAMOS LA VARIABLE DE SESSION
    echo "<script>";
    echo "alert('Equipo Fue Movido Satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/AdmonEquipo/index.php';";
    echo "</script>";
    
}

if (isset($_POST['DenegarSoliTrasladoBodega'])){
    session_start();
    
    $idEmpleadoDenegaSoliBodega = $_SESSION['IngresoSistema'];
    $idSolicitudDenegar = $_POST['txtIdSolicitudTrasladoBodega'];
    
    $objDenegarSolicitudBodega = new SolicitudTrasladoEquipo();
    $objDenegarSolicitudBodega->setIdEmpleadoDenegaAprueba($idEmpleadoDenegaSoliBodega);
    $objDenegarSolicitudBodega->setIdSolicitudDenegar($idSolicitudDenegar);
    $objDenegarSolicitudBodega->DenegarSoliTraslado();
    
    unset($_SESSION['IdSolicitudTrasladoParaProcesar']); //ELIMINAMOS LA VARIABLE DE SESSION
    echo "<script>";
    echo "alert('Solicitud de Traslado Denegada Satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/AdmonEquipo/VerSolicitudTrasladoEquipo.php';";
    echo "</script>";
    
}
     

?>
