<?php

include ("../../DAO_CAP/AdmonEquipo/variables_solicitudCompra.php");

if (isset($_POST['AprovarSoliCompra'])){
    $idSoliCompraApMan = $_POST['txtAprovSoliCompra'];
    $idEmpAprovMan = $_POST['txtIdEmpAprov'];
    $objAprovarSoliCompra = new EncaSolicitudCompra();
    
            
    $objAprovarSoliCompra->setIdSolicitudCompra($idSoliCompraApMan);
    $objAprovarSoliCompra->setIdEmpAprov($idEmpAprovMan);
    $varValidar = $objAprovarSoliCompra->ValidarAprovDenegSoliCompra();
    
    if($varValidar!=NULL){ 
        
        echo "<script>";
        echo "alert('La Solicitud ya fue Procesada Anteriormente');";  
        echo "window.location = '../../VIEW_CAP/AdmonEquipo/VerSolicitudesCompra.php';";
        echo "</script>";
        
    } else {

        $objAprovarSoliCompra->AprovarSoliCompra();

        echo "<script>";
        echo "alert('Solicitud Aprovada Satisfactoriamente');";  
        echo "window.location = '../../VIEW_CAP/AdmonEquipo/VerSolicitudesCompra.php';";
        echo "</script>";
    }

}

if (isset($_POST['DenegarSoliCompra'])){
    $idSoliCompraApMan = $_POST['txtAprovSoliCompra'];
    $idEmpAprovMan = $_POST['txtIdEmpAprov'];
    $objAprovarSoliCompra = new EncaSolicitudCompra();
    
    
    $objAprovarSoliCompra->setIdSolicitudCompra($idSoliCompraApMan);
    $objAprovarSoliCompra->setIdEmpAprov($idEmpAprovMan);
    $varValidar = $objAprovarSoliCompra->ValidarAprovDenegSoliCompra();
    
    if($varValidar!=NULL){
        
        echo "<script>";
        echo "alert('La Solicitud ya fue Procesada Anteriormente');";  
        echo "window.location = '../../VIEW_CAP/AdmonEquipo/VerSolicitudesCompra.php';";
        echo "</script>";
        
    } else {
            
        $objAprovarSoliCompra->DenegarSoliCompra();

        echo "<script>";
        echo "alert('Solicitud Denegada Satisfactoriamente');";  
        echo "window.location = '../../VIEW_CAP/AdmonEquipo/VerSolicitudesCompra.php';";
        echo "</script>";
    }

}

