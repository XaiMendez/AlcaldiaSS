<?php

include ("../../DAO_CAP/AdmonEquipo/Mantenimiento_Unidad_Almacenamiento.php");

if (isset($_POST['ModificarUnidad_Almacenamiento'])){
    $idUnidadAlmacenamientoMantenimiento = $_POST['idUnidadModificar'];
    $codeUnidadMan = $_POST['codeUnidad'];
    $descUnidadMan = $_POST['descUnidad'];

    $objModUnidadAlm = new MantenimientoUnidad_Almacenamiento();
    $objModUnidadAlm->setIdUnidad_Almacenamiento($idUnidadAlmacenamientoMantenimiento);
    $objModUnidadAlm->setCode_Unidad($codeUnidadMan);
    $objModUnidadAlm->setDesc_Unidad($descUnidadMan);
    $objModUnidadAlm->ModificarUnidad_Almacenamiento();
    
     echo '<script>';
    echo "alert('Unidad Almacenamiento Modificado Satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/Catalogos/Mantenimiento_Unidad_Almacenamiento.php';";
    echo '</script>';
    }

if (isset($_POST['InsertarUnidad_Almacenamiento'])){
    $codeUnidadManInsert = $_POST['txtcodeUnidadInsertar'];
    $descUnidadManInsert = $_POST['txtdescUnidadInsertar'];
    
    $objModUnidadAlm = new MantenimientoUnidad_Almacenamiento();
    $objModUnidadAlm->setCode_Unidad($codeUnidadManInsert);
    $objModUnidadAlm->setDesc_Unidad($descUnidadManInsert);
    $objModUnidadAlm->InsertarUnidad_Almacenamiento();
    
    echo '<script>';
    echo "alert('Unidad Almacenamiento Insertado Satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/Catalogos/Mantenimiento_Unidad_Almacenamiento.php';";
    echo '</script>';
}
