<?php

include ("../../DAO_CAP/AdmonEquipo/Mantenimiento_Modelo_Equipo.php");

if (isset($_POST['ModificarModeloEquipo'])){
    $idModeloEquipoMan = $_POST['idmodeloEquipoModificar'];
    $nombreModeloMan = $_POST['NombreModelo'];
    $idMarcaEquipoMan = $_POST['cbxModeloEquipoMan'];
    
    $objModModeloEquipo = new MantenimientoModeloEquipo();
    $objModModeloEquipo->setId_Modelo_Equipo($idModeloEquipoMan);
    $objModModeloEquipo->setNombre_Modelo($nombreModeloMan);
    $objModModeloEquipo->setIdMarca_Equipo($idMarcaEquipoMan);
    $objModModeloEquipo->ModificarModeloEquipo();
    
    echo '<script>';
    echo "alert('Modelo de Equipo Modificado Satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/Catalogos/Mantenimiento_Modelo_Equipo.php';";                     
    echo '</script>';
    }
    if (isset($_POST['InsertarModeloEquipo'])){
    $modeloEquipoManInsert = $_POST['txtModeloEquipoInsertar'];
    $idmarcaEquipoManInsert = $_POST['cbxMarcaEquipoInsertar'];
    
    $objModModeloEquipo = new MantenimientoModeloEquipo();
    $objModModeloEquipo->setNombre_Modelo($modeloEquipoManInsert);
    $objModModeloEquipo->setIdMarca_Equipo($idmarcaEquipoManInsert);
    $objModModeloEquipo->InsertarModeloEquipo();
    
    echo '<script>';
    echo "alert('Marca de Equipo Insertado Satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/Catalogos/Mantenimiento_Marca_Equipo.php';";                  
    echo '</script>';
}