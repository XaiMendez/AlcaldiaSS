<?php
include("../../DAO_CAP/AdmonEquipo/variables_login.php");

if(isset($_POST['ValidaLogin'])){
    $usuarioMan = $_POST['txtUsuario'];
    $contraMan = $_POST['txtContra'];
    $hashedPass = crypt($contraMan , '$5$');

    $objLogIn = new Login();
    $objLogIn->setUsuario($usuarioMan);
    $objLogIn->setContra($hashedPass);
   $resultado = $objLogIn->ValidarLogin(); //ID DEL LOGUEADO
    //
  ///  $objLogIn->setContra($contraMan);
//   $objLogIn->EncriptarContra();



    if (pg_num_rows($resultado)>0){

        $idUsuarioIngresado = pg_fetch_result($resultado, 0, 'id_emp');
        $rolUsuarioIngresado = pg_fetch_result($resultado, 0, 'rol_emp');

        $objAccesoEmp = new AccesoEmpleado();
        $objAccesoEmp->setIdUsuario($idUsuarioIngresado);
        $objAccesoEmp->InsertarAccesoEmpleado();

        $objEmpleado = new NombreEmpleadoLogeado();
        $objEmpleado->setIdEmpleado($idUsuarioIngresado);
        $nombreEmpleado = $objEmpleado->RetornarNombreUsuarioLogeado();

        // ----------- Iniciamos sesion ------------
        session_start();
        $_SESSION['IngresoSistema']=$idUsuarioIngresado; // ID DEL USUARIO INGRESADO AL SISTEMA PARA TRABAJAR PROCESOS ETC
        $_SESSION['NombreEmpIngresoSistema']=$nombreEmpleado; // NOMBRE DEL EMPLEADO LOGUEADO
        $_SESSION['RolEmpIngresoSistema']= $rolUsuarioIngresado; //ROL DEL EMPLEADO LOGUEADO
        //--------------------------------------------

        header( 'Location: ../../VIEW_CAP/AdmonEquipo/index.php' ) ;


    } else {
            echo "<script>";
            echo "alert('Ingrese datos correctos');";
            echo "window.location = '../../index.php';";
            echo "</script>";
    }
}
