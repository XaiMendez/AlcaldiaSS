<?php
include ("../../DAO_CAP/AdmonEquipo/variables_solicitudCompra.php");

if(isset($_POST['AgregarDetaSoli'])){
    
    $IdSolicitudMan = $_POST['txtIdSoliCompra'];
    $DescDetalleMan = $_POST['txtDescDetaSoliCompra'];
    $DescArticuloMan = $_POST['txtDescArticulo'];
    $CantidadMan = $_POST['txtCantidadDesSoli'];
    
    $objDetaSolicitudCompra = new DetaSolicitudCompra();
    $objDetaSolicitudCompra->setIdSolicitud($IdSolicitudMan);
    $objDetaSolicitudCompra->setDescDetalle($DescDetalleMan);
    $objDetaSolicitudCompra->setDescArticulo($DescArticuloMan);
    $objDetaSolicitudCompra->setCantidad($CantidadMan);
    
    $objDetaSolicitudCompra->IngresarDetaSolicitudCompra();
    
     echo "<script>";
     echo "alert('Detalle Almacenado');";  
     echo "window.location = '../../VIEW_CAP/AdmonEquipo/SolicitudDetaCompra.php';";
     echo "</script>";
    
}

if(isset($_POST['QuitarLineaDetaSoli'])){
    
    $IdSolicitudDetaMan = $_POST['txtIdSoliDetaCompra'];
    
    $objQuitarLineaSoli = new DetaSolicitudCompra();
    $objQuitarLineaSoli->setIdDetaSoli($IdSolicitudDetaMan);
    $objQuitarLineaSoli->QuitarLineaDetaSoliCompra();
    
    echo "<script>";
    echo "alert('Linea removida satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/AdmonEquipo/SolicitudDetaCompra.php';";
    echo "</script>";
       
}

if (isset($_POST['FinalizarSoli'])){
    
    echo "<script>";
    echo "alert('Solicitud Finalizada Satisfactoriamente');";  
    echo "window.location = '../../VIEW_CAP/AdmonEquipo/index.php';";
    echo "</script>";
}

if (isset($_POST['CancelarSoli'])){
    $IdSoliCompraMan = $_POST['txtIdSoliCompraCanc'];
    
    $objCancSoliCompra = new EncaSolicitudCompra();
    $objCancSoliCompra->setIdSolicitudCompra($IdSoliCompraMan);
    $objCancSoliCompra->CancelarSoliCompra();
    
    echo "<script>";
    echo "alert('Solicitud Cancelada');";  
    echo "window.location = '../../VIEW_CAP/AdmonEquipo/index.php';";
    echo "</script>";
    
}


