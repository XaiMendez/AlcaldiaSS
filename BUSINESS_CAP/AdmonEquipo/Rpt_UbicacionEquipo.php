<?php

require '../../DAO_CAP/AdmonEquipo/variables_equipoTecnologico.php';
require '../../Resources/FPDF/fpdf.php';

$rpt_ubicacionEquipoPdf = new FPDF('L','mm','A4');
$rpt_ubicacionEquipoPdf->AddPage();
$rpt_ubicacionEquipoPdf->SetFont('Arial','',10); //Tipo letra, Efecto, Tamaño
$rpt_ubicacionEquipoPdf->Image('../../Resources/img/login.png',17,10,20,20,'PNG'); //ancho, alto , eje x , eje y
$rpt_ubicacionEquipoPdf->Cell(18,10,'',0); //Cell() configuracion para celdas (largo,alto,'(texto o algo)',grosor de celda o borde)
$rpt_ubicacionEquipoPdf->Cell(230,10,'',0);
$rpt_ubicacionEquipoPdf->SetFont('Arial','',8);
$rpt_ubicacionEquipoPdf->Cell(50,10,'Fecha: '.date('d-m-y').'',0);
$rpt_ubicacionEquipoPdf->Ln(15); //SALTO DE LINEA (CANTIDAD DE SALTO)
$rpt_ubicacionEquipoPdf->SetFont('Arial','B',15);
$rpt_ubicacionEquipoPdf->Cell(88,8,'',0);
$rpt_ubicacionEquipoPdf->Cell(100,8,'REPORTE DE UBICACION DE EQUIPOS',0);
$rpt_ubicacionEquipoPdf->Ln(20);
$rpt_ubicacionEquipoPdf->SetFont('Arial','B',8);
//inicia encabezado de las columnas del reporte. totalizar 190 para una buena distribucion de tabla.
$rpt_ubicacionEquipoPdf->Cell(8,8,'ID',0);
$rpt_ubicacionEquipoPdf->Cell(15,8,'ID_REL',0);
$rpt_ubicacionEquipoPdf->Cell(15,8,'COD',0);
$rpt_ubicacionEquipoPdf->Cell(70,8,'DESCRIPCIÓN',0);
$rpt_ubicacionEquipoPdf->Cell(40,8,'FECHA',0);
$rpt_ubicacionEquipoPdf->Cell(40,8,'DISTRITO',0);
$rpt_ubicacionEquipoPdf->Cell(45,8,'AREA DISTRITO',0);
$rpt_ubicacionEquipoPdf->Cell(40,8,'EMPLEADO ASIGNADO',0);
$rpt_ubicacionEquipoPdf->Line(8, 52, 285, 52);
//fin de encabezados
$rpt_ubicacionEquipoPdf->Ln(8);
$rpt_ubicacionEquipoPdf->SetFont('Arial','',8);

//Hacemos la Consulta de nuestra capa DAO.
$idEquipoRpt = $_POST['cbxIdEquipos']; //capturamos el ID que esta en el combobox
$objRptUbicacionEquipo = new EquipoTecnologico();
$objRptUbicacionEquipo->setIdEquipo($idEquipoRpt);
$rsRptPdf = $objRptUbicacionEquipo->ReporteSelectUbicacionesEquipos();
while ($filaRptPdf = pg_fetch_assoc($rsRptPdf)){
    $rpt_ubicacionEquipoPdf->Cell(8,8,$filaRptPdf['id_eq'],0);
    $rpt_ubicacionEquipoPdf->Cell(15,8,$filaRptPdf['id_eq_rel'],0);
    $rpt_ubicacionEquipoPdf->Cell(15,8,$filaRptPdf['cod_eq'],0);
    $rpt_ubicacionEquipoPdf->Cell(70,8,$filaRptPdf['desc_eq'],0);
    $rpt_ubicacionEquipoPdf->Cell(40,8,$filaRptPdf['fecha_asig'],0);
    $rpt_ubicacionEquipoPdf->Cell(40,8,$filaRptPdf['distrito'],0);
    $rpt_ubicacionEquipoPdf->Cell(45,8,$filaRptPdf['area_dist'],0);
    $rpt_ubicacionEquipoPdf->Cell(40,8,$filaRptPdf['empasig'],0);
    $rpt_ubicacionEquipoPdf->Ln(5);
}
ob_end_clean(); //limpiamos el bufer de salida por la sobreescritura de la variable $rpt_ubicacionEquipoPdf, aveces da error...
$rpt_ubicacionEquipoPdf->Output(); //GENERAMOS EL REPORTE
?>

