<?php
include ("../../DAO_CAP/Carnetizacion/dao_reporte_solicitante_nombre.php");
require '../../Resources/FPDF/fpdf.php';

$rpt_nombrepdf = new FPDF('L','mm','A4');
$rpt_nombrepdf->AddPage();
$rpt_nombrepdf->SetFont('Arial','',10); //Tipo letra, Efecto, Tamaño
$rpt_nombrepdf->Image('../../Resources/img/logo.png',30,10,38,35,'PNG'); //ancho, alto , eje x , eje y
$rpt_nombrepdf->Cell(18,10,'',0); //Cell() configuracion para celdas (largo,alto,'(texto o algo)',grosor de celda o borde)
$rpt_nombrepdf->Cell(230,10,'',0);
$rpt_nombrepdf->SetFont('Arial','',8);
$rpt_nombrepdf->Cell(50,10,'Fecha: '.date('d-m-y').'',0);
$rpt_nombrepdf->Ln(15); //SALTO DE LINEA (CANTIDAD DE SALTO)
$rpt_nombrepdf->SetFont('Arial','B',15);
$rpt_nombrepdf->Cell(88,8,'',0);
$rpt_nombrepdf->Cell(100,8,'REPORTE POR SOLICITANTE',0);
$rpt_nombrepdf->Ln(20);
$rpt_nombrepdf->SetFont('Arial','B',8);
//inicia encabezado de las columnas del reporte. totalizar 190 para una buena distribucion de tabla.
$rpt_nombrepdf->Cell(36,8,'NOMBRES',0);
$rpt_nombrepdf->Cell(30,8,'APELLIDOS',0);
$rpt_nombrepdf->Cell(14,8,'GENERO',0);
$rpt_nombrepdf->Cell(28,8,'ANIO DE PARTIDA',0);
$rpt_nombrepdf->Cell(47,8,'CENTRO ESTUDIO/TRABAJO',0);
$rpt_nombrepdf->Cell(25,8,'PADRE',0);
$rpt_nombrepdf->Cell(20,8,'MADRE',0);
$rpt_nombrepdf->Cell(18,8,'ISDEM',0);
$rpt_nombrepdf->Cell(30,8,'FECHA EMISION',0);
$rpt_nombrepdf->Cell(40,8,'FECHA VENCIMIENTO',0);
$rpt_nombrepdf->Line(8, 52, 285, 52);
//fin de encabezados
$rpt_nombrepdf->Ln(8);
$rpt_nombrepdf->SetFont('Arial','',8);

//Hacemos la Consulta de nuestra capa DAO.
$apellido_menor = $_POST['apellido_menor'];//capturamos el ID que esta en el combobox
$objRptNombre = new reporte_nombre();
$objRptNombre->setIdhistorial($apellido_menor);
$rsRptPdf = $objRptNombre->reporteApellido();
while ($filaRptPdf = pg_fetch_assoc($rsRptPdf)){
    $rpt_nombrepdf->Cell(34,8,$filaRptPdf['nombres'],0);
    $rpt_nombrepdf->Cell(36,8,$filaRptPdf['apellidos'],0);
    $rpt_nombrepdf->Cell(20,8,$filaRptPdf['genero_persona'],0);
    $rpt_nombrepdf->Cell(20,8,$filaRptPdf['anio_partida'],0);
    $rpt_nombrepdf->Cell(45,8,$filaRptPdf['centro_estudio_trabajo'],0);
    $rpt_nombrepdf->Cell(25,8,$filaRptPdf['nombre_padre'],0);
    $rpt_nombrepdf->Cell(23,8,$filaRptPdf['nombre_madre'],0);
    $rpt_nombrepdf->Cell(12,8,$filaRptPdf['num_isdem'],0);
    $rpt_nombrepdf->Cell(35,8,$filaRptPdf['fecha_emision'],0);
    $rpt_nombrepdf->Cell(40,8,$filaRptPdf['fecha_vencimiento'],0);
    $rpt_nombrepdf->Ln(5);
}

ob_end_clean(); //limpiamos el bufer de salida por la sobreescritura de |la variable $rpt_ubicacionEquipoPdf, aveces da error...
$rpt_nombrepdf->Output(); 

//GENERAMOS EL REPORTE


?>