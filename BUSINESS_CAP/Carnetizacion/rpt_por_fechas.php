<?php
include ("../../DAO_CAP/Carnetizacion/dao_reporte_fecha.php");
require '../../Resources/FPDF/fpdf.php';

$rpt_fechapdf = new FPDF('L','mm','A4');
$rpt_fechapdf->AddPage();
$rpt_fechapdf->SetFont('Arial','',10); //Tipo letra, Efecto, Tamaño
$rpt_fechapdf->Image('../../Resources/img/logo.png',30,10,38,35,'PNG'); //ancho, alto , eje x , eje y
$rpt_fechapdf->Cell(18,10,'',0); //Cell() configuracion para celdas (largo,alto,'(texto o algo)',grosor de celda o borde)
$rpt_fechapdf->Cell(230,10,'',0);
$rpt_fechapdf->SetFont('Arial','',8);
$rpt_fechapdf->Cell(50,10,'Fecha: '.date('d-m-y').'',0);
$rpt_fechapdf->Ln(15); //SALTO DE LINEA (CANTIDAD DE SALTO)
$rpt_fechapdf->SetFont('Arial','B',15);
$rpt_fechapdf->Cell(88,8,'',0);
$rpt_fechapdf->Cell(100,8,'REPORTE POR FECHA',0);
$rpt_fechapdf->Ln(20);
$rpt_fechapdf->SetFont('Arial','B',8);
//inicia encabezado de las columnas del reporte. totalizar 190 para una buena distribucion de tabla.
$rpt_fechapdf->Cell(50,8,'FECHA EMISION',0);
$rpt_fechapdf->Cell(42,8,'NOMBRE COMPLETO',0);
$rpt_fechapdf->Cell(20,8,'GENERO',0);
$rpt_fechapdf->Cell(21,8,'ISDEM',0);
$rpt_fechapdf->Cell(42,8,'FECHA VENCIMIENTO',0);
$rpt_fechapdf->Cell(35,8,'PROCESO CARNET',0);
$rpt_fechapdf->Cell(28,8,'DOCUMENTO',0);
$rpt_fechapdf->Cell(30,8,'NUMERO FACTURA',0);
$rpt_fechapdf->Line(8, 52, 285, 52);
//fin de encabezados
$rpt_fechapdf->Ln(8);
$rpt_fechapdf->SetFont('Arial','',8);

//Hacemos la Consulta de nuestra capa DAO.
$prm1 = $_POST['fecha1'];
$prm2 = $_POST['fecha2'];//capturamos el ID que esta en el combobox
$objRptFecha = new reporte_fecha();
$objRptFecha->setparametro1Fecha($prm1);
$objRptFecha->setparametro2Fecha($prm2);
$rsRptPdf = $objRptFecha->selectidfecha();
while ($filaRptPdf = pg_fetch_assoc($rsRptPdf)){
    $rpt_fechapdf->Cell(44,8,$filaRptPdf['fecha_emision'],0);
    $rpt_fechapdf->Cell(53,8,$filaRptPdf['nombre_completo'],0);
    $rpt_fechapdf->Cell(17,8,$filaRptPdf['genero_persona'],0);
    $rpt_fechapdf->Cell(20,8,$filaRptPdf['num_isdem'],0);
    $rpt_fechapdf->Cell(44,8,$filaRptPdf['fecha_vencimiento'],0);
    $rpt_fechapdf->Cell(40,8,$filaRptPdf['tipo_proceso_carnet'],0);
    $rpt_fechapdf->Cell(26,8,$filaRptPdf['documentos_emision'],0);
    $rpt_fechapdf->Cell(12,8,$filaRptPdf['num_factura'],0);
    
    $rpt_fechapdf->Ln(5);
}

ob_end_clean(); //limpiamos el bufer de salida por la sobreescritura de |la variable $rpt_ubicacionEquipoPdf, aveces da error...
$rpt_fechapdf->Output(); 

//GENERAMOS EL REPORTE


?>