<?php

include_once("../../DAO_CAP/Connection/Connection.class.php");

class modelEstadoCaso{

  private $id_EstadoCaso;
	private $estado_Caso;

	public function getId_EstadoCaso(){
		return $this->id_EstadoCaso;
	}

	public function setId_EstadoCaso($id_EstadoCaso){
		$this->id_EstadoCaso = $id_EstadoCaso;
	}

	public function getEstado_Caso(){
		return $this->estado_Caso;
	}

	public function setEstado_Caso($estado_Caso){
		$this->estado_Caso = $estado_Caso;
	}


  function create(){
     $objConnection = new Connection();
    $sql="SELECT * FROM HELPDESK_INSERT_ESTADOCASO()";
     $result=$objConnection->Consultas($sql);

     if (!$result) {
       echo "An error occurred.\n";
       exit;
     }
     return $result;
  }

  function findAll(){
     $objConnection = new Connection();
     $sql="SELECT * FROM HELPDESK_FINDALL_ESTADOCASO()";
     $result=$objConnection->Consultas($sql);

     if (!$result) {
       echo "An error occurred.\n";
       exit;
     }
     return $result;
  }

  function update($id){
     $objConnection = new Connection();
    $sql="SELECT * FROM HELPDESK_UPDATE_ESTADOCASO()";
     $result=$objConnection->Consultas($sql);

     if (!$result) {
       echo "An error occurred.\n";
       exit;
     }
     return $result;
  }

  function delete($id){
     $objConnection = new Connection();
    $sql="SELECT * FROM HELPDESK_DELETE_ESTADOCASO()";
     $result=$objConnection->Consultas($sql);

     if (!$result) {
       echo "An error occurred.\n";
       exit;
     }
     return $result;
  }


}




 ?>
