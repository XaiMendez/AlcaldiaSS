<?php

//
include("../../DAO_CAP/Conexion/admon_conexion.php");

class RevisionCaso {

    private $id_solicitud_caso = "";
    private $descripcion_caso = "";
    private $fecha_caso = "";
    private $id_deta_empleado_solicitante = "";
    private $id_estado_caso = "";

    public function getId_solicitud_caso() {
        return $this->id_solicitud_caso;
    }

    public function setId_solicitud_caso($id_solicitud_caso) {
        $this->id_solicitud_caso = $id_solicitud_caso;
    }

    public function getDescripcion_caso() {
        return $this->descripcion_caso;
    }

    public function setDescripcion_caso($descripcion_caso) {
        $this->descripcion_caso = $descripcion_caso;
    }

    public function getId_deta_empleado() {
        return $this->id_deta_empleado;
    }

    public function setId_deta_persona($id_deta_empleado) {
        $this->id_deta_empleado = $id_deta_empleado;
    }

    public function getFecha_caso() {
        return $this->fecha_caso;
    }

    public function setfecha_caso($fecha_caso) {
        $this->fecha_caso = $fecha_caso;
    }

    public function getId_estado_caso() {
        return $this->id_estado_caso;
    }

    public function setId_estado_caso($id_estado_caso) {
        $this->id_estado_caso = $id_estado_caso;
    }

    //--------------------------------------------

    function SelectTodosCasos(){
        $query = "SELECT
                A.ID_SOLICITUD_CASO,
                A.DESCRIPCION_CASO,
                A.FECHA,
                (C.CODIGO_EMPLEADO||' '||D.NOMBRE1_PERSONA||' '||D.NOMBRE2_PERSONA)AS SOLICITANTE ,
                (F.DETALLE_DISTRITO||' , '||E.AREA)AS UBICACIONSOLI,
                B.ESTADO_CASO,
                A1.CODIGO_EQUIPO,
                A.CALIFICACION_CASO
                FROM SOLICITUD_CASO A
                LEFT JOIN EQUIPO_TECNOLOGICO A1 ON (A.ID_EQUIPO = A1.ID_EQUIPO)
                INNER JOIN ESTADO_CASO B ON (A.ID_ESTADO_CASO = B.ID_ESTADO_CASO)
                INNER JOIN DETALLE_EMPLEADO C ON (A.ID_DETA_EMPLEADO_SOLICITANTE = C.ID_DETA_EMPLEADO)
                INNER JOIN PERSONA D ON (C.ID_PERSONA = D.ID_PERSONA)
                INNER JOIN AREA_DISTRITO E ON (C.ID_AREA_DISTRITO = E.ID_AREA_DISTRITO)
                INNER JOIN DISTRITO F ON (F.ID_DISTRITO = E.ID_DISTRITO)
                ORDER BY A.ID_SOLICITUD_CASO ASC
                ";

        $rs = pg_query($query);
        return $rs;
    }

    function SelectAsignarCaso() {

        $query = "SELECT
                A.ID_SOLICITUD_CASO,
                A.DESCRIPCION_CASO,
                A.FECHA,
                (C.CODIGO_EMPLEADO||' '||D.NOMBRE1_PERSONA||' '||D.NOMBRE2_PERSONA)AS SOLICITANTE ,
                (F.DETALLE_DISTRITO||' , '||E.AREA)AS UBICACIONSOLI,
                B.ESTADO_CASO,
                A1.CODIGO_EQUIPO
                FROM SOLICITUD_CASO A
                LEFT JOIN EQUIPO_TECNOLOGICO A1 ON (A.ID_EQUIPO = A1.ID_EQUIPO)
                INNER JOIN ESTADO_CASO B ON (A.ID_ESTADO_CASO = B.ID_ESTADO_CASO)
                INNER JOIN DETALLE_EMPLEADO C ON (A.ID_DETA_EMPLEADO_SOLICITANTE = C.ID_DETA_EMPLEADO)
                INNER JOIN PERSONA D ON (C.ID_PERSONA = D.ID_PERSONA)
                INNER JOIN AREA_DISTRITO E ON (C.ID_AREA_DISTRITO = E.ID_AREA_DISTRITO)
                INNER JOIN DISTRITO F ON (F.ID_DISTRITO = E.ID_DISTRITO)
                WHERE B.ID_ESTADO_CASO = 1
                ORDER BY A.ID_SOLICITUD_CASO ASC
                ";

        $rs = pg_query($query);
        return $rs;
    }

    function SelectFinalizarCaso() {

        $query = "SELECT
                A.ID_SOLICITUD_CASO,
                A.DESCRIPCION_CASO,
                A.FECHA,
                (C.CODIGO_EMPLEADO||' '||D.NOMBRE1_PERSONA||' '||D.NOMBRE2_PERSONA)AS SOLICITANTE ,
                (F.DETALLE_DISTRITO||' , '||E.AREA)AS UBICACIONSOLI,
                B.ESTADO_CASO,
                A1.CODIGO_EQUIPO,
                A.CALIFICACION_CASO
                FROM SOLICITUD_CASO A
                LEFT JOIN EQUIPO_TECNOLOGICO A1 ON (A.ID_EQUIPO = A1.ID_EQUIPO)
                INNER JOIN ESTADO_CASO B ON (A.ID_ESTADO_CASO = B.ID_ESTADO_CASO)
                INNER JOIN DETALLE_EMPLEADO C ON (A.ID_DETA_EMPLEADO_SOLICITANTE = C.ID_DETA_EMPLEADO)
                INNER JOIN PERSONA D ON (C.ID_PERSONA = D.ID_PERSONA)
                INNER JOIN AREA_DISTRITO E ON (C.ID_AREA_DISTRITO = E.ID_AREA_DISTRITO)
                INNER JOIN DISTRITO F ON (F.ID_DISTRITO = E.ID_DISTRITO)
                WHERE B.ID_ESTADO_CASO = 4
                ORDER BY A.ID_SOLICITUD_CASO ASC
                ";

        $rsFinalizarCaso = pg_query($query);
        return $rsFinalizarCaso;
    }

    function SelectCasosAbiertos() {

        $query = "  SELECT
                    A.ID_SOLICITUD_CASO,
                    A.DESCRIPCION_CASO,
                    B.ESTADO_CASO,
                    A.FECHA,
                    (C.CODIGO_EMPLEADO||' '||D.NOMBRE1_PERSONA||' '||D.APELLIDO1_PERSONA)AS SOLICITANTE,
                    UBICACION.UBICACIONSOLI,
                    E.CODIGO_EQUIPO,
                    G.ID_ESCALABILIDAD,
                    G.ESCALABILIDAD,
                    G.TIPO_CASO
                    FROM SOLICITUD_CASO A
                    INNER JOIN ESTADO_CASO B ON (A.ID_ESTADO_CASO = B.ID_ESTADO_CASO)
                    INNER JOIN DETALLE_EMPLEADO C ON (A.ID_DETA_EMPLEADO_SOLICITANTE = C.ID_DETA_EMPLEADO)
                    INNER JOIN 
                    (
			     SELECT 
			     AREA.ID_AREA_DISTRITO,(DIST.DETALLE_DISTRITO||' '||AREA.AREA)AS UBICACIONSOLI
			     FROM DISTRITO DIST 
			     INNER JOIN AREA_DISTRITO AREA ON (DIST.ID_DISTRITO = AREA.ID_DISTRITO)
                    ) UBICACION ON (C.ID_AREA_DISTRITO = UBICACION.ID_AREA_DISTRITO)
                    INNER JOIN PERSONA D ON (D.ID_PERSONA = C.ID_PERSONA)
                    INNER JOIN EQUIPO_TECNOLOGICO E ON (E.ID_EQUIPO = A.ID_EQUIPO)
                    LEFT JOIN
                    (
			SELECT 
			T1.ID_SOLICITUD_CASO,
			T1.MAXBITACORA,
			T2.ID_ESCALABILIDAD,
			T2.ESCALABILIDAD,
			T2.TIPO_CASO
			FROM
			(
                            SELECT 
                            A2.ID_SOLICITUD_CASO,
                            MAX(A2.ID_BITACORA_CASO)AS MAXBITACORA
                            FROM BITACORA_CASO A2
                            GROUP BY A2.ID_SOLICITUD_CASO
                            ORDER BY 1
			)AS T1
			INNER JOIN 
			(
                            SELECT 
                            A3.ID_SOLICITUD_CASO,
                            A3.ID_BITACORA_CASO,
                            C3.TIPO_CASO,
                            B3.ID_ESCALABILIDAD,
                            B3.ESCALABILIDAD 
                            FROM BITACORA_CASO A3 
                            LEFT JOIN ESCALABILIDAD B3 ON (A3.ID_ESCALABILIDAD = B3.ID_ESCALABILIDAD)
                            LEFT JOIN TIPO_CASO C3 ON (C3.ID_TIPO_CASO = A3.ID_TIPO_CASO)
                            ORDER BY 1
			) AS T2 ON (T1.MAXBITACORA = T2.ID_BITACORA_CASO)
                    ) G ON (G.ID_SOLICITUD_CASO = A.ID_SOLICITUD_CASO)
                    WHERE B.ESTADO_CASO = 'ABIERTO'
                    ORDER BY G.ID_ESCALABILIDAD DESC, FECHA DESC;";

        $rsFinalizarCaso = pg_query($query);
        return $rsFinalizarCaso;
    }

    function SelectCasosFinalizados() {


        $query = "SELECT
                      A.ID_SOLICITUD_CASO,
                      A.DESCRIPCION_CASO,
                      A.FECHA,
                      (C.CODIGO_EMPLEADO||' '||D.NOMBRE1_PERSONA||' '||D.NOMBRE2_PERSONA)AS SOLICITANTE ,
                      (F.DETALLE_DISTRITO||' , '||E.AREA)AS UBICACIONSOLI,
                      B.ESTADO_CASO
                      FROM SOLICITUD_CASO A
                      INNER JOIN ESTADO_CASO B ON (A.ID_ESTADO_CASO = B.ID_ESTADO_CASO)
                      INNER JOIN DETALLE_EMPLEADO C ON (A.ID_DETA_EMPLEADO_SOLICITANTE = C.ID_DETA_EMPLEADO)
                      INNER JOIN PERSONA D ON (C.ID_PERSONA = D.ID_PERSONA)
                      INNER JOIN AREA_DISTRITO E ON (C.ID_AREA_DISTRITO = E.ID_AREA_DISTRITO)
                      INNER JOIN DISTRITO F ON (F.ID_DISTRITO = E.ID_DISTRITO)
                      WHERE B.ID_ESTADO_CASO = 4
                      ORDER BY A.ID_SOLICITUD_CASO ASC
                      ";

        $rs = pg_query($query);
        return $rs;
    }

    function casos_cerrar() {

        $select = "SELECT * FROM solicitud_caso
		INNER JOIN detalle_empleado ON solicitud_caso.id_deta_empleado_solicitante= detalle_empleado.id_deta_empleado
		INNER JOIN estado_caso ON solicitud_caso.id_estado_caso= estado_caso.id_estado_caso
		WHERE id_estado_caso= '4' ORDER BY fecha_caso ASC";

        $result = pg_query($con, $select);

        echo "Fetch row: <br>";

        while ($row = pg_fetch_row($result)) {
            print_r($row[1]);

            pg_close($con);
        }
    }

    //-----

    function SelectFinalizarCasoById($id) {
        $idEstadoAprov = 4;
        $queryActualizarSolicitud = "UPDATE SOLICITUD_CASO SET ID_ESTADO_CASO = $idEstadoAprov WHERE ID_SOLICITUD_CASO = $id";
        pg_query($queryActualizarSolicitud);

        return $rs;
    }

}

class AsignarFinalizarCaso {

    private $descAsignacion = ""; // primera bitacora
    private $idSolicitudCaso = ""; //caso aprovar
    private $idTipoCaso = "";
    private $idTecnicoAsig = "";
    private $idEscabilidad = "";
    private $idEmpAsignaCaso = "";

    public function getDescAsignacion() {
        return $this->descAsignacion;
    }

    public function getIdSolicitudCaso() {
        return $this->idSolicitudCaso;
    }

    public function getIdTipoCaso() {
        return $this->idTipoCaso;
    }

    public function getIdTecnicoAsig() {
        return $this->idTecnicoAsig;
    }

    public function getIdEscabilidad() {
        return $this->idEscabilidad;
    }

    public function getIdEmpAsignaCaso() {
        return $this->idEmpAsignaCaso;
    }

    public function setDescAsignacion($descAsignacion) {
        $this->descAsignacion = $descAsignacion;
    }

    public function setIdSolicitudCaso($idSolicitudCaso) {
        $this->idSolicitudCaso = $idSolicitudCaso;
    }

    public function setIdTipoCaso($idTipoCaso) {
        $this->idTipoCaso = $idTipoCaso;
    }

    public function setIdTecnicoAsig($idTecnicoAsig) {
        $this->idTecnicoAsig = $idTecnicoAsig;
    }

    public function setIdEscabilidad($idEscabilidad) {
        $this->idEscabilidad = $idEscabilidad;
    }

    public function setIdEmpAsignaCaso($idEmpAsignaCaso) {
        $this->idEmpAsignaCaso = $idEmpAsignaCaso;
    }

    function SelectCasoParaAsignar() {
        $fcnIdSolicitudCaso = $this->getIdSolicitudCaso();
        $query = "SELECT
                A.ID_SOLICITUD_CASO,
                A.DESCRIPCION_CASO,
                A.FECHA,
                (C.CODIGO_EMPLEADO||' '||D.NOMBRE1_PERSONA||' '||D.NOMBRE2_PERSONA)AS SOLICITANTE ,
                (F.DETALLE_DISTRITO||' , '||E.AREA)AS UBICACIONSOLI,
                B.ESTADO_CASO,
                A1.CODIGO_EQUIPO
                FROM SOLICITUD_CASO A
                LEFT JOIN EQUIPO_TECNOLOGICO A1 ON (A.ID_EQUIPO = A1.ID_EQUIPO)
                INNER JOIN ESTADO_CASO B ON (A.ID_ESTADO_CASO = B.ID_ESTADO_CASO)
                INNER JOIN DETALLE_EMPLEADO C ON (A.ID_DETA_EMPLEADO_SOLICITANTE = C.ID_DETA_EMPLEADO)
                INNER JOIN PERSONA D ON (C.ID_PERSONA = D.ID_PERSONA)
                INNER JOIN AREA_DISTRITO E ON (C.ID_AREA_DISTRITO = E.ID_AREA_DISTRITO)
                INNER JOIN DISTRITO F ON (F.ID_DISTRITO = E.ID_DISTRITO)
                WHERE A.ID_SOLICITUD_CASO = $fcnIdSolicitudCaso";

        $rs = pg_query($query);

        return $rs;
    }

    function AsignarCaso() {
        $fcnDescAsig = $this->getDescAsignacion();
        $fcnIdSolicitudCaso = $this->getIdSolicitudCaso();
        $fcnIdTipoCaso = $this->getIdTipoCaso();
        //$fcnIdTecnicoAsig = $this->getIdTecnicoAsig();
        $idEscalabilidad = $this->getIdEscabilidad();
        //$fcnIdEquipoTec = 'NULL';
        $fcnIdEmpleadoAsigna = $this->getIdEmpAsignaCaso();


        $query = "INSERT INTO bitacora_caso(
                    descripcion_seguimiento, fecha_seguimiento,
                    id_solicitud_caso, id_tipo_caso, id_escalabilidad,
                    id_deta_empleado_crea)
                    VALUES
                    ('$fcnDescAsig', current_timestamp, $fcnIdSolicitudCaso, $fcnIdTipoCaso, $idEscalabilidad, $fcnIdEmpleadoAsigna)";

        pg_query($query);

        // ACTUALIZAR SOLICITUD DE CASO A ABIERTO
        $queryEstadoAprovado = "SELECT ID_ESTADO_CASO FROM ESTADO_CASO WHERE ESTADO_CASO = 'ABIERTO'";
        $rsEstadoAprov = pg_query($queryEstadoAprovado);
        $idEstadoAprov = pg_fetch_result($rsEstadoAprov, 0);
        
        // en caso de reabrir limpiamos la calificacion pasada
        $queryActualizarSolicitud = "UPDATE SOLICITUD_CASO SET ID_ESTADO_CASO = $idEstadoAprov, CALIFICACION_CASO = NULL WHERE ID_SOLICITUD_CASO = $fcnIdSolicitudCaso";
        pg_query($queryActualizarSolicitud);
        
        // INSERTAMOS LA BITACORA DEL ESTADO DEL CASO
        $queryInsertarBitacoraEstado = "INSERT INTO BITACORA_ESTADO_CASO 
                                        (ID_SOLICITUD_CASO,ID_ESTADO_CASO,FECHA_ESTADO,ID_DETA_EMPLEADO_CREA) 
                                        VALUES 
                                        ($fcnIdSolicitudCaso,$idEstadoAprov,CURRENT_TIMESTAMP,$fcnIdEmpleadoAsigna);";
        pg_query($queryInsertarBitacoraEstado);
        
    }
    function FinalizarCaso(){
      $fcnIdSolicitudCaso = $this->getIdSolicitudCaso();
      $fcnDescAsig = $this->getDescAsignacion();
      $fcnIdEmpleadoAsigna = $this->getIdEmpAsignaCaso();

      $query = "INSERT INTO bitacora_caso(
                  descripcion_seguimiento, fecha_seguimiento,
                  id_solicitud_caso, id_deta_empleado_crea)
                  VALUES
                  ('$fcnDescAsig', current_timestamp, $fcnIdSolicitudCaso, $fcnIdEmpleadoAsigna)";

      pg_query($query);

      $queryEstadoDenegado = "SELECT ID_ESTADO_CASO FROM ESTADO_CASO WHERE ESTADO_CASO = 'FINALIZADO'";
      $rsEstadoDeneg = pg_query($queryEstadoDenegado);
      $idEstadoDeneg = pg_fetch_result($rsEstadoDeneg, 0);

      $queryActualizarSolicitud = "UPDATE SOLICITUD_CASO SET ID_ESTADO_CASO = $idEstadoDeneg WHERE ID_SOLICITUD_CASO = $fcnIdSolicitudCaso";
      pg_query($queryActualizarSolicitud);
      
      // INSERTAMOS LA BITACORA DEL ESTADO DEL CASO
        $queryInsertarBitacoraEstado = "INSERT INTO BITACORA_ESTADO_CASO 
                                        (ID_SOLICITUD_CASO,ID_ESTADO_CASO,FECHA_ESTADO,ID_DETA_EMPLEADO_CREA) 
                                        VALUES 
                                        ($fcnIdSolicitudCaso,$idEstadoDeneg,CURRENT_TIMESTAMP,$fcnIdEmpleadoAsigna);";
        pg_query($queryInsertarBitacoraEstado);
        
    }
    function DenegarCaso(){
        $fcnDescAsig = $this->getDescAsignacion();
        $fcnIdSolicitudCaso = $this->getIdSolicitudCaso();
        $fcnIdTipoCaso = $this->getIdTipoCaso();
        //$fcnIdTecnicoAsig = $this->getIdTecnicoAsig();
        $idEscalabilidad = $this->getIdEscabilidad();
        //$fcnIdEquipoTec = 'NULL';
        $fcnIdEmpleadoAsigna = $this->getIdEmpAsignaCaso();


        $query = "INSERT INTO bitacora_caso(
                    descripcion_seguimiento, fecha_seguimiento,
                    id_solicitud_caso, id_tipo_caso, id_escalabilidad,
                    id_deta_empleado_crea)
                    VALUES
                    ('$fcnDescAsig', current_timestamp, $fcnIdSolicitudCaso, $fcnIdTipoCaso, $idEscalabilidad, $fcnIdEmpleadoAsigna)";

        pg_query($query);

        // ACTUALIZAR SOLICITUD DE CASO A DENEGADO
        $queryEstadoDenegado = "SELECT ID_ESTADO_CASO FROM ESTADO_CASO WHERE ESTADO_CASO = 'DENEGADO'";
        $rsEstadoDeneg = pg_query($queryEstadoDenegado);
        $idEstadoDeneg = pg_fetch_result($rsEstadoDeneg, 0);

        $queryActualizarSolicitud = "UPDATE SOLICITUD_CASO SET ID_ESTADO_CASO = $idEstadoDeneg WHERE ID_SOLICITUD_CASO = $fcnIdSolicitudCaso";
        pg_query($queryActualizarSolicitud);
        
        // INSERTAMOS LA BITACORA DEL ESTADO DEL CASO
        $queryInsertarBitacoraEstado = "INSERT INTO BITACORA_ESTADO_CASO 
                                        (ID_SOLICITUD_CASO,ID_ESTADO_CASO,FECHA_ESTADO,ID_DETA_EMPLEADO_CREA) 
                                        VALUES 
                                        ($fcnIdSolicitudCaso,$idEstadoDeneg,CURRENT_TIMESTAMP,$fcnIdEmpleadoAsigna);";
        pg_query($queryInsertarBitacoraEstado);
    }

}

?>
