<?php

include("../../DAO_CAP/HelpDesk/model.revision_caso.php");

class GestionBitacoras extends AsignarFinalizarCaso {

    private $idCasoTecnico = "";
    private $idEmpAgregaQuita = "";
    private $idTecnicoAsignado = "";

    public function getIdCasoTecnico() {
        return $this->idCasoTecnico;
    }

    public function setIdCasoTecnico($idCasoTecnico) {
        $this->idCasoTecnico = $idCasoTecnico;
    }

    public function getIdEmpAgregaQuita() {
        return $this->idEmpAgregaQuita;
    }

    public function setIdEmpAgregaQuita($idEmpAgregaQuita) {
        $this->idEmpAgregaQuita = $idEmpAgregaQuita;
    }

    public function getIdTecnicoAsignado() {
        return $this->idTecnicoAsignado;
    }

    public function setIdTecnicoAsignado($idTecnicoAsignado) {
        $this->idTecnicoAsignado = $idTecnicoAsignado;
    }



    function IngresarBitacora(){
        $fcnDescBitacora = $this->getDescAsignacion();
        $fcnIdSolicitudCaso = $this->getIdSolicitudCaso();
        $fcnIdTipoCaso = $this->getIdTipoCaso();
        $fcnIdEmpleadoCrea = $this->getIdEmpAsignaCaso();

        $QueryEscalabilidad = "SELECT
                                ID_ESCALABILIDAD
                                FROM BITACORA_CASO
                                WHERE ID_BITACORA_CASO = (SELECT MAX(ID_BITACORA_CASO) FROM BITACORA_CASO WHERE ID_SOLICITUD_CASO = $fcnIdSolicitudCaso)";
        $rsEscalabilidad = pg_query($QueryEscalabilidad);
        $idEscalabilidad = pg_fetch_result($rsEscalabilidad, 0);

        $query = "INSERT INTO bitacora_caso(
                    descripcion_seguimiento, fecha_seguimiento,
                    id_solicitud_caso, id_tipo_caso, id_escalabilidad, id_deta_empleado_crea)
                    VALUES
                    ('$fcnDescBitacora',CURRENT_TIMESTAMP,$fcnIdSolicitudCaso,$fcnIdTipoCaso,$idEscalabilidad,$fcnIdEmpleadoCrea)";
        pg_query($query);


    }

    function CompletarCaso(){
        $fcnDescBitacora = $this->getDescAsignacion();
        $fcnIdSolicitudCaso = $this->getIdSolicitudCaso();
        $fcnIdTipoCaso = $this->getIdTipoCaso();
        $fcnIdEmpleadoCrea = $this->getIdEmpAsignaCaso();

        //buscamos la ultima escalabilidad sea el caso que la actualice el admin
        $QueryEscalabilidad = "SELECT
                                ID_ESCALABILIDAD
                                FROM BITACORA_CASO
                                WHERE ID_BITACORA_CASO = (SELECT MAX(ID_BITACORA_CASO) FROM BITACORA_CASO WHERE ID_SOLICITUD_CASO = $fcnIdSolicitudCaso)";
        $rsEscalabilidad = pg_query($QueryEscalabilidad);
        $idEscalabilidad = pg_fetch_result($rsEscalabilidad, 0);

        $query = "INSERT INTO bitacora_caso(
                    descripcion_seguimiento, fecha_seguimiento,
                    id_solicitud_caso, id_tipo_caso, id_escalabilidad,id_deta_empleado_crea)
                    VALUES
                    ('$fcnDescBitacora',CURRENT_TIMESTAMP,$fcnIdSolicitudCaso,$fcnIdTipoCaso,$idEscalabilidad,$fcnIdEmpleadoCrea)";
        pg_query($query);

        //obtenemos el id del estado compleatado
        $queryIdFinalizado = "SELECT ID_ESTADO_CASO FROM ESTADO_CASO WHERE ESTADO_CASO = 'COMPLETADO'";
        $rsEstadoFin = pg_query($queryIdFinalizado);
        $idEstadoFin = pg_fetch_result($rsEstadoFin, 0);

        //ACTUALIZAMOS EL CASO A COMPLETADO
        $queryUpdateCaso = "UPDATE SOLICITUD_CASO SET ID_ESTADO_CASO = $idEstadoFin WHERE ID_SOLICITUD_CASO = $fcnIdSolicitudCaso";
        pg_query($queryUpdateCaso);
        
        // INSERTAMOS LA BITACORA DEL ESTADO DEL CASO
        $queryInsertarBitacoraEstado = "INSERT INTO BITACORA_ESTADO_CASO 
                                        (ID_SOLICITUD_CASO,ID_ESTADO_CASO,FECHA_ESTADO,ID_DETA_EMPLEADO_CREA) 
                                        VALUES 
                                        ($fcnIdSolicitudCaso,$idEstadoFin,CURRENT_TIMESTAMP,$fcnIdEmpleadoCrea);";
        pg_query($queryInsertarBitacoraEstado);

    }

    function SelectCasosPorTecnicoAbiertos($idTecnicoAsignado){
        $query ="   SELECT
                    A.ID_SOLICITUD_CASO,
                    A.DESCRIPCION_CASO,
                    B.ESTADO_CASO,
                    A.FECHA,
                    (C.CODIGO_EMPLEADO||' '||D.NOMBRE1_PERSONA||' '||D.APELLIDO1_PERSONA)AS SOLICITANTE,
                    E.CODIGO_EQUIPO,
                    G.ID_ESCALABILIDAD,
                    G.ESCALABILIDAD,
                    G.TIPO_CASO
                    FROM SOLICITUD_CASO A
                    INNER JOIN ESTADO_CASO B ON (A.ID_ESTADO_CASO = B.ID_ESTADO_CASO)
                    INNER JOIN DETALLE_EMPLEADO C ON (A.ID_DETA_EMPLEADO_SOLICITANTE = C.ID_DETA_EMPLEADO)
                    INNER JOIN PERSONA D ON (D.ID_PERSONA = C.ID_PERSONA)
                    INNER JOIN EQUIPO_TECNOLOGICO E ON (E.ID_EQUIPO = A.ID_EQUIPO)
                    INNER JOIN
                    (
                        SELECT
                        ID_DETA_EMPLEADO_TECNICO,
                        ID_SOLICITUD_CASO
                        FROM CASO_TECNICO
                        WHERE
                        ACTIVIO = TRUE
                        AND ID_DETA_EMPLEADO_TECNICO = $idTecnicoAsignado
                    ) F ON (F.ID_SOLICITUD_CASO = A.ID_SOLICITUD_CASO)
                    LEFT JOIN
                    (
			SELECT 
			T1.ID_SOLICITUD_CASO,
			T1.MAXBITACORA,
			T2.ID_ESCALABILIDAD,
			T2.ESCALABILIDAD,
			T2.TIPO_CASO
			FROM
			(
                            SELECT 
                            A2.ID_SOLICITUD_CASO,
                            MAX(A2.ID_BITACORA_CASO)AS MAXBITACORA
                            FROM BITACORA_CASO A2
                            GROUP BY A2.ID_SOLICITUD_CASO
                            ORDER BY 1
			)AS T1
			INNER JOIN 
			(
                            SELECT 
                            A3.ID_SOLICITUD_CASO,
                            A3.ID_BITACORA_CASO,
                            C3.TIPO_CASO,
                            B3.ID_ESCALABILIDAD,
                            B3.ESCALABILIDAD 
                            FROM BITACORA_CASO A3 
                            LEFT JOIN ESCALABILIDAD B3 ON (A3.ID_ESCALABILIDAD = B3.ID_ESCALABILIDAD)
                            LEFT JOIN TIPO_CASO C3 ON (C3.ID_TIPO_CASO = A3.ID_TIPO_CASO)
                            ORDER BY 1
			) AS T2 ON (T1.MAXBITACORA = T2.ID_BITACORA_CASO)
                    ) G ON (G.ID_SOLICITUD_CASO = A.ID_SOLICITUD_CASO)
                    WHERE B.ESTADO_CASO = 'ABIERTO'
                    ORDER BY G.ID_ESCALABILIDAD DESC, FECHA;";
        $rs = pg_query($query);

        return $rs;
    }

    function SelectCasosPorTecnicoCompletadoFinalizado($idTecnicoAsignado){
        $query = "";
        $rs = pg_query($query);

        return $rs;
    }


    function SelectCasoVista($idCaso){
        $query = "SELECT
                    A.ID_SOLICITUD_CASO,
                    A.DESCRIPCION_CASO,
                    A.FECHA,
                    A.ID_DETA_EMPLEADO_SOLICITANTE,
                    B.ID_DETA_EMPLEADO,
                    (B.CODIGO_EMPLEADO||' '||D.NOMBRE1_PERSONA||' '||D.APELLIDO1_PERSONA)AS SOLICITANTE,
                    C.ESTADO_CASO,
                    A1.CODIGO_EQUIPO,
                    A.CALIFICACION_CASO
                    FROM SOLICITUD_CASO A
                    LEFT JOIN EQUIPO_TECNOLOGICO A1 ON (A.ID_EQUIPO = A1.ID_EQUIPO)
                    INNER JOIN DETALLE_EMPLEADO B ON (A.ID_DETA_EMPLEADO_SOLICITANTE = B.ID_DETA_EMPLEADO)
                    INNER JOIN ESTADO_CASO C ON (A.ID_ESTADO_CASO = C.ID_ESTADO_CASO)
                    INNER JOIN PERSONA D ON (B.ID_PERSONA = D.ID_PERSONA)
                    WHERE A.ID_SOLICITUD_CASO = $idCaso";
        $rs = pg_query($query);

        return $rs;
    }

    function SelectBitacorasGestionCaso($idCaso){
        $query = "SELECT
                    A.ID_BITACORA_CASO,
                    A.DESCRIPCION_SEGUIMIENTO,
                    A.FECHA_SEGUIMIENTO,
                    B.TIPO_CASO,
                    C.ESCALABILIDAD,
                    EMP.EMPLEADO
                    FROM BITACORA_CASO A
                    LEFT JOIN TIPO_CASO B ON (A.ID_TIPO_CASO = B.ID_TIPO_CASO)
                    LEFT JOIN ESCALABILIDAD C ON (A.ID_ESCALABILIDAD = C.ID_ESCALABILIDAD)
                    INNER JOIN
                    (
              			SELECT
              			A.ID_DETA_EMPLEADO,
              			(A.CODIGO_EMPLEADO||' '||NOMBRE1_PERSONA||' '||APELLIDO1_PERSONA)AS EMPLEADO
              			FROM
              			DETALLE_EMPLEADO A
              			INNER JOIN PERSONA B ON (A.ID_PERSONA = B.ID_PERSONA)
              		    ) EMP ON (EMP.ID_DETA_EMPLEADO = A.ID_DETA_EMPLEADO_CREA)
                                  WHERE A.ID_SOLICITUD_CASO = $idCaso
                                  ORDER BY FECHA_SEGUIMIENTO;";
        $rs = pg_query($query);

        return $rs;
    }

    function SelectTecnicosEnCasoActivo($idCaso){
        $query = "SELECT
                    A.ID_CASO_TECNICO,
                    A.ID_DETA_EMPLEADO_TECNICO,
                    B.CODIGO_EMPLEADO,
                    (D.NOMBRE1_PERSONA||' '||D.NOMBRE2_PERSONA||' '||APELLIDO1_PERSONA||' '||D.APELLIDO2_PERSONA)AS NOMBRE,
                    E.ESPECIALIDAD_TECNICO,
                    A.FECHA_OPERACION,
                    C.CODIGO_EMPLEADO
                    FROM CASO_TECNICO A
                    INNER JOIN DETALLE_EMPLEADO B ON (A.ID_DETA_EMPLEADO_TECNICO = B.ID_DETA_EMPLEADO)
                    INNER JOIN DETALLE_EMPLEADO C ON (A.ID_DETA_EMPLEADO_CREA = C.ID_DETA_EMPLEADO)
                    INNER JOIN PERSONA D ON (B.ID_PERSONA = D.ID_PERSONA)
                    INNER JOIN ESPECIALIDAD_TECNICO E ON (E.ID_ESPECIALIDAD_TECNICO = B.ID_ESPECIALIDAD_TECNICO)
                    WHERE D.ID_ROL = 3 --TECNICO
                    AND A.ACTIVIO = TRUE
                    AND A.ID_SOLICITUD_CASO = $idCaso
                    ";
        $rs = pg_query($query);

        return $rs;

    }

    function SelectBitacoraTecnicosCasos($idCaso){
        $query = "SELECT
                    A.ID_DETA_EMPLEADO_TECNICO,
                    B.CODIGO_EMPLEADO,
                    (D.NOMBRE1_PERSONA||' '||D.NOMBRE2_PERSONA||' '||APELLIDO1_PERSONA||' '||D.APELLIDO2_PERSONA)AS NOMBRE,
                    E.ESPECIALIDAD_TECNICO,
                    A.FECHA_OPERACION,
                    C.CODIGO_EMPLEADO AS REALIZADOPOR,
                    CASE WHEN A.ACTIVIO = TRUE THEN 'ACTIVO' ELSE 'INACTIVO' END AS ACTIVO
                    FROM CASO_TECNICO A
                    INNER JOIN DETALLE_EMPLEADO B ON (A.ID_DETA_EMPLEADO_TECNICO = B.ID_DETA_EMPLEADO)
                    INNER JOIN DETALLE_EMPLEADO C ON (A.ID_DETA_EMPLEADO_CREA = C.ID_DETA_EMPLEADO)
                    INNER JOIN PERSONA D ON (B.ID_PERSONA = D.ID_PERSONA)
                    INNER JOIN ESPECIALIDAD_TECNICO E ON (E.ID_ESPECIALIDAD_TECNICO = B.ID_ESPECIALIDAD_TECNICO)
                    WHERE D.ID_ROL = 3 --TECNICO
                    AND A.ID_SOLICITUD_CASO = $idCaso
                    ORDER BY A.FECHA_OPERACION, ACTIVO";
        $rs = pg_query($query);

        return $rs;

    }

    function SelectTecnicosParaAsignarCaso($idCasoSelec){
         $query = "SELECT
                    A.ID_DETA_EMPLEADO,
                    A.CODIGO_EMPLEADO,
                    (B.NOMBRE1_PERSONA||' '||B.NOMBRE2_PERSONA||' '||B.APELLIDO1_PERSONA||' '||B.APELLIDO2_PERSONA)AS NOMBRE,
                    D.ESPECIALIDAD_TECNICO,
                    E.ID_SOLICITUD_CASO,
                    CASE WHEN C.CASOSABIERTOS IS NULL THEN 0 ELSE C.CASOSABIERTOS END AS CASOSABIERTOS
                    FROM DETALLE_EMPLEADO A
                    INNER JOIN PERSONA B ON (A.ID_PERSONA = B.ID_PERSONA)
                    LEFT JOIN
                            (
                            SELECT
                            COUNT(A1.ID_SOLICITUD_CASO) AS CASOSABIERTOS,
                            A1.ID_DETA_EMPLEADO_TECNICO
                            FROM CASO_TECNICO A1
                            INNER JOIN SOLICITUD_CASO B1 ON (A1.ID_SOLICITUD_CASO = B1.ID_SOLICITUD_CASO)
                            WHERE ACTIVIO = TRUE
                            AND B1.ID_ESTADO_CASO = 2
                            GROUP BY A1.ID_DETA_EMPLEADO_TECNICO
                            ) C ON (A.ID_DETA_EMPLEADO = C.ID_DETA_EMPLEADO_TECNICO)
                    INNER JOIN ESPECIALIDAD_TECNICO D ON (D.ID_ESPECIALIDAD_TECNICO = A.ID_ESPECIALIDAD_TECNICO)
                    LEFT JOIN
			(
			SELECT
			ID_SOLICITUD_CASO,
			ID_DETA_EMPLEADO_TECNICO
			FROM CASO_TECNICO
			WHERE
			ID_SOLICITUD_CASO = $idCasoSelec
			AND ACTIVIO = TRUE
			) E ON (E.ID_DETA_EMPLEADO_TECNICO = A.ID_DETA_EMPLEADO)
                    WHERE B.ID_ROL = 3 --TECNICO
                    AND E.ID_SOLICITUD_CASO IS NULL";

        $rs = pg_query($query);

        return $rs;
    }
    
    function SelectEstadosDelCaso($idCaso) {
        $query = "
                SELECT
                A.ID_BITACORA_ESTADO_CASO,
                A.ID_SOLICITUD_CASO,
                B.ESTADO_CASO,
                A.FECHA_ESTADO,
                C.EMPLEADO
                FROM BITACORA_ESTADO_CASO A
                INNER JOIN ESTADO_CASO B ON (A.ID_ESTADO_CASO = B.ID_ESTADO_CASO)
		LEFT JOIN
		(
		SELECT
              	A.ID_DETA_EMPLEADO,
              	(A.CODIGO_EMPLEADO||' '||NOMBRE1_PERSONA||' '||APELLIDO1_PERSONA)AS EMPLEADO
              	FROM
              	DETALLE_EMPLEADO A
              	INNER JOIN PERSONA B ON (A.ID_PERSONA = B.ID_PERSONA)
		)C ON (C.ID_DETA_EMPLEADO = A.ID_DETA_EMPLEADO_CREA)
                WHERE ID_SOLICITUD_CASO = $idCaso
                ORDER BY A.FECHA_ESTADO ASC";

        $rs = pg_query($query);

        return $rs;
    }


    function AgregarTecnicoCaso(){
        $fcnIdEmpCrea = $this->getIdEmpAgregaQuita();
        $fcnIdTecnicoAgregado = $this->getIdTecnicoAsignado();
        $idCasoSeleccionado = $this->getIdSolicitudCaso();

        $query = "INSERT INTO CASO_TECNICO
                (ID_SOLICITUD_CASO,
                ID_DETA_EMPLEADO_TECNICO,
                FECHA_OPERACION,
                ID_DETA_EMPLEADO_CREA,
                ACTIVIO)
                VALUES
                ($idCasoSeleccionado,$fcnIdTecnicoAgregado,CURRENT_TIMESTAMP,$fcnIdEmpCrea,TRUE)";

        pg_query($query);
    }

    function QuitarTecnicoCaso(){
        $fcnIdCasoTecnico = $this->getIdCasoTecnico();
        $fcnIdEmpleadoQuita = $this->getIdEmpAgregaQuita();

        $query="UPDATE
                CASO_TECNICO
                SET ACTIVIO = FALSE ,
                FECHA_OPERACION = CURRENT_TIMESTAMP ,
                ID_DETA_EMPLEADO_CREA = $fcnIdEmpleadoQuita
                WHERE ID_CASO_TECNICO = $fcnIdCasoTecnico";
        pg_query($query);
    }
    
    function PriorizarCaso(){
        $fcnDescBitacora = $this->getDescAsignacion();
        $fcnIdSolicitudCaso = $this->getIdSolicitudCaso();
        $fcnIdEscalabilidad = $this->getIdEscabilidad();
        $fcnIdEmpleadoCrea = $this->getIdEmpAsignaCaso();
        
        //buscamos el ultimo tipo de caso
        $QueryTipoCaso = "SELECT
                                ID_TIPO_CASO
                                FROM BITACORA_CASO
                                WHERE ID_BITACORA_CASO = (SELECT MAX(ID_BITACORA_CASO) FROM BITACORA_CASO WHERE ID_SOLICITUD_CASO = $fcnIdSolicitudCaso)";
        $rsTipoCaso = pg_query($QueryTipoCaso);
        $idTipoCaso = pg_fetch_result($rsTipoCaso, 0);

        $query = "INSERT INTO bitacora_caso(
                    descripcion_seguimiento, fecha_seguimiento,
                    id_solicitud_caso,id_tipo_caso,id_escalabilidad,id_deta_empleado_crea)
                    VALUES
                    ('$fcnDescBitacora',CURRENT_TIMESTAMP,$fcnIdSolicitudCaso,$idTipoCaso,$fcnIdEscalabilidad,$fcnIdEmpleadoCrea)";
        pg_query($query);

    }

}
