<?php
include_once("../../DAO_CAP/Connection/Connection.class.php");
class modelSolicitudCaso{

  private $idSolicitudCaso;
  private $descripcionCaso;
  private $fechaCaso;
  private $idDetaEmpleadoSolicitante;
  private $idEstadoCaso;
  //------------- ID DEL EQUIPO EN SOLICITUD DE CASO
  private $idEquipoSolicitudCaso = "";
  //------------------------------------------------

  public function getIdSolicitudCaso(){
    return $this->idSolicitudCaso;
  }

  public function setIdSolicitudCaso($idSolicitudCaso){
    $this->idSolicitudCaso = $idSolicitudCaso;
  }

  public function getDescripcionCaso(){
    return $this->descripcionCaso;
  }

  public function setDescripcionCaso($descripcionCaso){
    $this->descripcionCaso = $descripcionCaso;
  }

  public function getFechaCaso(){
    return $this->fechaCaso;
  }

  public function setFechaCaso($fechaCaso){
    $this->fechaCaso = $fechaCaso;
  }

  public function getIdDetaEmpleadoSolicitante(){
    return $this->idDetaEmpleadoSolicitante;
  }

  public function setIdDetaEmpleadoSolicitante($idDetaEmpleadoSolicitante){
    $this->idDetaEmpleadoSolicitante = $idDetaEmpleadoSolicitante;
  }

  public function getIdEstadoCaso(){
    return $this->idEstadoCaso;
  }

  public function setIdEstadoCaso($idEstadoCaso){
    $this->idEstadoCaso = $idEstadoCaso;
  }
  
  public function getIdEquipoSolicitudCaso() {
      return $this->idEquipoSolicitudCaso;
  }

  public function setIdEquipoSolicitudCaso($idEquipoSolicitudCaso) {
      $this->idEquipoSolicitudCaso = $idEquipoSolicitudCaso;
  }

  

  function create(){
    $description_caso=$this->getDescripcionCaso();
    $id_deta_empleado_solicitante=$this->getidDetaEmpleadoSolicitante();
    $id_estado_caso=$this->getIdEstadoCaso();
    $id_equipo = $this->getIdEquipoSolicitudCaso();

     $objConnection = new Connection();
     //$sql="SELECT helpdesk_insert_solicitudcaso('$description_caso', $id_deta_empleado_solicitante, $id_estado_caso)";
     $sql = "INSERT INTO SOLICITUD_CASO
            (DESCRIPCION_CASO, FECHA, ID_DETA_EMPLEADO_SOLICITANTE, ID_ESTADO_CASO, ID_EQUIPO)
            VALUES ('$description_caso', CURRENT_TIMESTAMP,$id_deta_empleado_solicitante,$id_estado_caso,$id_equipo)";
     $result=$objConnection->Consultas($sql);
     
     $queryIdSolicitudInsertada = "SELECT MAX(ID_SOLICITUD_CASO)
                                    FROM SOLICITUD_CASO 
                                    WHERE ID_DETA_EMPLEADO_SOLICITANTE = $id_deta_empleado_solicitante
                                    AND ID_ESTADO_CASO = 1";
     $rsIdCaso = $objConnection->Consultas($queryIdSolicitudInsertada);
     $idCasoInsertado = pg_fetch_result($rsIdCaso, 0);
     
     $queryInsertBitacoraEstado = "INSERT INTO BITACORA_ESTADO_CASO (ID_SOLICITUD_CASO,ID_ESTADO_CASO,FECHA_ESTADO,ID_DETA_EMPLEADO_CREA) 
                                   VALUES ($idCasoInsertado,1,CURRENT_TIMESTAMP,$id_deta_empleado_solicitante)";
     $objConnection->Consultas($queryInsertBitacoraEstado);

     if (!$result) {
       echo "An error occurred.\n";
       exit;
     }

     return $result;
  }

  function update($id){

    $id_estado_caso=$this->getIdEstadoCaso();

    $objConnection = new Connection();
    $sql="SELECT * FROM HELPDESK_UPDATE_SOLICITUDCASO";
    $result=$objConnection->Consultas($sql);

    if (!$result) {
      echo "An error occurred.\n";
      exit;
    }

     return $result;
  }
  
  function SelectEquiposUsuarioParaSolicitudCaso($idEmpleado){
      
      $objConnection = new Connection();
      
      $queryAreaDistritoEmp = "SELECT A.ID_AREA_DISTRITO FROM DETALLE_EMPLEADO A WHERE A.ID_DETA_EMPLEADO = $idEmpleado";
      $rsArea = $objConnection->Consultas($queryAreaDistritoEmp);
      $idAreaDistritoEmpleado = pg_fetch_result($rsArea, 0);
      
      //PRIMER QUERY EQUIPOS ASIGNADOS AL USUARIO UNION SEGUNDO QUERY EQUIPOS ASOCIADOS A LA OFICINA COMPLETA
      $sql = "SELECT * FROM 
                ( 
                            SELECT 
                            B.ID_EQUIPO,
                            (C.CODIGO_EQUIPO||' '||D.MARCA)AS EQUIPO
                            FROM BITACORA_EQUIPO A
                            INNER JOIN TRANSACCION_EQUIPO B ON (A.ID_TRANSACCION_EQUIPO = B.ID_TRANSACCION_EQUIPO)
                            INNER JOIN EQUIPO_TECNOLOGICO C ON (B.ID_EQUIPO = C.ID_EQUIPO)
                            INNER JOIN
                            (
                            SELECT 
                            A1.ID_MODELO_EQUIPO,
                            (B1.MARCA_EQUIPO||' '||A1.NOMBRE_MODELO)AS MARCA
                            FROM MODELO_EQUIPO A1 
                            INNER JOIN MARCA_EQUIPO B1 ON (A1.ID_MARCA_EQUIPO = B1.ID_MARCA_EQUIPO)
                            ) D ON (C.ID_MODELO_EQUIPO = D.ID_MODELO_EQUIPO)
                            WHERE A.ID_DETA_EMPLEADO_ASIGNADO = $idEmpleado
                            AND A.UBICACION_ACTUAL = TRUE
                )AS EQUIPOEMP
                UNION
                SELECT * FROM
                (
                            SELECT
                            B.ID_EQUIPO,
                            (C.CODIGO_EQUIPO||' '||D.MARCA)AS EQUIPO
                            FROM BITACORA_EQUIPO A
                            INNER JOIN TRANSACCION_EQUIPO B ON (A.ID_TRANSACCION_EQUIPO = B.ID_TRANSACCION_EQUIPO)
                            INNER JOIN EQUIPO_TECNOLOGICO C ON (B.ID_EQUIPO = C.ID_EQUIPO)
                            INNER JOIN
                            (
                            SELECT 
                            A1.ID_MODELO_EQUIPO,
                            (B1.MARCA_EQUIPO||' '||A1.NOMBRE_MODELO)AS MARCA
                            FROM MODELO_EQUIPO A1 
                            INNER JOIN MARCA_EQUIPO B1 ON (A1.ID_MARCA_EQUIPO = B1.ID_MARCA_EQUIPO)
                            ) D ON (C.ID_MODELO_EQUIPO = D.ID_MODELO_EQUIPO)
                            WHERE 
                            B.ID_AREA_DISTRITO_DESTINO = $idAreaDistritoEmpleado
                            AND A.UBICACION_ACTUAL = TRUE
                )AS EQUIPOOFICINA
                ORDER BY 1";
      
      $rs = $objConnection->Consultas($sql);
      
      return $rs;

  }

}



 ?>
