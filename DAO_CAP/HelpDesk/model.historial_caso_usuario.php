<?php

//
include("../../DAO_CAP/Conexion/admon_conexion.php");

class HistorialCasoUsuario{


	private $id_solicitud_caso;
	private $fecha_caso;
	private $descripcion_caso;
	private $fecha_seguimiento;
	private $id_estado_caso;
	private $id_deta_empleado_tecnico;
	private $id_deta_empleado_solicitante;
	private $id_equipo_tecnologico;


	public function getId_solicitud_caso(){
		return $this->id_solicitud_caso;
	}

	public function setId_solicitud_caso($id_solicitud_caso){
		$this->id_solicitud_caso = $id_solicitud_caso;
	}

	public function getFecha_caso(){
		return $this->fecha_caso;
	}

	public function setFecha_caso($fecha_caso){
		$this->fecha_caso = $fecha_caso;
	}

	public function getFecha_seguimiento(){
		return $this->fecha_seguimiento;
	}

	public function setFecha_seguimiento($fecha_seguimiento){
		$this->fecha_seguimiento = $fecha_seguimiento;
}

	public function getId_estado_caso(){
		return $this->id_estado_caso;
	}

	public function setId_estado_caso($id_estado_caso){
		$this->id_estado_caso = $id_estado_caso;
	}

	public function getId_deta_empleado_tecnico(){
		return $this->id_deta_empleado_tecnico;
	}

	public function setId_deta_empleado_tecnico($id_deta_empleado_tecnico){
		$this->id_deta_empleado_tecnico = $id_deta_empleado_tecnico;
	}

	public function getId_deta_empleado_solicitante(){
		return $this->id_deta_empleado_solicitante;
	}

	public function getId_equipo_tecnologico(){
		return $this->id_equipo_tecnologico;
	}

	public function setId_equipo_tecnologico($id_equipo_tecnologico){
		$this->id_equipo_tecnologico = $id_equipo_tecnologico;
	}

	//--------------------------------------------

	function SelectHistorialCasosUsuario(){


    $query = "SELECT
			A.ID_SOLICITUD_CASO,
			A.id_bitacora_caso,
			A.DESCRIPCION_SEGUIMIENTO,
			A.FECHA_SEGUIMIENTO,
			(D.NOMBRE1_PERSONA||' '||D.NOMBRE2_PERSONA||' '||D.APELLIDO2_PERSONA)AS tecnico ,
			A.ID_TIPO_CASO,
			B.ESCALABILIDAD
			FROM BITACORA_CASO A
			INNER JOIN ESCALABILIDAD B ON (A.ID_ESCALABILIDAD = B.ID_ESCALABILIDAD)
			INNER JOIN DETALLE_EMPLEADO C ON (A.ID_DETA_EMPLEADO_TECNICO = C.ID_DETA_EMPLEADO)
			INNER JOIN PERSONA D ON (C.ID_PERSONA = D.ID_PERSONA)
			INNER JOIN AREA_DISTRITO E ON (C.ID_AREA_DISTRITO = E.ID_AREA_DISTRITO)
			INNER JOIN DISTRITO F ON (F.ID_DISTRITO = E.ID_DISTRITO)
                ";

    $rs = pg_query($query);
    return $rs;
	}

}


 ?>
