<?php

include_once("../../DAO_CAP/Connection/Connection.class.php");

class modelDetalleEmpleado{

  private $id_deta_empleado;
  private $usuario_deta_persona;
  private $pass_deta_persona;
  private $codigo_empleado;
  private $id_persona;
  private $id_estado_empleado;
  private $id_area_distrito;

  public function getId_deta_empleado(){
    return $this->id_deta_empleado;
  }

  public function setId_deta_empleado($id_deta_empleado){
    $this->id_deta_empleado = $id_deta_empleado;
  }

  public function getUsuario_deta_persona(){
    return $this->usuario_deta_persona;
  }

  public function setUsuario_deta_persona($usuario_deta_persona){
    $this->usuario_deta_persona = $usuario_deta_persona;
  }

  public function getPass_deta_persona(){
    return $this->pass_deta_persona;
  }

  public function setPass_deta_persona($pass_deta_persona){
    $this->pass_deta_persona = $pass_deta_persona;
  }

  public function getCodigo_empleado(){
    return $this->codigo_empleado;
  }

  public function setCodigo_empleado($codigo_empleado){
    $this->codigo_empleado = $codigo_empleado;
  }

  public function getId_persona(){
    return $this->id_persona;
  }

  public function setId_persona($id_persona){
    $this->id_persona = $id_persona;
  }

  public function getId_estado_empleado(){
    return $this->id_estado_empleado;
  }

  public function setId_estado_empleado($id_estado_empleado){
    $this->id_estado_empleado = $id_estado_empleado;
  }

  public function getId_area_distrito(){
    return $this->id_area_distrito;
  }

  public function setId_area_distrito($id_area_distrito){
    $this->id_area_distrito = $id_area_distrito;
  }

  function create(){
     $objConnection = new Connection();
     $sql="SELECT * FROM HELPDESK_INSERT_DETALLEEMPLEADO()";
     $result=$objConnection->Consultas($sql);

     if (!$result) {
       echo "An error occurred.\n";
       exit;
     }
     return $result;
  }

  function findAll(){
     $objConnection = new Connection();
    $sql="SELECT * FROM HELPDESK_FINDALL_DETALLEEMPLEADO()";
     $result=$objConnection->Consultas($sql);

     if (!$result) {
       echo "An error occurred.\n";
       exit;
     }
     return $result;
  }

  function update($id){
     $objConnection = new Connection();
    $sql="SELECT * FROM HELPDESK_UPDATE_DETALLEEMPLEADO()";
     $result=$objConnection->Consultas($sql);

     if (!$result) {
       echo "An error occurred.\n";
       exit;
     }
     return $result;
  }

  function delete($id){
     $objConnection = new Connection();
     $sql="SELECT * FROM HELPDESK_DELETE_DETALLEEMPLEADO()";
     $result=$objConnection->Consultas($sql);

     if (!$result) {
       echo "An error occurred.\n";
       exit;
     }
     return $result;
  }

}

?>
