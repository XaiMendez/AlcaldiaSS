<?php

include ("../../DAO_CAP/Conexion/admon_conexion.php");

class CasosUsuarios{
    private $idSolicitante = "";
    private $descCancelarCalificarCaso = "";
    private $idCasoSeleccionado = "";
    private $Calificacion = "";
    
    
    public function getIdSolicitante() {
        return $this->idSolicitante;
    }

    public function setIdSolicitante($idSolicitante) {
        $this->idSolicitante = $idSolicitante;
    }
    
    public function getDescCancelarCalificarCaso() {
        return $this->descCancelarCalificarCaso;
    }

    public function setDescCancelarCalificarCaso($descCancelarCalificarCaso) {
        $this->descCancelarCalificarCaso = $descCancelarCalificarCaso;
    }
    
    public function getIdCasoSeleccionado() {
        return $this->idCasoSeleccionado;
    }

    public function setIdCasoSeleccionado($idCasoSeleccionado) {
        $this->idCasoSeleccionado = $idCasoSeleccionado;
    }
    
    public function getCalificacion() {
        return $this->Calificacion;
    }

    public function setCalificacion($Calificacion) {
        $this->Calificacion = $Calificacion;
    }

    
    
    
    function CancelarCasoUsuario (){
        $fcnIdCaso = $this->getIdCasoSeleccionado();
        $fcnIdSolicitante = $this->getIdSolicitante();
        $fcnDesc = $this->getDescCancelarCalificarCaso();
        
        
        //INSERTAMOS LA BITACORA QUE DETALLA PORQUE SE CANCELA EL CASO
        $queryInsertBitacora = "INSERT INTO BITACORA_CASO 
                                (DESCRIPCION_SEGUIMIENTO,
                                FECHA_SEGUIMIENTO,
                                ID_SOLICITUD_CASO,
                                ID_TIPO_CASO,
                                ID_ESCALABILIDAD,
                                ID_DETA_EMPLEADO_CREA)
                                VALUES
                                ('$fcnDesc',CURRENT_TIMESTAMP,$fcnIdCaso,NULL,NULL,$fcnIdSolicitante)";
        
        pg_query($queryInsertBitacora);
        
        // SE ACTUALIZA EL CASO A CANCELADO
        $queryUpdateCancelar = "UPDATE SOLICITUD_CASO SET ID_ESTADO_CASO = 9 WHERE ID_SOLICITUD_CASO = $fcnIdCaso";
        pg_query($queryUpdateCancelar);
        
        // INSERTAMOS LA BITACORA DEL ESTADO DEL CASO
        $queryInsertarBitacoraEstado = "INSERT INTO BITACORA_ESTADO_CASO 
                                        (ID_SOLICITUD_CASO,ID_ESTADO_CASO,FECHA_ESTADO,ID_DETA_EMPLEADO_CREA) 
                                        VALUES 
                                        ($fcnIdCaso,9,CURRENT_TIMESTAMP,$fcnIdSolicitante);";
        pg_query($queryInsertarBitacoraEstado);
        
    }
    
    function CalificarCaso(){
        $fcnDescCalificacionMan = $this->getDescCancelarCalificarCaso();
        $fcnIdCasoCalificar = $this->getIdCasoSeleccionado();
        $fcnIdCalificador = $this->getIdSolicitante();
        $fcnCalificacion = $this->getCalificacion();
        
        //INSERTAMOS LA BITACORA QUE DETALLA LA CALIFICACION DEL CASO
        $queryInsertBitacora = "INSERT INTO BITACORA_CASO 
                                (DESCRIPCION_SEGUIMIENTO,
                                FECHA_SEGUIMIENTO,
                                ID_SOLICITUD_CASO,
                                ID_TIPO_CASO,
                                ID_ESCALABILIDAD,
                                ID_DETA_EMPLEADO_CREA)
                                VALUES
                                ('$fcnDescCalificacionMan',CURRENT_TIMESTAMP,$fcnIdCasoCalificar,NULL,NULL,$fcnIdCalificador)";
        
        pg_query($queryInsertBitacora);
        // SE ACTUALIZA EL CASO A CALIFICADO POR USUARIO
        $queryUpdateCancelar = "UPDATE SOLICITUD_CASO SET ID_ESTADO_CASO = 4 , CALIFICACION_CASO = $fcnCalificacion  WHERE ID_SOLICITUD_CASO = $fcnIdCasoCalificar";
        pg_query($queryUpdateCancelar);
        
        // INSERTAMOS LA BITACORA DEL ESTADO DEL CASO
        $queryInsertarBitacoraEstado = "INSERT INTO BITACORA_ESTADO_CASO 
                                        (ID_SOLICITUD_CASO,ID_ESTADO_CASO,FECHA_ESTADO,ID_DETA_EMPLEADO_CREA) 
                                        VALUES 
                                        ($fcnIdCasoCalificar,4,CURRENT_TIMESTAMP,$fcnIdCalificador);";
        pg_query($queryInsertarBitacoraEstado);
    }

    
    function SelectCasosSolicitantePendientes(){
    $fcnIdSolicitante = $this->getIdSolicitante();
    $query = "SELECT 
            A.ID_SOLICITUD_CASO,
            A.DESCRIPCION_CASO,
            A.FECHA,
            B.ESTADO_CASO,
            C.CODIGO_EQUIPO,
            (E.MARCA_EQUIPO||' '||D.NOMBRE_MODELO)AS MARCA
            FROM SOLICITUD_CASO A 
            INNER JOIN ESTADO_CASO B ON (A.ID_ESTADO_CASO = B.ID_ESTADO_CASO)
            INNER JOIN EQUIPO_TECNOLOGICO C ON (A.ID_EQUIPO = C.ID_EQUIPO)
            INNER JOIN MODELO_EQUIPO D ON (C.ID_MODELO_EQUIPO = D.ID_MODELO_EQUIPO)
            INNER JOIN MARCA_EQUIPO E ON (D.ID_MARCA_EQUIPO = E.ID_MARCA_EQUIPO)
            WHERE ID_DETA_EMPLEADO_SOLICITANTE = $fcnIdSolicitante AND B.ESTADO_CASO='PENDIENTE'";
    
    $rs = pg_query($query);
    
    return $rs;
    }
    
    function SelectCasosSolicitanteCompletados(){
    $fcnIdSolicitante = $this->getIdSolicitante();
    $query = "SELECT 
            A.ID_SOLICITUD_CASO,
            A.DESCRIPCION_CASO,
            A.FECHA,
            B.ESTADO_CASO,
            C.CODIGO_EQUIPO,
            (E.MARCA_EQUIPO||' '||D.NOMBRE_MODELO)AS MARCA
            FROM SOLICITUD_CASO A 
            INNER JOIN ESTADO_CASO B ON (A.ID_ESTADO_CASO = B.ID_ESTADO_CASO)
            INNER JOIN EQUIPO_TECNOLOGICO C ON (A.ID_EQUIPO = C.ID_EQUIPO)
            INNER JOIN MODELO_EQUIPO D ON (C.ID_MODELO_EQUIPO = D.ID_MODELO_EQUIPO)
            INNER JOIN MARCA_EQUIPO E ON (D.ID_MARCA_EQUIPO = E.ID_MARCA_EQUIPO)
            WHERE ID_DETA_EMPLEADO_SOLICITANTE = $fcnIdSolicitante AND B.ESTADO_CASO='COMPLETADO'
            order by 3";
    
    $rs = pg_query($query);
    
    return $rs;
    }
    
    function SelectCasosAbiertosUsuarioVerificacion() {
        $fcnIdSolicitante = $this->getIdSolicitante();
        $query = "SELECT 
            A.ID_SOLICITUD_CASO,
            A.DESCRIPCION_CASO,
            A.FECHA,
            B.ESTADO_CASO,
            C.CODIGO_EQUIPO,
            (E.MARCA_EQUIPO||' '||D.NOMBRE_MODELO)AS MARCA
            FROM SOLICITUD_CASO A 
            INNER JOIN ESTADO_CASO B ON (A.ID_ESTADO_CASO = B.ID_ESTADO_CASO)
            INNER JOIN EQUIPO_TECNOLOGICO C ON (A.ID_EQUIPO = C.ID_EQUIPO)
            INNER JOIN MODELO_EQUIPO D ON (C.ID_MODELO_EQUIPO = D.ID_MODELO_EQUIPO)
            INNER JOIN MARCA_EQUIPO E ON (D.ID_MARCA_EQUIPO = E.ID_MARCA_EQUIPO)
            WHERE ID_DETA_EMPLEADO_SOLICITANTE = $fcnIdSolicitante AND B.ESTADO_CASO='ABIERTO'
            order by 3";

        $rs = pg_query($query);

        return $rs;
    }
    
    function SelectEstadosDelCaso($idCaso) {
        $query = "
                SELECT
                A.ID_BITACORA_ESTADO_CASO,
                A.ID_SOLICITUD_CASO,
                B.ESTADO_CASO,
                A.FECHA_ESTADO,
                C.EMPLEADO
                FROM BITACORA_ESTADO_CASO A
                INNER JOIN ESTADO_CASO B ON (A.ID_ESTADO_CASO = B.ID_ESTADO_CASO)
		LEFT JOIN
		(
		SELECT
              	A.ID_DETA_EMPLEADO,
              	(A.CODIGO_EMPLEADO||' '||NOMBRE1_PERSONA||' '||APELLIDO1_PERSONA)AS EMPLEADO
              	FROM
              	DETALLE_EMPLEADO A
              	INNER JOIN PERSONA B ON (A.ID_PERSONA = B.ID_PERSONA)
		)C ON (C.ID_DETA_EMPLEADO = A.ID_DETA_EMPLEADO_CREA)
                WHERE ID_SOLICITUD_CASO = $idCaso
                ORDER BY A.FECHA_ESTADO ASC";

        $rs = pg_query($query);

        return $rs;
    }
    
}

?>

