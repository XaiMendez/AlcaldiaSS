<?php

include("../../DAO_CAP/Conexion/admon_conexion.php");
include_once("../../DAO_CAP/Connection/Connection.class.php");
/*
function CryptPass($input, $rounds = 9){
$salt ="";
$saltChars = array_merge(range('A' , 'Z' ), range('a' , 'z'), range(0 , 9));
for ($i=0; $i < 22 ; $i++) {
$salt .= $saltChars [array_rand($saltChars)];
}
return crypt($input, sprintf('$2y$%02d$', $rounds). $salt);

}*/
class IngresoTecnico {

  private $idDetaEmpleado = "";
  private $usuarioDetaPersona = "";
  private $passDetaPersona = "";
  private $confPassDetaPersona = "";
  private $codigoEmpleado = "";
  private $idAreaDistrito = "";
  private $area = "";
  private $idPersona = "";
  private $idEstadoEmpleado = "";
  private $estadoEmpleado = "";
  private $idEspecialidadTec = "";

  public function getIdDetaEmpleado() {
    return $this->idDetaEmpleado;
  }

  public function getUsuarioDetaPersona() {
    return $this->usuarioDetaPersona;
  }

  public function getPassDetaPersona() {
    return $this->passDetaPersona;
  }

  public function getConfPassDetaPersona() {
    return $this->confPassDetaPersona;
  }

  public function getCodigoEmpleado() {
    return $this->codigoEmpleado;
  }

  public function getIdAreaDistrito() {
    return $this->idAreaDistrito;
  }

  public function getArea() {
    return $this->area;
  }

  public function getIdPersona() {
    return $this->idPersona;
  }

  public function getIdEstadoEmpleado() {
    return $this->idEstadoEmpleado;
  }

  public function getEstadoEmpleado() {
    return $this->estadoEmpleado;
  }
  public function getIdEspecialidadTec(){
    return $this->idEspecialidadTec;
  }

  public function setIdDetaEmpleado($idDetaEmpleado) {
    $this->idDetaEmpleado = $idDetaEmpleado;
  }

  public function setUsuarioDetaPersona($usuarioDetaPersona) {
    $this->usuarioDetaPersona = $usuarioDetaPersona;
  }

  public function setPassDetaPersona($passDetaPersona) {
    $this->passDetaPersona = $passDetaPersona;
  }

  public function setConfPassDetaPersona($confPassDetaPersona) {
    $this->confPassDetaPersona = $confPassDetaPersona;
  }

  public function setCodigoEmpleado($codigoEmpleado) {
    $this->codigoEmpleado = $codigoEmpleado;
  }

  public function setIdAreaDistrito($idAreaDistrito) {
    $this->idAreaDistrito = $idAreaDistrito;
  }

  public function setArea($area) {
    $this->area = $area;
  }

  public function setIdPersona($idPersona) {
    $this->idPersona = $idPersona;
  }

  public function setIdEstadoEmpleado($idEstadoEmpleado) {
    $this->idEstadoEmpleado = $idEstadoEmpleado;
  }

  public function SetEstadoEmpleado($estadoEmpleado) {
    $this->estadoEmpleado = $estadoEmpleado;
  }
  public function SetIdEspecialidadTec($idEspecialidadTec){
    $this->idEspecialidadTec = $idEspecialidadTec;
  }



  function SelectIdEstadoEmpleado(){
    $query = "SELECT id_estado_empleado, estado_empleado FROM estado_empleado";
    $rsEstadoEmpleado = pg_query($query);

    return $rsEstadoEmpleado;
  }

  function SelectIdAreaDistrito(){
    $query = "SELECT id_area_distrito, area FROM area_distrito";
    $rsAreaDistrito = pg_query($query);

    return $rsAreaDistrito;
  }
  function SelectIdEspecialidadTec(){
    $query = "SELECT id_especialidad_tecnico, especialidad_tecnico FROM especialidad_tecnico";
    $rsEspecialidadTec = pg_query($query);

    return $rsEspecialidadTec;
  }

  function SelectIdPersona(){
    $persona = "select id_persona,  nombre_log from (
    select
    A.usuario_deta_persona,
    B.id_persona,
    (B.nombre1_persona || ' ' || B.nombre2_persona || ' ' || B.apellido1_persona || ' ' || B.apellido2_persona) AS nombre_log
    from persona B
    LEFT JOIN detalle_empleado A ON (A.id_persona = B.id_persona)
    where B.id_rol = 3)
    as Tabla
    where Tabla.usuario_deta_persona is NULL;";
    $rsPersona = pg_query($persona);

    return $rsPersona;
  }

  function InsertarTecnico(){
    $fcnCodTec = $this->getCodigoEmpleado();
    $fcnidPersona = $this->getIdPersona();
    $fcnidEstadoEmp = $this->getIdEstadoEmpleado();
    $fcnidAreaDistrito = $this->getIdAreaDistrito();
    $fcnUsuarioDeta = $this->getUsuarioDetaPersona();
    $fcnPassDeta = $this->getPassDetaPersona();
    $fcnConfPassDeta = $this->getConfPassDetaPersona();
    $fcnIdEspecialidadTec = $this -> getIdEspecialidadTec();


    $usuarioBusc = "SELECT usuario_deta_persona FROM DETALLE_EMPLEADO WHERE usuario_deta_persona = '$fcnUsuarioDeta'";
    $rs= pg_query($usuarioBusc);
    $num = pg_num_rows($rs);

    if ($num > 0) {
      echo '<script language="javascript">';
      echo 'alert("El usuario ya Existe");';
      echo "window.location = '../../VIEW_CAP/HelpDesk/ingresoTecnico.php';";

      echo '</script>' ;

    }else{

      if ($fcnPassDeta == $fcnConfPassDeta ) {
        $query = "INSERT INTO DETALLE_EMPLEADO (usuario_deta_persona, pass_deta_persona, codigo_empleado, id_persona, id_estado_empleado, id_area_distrito, id_especialidad_tecnico )
        VALUES ('$fcnUsuarioDeta', '$fcnPassDeta', '$fcnCodTec', $fcnidPersona, $fcnidEstadoEmp, $fcnidAreaDistrito, $fcnIdEspecialidadTec );";

        pg_query($query);

        echo '<script language="javascript">';
        echo 'alert("Usuario insertado Exitosamente");';
        echo "window.location = '../../VIEW_CAP/HelpDesk/ingresoTecnico.php';";
        echo '</script>';
      }else {

        echo '<script language="javascript">';
        echo 'alert("Contraseñas no coinciden");';
        echo "window.location = '../../VIEW_CAP/HelpDesk/ingresoTecnico.php';";

        echo '</script>' ;
      }
    }
  }

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  function update($id){

    $id_especialidad_tecnico=$this->getIdEspecialidadTec();
    $id_estado_empleado=$this->getIdEstadoEmpleado();
    $id_area_distrito=$this->getIdAreaDistrito();

    $objConnection = new Connection();
    $sql="update detalle_empleado set id_especialidad_tecnico=$id_especialidad_tecnico,
                                      id_estado_empleado= $id_estado_empleado,
                                      id_area_distrito=$id_area_distrito
                                      where id_deta_empleado=$id;";
    $result=$objConnection->Consultas($sql);

    if (!$result) {
      echo "An error occurred.\n";
      exit;
    }

     return $result;
  }

  function updatePassword($id){

    $Pass=$this->getPassDetaPersona();


    $objConnection = new Connection();
    $sql="update detalle_empleado set pass_deta_persona='$Pass'
                                      where id_deta_empleado=$id;";
    $result=$objConnection->Consultas($sql);

    if (!$result) {
      echo "An error occurred.\n";
      exit;
    }

     return $result;
  }

}

?>
