<?php

include("../../DAO_CAP/Conexion/admon_conexion.php") ;

function combo_escalabilidad(){
    $queryEscalabilidad = "SELECT ID_ESCALABILIDAD,ESCALABILIDAD FROM ESCALABILIDAD;";
    $nombreCombo = "cbxEscalabilidad";
    $idEscalabilidad = 'id_escalabilidad';
    $escalabilidad = 'escalabilidad';
    
    $rsEscalabilidad = pg_query($queryEscalabilidad);
    $html_comboEscalabilidad = "<select class='form-control m-bot15' name=$nombreCombo>\n";
    
    while($fila = pg_fetch_assoc($rsEscalabilidad)){
        $html_comboEscalabilidad=$html_comboEscalabilidad."<option value='".$fila[$idEscalabilidad]."'>".$fila[$escalabilidad]."</option>\n";
    }
    $html_comboEscalabilidad=$html_comboEscalabilidad."</select>";
    
    return $html_comboEscalabilidad;
}

function combo_tipo_caso(){
    $queryTipoCaso = "SELECT ID_TIPO_CASO , TIPO_CASO FROM TIPO_CASO";
    $nombreCombo = "cbxTipoCaso";
    $idTipoCaso = "id_tipo_caso";
    $tipoCaso = "tipo_caso";
    
    $rsTipoCaso = pg_query($queryTipoCaso);
    $html_comboTipoCaso = "<select class='form-control m-bot15' name=$nombreCombo>\n";
    
    while($fila = pg_fetch_assoc($rsTipoCaso)){
        $html_comboTipoCaso=$html_comboTipoCaso."<option value='".$fila[$idTipoCaso]."'>".$fila[$tipoCaso]."</option>\n";
    }
    $html_comboTipoCaso=$html_comboTipoCaso."</select>";
    
    return $html_comboTipoCaso;
}

function combo_tecnico_asignar(){
    $queryTecnico = "SELECT
                    A.ID_DETA_EMPLEADO,
                    (A.CODIGO_EMPLEADO||' '||B.NOMBRE1_PERSONA||' '||B.APELLIDO1_PERSONA)AS TECNICO
                    FROM DETALLE_EMPLEADO A
                    INNER JOIN PERSONA B ON (A.ID_PERSONA = B.ID_PERSONA)
                    INNER JOIN ROL C ON (B.ID_ROL = C.ID_ROL)
                    WHERE C.NOMBRE_ROL = 'TECNICO'";
    $nombreCombo = "cbxTecnico";
    $idEmp = "id_deta_empleado";
    $tecnico = "tecnico";
    
    $rsTecnico = pg_query($queryTecnico);
    $html_comboTecnico = "<select class='form-control m-bot15' name=$nombreCombo>\n";
    
    while($fila = pg_fetch_assoc($rsTecnico)){
        $html_comboTecnico=$html_comboTecnico."<option value='".$fila[$idEmp]."'>".$fila[$tecnico]."</option>\n";
    }
    $html_comboTecnico=$html_comboTecnico."</select>";
    
    return $html_comboTecnico;
}



