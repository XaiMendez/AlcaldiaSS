<?php

require ("../../DAO_CAP/AdmonEquipo/variables_solicitudCompra.php");

class AsignarEquipoTecnologico extends EncaSolicitudCompra{
    //--- variables transaccion equipo de asignar equipo
    private $detalleTransaccion = "";
    private $idEquipo = "";
    private $idEmpleadoAutoriza = "";
    private $idAreaDistritoDestino = "";
    //--- variables bitacora caso
    private $idEmpleadoAsignado = "";
            
    public function getDetalleTransaccion() {
        return $this->detalleTransaccion;
    }

    public function getIdEquipo() {
        return $this->idEquipo;
    }

    public function getIdEmpleadoAutoriza() {
        return $this->idEmpleadoAutoriza;
    }

    public function getIdAreaDistritoDestino() {
        return $this->idAreaDistritoDestino;
    }

    public function getIdEmpleadoAsignado() {
        return $this->idEmpleadoAsignado;
    }

    public function setDetalleTransaccion($detalleTransaccion) {
        $this->detalleTransaccion = $detalleTransaccion;
    }

    public function setIdEquipo($idEquipo) {
        $this->idEquipo = $idEquipo;
    }

    public function setIdEmpleadoAutoriza($idEmpleadoAutoriza) {
        $this->idEmpleadoAutoriza = $idEmpleadoAutoriza;
    }

    public function setIdAreaDistritoDestino($idAreaDistritoDestino) {
        $this->idAreaDistritoDestino = $idAreaDistritoDestino;
    }

    public function setIdEmpleadoAsignado($idEmpleadoAsignado) {
        $this->idEmpleadoAsignado = $idEmpleadoAsignado;
    }

    public function AsignarEquipo(){
        $fcnDetaTrans = $this->getDetalleTransaccion();
        $fcnIdEquipo = $this->getIdEquipo();
        $fcnIdEmpAuto = $this->getIdEmpleadoAutoriza();
        $fcnIdAreaDest = $this->getIdAreaDistritoDestino();
        $fcnIdEmpAsig = $this->getIdEmpleadoAsignado();
        $fcnIdCaso = $this->getIdCaso();
        
        $queryTipoTrans = "SELECT ID_TIPO_TRANSACCION FROM TIPO_TRANSACCION WHERE TIPO_TRANSACCION = 'PRIMERA ASIGNACION'";
        $rsTipoTrans = pg_query($queryTipoTrans);
        $idTransaccion = pg_fetch_result($rsTipoTrans, 0);
        
        $queryAsignacion = "SELECT Admon_Asignar_Equipo('$fcnDetaTrans',$idTransaccion,$fcnIdEquipo,$fcnIdEmpAuto,$fcnIdAreaDest,$fcnIdCaso)";
        $rsAsig = pg_query($queryAsignacion);
        $idTransAsignacionInsertado = pg_fetch_result($rsAsig, 0);
        
        $queryBitacoraAsig = 
                "INSERT INTO BITACORA_EQUIPO 
                (ID_DETA_EMPLEADO_ASIGNADO,ID_TRANSACCION_EQUIPO,UBICACION_ACTUAL)VALUES($fcnIdEmpAsig,$idTransAsignacionInsertado,TRUE)";
        
        pg_query($queryBitacoraAsig);
        
    }
    
     function SelectEquiposAsignar(){
            $queryEquipoAsig = "SELECT * FROM Admon_Select_Equipos_Asignar();";
            $rsEqAs = pg_query($queryEquipoAsig);
            
            return $rsEqAs;
        }
}

class MoverEquipoTecnologico extends AsignarEquipoTecnologico {
    
    private $IdAreaDistritoOrigen = "";
    private $idBitacoraActualizar = "";
    private $idSolicitudTrasladoEquipo = "";
    
    public function getIdAreaDistritoOrigen() {
        return $this->IdAreaDistritoOrigen;
    }

    public function setIdAreaDistritoOrigen($IdAreaDistritoOrigen) {
        $this->IdAreaDistritoOrigen = $IdAreaDistritoOrigen;
    }

        
    public function getIdBitacoraActualizar() {
        return $this->idBitacoraActualizar;
    }

    public function setIdBitacoraActualizar($idBitacoraActualizar) {
        $this->idBitacoraActualizar = $idBitacoraActualizar;
    }
    
    public function getIdSolicitudTrasladoEquipo() {
        return $this->idSolicitudTrasladoEquipo;
    }

    public function setIdSolicitudTrasladoEquipo($idSolicitudTrasladoEquipo) {
        $this->idSolicitudTrasladoEquipo = $idSolicitudTrasladoEquipo;
    }

    
    
        
    function SelectEquiposMovimiento(){
            $queryEquipoMover = "SELECT * FROM Admon_Select_Equipos_Mover();";
            $rsEqMove = pg_query($queryEquipoMover);
            
            return $rsEqMove;
    }
    
    function SelectUbicacionActualParaMover(){
            $fcnEquipo = $this->getIdEquipo();
            $query = "SELECT * FROM Admon_Select_Ubicacion_Actual_Mover($fcnEquipo)";
            $rsEqActual = pg_query($query);
            
            return $rsEqActual;
    }
    
    
    
    public function MoverEquipo() { 
        $fcnDetaTrans = $this->getDetalleTransaccion();
        $fcnIdEquipo = $this->getIdEquipo();
        $fcnIdEmpAuto = $this->getIdEmpleadoAutoriza();
        $fcnIdAreaOrigen = $this->getIdAreaDistritoOrigen();
        $fcnIdAreaDest = $this->getIdAreaDistritoDestino();
        $fcnIdEmpAsig = $this->getIdEmpleadoAsignado();
        $fcnIdCaso = 'NULL'; //este id de caso se inserta en la transaccion, como ya se inserta en la solicitud no es necesario repetirlo
        $fcnIdBitacoraFalse = $this->getIdBitacoraActualizar(); //ID DE BITACORA A ACTUALIZAR A FALSE
        $fcnIdSolicitudTraslado = $this->getIdSolicitudTrasladoEquipo();
        
        if ($fcnIdEmpAsig == 0){
            $fcnIdEmpAsig = 'NULL';
        }
        
        // OBTENGO EL ID DEL TIPO DE TRANSACCION MOVIMIENTO
        $queryTipoTrans = "SELECT ID_TIPO_TRANSACCION FROM TIPO_TRANSACCION WHERE TIPO_TRANSACCION LIKE '%MOVIMIENTO%'";
        $rsTipoTrans = pg_query($queryTipoTrans);
        $idTransaccion = pg_fetch_result($rsTipoTrans, 0); //ID DE TRANSACCION
        
        // REALIZO POR MEDIO DE UNA FUNCION LA INSERCION DE LOS DATOS DE MOVER EQUIPO
        $query = "SELECT Admon_Mover_Equipo('$fcnDetaTrans',$idTransaccion,$fcnIdEquipo,$fcnIdEmpAuto,$fcnIdAreaOrigen,$fcnIdAreaDest,$fcnIdCaso,$fcnIdSolicitudTraslado)";
        $rsIdTrans = pg_query($query);
        $idTransaccionMoverInsertado = pg_fetch_result($rsIdTrans, 0); //REGRESO EL ID DE LA TRANSACCION INSERTADA PARA ENVIARLA USARLA EN LA BITACORA
        
        //ACTUALIZAMOS LA ULTIMA BITACORA QUE ERA TRUE A FALSE PARA PREPARAR LA NUEVA BITACORA
            $queryActualizarBitacora = "UPDATE BITACORA_EQUIPO SET UBICACION_ACTUAL = FALSE WHERE ID_BITACORA = $fcnIdBitacoraFalse";
            pg_query($queryActualizarBitacora);  
            
        //INSERTAMOS LA NUEVA BITACORA CON LA TRANSACCION OBTENIDA
            $queryBitacoraMov = 
            "INSERT INTO BITACORA_EQUIPO 
            (ID_DETA_EMPLEADO_ASIGNADO,ID_TRANSACCION_EQUIPO,UBICACION_ACTUAL)VALUES($fcnIdEmpAsig,$idTransaccionMoverInsertado,TRUE)";

            pg_query($queryBitacoraMov);
            
       //LUEGO LA SOLICITUD DE TRASLADO DE EQUIPO LA CAMBIAMOS A APROVADA
            //PRIMERO OBTENEMOS EL ID DE LA SOLICITUD QUE ES APROVADA
            $queryEstadoAprov = "SELECT ID_ESTADO_COMPRA FROM ESTADO_SOLI_COMPRA WHERE DESCRIPCION LIKE '%APRO%'"; //query lineal
            $rsEstadoAprov = pg_query($queryEstadoAprov);
            $idEstadoAprov = pg_fetch_result($rsEstadoAprov, 0);
      
            //AHORA ACTUALIZAMOS LA SOLICITUD A APROVADA CON EL ID Y FECHA DEL EMPLEADO QUE LA APROVO O GENERO EL MOVIMIENTO
            $queryAprovarSoliTras = "UPDATE 
                            SOLICITUD_TRASLADO_EQUIPO 
                            SET 
                            ID_DETA_EMPLEADO_APROV_MODIF = $fcnIdEmpAuto,
                            ID_ESTADO_SOLICITUD_TRASLADO = $idEstadoAprov,
                            FECHA_APROV_MODIF = CURRENT_TIMESTAMP
                            WHERE ID_SOLICITUD_TRASLADO_EQUIPO = $fcnIdSolicitudTraslado
                            ";
            pg_query($queryAprovarSoliTras);
        }
        
    public function MoverEquipoBodega() {
        $fcnIdEquipo = $this->getIdEquipo();
        $fcnIdEmpleadoAutoriza = $this->getIdEmpleadoAutoriza();
        $fcnIdAreaOrigen = $this->getIdAreaDistritoOrigen();
        //area de destino la sacaremos del query de la solicitud
        $fcnIdEmpleadoAsignado = 'NULL'; //NO HAY EMPLEADO ASIGNADO EN LAS BODEGA SINO DEL TECNICO QUE LE DARA MANTENIMIENTO
        $fcnIdCaso = 'NULL';
        $fcnIdBitacoraFalse = $this->getIdBitacoraActualizar();
        $fcnIdSolicitudTraslado = $this->getIdSolicitudTrasladoEquipo();
        $fcnDetaTransaccion = "Aprovación de Solicitud de traslado a bodega numero ". $fcnIdSolicitudTraslado;
        
        //OBTENDREMOS PRIMERAMENTE EL ID DE LA BODEGA DE DESTINO POR MEDIO DEL ID DE LA SOLICITUD DE TRASLADO
        $queryAreaDestino = "SELECT ID_AREA_DISTRITO FROM SOLICITUD_TRASLADO_EQUIPO WHERE ID_SOLICITUD_TRASLADO_EQUIPO = $fcnIdSolicitudTraslado";
        $rsIdAreaDestino = pg_query($queryAreaDestino);
        $idAreaDestino = pg_fetch_result($rsIdAreaDestino, 0); //ID DEL AREA DE DESTINO QUE EN ESTE CASO ES BODEGA
        
        //OBTENDREMOS EL ID DEL TIPO DE TRANSACCION QUE ES MOVIMIENTO DE EQUIPO
        $queryTipoTrans = "SELECT ID_TIPO_TRANSACCION FROM TIPO_TRANSACCION WHERE TIPO_TRANSACCION LIKE '%MOVIMIENTO%%EQUIPO%'";
        $rsIdTipoTrans = pg_query($queryTipoTrans);
        $idTipoTransaccion = pg_fetch_result($rsIdTipoTrans, 0); //ID DE LA TRANSACCION
        
        //AHORA REALIZAMOS EL INSERT PORMEDIO DE UNA FUNCION
        $queryInsertarTransaccion = "SELECT Admon_Mover_Equipo('$fcnDetaTransaccion',$idTipoTransaccion,$fcnIdEquipo,$fcnIdEmpleadoAutoriza,$fcnIdAreaOrigen,$idAreaDestino,$fcnIdCaso,$fcnIdSolicitudTraslado)";
        $rsIdTransaccion = pg_query($queryInsertarTransaccion);
        $idTransaccionInsertada = pg_fetch_result($rsIdTransaccion, 0); //ID DE TRANSACCION INSERTADA
        
        //ACTUALIZAMOS LA BITACORA DE UBICACION ACTUAL A FALSE PARA INGRESAR PSTERIORMENTE LA NUEVA
        $queryActualizarBitacora = "UPDATE BITACORA_EQUIPO SET UBICACION_ACTUAL = FALSE WHERE ID_BITACORA = $fcnIdBitacoraFalse";
        pg_query($queryActualizarBitacora);
        
         //INSERTAMOS LA NUEVA BITACORA CON LA TRANSACCION OBTENIDA Y SU UBICACION ACTUAL TRUE
         $queryBitacoraMov = 
            "INSERT INTO BITACORA_EQUIPO 
            (ID_DETA_EMPLEADO_ASIGNADO,ID_TRANSACCION_EQUIPO,UBICACION_ACTUAL)VALUES($fcnIdEmpleadoAsignado,$idTransaccionInsertada,TRUE)";
         pg_query($queryBitacoraMov);
        
         //AHORA ACTUALIZAMOS LA SOLICITUD A APROVADA CON EL ID Y FECHA DEL EMPLEADO QUE LA APROVO O GENERO EL MOVIMIENTO
         //PRIMERO OBTENEMOS EL ID DE LA SOLICITUD QUE ES APROVADA
         $queryEstadoAprov = "SELECT ID_ESTADO_COMPRA FROM ESTADO_SOLI_COMPRA WHERE DESCRIPCION LIKE '%APRO%'"; //query lineal
         $rsEstadoAprov = pg_query($queryEstadoAprov);
         $idEstadoAprov = pg_fetch_result($rsEstadoAprov, 0);
            
         $queryAprovarSoliTras = "UPDATE 
                            SOLICITUD_TRASLADO_EQUIPO 
                            SET 
                            ID_DETA_EMPLEADO_APROV_MODIF = $fcnIdEmpleadoAutoriza,
                            ID_ESTADO_SOLICITUD_TRASLADO = $idEstadoAprov,
                            FECHA_APROV_MODIF = CURRENT_TIMESTAMP
                            WHERE ID_SOLICITUD_TRASLADO_EQUIPO = $fcnIdSolicitudTraslado
                            ";
         pg_query($queryAprovarSoliTras);
        
    }
        
 
    }
    

class SolicitudTrasladoEquipo extends EncaSolicitudCompra{
    private $descSoliTraslado = "";
    private $equipoTraslado = "";
    private $idEmpCreaSoliTras = "";
    private $idAreaDist = "";
    private $idSolicitudDenegar = "";
    private $idEmpleadoDenegaAprueba = "";
    private $idEquipoSeleccionado = "";
            
    public function getDescSoliTraslado() {
        return $this->descSoliTraslado;
    }

    public function getEquipoTraslado() {
        return $this->equipoTraslado;
    }

    public function getIdEmpCreaSoliTras() {
        return $this->idEmpCreaSoliTras;
    }

    public function getIdAreaDist() {
        return $this->idAreaDist;
    }

    public function setDescSoliTraslado($descSoliTraslado) {
        $this->descSoliTraslado = $descSoliTraslado;
    }

    public function setEquipoTraslado($equipoTraslado) {
        $this->equipoTraslado = $equipoTraslado;
    }

    public function setIdEmpCreaSoliTras($idEmpCreaSoliTras) {
        $this->idEmpCreaSoliTras = $idEmpCreaSoliTras;
    }


    public function setIdAreaDist($idAreaDist) {
        $this->idAreaDist = $idAreaDist;
    }
    
    public function getIdSolicitudDenegar() {
        return $this->idSolicitudDenegar;
    }

    public function getIdEmpleadoDenegaAprueba() {
        return $this->idEmpleadoDenegaAprueba;
    }

    public function setIdSolicitudDenegar($idSolicitudDenegar) {
        $this->idSolicitudDenegar = $idSolicitudDenegar;
    }

    public function setIdEmpleadoDenegaAprueba($idEmpleadoDenegaAprueba) {
        $this->idEmpleadoDenegaAprueba = $idEmpleadoDenegaAprueba;
    }
    
    public function getIdEquipoSeleccionado() {
        return $this->idEquipoSeleccionado;
    }

    public function setIdEquipoSeleccionado($idEquipoSeleccionado) {
        $this->idEquipoSeleccionado = $idEquipoSeleccionado;
    }

    
    
    function IngresarSolicitudTraslado(){
     $fcndescSoliTraslado = $this->getDescSoliTraslado();
     $fcnequipoTraslado = $this->getEquipoTraslado();
     $fcnidEmpCreaSoliTras = $this->getIdEmpCreaSoliTras();
     $fcnidAreaDist = $this->getIdAreaDist();
     $fcnidBitacoraCaso = $this->getIdCaso();
     
     $queryEstadoSoli = "SELECT ID_ESTADO_COMPRA FROM ESTADO_SOLI_COMPRA WHERE DESCRIPCION LIKE '%PENDI%'"; //query lineal
        $rsEstado = pg_query($queryEstadoSoli);
        $idEstadoSolicitud = pg_fetch_result($rsEstado, 0); // AQUI ESTA EL ESTADO PENDIENDE VALIDAR
        
     $queryTipoSolicitudTraslado = "select id_tipo_solicitud_traslado from tipo_solicitud_traslado where tipo_solicitud_traslado = 'ASIGNAR EQUIPO'";
     $rsTipoSolicitudTraslado = pg_query($queryTipoSolicitudTraslado);
     $idTipoSolicitud = pg_fetch_result($rsTipoSolicitudTraslado, 0);
     
     
     $query = "INSERT INTO solicitud_traslado_equipo(
            detalle_solicitud_traslado, detalle_equipo_traslado, 
            fecha_solicitud, id_deta_empleado_crea, id_estado_solicitud_traslado, 
            id_area_distrito, id_bitacora_caso, id_tipo_solicitud_traslado)
            VALUES ('$fcndescSoliTraslado', '$fcnequipoTraslado', CURRENT_TIMESTAMP, $fcnidEmpCreaSoliTras, $idEstadoSolicitud, $fcnidAreaDist, $fcnidBitacoraCaso, $idTipoSolicitud)";
     
     pg_query($query);
    }
    
    function IngresarSolicitudTrasladoBodega(){
        $fcndescSoliTraslado = $this->getDescSoliTraslado();
        $fcnequipoTraslado = $this->getEquipoTraslado();
        $fcnidEmpCreaSoliTras = $this->getIdEmpCreaSoliTras();
        $fcnidBitacoraCaso = $this->getIdCaso();
        $fcnIdEquipo = $this->getIdEquipoSeleccionado();
        

        $queryEstadoSoli = "SELECT ID_ESTADO_COMPRA FROM ESTADO_SOLI_COMPRA WHERE DESCRIPCION LIKE '%PENDI%'"; //query lineal
           $rsEstado = pg_query($queryEstadoSoli);
           $idEstadoSolicitud = pg_fetch_result($rsEstado, 0); // AQUI ESTA EL ESTADO PENDIENDE VALIDAR

        $queryTipoSolicitudTraslado = "select id_tipo_solicitud_traslado from tipo_solicitud_traslado where tipo_solicitud_traslado = 'MOVER A BODEGA'";
        $rsTipoSolicitudTraslado = pg_query($queryTipoSolicitudTraslado);
        $idTipoSolicitud = pg_fetch_result($rsTipoSolicitudTraslado, 0);

       $queryAreaDistritoBodega = "SELECT ID_AREA_DISTRITO FROM AREA_DISTRITO WHERE AREA='BODEGA EQUIPOS TI'";
       $RsAreaBodega = pg_query($queryAreaDistritoBodega);
       $idAreaBodega = pg_fetch_result($RsAreaBodega, 0);

        $query = "INSERT INTO solicitud_traslado_equipo(
               detalle_solicitud_traslado, detalle_equipo_traslado, 
               fecha_solicitud, id_deta_empleado_crea, id_estado_solicitud_traslado, 
               id_area_distrito, id_bitacora_caso, id_tipo_solicitud_traslado,id_equipo)
               VALUES ('$fcndescSoliTraslado', '$fcnequipoTraslado', CURRENT_TIMESTAMP, $fcnidEmpCreaSoliTras, $idEstadoSolicitud, $idAreaBodega, $fcnidBitacoraCaso, $idTipoSolicitud, $fcnIdEquipo)";

        pg_query($query); 
    }
    
    function DenegarSoliTraslado(){
        $fcnIdEmpleadoDenega = $this->getIdEmpleadoDenegaAprueba();
        $fcnIdSoliTrasDeneg = $this->getIdSolicitudDenegar();
        
        $queryEstadoDeneg = "SELECT ID_ESTADO_COMPRA FROM ESTADO_SOLI_COMPRA WHERE DESCRIPCION LIKE '%DENEG%'"; //query lineal
        $rsEstadoDeneg = pg_query($queryEstadoDeneg);
        $idEstadoDeneg = pg_fetch_result($rsEstadoDeneg, 0);
        
        $queryDenegar = "UPDATE 
                        SOLICITUD_TRASLADO_EQUIPO 
                        SET 
                        ID_DETA_EMPLEADO_APROV_MODIF = $fcnIdEmpleadoDenega,
                        ID_ESTADO_SOLICITUD_TRASLADO = $idEstadoDeneg,
                        FECHA_APROV_MODIF = CURRENT_TIMESTAMP
                        WHERE ID_SOLICITUD_TRASLADO_EQUIPO = $fcnIdSoliTrasDeneg
                        ";
        pg_query($queryDenegar);
    }
    
    function SelectSolicitudesPend(){
        $query = "SELECT * FROM Admon_Select_Solicitud_Traslado_Pend()";
        $rs = pg_query($query);
        
        return $rs;
    }
    
    function SelectSolicitudesPendId($idSolicitudTrasladoSeleccionada){
        $query = "select * from Admon_Select_Solicitud_Traslado_Seleccionada($idSolicitudTrasladoSeleccionada)";
        $rs = pg_query($query);
        
        return $rs;
    }
    
    function SelectEquiposMoverBodega(){
            $query 
                    = "SELECT * FROM
                        (
                        SELECT 
                        C.ID_EQUIPO, C.CODIGO_EQUIPO, F.M_TIPO_EQUIPO, E.TIPO_EQUIPO, H.DETALLE_DISTRITO, G.AREA,
                        (I.CODIGO_EMPLEADO || ' ' ||J.NOMBRE1_PERSONA || ' ' ||J.NOMBRE2_PERSONA) AS EMPLEADOASIG,
                        B.UBICACION_ACTUAL, (K.ID_EQUIPO) AS EXISTESOLI 
                        FROM TRANSACCION_EQUIPO A
                        INNER JOIN BITACORA_EQUIPO B ON (A.ID_TRANSACCION_EQUIPO = B.ID_TRANSACCION_EQUIPO)
                        INNER JOIN EQUIPO_TECNOLOGICO C ON (C.ID_EQUIPO = A.ID_EQUIPO)
                        INNER JOIN ESTADO_EQUIPO D ON (D.ID_ESTADO = C.ID_ESTADO)
                        INNER JOIN TIPO_EQUIPO E ON (E.ID_TIPO_EQUIPO = C.ID_TIPO_EQUIPO)
                        INNER JOIN M_TIPO_EQUIPO F ON (F.ID_M_TIPO_EQUIPO = E.ID_M_TIPO_EQUIPO)
                        INNER JOIN AREA_DISTRITO G ON (G.ID_AREA_DISTRITO = A.ID_AREA_DISTRITO_DESTINO)
                        INNER JOIN DISTRITO H ON (G.ID_DISTRITO = H.ID_DISTRITO)
                        LEFT JOIN DETALLE_EMPLEADO I ON (I.ID_DETA_EMPLEADO = B.ID_DETA_EMPLEADO_ASIGNADO)
                        LEFT JOIN PERSONA J ON (J.ID_PERSONA = I.ID_PERSONA)
                        LEFT JOIN SOLICITUD_TRASLADO_EQUIPO K ON (K.ID_EQUIPO = A.ID_EQUIPO AND K.ID_ESTADO_SOLICITUD_TRASLADO = 1)
                        WHERE 
                        B.UBICACION_ACTUAL = TRUE
                        AND D.ESTADO_EQUIPO = 'ACTIVO'
                        AND G.AREA NOT LIKE '%BODEGA%'
                    )AS TABLA 
                    WHERE TABLA.EXISTESOLI IS NULL
                        ";
            $rsEqMoverBodega = pg_query($query);
            
            return $rsEqMoverBodega;
    }
    
    function SelectEquiposMoverBodegaSeleccionado($idEquipoSelec){
        
            $query 
                    = "SELECT 
                        C.ID_EQUIPO, C.CODIGO_EQUIPO, F.M_TIPO_EQUIPO, E.TIPO_EQUIPO, H.DETALLE_DISTRITO, G.AREA,
                        (I.CODIGO_EMPLEADO || ' ' ||J.NOMBRE1_PERSONA || ' ' ||J.NOMBRE2_PERSONA) AS EMPLEADOASIG
                        FROM TRANSACCION_EQUIPO A
                        INNER JOIN BITACORA_EQUIPO B ON (A.ID_TRANSACCION_EQUIPO = B.ID_TRANSACCION_EQUIPO)
                        INNER JOIN EQUIPO_TECNOLOGICO C ON (C.ID_EQUIPO = A.ID_EQUIPO)
                        INNER JOIN ESTADO_EQUIPO D ON (D.ID_ESTADO = C.ID_ESTADO)
                        INNER JOIN TIPO_EQUIPO E ON (E.ID_TIPO_EQUIPO = C.ID_TIPO_EQUIPO)
                        INNER JOIN M_TIPO_EQUIPO F ON (F.ID_M_TIPO_EQUIPO = E.ID_M_TIPO_EQUIPO)
                        INNER JOIN AREA_DISTRITO G ON (G.ID_AREA_DISTRITO = A.ID_AREA_DISTRITO_DESTINO)
                        INNER JOIN DISTRITO H ON (G.ID_DISTRITO = H.ID_DISTRITO)
                        LEFT JOIN DETALLE_EMPLEADO I ON (I.ID_DETA_EMPLEADO = B.ID_DETA_EMPLEADO_ASIGNADO)
                        LEFT JOIN PERSONA J ON (J.ID_PERSONA = I.ID_PERSONA)
                        WHERE 
                        B.UBICACION_ACTUAL = TRUE
                        AND D.ESTADO_EQUIPO = 'ACTIVO'
                        AND G.AREA NOT LIKE '%BODEGA%'
                        AND A.ID_EQUIPO = $idEquipoSelec
                        ";
            $rsEqMoverBodega = pg_query($query);
            
            return $rsEqMoverBodega;
    }
    
    function SelectSolicitudesPorTecnico($idTecnico){
        $query = "SELECT 
                  A.ID_SOLICITUD_TRASLADO_EQUIPO,
                  A.DETALLE_SOLICITUD_TRASLADO,
                  A.DETALLE_EQUIPO_TRASLADO,
                  A.FECHA_SOLICITUD,
                  B.DESCRIPCION,
                  D2.DETALLE_DISTRITO,
                  D.AREA,
                  E.ID_SOLICITUD_CASO,
                  E.ID_BITACORA_CASO,
                  A.ID_EQUIPO,
                  C.APROVADOPOR,
                  A.FECHA_APROV_MODIF
                  FROM SOLICITUD_TRASLADO_EQUIPO A
                  INNER JOIN ESTADO_SOLI_COMPRA B ON (A.ID_ESTADO_SOLICITUD_TRASLADO = B.ID_ESTADO_COMPRA)
                  LEFT JOIN 
                  (
                  SELECT
                  (B1.NOMBRE1_PERSONA || ' ' || B1.APELLIDO1_PERSONA)AS APROVADOPOR,
                  A1.ID_DETA_EMPLEADO
                  FROM DETALLE_EMPLEADO A1
                  INNER JOIN PERSONA B1 ON (A1.ID_PERSONA = B1.ID_PERSONA)
                  )
                  C ON (A.ID_DETA_EMPLEADO_APROV_MODIF = C.ID_DETA_EMPLEADO)
                  INNER JOIN AREA_DISTRITO D ON (A.ID_AREA_DISTRITO = D.ID_AREA_DISTRITO)
                  INNER JOIN DISTRITO D2 ON (D.ID_DISTRITO = D2.ID_DISTRITO)
                  LEFT JOIN BITACORA_CASO E ON (A.ID_BITACORA_CASO = E.ID_BITACORA_CASO)
                  WHERE A.ID_DETA_EMPLEADO_CREA = $idTecnico
                  ORDER BY FECHA_SOLICITUD ASC
                  "
                  ;
        $rs = pg_query($query);
        
        return $rs;
    }
}

?>