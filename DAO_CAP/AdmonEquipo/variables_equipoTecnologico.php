<?php

include("../../DAO_CAP/Conexion/admon_conexion.php");

class EquipoTecnologico{
    private $descEquipo="";
    private $codEquipo="";
    private $idTipoEquipo="";
    private $idEstadoEquipo="";
    private $idTipoAdquisicion="";
    private $idModelo="";
    private $idEmpCreaModif="";
    private $idEquipo = "";
    
    public function getDescEquipo() {
        return $this->descEquipo;
    }

    public function getCodEquipo() {
        return $this->codEquipo;
    }

    public function getIdTipoEquipo() {
        return $this->idTipoEquipo;
    }

    public function getIdEstadoEquipo() {
        return $this->idEstadoEquipo;
    }

    public function getIdTipoAdquisicion() {
        return $this->idTipoAdquisicion;
    }

    public function getIdModelo() {
        return $this->idModelo;
    }

    public function getIdEmpCreaModif() {
        return $this->idEmpCreaModif;
    }
    
    public function getIdEquipo() {
        return $this->idEquipo;
    }

    public function setDescEquipo($descEquipo) {
        $this->descEquipo = $descEquipo;
    }

    public function setCodEquipo($codEquipo) {
        $this->codEquipo = $codEquipo;
    }

    public function setIdTipoEquipo($idTipoEquipo) {
        $this->idTipoEquipo = $idTipoEquipo;
    }

    public function setIdEstadoEquipo($idEstadoEquipo) {
        $this->idEstadoEquipo = $idEstadoEquipo;
    }

    public function setIdTipoAdquisicion($idTipoAdquisicion) {
        $this->idTipoAdquisicion = $idTipoAdquisicion;
    }

    public function setIdModelo($idModelo) {
        $this->idModelo = $idModelo;
    }

    public function setIdEmpCreaModif($idEmpCreaModif) {
        $this->idEmpCreaModif = $idEmpCreaModif;
    }

    public function setIdEquipo($idEquipo) {
        $this->idEquipo = $idEquipo;
    }

        
    //-----------------------------------------
    
    function ValidarCodEquipo(){
        $fcnCodEquipo = $this->getCodEquipo();
        $query = "SELECT CODIGO_EQUIPO FROM EQUIPO_TECNOLOGICO WHERE CODIGO_EQUIPO = '$fcnCodEquipo'";
        $rsCod = pg_query($query);
        $ExisteCod = pg_fetch_result($rsCod, 0);
        
        return $ExisteCod;
        
    }
    
    function DesactivarEquipo() {
        $fcnIdEquipo = $this->getIdEquipo();
        $idEmpDesactiva = $this->getIdEmpCreaModif();
        $idEstadoInactivo = 2;
        $query = "UPDATE EQUIPO_TECNOLOGICO
                      SET ID_ESTADO = $idEstadoInactivo,
                      ID_DETA_EMPLEADO_MODIF = $idEmpDesactiva,
                      FECHA_MODIFICACION = CURRENT_TIMESTAMP
                      WHERE ID_EQUIPO = $fcnIdEquipo";
        pg_query($query);
    }

    function ActivarEquipo() {
        $fcnIdEquipo = $this->getIdEquipo();
        $idEmpDesactiva = $this->getIdEmpCreaModif();
        $idEstadoInactivo = 1;
        $query = "UPDATE EQUIPO_TECNOLOGICO
                      SET ID_ESTADO = $idEstadoInactivo,
                      ID_DETA_EMPLEADO_MODIF = $idEmpDesactiva,
                      FECHA_MODIFICACION = CURRENT_TIMESTAMP
                      WHERE ID_EQUIPO = $fcnIdEquipo";
        pg_query($query);
    }

    function RepararEquipo() {
        $fcnIdEquipo = $this->getIdEquipo();
        $idEmpDesactiva = $this->getIdEmpCreaModif();
        $idEstadoInactivo = 3;
        $query = "UPDATE EQUIPO_TECNOLOGICO
                      SET ID_ESTADO = $idEstadoInactivo,
                      ID_DETA_EMPLEADO_MODIF = $idEmpDesactiva,
                      FECHA_MODIFICACION = CURRENT_TIMESTAMP
                      WHERE ID_EQUIPO = $fcnIdEquipo";
        pg_query($query);
    }

    function IngresarEquipoTec(){
        $fcnDescEquipo= $this->getDescEquipo();
        $fcnCodEquipo= $this->getCodEquipo();
        $fcnIdTipoEquipo= $this->getIdTipoEquipo();
        $fcnIdModelo= $this->getIdModelo();
        $fcnIdEstadoEquipo= $this->getIdEstadoEquipo();
        $fcnIdTipoAdquisicion= $this->getIdTipoAdquisicion();
        $fcnIdEmpCreaModif= $this->getIdEmpCreaModif();
         
        // el primer parametro es el valor del case en la funcion plpgsql y el ultimo es el id del equipo
        $queryInsertar = 
                "select Admon_Insertar_EquipoTecnologico('$fcnDescEquipo','$fcnCodEquipo',$fcnIdTipoEquipo,$fcnIdModelo,$fcnIdEstadoEquipo,$fcnIdTipoAdquisicion,$fcnIdEmpCreaModif,0)";
        
        $rsIdEquipo = pg_query($queryInsertar);
        $idEquipoInsertado = pg_fetch_result($rsIdEquipo, 0);
        
        $queryTransaccionIngresoEquipo = "select Admon_Insertar_Transaccion_IngresarEquipo($idEquipoInsertado,$fcnIdEmpCreaModif)";
        $rsIdTransaccionIngreso = pg_query($queryTransaccionIngresoEquipo);
        $idTransaccionIngresoEq = pg_fetch_result($rsIdTransaccionIngreso, 0);
        
        $queryInsertarBitacora = "INSERT INTO BITACORA_EQUIPO (
                                    ID_DETA_EMPLEADO_ASIGNADO,
                                    ID_TRANSACCION_EQUIPO,
                                    UBICACION_ACTUAL)
                                    VALUES
                                    (NULL,$idTransaccionIngresoEq,TRUE)";
        pg_query($queryInsertarBitacora);
    }
    
    function ModificarEquipoTec(){
        $fcnDescEquipo= $this->getDescEquipo();
        $fcnIdTipoEquipo= $this->getIdTipoEquipo();
        $fcnIdModelo= $this->getIdModelo();
        $fcnIdTipoAdquisicion= $this->getIdTipoAdquisicion();
        $fcnIdEmpCreaModif= $this->getIdEmpCreaModif();
        $fcnIdEquipo = $this->getIdEquipo();
        
        $queryModificar = "UPDATE EQUIPO_TECNOLOGICO 
                            SET
                            DESCRIPCION_EQUIPO = '$fcnDescEquipo',
                            ID_TIPO_EQUIPO = $fcnIdTipoEquipo,
                            ID_TIPO_ADQUISICION = $fcnIdTipoAdquisicion,
                            ID_DETA_EMPLEADO_MODIF = $fcnIdEmpCreaModif,
                            ID_MODELO_EQUIPO = $fcnIdModelo,
                            FECHA_MODIFICACION = CURRENT_TIMESTAMP
                            WHERE ID_EQUIPO = $fcnIdEquipo";
        pg_query($queryModificar);
    }
    
    function SelectValidarUbicacionEquipoCambioEstado($idEquipo){
        $query = "SELECT
                B.ID_AREA_DISTRITO_DESTINO
                FROM BITACORA_EQUIPO A
                INNER JOIN TRANSACCION_EQUIPO B ON (A.ID_TRANSACCION_EQUIPO = B.ID_TRANSACCION_EQUIPO)
                WHERE B.ID_EQUIPO = $idEquipo
                AND B.ID_TIPO_TRANSACCION IN (1,3)
                AND A.UBICACION_ACTUAL = TRUE;";
        $rs = pg_query($query);
        $idarea = pg_fetch_result($rs, 0);
        return $idarea;
    }
            
    function SelectFullDataEquipo(){
        $fcnIdEquipo = $this->getIdEquipo();
        $query = "SELECT * FROM Admon_Select_DatosFull_Equipo($fcnIdEquipo)";
        $rsDatos = pg_query($query);
        
        return $rsDatos;
    }
    
    function SelectDetalleSoftware(){
        $fcnIdEquipo = $this->getIdEquipo();
        $query = "SELECT * FROM Admon_Select_SoftwareInstalado_Eq($fcnIdEquipo)";
        $rsSoft = pg_query($query);
        
        return $rsSoft;
    }
    
    function SelectBitacoraEquipoRelacionado(){
        $fcnIdEquipo = $this->getIdEquipo();
        $query = "SELECT * FROM Admon_Select_Bitacora_Relacion_Equipo($fcnIdEquipo)";
        $rsRela = pg_query($query);
        
        return $rsRela;
    }
    
    function SelectDetalleEquipoRelacionado(){
        $fcnIdEquipo = $this->getIdEquipo();
        $query = "SELECT * FROM Admon_Select_Equipo_Relacionado_2($fcnIdEquipo)";
        $rsRela2 = pg_query($query);
        
        return $rsRela2;
    }
    
    function SelectBitacoraUbicaciones(){
        $fcnIdEquipo = $this->getIdEquipo();
        $query = "SELECT * FROM Admon_Select_Bitacora_Ubicacion($fcnIdEquipo)";
        $rsBitacoraEquipo = pg_query($query);
        
        return $rsBitacoraEquipo;
    }
    
    function SelectVerEquipos(){
        $queryEquipoV = "select * from Admon_Select_Ver_Equipos()";
        $rs = pg_query($queryEquipoV);
        return $rs;
    }
    
    function SelectInfoEquipoParaModificar() {
        $idEq = $this->getIdEquipo();
        $query = "SELECT 
                    A.ID_EQUIPO,
                    A.CODIGO_EQUIPO,
                    A.DESCRIPCION_EQUIPO,
                    F.ESTADO_EQUIPO,
                    D.TIPO_ADQUISICION,
                    E.M_TIPO_EQUIPO,
                    B.TIPO_EQUIPO,
                    G.MARCA_EQUIPO,
                    C.NOMBRE_MODELO
                    FROM EQUIPO_TECNOLOGICO A
                    INNER JOIN TIPO_EQUIPO B ON (A.ID_TIPO_EQUIPO = B.ID_TIPO_EQUIPO)
                    INNER JOIN MODELO_EQUIPO C ON (C.ID_MODELO_EQUIPO = A.ID_MODELO_EQUIPO)
                    INNER JOIN TIPO_ADQUISICION D ON (D.ID_TIPO_ADQUISICION = A.ID_TIPO_ADQUISICION)
                    INNER JOIN M_TIPO_EQUIPO E ON (E.ID_M_TIPO_EQUIPO = B.ID_M_TIPO_EQUIPO)
                    INNER JOIN ESTADO_EQUIPO F ON (F.ID_ESTADO = A.ID_ESTADO)
                    INNER JOIN MARCA_EQUIPO G ON (G.ID_MARCA_EQUIPO = C.ID_MARCA_EQUIPO)
                    WHERE A.ID_EQUIPO = $idEq";
        $rs = pg_query($query);
        return $rs;
    }

    function ReporteSelectUbicacionesEquipos(){
        $fcnIdEquipo = $this->getIdEquipo();
        
        if($fcnIdEquipo == 0){
            $fcnIdEquipo = 'NULL';
        }
        
        $query = "select * from Report_admon_ubicacion_actual($fcnIdEquipo)";
        $rs = pg_query($query);
        
        return $rs;
    }
    
    
}

?>

