
<?php

require ("../../DAO_CAP/AdmonEquipo/variables_solicitudCompra.php");

class MantenimientoSoftware {
    
    private $idSoftware = "";
    private $descSoftware = "";
    private $licencia = "";
    private $idTipoSoftware = "";
    
    public function getIdSoftware() {
        return $this->idSoftware;
    }

    public function getDescSoftware() {
        return $this->descSoftware;
    }

    public function getLicencia() {
        return $this->licencia;
    }

    public function getIdTipoSoftware() {
        return $this->idTipoSoftware;
    }

    public function setIdSoftware($idSoftware) {
        $this->idSoftware = $idSoftware;
    }

    public function setDescSoftware($descSoftware) {
        $this->descSoftware = $descSoftware;
    }

    public function setLicencia($licencia) {
        $this->licencia = $licencia;
    }

    public function setIdTipoSoftware($idTipoSoftware) {
        $this->idTipoSoftware = $idTipoSoftware;
    }

    function SelectSoftware(){
        $query = "SELECT 
                    A.ID_SOFTWARE,
                    A.DESCRIPCION_SOFTWARE,
                    A.LICENCIA_SOFTWARE,
                    B.ID_TIPO_SOFTWARE,
                    B.TIPO_SOFTWARE
                    FROM SOFTWARE A 
                    INNER JOIN TIPO_SOFTWARE B ON (A.ID_TIPO_SOFTWARE = B.ID_TIPO_SOFTWARE)
                    ORDER BY 1";
        $rsSoftware = pg_query($query);
        
        return $rsSoftware;
    }
    
    function SelectTipoSoftware($idParaRemover){
        $query = "SELECT ID_TIPO_SOFTWARE, TIPO_SOFTWARE FROM TIPO_SOFTWARE WHERE ID_TIPO_SOFTWARE != $idParaRemover";
        $rsTipoSoftware = pg_query($query);
        
        return $rsTipoSoftware;
    }
    
    function SelectTipoSoftwareParaInsertar(){
        $query = "SELECT ID_TIPO_SOFTWARE, TIPO_SOFTWARE FROM TIPO_SOFTWARE";
        $rsTipoSoftware = pg_query($query);
        
        return $rsTipoSoftware;
    }
    
    function ModificarSoftware(){
        $fcnIdSoftware = $this->getIdSoftware();
        $fcnDescSoftware = $this->getDescSoftware();
        $fcnLicencia = $this->getLicencia();
        $fcnIdTipoSoft = $this->getIdTipoSoftware();
        
        $query = "UPDATE SOFTWARE SET
                DESCRIPCION_SOFTWARE = '$fcnDescSoftware' ,
                LICENCIA_SOFTWARE = '$fcnLicencia' ,
                ID_TIPO_SOFTWARE = $fcnIdTipoSoft
                WHERE ID_SOFTWARE = $fcnIdSoftware ";
        
        pg_query($query);
    }
    
    function InsertarSoftware(){
        $fcnDescSoftware = $this->getDescSoftware();
        $fcnLicencia = $this->getLicencia();
        $fcnIdTipoSoft = $this->getIdTipoSoftware();
        
        $query = "INSERT INTO SOFTWARE (DESCRIPCION_SOFTWARE,LICENCIA_SOFTWARE,ID_TIPO_SOFTWARE)
                  VALUES ('$fcnDescSoftware','$fcnLicencia',$fcnIdTipoSoft)";
        
        pg_query($query);
    }
}
