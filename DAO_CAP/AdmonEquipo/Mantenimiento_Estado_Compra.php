
<?php

require ("../../DAO_CAP/AdmonEquipo/variables_solicitudCompra.php");

class MantenimientoEstado {
    
    private $idEstado_Compra = "";
    private $descEstado_Compra = "";
   
   public function getIdEstado_Compra(){
		return $this->idEstado_Compra;
	}

	public function setIdEstado_Compra($idEstado_Compra){
		$this->idEstado_Compra = $idEstado_Compra;
	}

	public function getDescEstado_Compra(){
		return $this->descEstado_Compra;
	}

	public function setDescEstado_Compra($descEstado_Compra){
		$this->descEstado_Compra = $descEstado_Compra;
	}

	function SelectEstado(){
        $query = "SELECT  
		ID_ESTADO_COMPRA,
		DESCRIPCION
		FROM ESTADO_SOLI_COMPRA";

        $rsEstado = pg_query($query);
        
        return $rsEstado;
    }
    function ModificarEstado_Compra(){
    	 $fcnidEstado_Compra = $this->getIdEstado_Compra();
       	 $fcndescEstado_Compra = $this->getDescEstado_Compra();

       	 $query = "UPDATE ESTADO_SOLI_COMPRA 
		SET DESCRIPCION = '$fcndescEstado_Compra'  
		WHERE ID_ESTADO_COMPRA = $fcnidEstado_Compra";
    pg_query($query);
    }
     function InsertarEstado_Compra(){
     	 $fcndescEstado_Compra = $this->getDescEstado_Compra();
     	 $query = "INSERT INTO ESTADO_SOLI_COMPRA (DESCRIPCION)
                  VALUES ('$fcndescEstado_Compra')";
      pg_query($query);
    }
}
