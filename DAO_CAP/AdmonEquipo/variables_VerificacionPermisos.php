<?php
class VerificacionPermisos{
    
        function SelectVerificacionPermisosPorUsuario($fcnIdTecnicoPermisos){
        $query = "SELECT 
                    A.ID_PERMISO,
                    A.DETALLE_PERMISO,
                    B.ID_DETA_EMPLEADO,
                    B.ACTIVO,
                    CASE WHEN B.ACTIVO = TRUE THEN 'SI' ELSE 'NO' END AS TIENE_PERMISO
                    FROM PERMISO A
                    LEFT JOIN USUARIO_PERMISO B ON (A.ID_PERMISO = B.ID_PERMISO 
                                                    AND B.ID_DETA_EMPLEADO = $fcnIdTecnicoPermisos
                                                    AND B.ACTIVO = TRUE)
                    ORDER BY A.ID_PERMISO ASC;";
        $rs = pg_query($query);
        return $rs;
    }
}

?>