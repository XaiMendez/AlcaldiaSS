<?php

require ("../../DAO_CAP/AdmonEquipo/variables_solicitudCompra.php");

class MantenimientoMarcaEquipo {
    
    private $id_Marca_Equipo = "";
    private $Marca_Equipo = "";
    private $idm_Tipo_Equipo = "";

    public function getId_Marca_Equipo(){
		return $this->id_Marca_Equipo;
	}

	public function setId_Marca_Equipo($id_Marca_Equipo){
		$this->id_Marca_Equipo = $id_Marca_Equipo;
	}

	public function getMarca_Equipo(){
		return $this->Marca_Equipo;
	}

	public function setMarca_Equipo($Marca_Equipo){
		$this->Marca_Equipo = $Marca_Equipo;
	}

	public function getIdm_Tipo_Equipo(){
		return $this->idm_Tipo_Equipo;
	}

	public function setIdm_Tipo_Equipo($idm_Tipo_Equipo){
		$this->idm_Tipo_Equipo = $idm_Tipo_Equipo;
	}
	function SelectMarca_Equipo(){
        $query = "	SELECT 
                    A.ID_MARCA_EQUIPO,
                    A.MARCA_EQUIPO,
                    B.ID_M_TIPO_EQUIPO,
                    B.M_TIPO_EQUIPO
                    FROM MARCA_EQUIPO A 
                    INNER JOIN M_TIPO_EQUIPO B ON (A.ID_M_TIPO_EQUIPO = B.ID_M_TIPO_EQUIPO)
                    ORDER BY 1";

        $rsMarcaEquipo = pg_query($query);
        
        return $rsMarcaEquipo;
    }
      function SelectMarEquipo($id_Remover){
        $query = "SELECT ID_M_TIPO_EQUIPO, M_TIPO_EQUIPO FROM M_TIPO_EQUIPO WHERE ID_M_TIPO_EQUIPO != $id_Remover";
        $rsMarEquipo = pg_query($query);
        
        return $rsMarEquipo;
    }
    function SelectMarcaEquipoParaInsertar(){
        $query = "SELECT ID_M_TIPO_EQUIPO, M_TIPO_EQUIPO FROM M_TIPO_EQUIPO";
        $rsMarEquipo = pg_query($query);
        
        return $rsMarEquipo;
    
    }
    function ModificarMarcaEquipo(){
        $fcnidMarca_Equipo = $this->getId_Marca_Equipo();
        $fcnmarca_Equipo = $this->getMarca_Equipo();
        $fcnidmTipo_Equipo = $this->getIdm_Tipo_Equipo();
        
        $query = " UPDATE MARCA_EQUIPO SET
                MARCA_EQUIPO = '$fcnmarca_Equipo' ,
                ID_M_TIPO_EQUIPO = $fcnidmTipo_Equipo
                WHERE ID_MARCA_EQUIPO = $fcnidMarca_Equipo";
        
        pg_query($query);	
    }
     function InsertarMarcaEquipo(){
        $fcnmarca_Equipo = $this->getMarca_Equipo();
        $fcnidmTipo_Equipo = $this->getIdm_Tipo_Equipo();
        
        $query = "INSERT INTO MARCA_EQUIPO (MARCA_EQUIPO,ID_M_TIPO_EQUIPO)
                  VALUES ('$fcnmarca_Equipo',$fcnidmTipo_Equipo)";
        
        pg_query($query);
    }
}
