<?php

include("../../DAO_CAP/Conexion/admon_conexion.php") ;

class Login {
    private $usuario="";
    private $contra ="";
    
    public function getUsuario() {
        return $this->usuario;
    }

    public function getContra() {
        return $this->contra;
    }

    public function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    public function setContra($contra) {
        $this->contra = $contra;
    }
    
    //-------------------------------------
    function ValidarLogin() {
        $fcnUsuario = $this->getUsuario();
        $fcnContra = $this->getContra();
        $query = "SELECT * FROM Admon_Validar_LogIn('$fcnUsuario','$fcnContra')";
        $resultadoQueryLogin = pg_query($query);
        //$idEmpleadoLogeado = pg_fetch_result($resultadoQueryLogin, 0); //obtenemos el dato de la consulta
        
        return $resultadoQueryLogin; //van el id del usuario ingresado y su rol
        }
    //--------------------------------------
    
    // FUNCION HECHA PARA ENCRIPTAR CONTRASE;AS EN EL CASO QUE LAS TENGAMOS COMO TEXTO PLANO EN LA BD
        // ESTA FUNCION NO SE USA EN PROCESOS NI EN VALIDACIONES.
    function EncriptarContra(){
        $fcnUsuario = $this->getUsuario();
        $fcnContra = $this->getContra();
        $hashedPass = crypt($fcnContra , '$5$');
        
        $query = "UPDATE DETALLE_EMPLEADO SET PASS_DETA_PERSONA = '$hashedPass' WHERE USUARIO_DETA_PERSONA = '$fcnUsuario'";
        pg_query($query);

    }
} //fin clase LogIn


class AccesoEmpleado {
     private $idUsuario="";
     
     public function getIdUsuario() {
         return $this->idUsuario;
     }

     public function setIdUsuario($idUsuario) {
         $this->idUsuario = $idUsuario;
     }
     
     //------------------ metodos -------------------------
     function  InsertarAccesoEmpleado() {
        $fcnIdUsuarioAcceso = $this->getIdUsuario();
        $query = "SELECT admon_insertar_acceso_empleado($fcnIdUsuarioAcceso)" ;
        pg_query($query);
        }


} // fin clase AccesoEmpleado

class NombreEmpleadoLogeado {
    private $idEmpleado="";
    
    public function getIdEmpleado() {
        return $this->idEmpleado;
    }

    public function setIdEmpleado($idEmpleado) {
        $this->idEmpleado = $idEmpleado;
    }

    //---------------- metodos ------------------
    function RetornarNombreUsuarioLogeado(){
        $fcnIdEmpleado = $this->getIdEmpleado();
        $query = "SELECT * FROM Admon_Retornar_Nombre_EmpLogueado($fcnIdEmpleado)";
        $RsEmpleadoLogeado = pg_query($query);
        $NombreEmpleado = pg_fetch_result($RsEmpleadoLogeado, 0);
        
        return $NombreEmpleado;
    }
}