<?php

require ("../../DAO_CAP/AdmonEquipo/variables_solicitudCompra.php");

class MantenimientoModeloEquipo {
    
    private $id_Modelo_Equipo = "";
    private $Nombre_Modelo = "";
    private $idMarca_Equipo = "";

  public function getId_Modelo_Equipo(){
		return $this->id_Modelo_Equipo;
	}

	public function setId_Modelo_Equipo($id_Modelo_Equipo){
		$this->id_Modelo_Equipo = $id_Modelo_Equipo;
	}

	public function getNombre_Modelo(){
		return $this->Nombre_Modelo;
	}

	public function setNombre_Modelo($Nombre_Modelo){
		$this->Nombre_Modelo = $Nombre_Modelo;
	}

	public function getIdMarca_Equipo(){
		return $this->idMarca_Equipo;
	}

	public function setIdMarca_Equipo($idMarca_Equipo){
		$this->idMarca_Equipo = $idMarca_Equipo;
	}
	function SelectModelo_Equipo(){
        $query = "SELECT 
                    A.ID_MODELO_EQUIPO,
                    A.NOMBRE_MODELO,
                    B.ID_MARCA_EQUIPO,
                    B.MARCA_EQUIPO
                    FROM MODELO_EQUIPO A 
                    INNER JOIN MARCA_EQUIPO B ON (A.ID_MARCA_EQUIPO = B.ID_MARCA_EQUIPO)
                    ORDER BY 1";


        $rsModeloEquipo = pg_query($query);
        
        return $rsModeloEquipo;
    }
    function SelectModEquipo($id_Remover){
        $query = "SELECT ID_MARCA_EQUIPO, MARCA_EQUIPO FROM MARCA_EQUIPO WHERE ID_MARCA_EQUIPO != $id_Remover";
        $rsModEquipo = pg_query($query);
        
        return $rsModEquipo;
    }
    function SelectModeloEquipoParaInsertar(){
        $query = "SELECT ID_MARCA_EQUIPO, MARCA_EQUIPO FROM MARCA_EQUIPO;";
        $rsModEquipo = pg_query($query);
        
        return $rsModEquipo;
    }
      function ModificarModeloEquipo(){
        $fcnidModelo_Equipo = $this->getId_Modelo_Equipo();
        $fcnnombre_Modelo = $this->getNombre_Modelo();
        $fcnidmarca_Equipo = $this->getIdMarca_Equipo();
        
        $query = " UPDATE MODELO_EQUIPO SET
                NOMBRE_MODELO = '$fcnnombre_Modelo' ,
                ID_MARCA_EQUIPO = $fcnidmarca_Equipo
                WHERE ID_MODELO_EQUIPO = $fcnidModelo_Equipo";
        
        pg_query($query);	
     }
     function InsertarModeloEquipo(){
        $fcnnombre_Modelo = $this->getNombre_Modelo();
        $fcnidmarca_Equipo = $this->getIdMarca_Equipo();
        
        $query = "INSERT INTO MODELO_EQUIPO (NOMBRE_MODELO,ID_MARCA_EQUIPO)
                  VALUES ('$fcnnombre_Modelo',$fcnidmarca_Equipo)";
        
        pg_query($query);
    }
}
