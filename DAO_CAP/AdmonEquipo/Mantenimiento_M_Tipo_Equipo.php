<?php

require ("../../DAO_CAP/AdmonEquipo/variables_solicitudCompra.php");

class MantenimientoM_TipoEquipo {
    
    private $id_m_Tipo_Equipo = "";
    private $m_tipo_Equipo = "";
    private $idm2_Tipo_Equipo = "";

	public function getId_m_Tipo_Equipo(){
		return $this->id_m_Tipo_Equipo;
	}

	public function setId_m_Tipo_Equipo($id_m_Tipo_Equipo){
		$this->id_m_Tipo_Equipo = $id_m_Tipo_Equipo;
	}

	public function getM_tipo_Equipo(){
		return $this->m_tipo_Equipo;
	}

	public function setM_tipo_Equipo($m_tipo_Equipo){
		$this->m_tipo_Equipo = $m_tipo_Equipo;
	}

	public function getIdm2_Tipo_Equipo(){
		return $this->idm2_Tipo_Equipo;
	}

	public function setIdm2_Tipo_Equipo($idm2_Tipo_Equipo){
		$this->idm2_Tipo_Equipo = $idm2_Tipo_Equipo;
	}

	function SelectMTipoEquipo(){
        $query = "SELECT 
                    A.ID_M_TIPO_EQUIPO,
                    A.M_TIPO_EQUIPO,
                    B.ID_M2_TIPO_EQUIPO,
                    B.M2_TIPO_EQUIPO
                    FROM M_TIPO_EQUIPO A 
                    INNER JOIN M2_TIPO_EQUIPO B ON (A.ID_M2_TIPO_EQUIPO = B.ID_M2_TIPO_EQUIPO)
                    ORDER BY 1";

        $rsMTipoEquipo = pg_query($query);
        
        return $rsMTipoEquipo;
    }
    function SelectMEquipo($idmover){
        $query = "SELECT ID_M2_TIPO_EQUIPO, M2_TIPO_EQUIPO FROM M2_TIPO_EQUIPO WHERE ID_M2_TIPO_EQUIPO != $idmover";
        $rsTEquipo = pg_query($query);
        
        return $rsTEquipo;
    }
    function SelectMTipoEquipoParaInsertar(){
        $query = "SELECT ID_M2_TIPO_EQUIPO, M2_TIPO_EQUIPO FROM M2_TIPO_EQUIPO";
        $rsTEquipo = pg_query($query);
        
        return $rsTEquipo;
         }
    function ModificarMEquipo(){
        $fcnidm_TipoEquipo = $this->getId_m_Tipo_Equipo();
        $fcnm_tipo_Equipo = $this->getM_tipo_Equipo();
        $fcnidm2Tipo_Equipo = $this->getIdm2_Tipo_Equipo();
        
        $query = " UPDATE M_TIPO_EQUIPO SET
                M_TIPO_EQUIPO = '$fcnm_tipo_Equipo' ,
                ID_M2_TIPO_EQUIPO = $fcnidm2Tipo_Equipo
                WHERE ID_M_TIPO_EQUIPO = $fcnidm_TipoEquipo";
        
        pg_query($query);
    }
    function InsertarMEquipo(){
        $fcnm_tipo_Equipo = $this->getM_tipo_Equipo();
        $fcnidm2Tipo_Equipo = $this->getIdm2_Tipo_Equipo();
        
        $query = " INSERT INTO M_TIPO_EQUIPO (M_TIPO_EQUIPO,ID_M2_TIPO_EQUIPO)
                  VALUES ('$fcnm_tipo_Equipo',$fcnidm2Tipo_Equipo)";
        
        pg_query($query);
    }
}
