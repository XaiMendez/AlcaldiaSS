<?php

require ("../../DAO_CAP/AdmonEquipo/variables_solicitudCompra.php");

class MantenimientoEstadoEquipo {
    
    private $idEstado = "";
    private $estadoEquipo = "";

    	public function getIdEstado(){
		return $this->idEstado;
	}

	public function setIdEstado($idEstado){
		$this->idEstado = $idEstado;
	}

	public function getEstadoEquipo(){
		return $this->estadoEquipo;
	}

	public function setEstadoEquipo($estadoEquipo){
		$this->estadoEquipo = $estadoEquipo;
	}

	function SelectEstado_Equipo(){
        $query = "SELECT  
		ID_ESTADO,
		ESTADO_EQUIPO
		FROM ESTADO_EQUIPO";

        $rsEstadoEquipo = pg_query($query);
        
        return $rsEstadoEquipo;
    }
    function ModificarEstado_Equipo(){
    	 $fcnidEstado_Equipo = $this->getIdEstado();
       	 $fcnEstado_Equipo = $this->getEstadoEquipo();

       	 $query = "UPDATE ESTADO_EQUIPO 
		SET ESTADO_EQUIPO = '$fcnEstado_Equipo'  
		WHERE ID_ESTADO =  $fcnidEstado_Equipo";
    pg_query($query);
    }
     function InsertarEstado_Equipo(){
     	 $fcnEstado_Equipo = $this->getEstadoEquipo();
     	 $query = "INSERT INTO ESTADO_EQUIPO (ESTADO_EQUIPO)
                  VALUES ('$fcnEstado_Equipo')";
      pg_query($query);
    }
}
