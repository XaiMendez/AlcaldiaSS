<?php

include("../../DAO_CAP/Conexion/admon_conexion.php");

class IngresarSoftwareEquipo {
    private $idEquipo = "";
    private $idSoftware = "";
    private $idEmpleadoIns = "";
    private $idDesact = "";
    
    public function getIdDesact() {
        return $this->idDesact;
    }
        
    public function getIdEquipo() {
        return $this->idEquipo;
    }

    public function getIdSoftware() {
        return $this->idSoftware;
    }

    public function setIdDesact($idDesact) {
        $this->idDesact = $idDesact;
    }

    public function getIdEmpleadoIns() {
        return $this->idEmpleadoIns;
    }

    public function setIdEquipo($idEquipo) {
        $this->idEquipo = $idEquipo;
    }

    public function setIdSoftware($idSoftware) {
        $this->idSoftware = $idSoftware;
    }

    public function setIdEmpleadoIns($idEmpleadoIns) {
        $this->idEmpleadoIns = $idEmpleadoIns;
    }

    //------------------------------------------
    function IngresarSoftware(){
        $fcnIdEqu = $this->getIdEquipo();
        $fcnIdSoft = $this->getIdSoftware();
        $fcnIdEmp = $this->getIdEmpleadoIns();
        
        $queryIn = "SELECT Admon_IngSoftware_Man(1,$fcnIdEqu,$fcnIdSoft,$fcnIdEmp,0)";
        pg_query($queryIn);
    }

    function validarSoftwareActivo(){
        $fcnIdEquipo = $this->getIdEquipo();
        $fcnIdSoftware = $this->getIdSoftware();
        
        $queryValid = "SELECT CASE WHEN ACTIVO = TRUE THEN 'SI' ELSE 'NO' END AS ESTADO_SOFT 
                        FROM EQUIPO_SOFTWARE 
                        WHERE 
                        ID_EQUIPO = $fcnIdEquipo
                        AND ID_SOFTWARE = $fcnIdSoftware
                        AND ID_EMPLEADO_DESINS ISNULL";
        $rsValid = pg_query($queryValid);
        $activo = pg_fetch_result($rsValid, 0);
        
        return $activo;
    }
    
    function validarDesactivarSoftware(){
        $fcnLinea = $this->getIdDesact();
        
        $queryValidDes = "SELECT ID_EMPLEADO_DESINS FROM EQUIPO_SOFTWARE WHERE ID_EQUIPO_SOFTWARE = $fcnLinea";
        $rsDesac = pg_query($queryValidDes);
        $noActivo = pg_fetch_result($rsDesac, 0);
        
        return $noActivo;
    }
    
    function EditarSoftware(){
        $fcnIdEmpDesact = $this->getIdEmpleadoIns();
        $fcnIdEqSoftDesac = $this->getIdDesact();
        
        $queryDesact = "SELECT Admon_IngSoftware_Man(2,0,0,$fcnIdEmpDesact,$fcnIdEqSoftDesac)";
        pg_query($queryDesact);
    }
    
    function SelectEquiposParaSoftware(){
        $queryEquipoS = "SELECT
                        A.ID_EQUIPO,
                        A.CODIGO_EQUIPO,
                        C.M_TIPO_EQUIPO,
                        B.TIPO_EQUIPO,
                        D.ESTADO_EQUIPO
                        FROM 
                        EQUIPO_TECNOLOGICO A 
                        INNER JOIN TIPO_EQUIPO B ON (A.ID_TIPO_EQUIPO = B.ID_TIPO_EQUIPO)
                        INNER JOIN M_TIPO_EQUIPO C ON (B.ID_M_TIPO_EQUIPO = C.ID_M_TIPO_EQUIPO)
                        INNER JOIN ESTADO_EQUIPO D ON (A.ID_ESTADO = D.ID_ESTADO)
                        WHERE C.ID_M2_TIPO_EQUIPO = 2
                        ;";
        $rs = pg_query($queryEquipoS);
        return $rs;
    }
    
}

