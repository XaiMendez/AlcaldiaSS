<?php

require ("../../DAO_CAP/AdmonEquipo/variables_solicitudCompra.php");

class MantenimientoTipoEquipo {
    
    private $idTipo_Equipo = "";
    private $tipo_Equipo = "";
    private $idmTipo_Equipo = "";

	public function getIdTipo_Equipo(){
		return $this->idTipo_Equipo;
	}

	public function setIdTipo_Equipo($idTipo_Equipo){
		$this->idTipo_Equipo = $idTipo_Equipo;
	}

	public function getTipo_Equipo(){
		return $this->tipo_Equipo;
	}

	public function setTipo_Equipo($tipo_Equipo){
		$this->tipo_Equipo = $tipo_Equipo;
	}

	public function getIdmTipo_Equipo(){
		return $this->idmTipo_Equipo;
	}

	public function setIdmTipo_Equipo($idmTipo_Equipo){
		$this->idmTipo_Equipo = $idmTipo_Equipo;
	}

	function SelectTipoEquipo(){
        $query = "	SELECT 
                    A.ID_TIPO_EQUIPO,
                    A.TIPO_EQUIPO,
                    B.ID_M_TIPO_EQUIPO,
                    B.M_TIPO_EQUIPO
                    FROM TIPO_EQUIPO A 
                    INNER JOIN M_TIPO_EQUIPO B ON (A.ID_M_TIPO_EQUIPO = B.ID_M_TIPO_EQUIPO)
                    ORDER BY 1";

        $rsTipoEquipo = pg_query($query);
        
        return $rsTipoEquipo;
    }
      function SelectEquipo($idRemover){
        $query = "SELECT ID_M_TIPO_EQUIPO, M_TIPO_EQUIPO FROM M_TIPO_EQUIPO WHERE ID_M_TIPO_EQUIPO != $idRemover";
        $rsEquipo = pg_query($query);
        
        return $rsEquipo;
    }
    function SelectTipoEquipoParaInsertar(){
        $query = "SELECT ID_M_TIPO_EQUIPO, M_TIPO_EQUIPO FROM M_TIPO_EQUIPO";
        $rsEquipo = pg_query($query);
        
        return $rsEquipo;
    }
    function ModificarEquipo(){
        $fcnidTipo_Equipo = $this->getIdTipo_Equipo();
        $fcntipo_Equipo = $this->getTipo_Equipo();
        $fcnidmTipo_Equipo = $this->getIdmTipo_Equipo();
        
        $query = " UPDATE TIPO_EQUIPO SET
                TIPO_EQUIPO = '$fcntipo_Equipo' ,
                ID_M_TIPO_EQUIPO = $fcnidmTipo_Equipo
                WHERE ID_TIPO_EQUIPO = $fcnidTipo_Equipo";
        
        pg_query($query);
    }
     function InsertarEquipo(){
        $fcntipo_Equipo = $this->getTipo_Equipo();
        $fcnidmTipo_Equipo = $this->getIdmTipo_Equipo();
        
        $query = "INSERT INTO TIPO_EQUIPO (TIPO_EQUIPO,ID_M_TIPO_EQUIPO)
                  VALUES ('$fcntipo_Equipo',$fcnidmTipo_Equipo)";
        
        pg_query($query);
    }
}
