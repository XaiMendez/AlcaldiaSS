<?php
 

include("../../DAO_CAP/Conexion/admon_conexion.php") ;
class IngresoDetalleCpu{
    
    private $almacenamiento="";
    private $usuarioDeRed="";
    private $nombreDeEquipo="";
    private $idSoftware="";
    private $idUndAlmac="";
    private $idProcesador="";
    private $idRam="";
    private $idEquipo="";
    private $idEmpleado="";
    private $CantidadMemoria="";
    
   
    
    public function getAlmacenamiento() {
        return $this->almacenamiento;
    }

    public function getUsuarioDeRed() {
        return $this->usuarioDeRed;
    }

    public function getNombreDeEquipo() {
        return $this->nombreDeEquipo;
    }

    public function getIdSoftware() {
        return $this->idSoftware;
    }

    public function getIdUndAlmac() {
        return $this->idUndAlmac;
    }

    public function getIdProcesador() {
        return $this->idProcesador;
    }

    public function getIdRam() {
        return $this->idRam;
    }

    public function getIdEquipo() {
        return $this->idEquipo;
    }

    public function getIdEmpleado() {
        return $this->idEmpleado;
    }

    public function setAlmacenamiento($almacenamiento) {
        $this->almacenamiento = $almacenamiento;
    }

    public function setUsuarioDeRed($usuarioDeRed) {
        $this->usuarioDeRed = $usuarioDeRed;
    }

    public function setNombreDeEquipo($nombreDeEquipo) {
        $this->nombreDeEquipo = $nombreDeEquipo;
    }

    public function setIdSoftware($idSoftware) {
        $this->idSoftware = $idSoftware;
    }

    public function setIdUndAlmac($idUndAlmac) {
        $this->idUndAlmac = $idUndAlmac;
    }

    public function setIdProcesador($idProcesador) {
        $this->idProcesador = $idProcesador;
    }

    public function setIdRam($idRam) {
        $this->idRam = $idRam;
    }

    public function setIdEquipo($idEquipo) {
        $this->idEquipo = $idEquipo;
    }

    public function setIdEmpleado($idEmpleado) {
        $this->idEmpleado = $idEmpleado;
    }
    
    public function getCantidadMemoria() {
        return $this->CantidadMemoria;
    }

    public function setCantidadMemoria($CantidadMemoria) {
        $this->CantidadMemoria = $CantidadMemoria;
    }
    
    function IngresarDetalleCpu(){
        $fcnAlmacenamientoDefecto = $this->getAlmacenamiento();
        $fcnUsuarioRed = $this->getUsuarioDeRed();
        $fcnNombreEquipo = $this->getNombreDeEquipo();
        $fcnIdEquipo = $this->getIdEquipo();
        $fcnIdSoftwareSo = $this->getIdSoftware();
        $fcnUnidadAlmacenamientoHarddrive = $this->getIdUndAlmac();
        $fcnIdDetaEmpleadoCrea = $this->getIdEmpleado();
        $fcnIdMarcaProcesador = $this->getIdProcesador();
        $fcnIdMarcaRam = $this->getIdRam();
        $fcnCantidadRam = $this->getCantidadMemoria();
        
        $query="SELECT detalle_cpu($fcnAlmacenamientoDefecto,'$fcnUsuarioRed','$fcnNombreEquipo',$fcnIdEquipo,$fcnIdSoftwareSo,$fcnUnidadAlmacenamientoHarddrive,$fcnIdDetaEmpleadoCrea,$fcnIdMarcaProcesador,$fcnIdMarcaRam,$fcnCantidadRam)";
        pg_query($query);

        
    }
    
    function SelecteDatosFullDetaCpu(){
        $fcnIdEquipoSeleccionado = $this->getIdEquipo();
        
        $query = "SELECT 
                    A.ID_DETALLE_CPU,
                    A.ID_EQUIPO,
                    A.USUARIO_RED,
                    A.NOMBRE_EQUIPO,
                    B.DESCRIPCION_SOFTWARE,
                    A.ALMACENAMIENTO_DEFECTO,
                    (C.CODE_UNIDAD || ' ' ||C.DESCRIPCION_UNIDAD) AS UNIDADALMACENAMIENTO,
                    PROCE.MARCA_EQUIPO AS marca_proc,
                    PROCE.NOMBRE_MODELO AS modelo_proc,
                    RAM.MARCA_EQUIPO AS marca_ram,
                    RAM.NOMBRE_MODELO AS modelo_ram,
                    A.CANTIDAD_MEMORIA_RAM,
                    E.CODIGO_EMPLEADO, --CREADOR POR 
                    A.FECHA_CREACION
                    FROM DETALLE_CPU A
                    INNER JOIN SOFTWARE B ON (A.ID_SOFTWARE_SO = B.ID_SOFTWARE)
                    INNER JOIN UNIDAD_ALMACENAMIENTO C ON (C.ID_UNIDAD_ALMACENAMIENTO = ID_UNIDAD_ALMACENAMIENTO_HARDDRIVE)
                    INNER JOIN MODELO_EQUIPO D ON (D.ID_MODELO_EQUIPO = A.ID_MODELO_PROCESADOR)
                    INNER JOIN 
                    (
                            SELECT 
                            B1.ID_MODELO_EQUIPO,
                            A1.MARCA_EQUIPO ,
                            B1.NOMBRE_MODELO 
                            FROM MARCA_EQUIPO A1 
                            INNER JOIN MODELO_EQUIPO B1 ON (A1.ID_MARCA_EQUIPO = B1.ID_MARCA_EQUIPO)
                            INNER JOIN M_TIPO_EQUIPO C1 ON (A1.ID_M_TIPO_EQUIPO = C1.ID_M_TIPO_EQUIPO)
                            WHERE M_TIPO_EQUIPO = 'PROCESADOR'
                    ) AS PROCE ON (A.ID_MODELO_PROCESADOR = PROCE.ID_MODELO_EQUIPO)
                    INNER JOIN 
                    (
                            SELECT 
                            B1.ID_MODELO_EQUIPO,
                            A1.MARCA_EQUIPO ,
                            B1.NOMBRE_MODELO 
                            FROM MARCA_EQUIPO A1 
                            INNER JOIN MODELO_EQUIPO B1 ON (A1.ID_MARCA_EQUIPO = B1.ID_MARCA_EQUIPO)
                            INNER JOIN M_TIPO_EQUIPO C1 ON (A1.ID_M_TIPO_EQUIPO = C1.ID_M_TIPO_EQUIPO)
                            WHERE M_TIPO_EQUIPO = 'MEMORIA RAM'
                    ) AS RAM ON (A.ID_MODELO_RAM = RAM.ID_MODELO_EQUIPO)
                    INNER JOIN DETALLE_EMPLEADO E ON (A.ID_DETA_EMPLEADO_CREA = E.ID_DETA_EMPLEADO)
                    LEFT JOIN DETALLE_EMPLEADO F ON (A.ID_DETA_EMPLEADO_MODIF = F.ID_DETA_EMPLEADO)
                    WHERE A.ID_EQUIPO = $fcnIdEquipoSeleccionado
                    AND A.ID_DETALLE_CPU = (SELECT MAX(ID_DETALLE_CPU) FROM DETALLE_CPU WHERE ID_EQUIPO = $fcnIdEquipoSeleccionado)";
        $rs = pg_query($query);
        
        return $rs;
    }
    
    function SelectEquiposParaDetalleCPU(){
        $query = "SELECT *
                    FROM
                    (
                    SELECT 
                    A.ID_EQUIPO,
                    A.CODIGO_EQUIPO,
                    C.M_TIPO_EQUIPO,
                    B.TIPO_EQUIPO,
                    D.ESTADO_EQUIPO,
                    E.ID_EQUIPO AS TIENEDETA
                    FROM EQUIPO_TECNOLOGICO A 
                    INNER JOIN TIPO_EQUIPO B ON (A.ID_TIPO_EQUIPO = B.ID_TIPO_EQUIPO)
                    INNER JOIN M_TIPO_EQUIPO C ON (B.ID_M_TIPO_EQUIPO = C.ID_M_TIPO_EQUIPO)
                    INNER JOIN ESTADO_EQUIPO D ON (A.ID_ESTADO = D.ID_ESTADO)
                    LEFT JOIN DETALLE_CPU E ON (A.ID_EQUIPO = E.ID_EQUIPO)
                    WHERE C.ID_M_TIPO_EQUIPO = 4
                    GROUP BY 
                    A.ID_EQUIPO,
                    A.CODIGO_EQUIPO,
                    C.M_TIPO_EQUIPO,
                    B.TIPO_EQUIPO,
                    D.ESTADO_EQUIPO,
                    E.ID_EQUIPO
                    )AS TABLA 
                    ";
        $rs = pg_query($query);
        return $rs;
    }



        
}