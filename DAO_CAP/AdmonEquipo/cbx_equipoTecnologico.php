<?php

include("../../DAO_CAP/Conexion/admon_conexion.php") ;

function combo_estado_equipo(){
    $queryEstadoEquipo = "SELECT ID_ESTADO,ESTADO_EQUIPO FROM ESTADO_EQUIPO";
    $nombreCombo = "cbxEstadoEquipo";
    $idEstado = "id_estado";
    $estado = "estado_equipo";
    
    $rsEstadoEquipo = pg_query($queryEstadoEquipo);
    $html_comboEstado = "<select class='form-control m-bot15' name=$nombreCombo>\n";
    
    while($fila = pg_fetch_assoc($rsEstadoEquipo)){
        $html_comboEstado=$html_comboEstado."<option value='".$fila[$idEstado]."'>".$fila[$estado]."</option>\n";
    }
    $html_comboEstado=$html_comboEstado."</select>";
    
    return $html_comboEstado;
}

function combo_tipo_adquisicion(){
    $queryTipoAdquisicion = "SELECT ID_TIPO_ADQUISICION,TIPO_ADQUISICION FROM TIPO_ADQUISICION";
    $nombreCombo = "cbxTipoAdquisicion";
    $idAdquisicion = "id_tipo_adquisicion";
    $adquisicion = "tipo_adquisicion";
    
    $rsTipoAdquisicion = pg_query($queryTipoAdquisicion);
    $html_comboAdquisicion = "<select class='form-control m-bot15' name=$nombreCombo>\n";
    
    while($fila = pg_fetch_assoc($rsTipoAdquisicion)){
        $html_comboAdquisicion=$html_comboAdquisicion."<option value='".$fila[$idAdquisicion]."'>".$fila[$adquisicion]."</option>\n";
    }
    $html_comboAdquisicion=$html_comboAdquisicion."</select>";
    
    return $html_comboAdquisicion;
}



/*
 *si se quiere hacer en la pagina de vistas
 <select name="cbxEstadoEquipo" class="form-control m-bot15">
                             <?php 
                             $queryEstadoEquipo = "SELECT ID_ESTADO,ESTADO_EQUIPO FROM ESTADO_EQUIPO";
                             $rsEstadoEquipo = pg_query($queryEstadoEquipo);
                             $numEstadoEquipo = pg_num_rows($rsEstadoEquipo);
                             for ($Estado=0;$Estado<$numEstadoEquipo;$Estado++)
                             {
                                 $idEstadoEq = pg_fetch_result($rsEstadoEquipo, $Estado, 'ID_ESTADO');
                                 $EstadoEq = pg_fetch_result($rsEstadoEquipo, $Estado, 'ESTADO_EQUIPO');
                                 printf("<option value=%d> %s </option>",$idEstadoEq,$EstadoEq);
                             }
                             ?>
                        </select>
 *  o de esta otra forma
  <select name="cbxMarcaEquipo" class="form-control m-bot15" id="Marca">
                            <?php 
                            $queryMarca = "SELECT ID_MARCA_EQUIPO,MARCA_EQUIPO FROM MARCA_EQUIPO";
                            $rsMarca = pg_query($queryMarca); 
                            while ($Marca = pg_fetch_assoc($rsMarca)) {
                            ?>
                                <option value="<?php echo $Marca['id_marca_equipo'] ?>"><?php echo $Marca['marca_equipo']; ?></option>   
                            <?php
                            }
                            ?>
                        </select>
 * 
 *
 */

?>
