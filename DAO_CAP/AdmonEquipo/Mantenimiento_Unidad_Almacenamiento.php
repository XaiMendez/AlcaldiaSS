<?php

require ("../../DAO_CAP/AdmonEquipo/variables_solicitudCompra.php");

class MantenimientoUnidad_Almacenamiento {
    
    private $idUnidad_Almacenamiento = "";
    private $code_Unidad = "";
    private $desc_Unidad = "";

    public function getIdUnidad_Almacenamiento(){
		return $this->idUnidad_Almacenamiento;
	}

	public function setIdUnidad_Almacenamiento($idUnidad_Almacenamiento){
		$this->idUnidad_Almacenamiento = $idUnidad_Almacenamiento;
	}

	public function getCode_Unidad(){
		return $this->code_Unidad;
	}

	public function setCode_Unidad($code_Unidad){
		$this->code_Unidad = $code_Unidad;
	}

	public function getDesc_Unidad(){
		return $this->desc_Unidad;
	}

	public function setDesc_Unidad($desc_Unidad){
		$this->desc_Unidad = $desc_Unidad;
	}
	function SelectUnidad(){
        $query = "SELECT  
		ID_UNIDAD_ALMACENAMIENTO,
		CODE_UNIDAD,
		DESCRIPCION_UNIDAD
		FROM UNIDAD_ALMACENAMIENTO";

        $rsUnidad = pg_query($query);
        
        return $rsUnidad;
    }
     function ModificarUnidad_Almacenamiento(){
    	 $fcnidUnidad_Almacenamiento = $this->getIdUnidad_Almacenamiento();
       	 $fcncode_Unidad = $this->getCode_Unidad();
       	 $fcndesc_Unidad = $this->getDesc_Unidad();

       	 $query = "UPDATE UNIDAD_ALMACENAMIENTO 
		SET CODE_UNIDAD = '$fcncode_Unidad',
            DESCRIPCION_UNIDAD ='$fcndesc_Unidad'
		WHERE ID_UNIDAD_ALMACENAMIENTO = $fcnidUnidad_Almacenamiento";
    pg_query($query);
    }
     function InsertarUnidad_Almacenamiento(){
     	 $fcncode_Unidad = $this->getCode_Unidad();
     	 $fcndesc_Unidad = $this->getDesc_Unidad();
     	 $query = "INSERT INTO UNIDAD_ALMACENAMIENTO (CODE_UNIDAD, DESCRIPCION_UNIDAD)
                  VALUES ('$fcncode_Unidad', '$fcndesc_Unidad')";
      pg_query($query);
    }
}
