<?php

require ("../../DAO_CAP/AdmonEquipo/variables_solicitudCompra.php");

class MantenimientoDocumento {
    
    private $idTipo_Documento_Compra = "";
    private $tipoDocumento_Compra = "";

    public function getIdTipo_Documento_Compra(){
		return $this->idTipo_Documento_Compra;
	}

	public function setIdTipo_Documento_Compra($idTipo_Documento_Compra){
		$this->idTipo_Documento_Compra = $idTipo_Documento_Compra;
	}

	public function getTipoDocumento_Compra(){
		return $this->tipoDocumento_Compra;
	}

	public function setTipoDocumento_Compra($tipoDocumento_Compra){
		$this->tipoDocumento_Compra = $tipoDocumento_Compra;
	}
	function SelectDocumento(){
        $query = "SELECT  
	ID_TIPO_DOCUMENTO,
	TIPO_DOCUMENTO
	FROM TIPO_DOCUMENTO_COMPRA";

        $rsTipoDoc = pg_query($query);
        
        return $rsTipoDoc;
         }
    function ModificarTipo_Doc_Compra(){
    	 $fcnidTipo_Documento_Compra = $this->getIdTipo_Documento_Compra();
       	 $fcntipoDocumento_Compra = $this->getTipoDocumento_Compra();

       	 $query = "UPDATE TIPO_DOCUMENTO_COMPRA 
		SET TIPO_DOCUMENTO = '$fcntipoDocumento_Compra'  
		WHERE ID_TIPO_DOCUMENTO = $fcnidTipo_Documento_Compra";
    pg_query($query);
    }
     function InsertarTipo_Doc_Compra(){
     	$fcntipoDocumento_Compra = $this->getTipoDocumento_Compra();
     	 $query = "INSERT INTO TIPO_DOCUMENTO_COMPRA (TIPO_DOCUMENTO)
                  VALUES ('$fcntipoDocumento_Compra')";
      pg_query($query);
    }
}
