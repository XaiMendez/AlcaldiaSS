<?php

include("../../DAO_CAP/Conexion/admon_conexion.php") ;

class ProcesoEquipoRelacionado   {
    private $IdEquipoRelacionar="";
    private $IdEquipoSeleccionado="";
    private $IdEmpleadoCrea="";
    
    public function getIdEmpleadoCrea() {
        return $this->IdEmpleadoCrea;
    }

    public function setIdEmpleadoCrea($IdEmpleadoCrea) {
        $this->IdEmpleadoCrea = $IdEmpleadoCrea;
    }
    
    public function getIdEquipoRelacionar() {
        return $this->IdEquipoRelacionar;
    }

    public function getIdEquipoSeleccionado() {
        return $this->IdEquipoSeleccionado;
    }

    public function setIdEquipoRelacionar($IdEquipoRelacionar) {
        $this->IdEquipoRelacionar = $IdEquipoRelacionar;
    }

    public function setIdEquipoSeleccionado($IdEquipoSeleccionado) {
        $this->IdEquipoSeleccionado = $IdEquipoSeleccionado;
    }

    function AgregarEquipoRelacionado(){
        $fcnIdEquipoRela = $this->getIdEquipoRelacionar(); // EJEMPLO ID DE COMPUTADORA
        $fcnIdEquipoDestino = $this->getIdEquipoSeleccionado(); // EJEMPLO ID DE MEMORIA RAM
        $idEmpleadoCrea = $this->getIdEmpleadoCrea();
        
        $queryRelacionar = "SELECT Admon_Relacionar_Equipo($fcnIdEquipoRela,$fcnIdEquipoDestino)";
        pg_query($queryRelacionar);
        
        $queryBitacoraRelacionar = "SELECT Admon_Bitacora_Relacion_Equipo(1,$fcnIdEquipoRela,$fcnIdEquipoDestino,$idEmpleadoCrea)";
        pg_query($queryBitacoraRelacionar);
        
    }
    
    function QuitarEquipoRelacionado(){
        $fcnIdEquipoRela = $this->getIdEquipoRelacionar(); // EJEMPLO ID DE COMPUTADORA
        $fcnIdEquipoDestino = $this->getIdEquipoSeleccionado(); // EJEMPLO ID DE MEMORIA RAM
        $idEmpleadoCrea = $this->getIdEmpleadoCrea();
        
        $queryQuitarRelacion = "UPDATE EQUIPO_TECNOLOGICO
                                SET ID_EQUIPO_RELACIONADO = NULL
                                WHERE ID_EQUIPO = $fcnIdEquipoDestino";
        pg_query($queryQuitarRelacion);
        
        $queryBitacoraQuitarRela = "SELECT Admon_Bitacora_Relacion_Equipo(2,$fcnIdEquipoRela,$fcnIdEquipoDestino,$idEmpleadoCrea)";
        pg_query($queryBitacoraQuitarRela);
        
    }
}
