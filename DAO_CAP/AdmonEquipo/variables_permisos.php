<?php

include("../../DAO_CAP/Conexion/admon_conexion.php");

class Permisos {
    private $idPermiso="";
    private $permiso="";
    private $idTecnico="";
    private $idEmpAsignaPermiso="";
    
    public function getIdPermiso() {
        return $this->idPermiso;
    }

    public function getPermiso() {
        return $this->permiso;
    }

    public function getIdTecnico() {
        return $this->idTecnico;
    }

    public function setIdPermiso($idPermiso) {
        $this->idPermiso = $idPermiso;
    }

    public function setPermiso($permiso) {
        $this->permiso = $permiso;
    }

    public function setIdTecnico($idTecnico) {
        $this->idTecnico = $idTecnico;
    }
    
    public function getIdEmpAsignaPermiso() {
        return $this->idEmpAsignaPermiso;
    }

    public function setIdEmpAsignaPermiso($idEmpAsignaPermiso) {
        $this->idEmpAsignaPermiso = $idEmpAsignaPermiso;
    }

        
    function AgregarPermisoTecnico(){
        $fcnIdPermiso = $this->getIdPermiso();
        $fcnIdTecnico = $this->getIdTecnico();
        $fcnIdEmpAsigna = $this->getIdEmpAsignaPermiso();
        $query = "INSERT INTO USUARIO_PERMISO(ID_DETA_EMPLEADO,ID_PERMISO,ACTIVO,FECHA_CREA,EMP_CREA) VALUES ($fcnIdTecnico,$fcnIdPermiso,TRUE,CURRENT_TIMESTAMP,$fcnIdEmpAsigna)";
        
        if (pg_query($query)){
            return 1;
        }else {
            return 0;
        }
    }
    
    function QuitarPermisoTecnico(){
        $fcnIdPermiso = $this->getIdPermiso();
        $fcnIdEmpAsigna = $this->getIdEmpAsignaPermiso();
        $query = "UPDATE USUARIO_PERMISO 
                  SET ACTIVO = FALSE ,
                  FECHA_QUITAR = CURRENT_TIMESTAMP,
                  EMP_QUITA = $fcnIdEmpAsigna
                  WHERE ID_USUARIO_PERMISO = $fcnIdPermiso";
        
        if (pg_query($query)){
            return 1;
        }else {
            return 0;
        }
    }
   

    function SelectTecnicosPermisos(){
        $query ="SELECT 
                B.ID_DETA_EMPLEADO,
                B.CODIGO_EMPLEADO,
                (A.NOMBRE1_PERSONA||' '||A.NOMBRE2_PERSONA||' '||A.APELLIDO1_PERSONA||' '||A.APELLIDO2_PERSONA)AS NOMBRETEC,
                C.ESPECIALIDAD_TECNICO,
                D.ESTADO_EMPLEADO
                FROM PERSONA A
                INNER JOIN DETALLE_EMPLEADO B ON (A.ID_PERSONA = B.ID_PERSONA)
                INNER JOIN ESPECIALIDAD_TECNICO C ON (C.ID_ESPECIALIDAD_TECNICO = B.ID_ESPECIALIDAD_TECNICO)
                INNER JOIN ESTADO_EMPLEADO D ON (B.ID_ESTADO_EMPLEADO = D.ID_ESTADO_EMPLEADO)
                WHERE A.ID_ROL = 3;";
        $rs = pg_query($query);
        return $rs;
    }
    
    function SelectTecnicosPermisosSeleccionado($idTecnico){
        $query ="SELECT 
                B.ID_DETA_EMPLEADO,
                B.CODIGO_EMPLEADO,
                (A.NOMBRE1_PERSONA||' '||A.NOMBRE2_PERSONA||' '||A.APELLIDO1_PERSONA||' '||A.APELLIDO2_PERSONA)AS NOMBRETEC,
                C.ESPECIALIDAD_TECNICO,
                D.ESTADO_EMPLEADO
                FROM PERSONA A
                INNER JOIN DETALLE_EMPLEADO B ON (A.ID_PERSONA = B.ID_PERSONA)
                INNER JOIN ESPECIALIDAD_TECNICO C ON (C.ID_ESPECIALIDAD_TECNICO = B.ID_ESPECIALIDAD_TECNICO)
                INNER JOIN ESTADO_EMPLEADO D ON (B.ID_ESTADO_EMPLEADO = D.ID_ESTADO_EMPLEADO)
                WHERE A.ID_ROL = 3
                AND B.ID_DETA_EMPLEADO = $idTecnico;";
        $rs = pg_query($query);
        return $rs;
    }
    
    function SelectPermisosParaUsuario($idTecnico){
        $query ="SELECT * FROM
                    (
                    SELECT 
                    TABLA.ID_PERMISO AS EXISTEPERMISO,
                    B2.ID_PERMISO,
                    B2.DETALLE_PERMISO
                    FROM
                    (
                    SELECT 
                    A.ID_PERMISO,
                    B.DETALLE_PERMISO
                    FROM USUARIO_PERMISO A
                    INNER JOIN PERMISO B ON (A.ID_PERMISO = B.ID_PERMISO)
                    WHERE A.ID_DETA_EMPLEADO = $idTecnico
                    AND A.ACTIVO = TRUE
                    )AS TABLA
                    RIGHT JOIN PERMISO B2 ON (TABLA.ID_PERMISO = B2.ID_PERMISO)
                    ) AS TABLA2
                    WHERE TABLA2.EXISTEPERMISO IS NULL;";
        $rs = pg_query($query);
        return $rs;
    }
    
    function SelectPermisosActuales($IdTecnico){
        $query ="SELECT 
                A.ID_USUARIO_PERMISO,
                A.ID_PERMISO,
                B.DETALLE_PERMISO
                FROM USUARIO_PERMISO A
                INNER JOIN PERMISO B ON (A.ID_PERMISO = B.ID_PERMISO)
                WHERE A.ID_DETA_EMPLEADO = $IdTecnico
                AND A.ACTIVO = TRUE;";
        $rs = pg_query($query);
        return $rs;
    }
    
    
}

