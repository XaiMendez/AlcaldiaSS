<?php

include ("../../DAO_CAP/Conexion/admon_conexion.php");

class reporte_fecha
{
	private $parametro1="";
	private $parametro2="";
	

	public function getparametro1Fecha(){
		return $this->parametro1;
	}
	
	public function getparametro2Fecha(){
		return $this->parametro2;
	}
	

	public function setparametro1Fecha($parametro1) {
		$this->parametro1 = $parametro1;
	}
	public function setparametro2Fecha($parametro2) {
		$this->parametro2 = $parametro2;
	}
	function selectidfecha()
	{
		$fcn_fecha1=$this->getparametro1Fecha();
		$fcn_fecha2=$this->getparametro2Fecha();

		
			$query = "SELECT EMISION.NUM_ISDEM,
			EMISION.DOCUMENTOS_EMISION,
			EMISION.FECHA_EMISION,EMISION.FECHA_VENCIMIENTO, 
			PROC_SOLI_CARNE.FECHA_SOLICITUD,
			TIPO_PROC_CARNET.TIPO_PROCESO_CARNET,
			(PERSONA.NOMBRE1_PERSONA ||' '|| PERSONA.NOMBRE2_PERSONA||' '|| PERSONA.APELLIDO1_PERSONA||' '|| PERSONA.APELLIDO2_PERSONA) NOMBRE_COMPLETO,
			PERSONA.GENERO_PERSONA,
			PERSONA.FECHA_CREACION_PERSONA,
			PERSONA.FECHA_MODIF_PERSONA,
			DETALLE_EMPLEADO.CODIGO_EMPLEADO,
			FACTURA.NUM_FACTURA,
			FACTURA.MONTO,
			PARENTESCO.TIPO_PARENTESCO 
			FROM EMISION	
			INNER JOIN PROC_SOLI_CARNE
			ON EMISION.ID_PROC_SOLI_CARNE = PROC_SOLI_CARNE.ID_PROC_SOLI_CARNE
			INNER JOIN TIPO_PROC_CARNET
			ON PROC_SOLI_CARNE.ID_TIPO_PROC_SOLI_CARNE=TIPO_PROC_CARNET.ID_TIPO_PROC_SOLI_CARNE
			INNER JOIN PERSONA
			ON PROC_SOLI_CARNE.ID_PERSONA_RESPONSABLE=PERSONA.ID_PERSONA
			INNER JOIN DETALLE_EMPLEADO
			ON PROC_SOLI_CARNE.ID_DETA_EMPLEADO = DETALLE_EMPLEADO.ID_DETA_EMPLEADO
			INNER JOIN FACTURA
			ON PROC_SOLI_CARNE.ID_FACTURA = FACTURA.ID_FACTURA
			INNER JOIN PARENTESCO
			ON PROC_SOLI_CARNE.ID_PARENTESCO = PARENTESCO.ID_PARENTESCO
			WHERE EMISION.FECHA_EMISION BETWEEN ('$fcn_fecha1') AND ('$fcn_fecha2')
			ORDER BY EMISION.FECHA_EMISION DESC";

			$result_set = pg_query($query);
			return $result_set;

		}


	}

	?>