<?php

include("../../DAO_CAP/Conexion/admon_conexion.php") ;

class fotomenor
{
    private $fotomenor;
    private $id_detalle_menor;

    public function getFotomenor()
    {
        return $this->fotomenor;
    }
    public function getId_detalle_menor()
    {
        return $this->id_detalle_menor;
    }
    public function setFotomenor($fotomenor)
    {
        return $this->fotomenor = $fotomenor;
    }
    public function setId_detalle_menor($id_detalle_menor)
    {
        return $this->id_detalle_menor = $id_detalle_menor;
    }
}
class procsolicarne extends fotomenor
{
	private $id_tipo_proc_solicitud;
	private $id_detalle_empleado;
	private $parentesco;

	public function getId_tipo_proc_solicitud() {
		return $this->id_tipo_proc_solicitud;
	}

	public function getId_detalle_empleado() {
		return $this->id_detalle_empleado;
	}

	public function setId_tipo_proc_solicitud($id_tipo_proc_solicitud) {
		$this->id_tipo_proc_solicitud = $id_tipo_proc_solicitud;
	}

	public function setId_detalle_empleado($id_detalle_empleado) {
		$this->id_detalle_empleado = $id_detalle_empleado;
	}
	public function getParentesco() {
		return $this->parentesco;
	}

	public function setParentesco($parentesco) {
		$this->parentesco = $parentesco;
	}

    function SelectParentesco(){
        $query = "select id_parentesco,tipo_parentesco from parentesco";
        $rs = pg_query($query); 

        return $rs;
    }

    function SelectColor_piel(){
        $query = "SELECT ID_COLOR_PIEL,COLOR_PIEL FROM COLOR_PIEL";
        $rs = pg_query($query);

        return $rs;
    }

    function SelectColor_ojos(){
        $query = "SELECT * FROM COLOR_OJO";
        $rs = pg_query($query);

        return $rs;
    }

    function SelectColor_pelo(){
        $query = "SELECT * FROM COLOR_PELO";
        $rs = pg_query($query);

        return $rs;
    }

    function SelectRasgoEspecial(){
        $query = "SELECT * FROM RASGO_ESPECIAL";
        $rs = pg_query($query);

        return $rs;
    }
       function SelectOcupacion(){
        $query = "SELECT * FROM OCUPACION";
        $rs = pg_query($query);

        return $rs;
    }

}

class facturacarne
extends procsolicarne
{
	private $numfactura;
	private $monto;
	private $tipo_factura;
	 public function getNumfactura() {
            return $this->numfactura;
        }

        public function getMonto() {
            return $this->monto;
        }

        public function getTipo_factura() {
            return $this->tipo_factura;
        }

        public function setNumfactura($numfactura) {
            $this->numfactura = $numfactura;
        }

        public function setMonto($monto) {
            $this->monto = $monto;
        }

        public function setTipo_factura($tipo_factura) {
            $this->tipo_factura = $tipo_factura;
        }

}

class Epersona
extends facturacarne
{
    private $nombre1_encargado;
    private $nombre2_encargado;
    private $apellido1_encargado;
    private $apellido2_encargado;
    private $genero_encargado;
    private $nombre1_menor;
    private $nombre2_menor;
    private $apellido1_menor;
    private $apellido2_menor;
    private $genero_menor;
    public function getNombre1_encargado() {
        return $this->nombre1_encargado;
    }

    public function getNombre2_encargado() {
        return $this->nombre2_encargado;
    }

    public function getApellido1_encargado() {
        return $this->apellido1_encargado;
    }

    public function getApellido2_encargado() {
        return $this->apellido2_encargado;
    }

    public function getGenero_encargado() {
        return $this->genero_encargado;
    }

    public function getNombre1_menor() {
        return $this->nombre1_menor;
    }

    public function getNombre2_menor() {
        return $this->nombre2_menor;
    }

    public function getApellido1_menor() {
        return $this->apellido1_menor;
    }

    public function getApellido2_menor() {
        return $this->apellido2_menor;
    }

    public function getGenero_menor() {
        return $this->genero_menor;
    }

    public function setNombre1_encargado($nombre1_encargado) {
        $this->nombre1_encargado = $nombre1_encargado;
    }

    public function setNombre2_encargado($nombre2_encargado) {
        $this->nombre2_encargado = $nombre2_encargado;
    }

    public function setApellido1_encargado($apellido1_encargado) {
        $this->apellido1_encargado = $apellido1_encargado;
    }

    public function setApellido2_encargado($apellido2_encargado) {
        $this->apellido2_encargado = $apellido2_encargado;
    }

    public function setGenero_encargado($genero_encargado) {
        $this->genero_encargado = $genero_encargado;
    }

    public function setNombre1_menor($nombre1_menor) {
        $this->nombre1_menor = $nombre1_menor;
    }

    public function setNombre2_menor($nombre2_menor) {
        $this->nombre2_menor = $nombre2_menor;
    }

    public function setApellido1_menor($apellido1_menor) {
        $this->apellido1_menor = $apellido1_menor;
    }

    public function setApellido2_menor($apellido2_menor) {
        $this->apellido2_menor = $apellido2_menor;
    }

    public function setGenero_menor($genero_menor) {
        $this->genero_menor = $genero_menor;
    }


    
}
class emisioncarne
extends Epersona
{
    private $direccion_menor;
    private $direccion_encargado;
    private $num_partida;
    private $color_pelo;
    private $color_piel;
    private $rasgo_especial;
    private $color_ojo;
    private $ocupacion;
    private $num_dui;
    private $num_nit;
    private $correo;
    private $telefono;
    private $parentesco;
    private $id_municipio;
    private $centro_estudio_trabajo;
    private $departamento;
    private $municipio;

    public function getDireccion_menor() {
        return $this->direccion_menor;
    }

    public function getDireccion_encargado() {
        return $this->direccion_encargado;
    }

    public function getNum_partida() {
        return $this->num_partida;
    }

    public function getColor_pelo() {
        return $this->color_pelo;
    }

    public function getColor_piel() {
        return $this->color_piel;
    }

    public function getRasgo_especial() {
        return $this->rasgo_especial;
    }

    public function getColor_ojo() {
        return $this->color_ojo;
    }

    public function getOcupacion() {
        return $this->ocupacion;
    }

    public function getNum_dui() {
        return $this->num_dui;
    }

    public function getNum_nit() {
        return $this->num_nit;
    }

    public function getCorreo() {
        return $this->correo;
    }

    public function getTelefono() {
        return $this->telefono;
    }

    public function getParentesco() {
        return $this->parentesco;
    }

    public function setDireccion_menor($direccion_menor) {
        $this->direccion_menor = $direccion_menor;
    }

    public function setDireccion_encargado($direccion_encargado) {
        $this->direccion_encargado = $direccion_encargado;
    }

    public function setNum_partida($num_partida) {
        $this->num_partida = $num_partida;
    }

    public function setColor_pelo($color_pelo) {
        $this->color_pelo = $color_pelo;
    }

    public function setColor_piel($color_piel) {
        $this->color_piel = $color_piel;
    }

    public function setRasgo_especial($rasgo_especial) {
        $this->rasgo_especial = $rasgo_especial;
    }

    public function setColor_ojo($color_ojo) {
        $this->color_ojo = $color_ojo;
    }

    public function setOcupacion($ocupacion) {
        $this->ocupacion = $ocupacion;
    }

    public function setNum_dui($num_dui) {
        $this->num_dui = $num_dui;
    }

    public function setNum_nit($num_nit) {
        $this->num_nit = $num_nit;
    }

    public function setCorreo($correo) {
        $this->correo = $correo;
    }

    public function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    public function setParentesco($parentesco) {
    	$this->parentesco = $parentesco;
    }
    public function getId_municipio() {
    	return $this->id_municipio;
    }

    public function setId_municipio($id_municipio) {
    	$this->id_municipio = $id_municipio;
    }
    public function getCentro_estudio_trabajo() {
    	return $this->centro_estudio_trabajo;
    }

    public function setCentro_estudio_trabajo($centro_estudio_trabajo) {
    	$this->centro_estudio_trabajo = $centro_estudio_trabajo;
    }

    public function setDepartamento($departamento)
    {
        $this->departamento = $departamento;
    }

     public function setMunicipio($municipio)
     {
        $this->municipio = $municipio;
     }





    public function ingresarEncargadoMenor()
    {
    	$nombreenca1= $this->getNombre1_encargado();
    	$nombreenca2= $this->getNombre2_encargado();
    	$apellidoenca1= $this->getApellido1_encargado();
    	$apellidoenca2= $this->getApellido2_encargado();
    	$generoenca= $this->getGenero_encargado();
    	$nombremenor1= $this->getNombre1_menor();
    	$nombremenor2= $this->getNombre2_menor();
    	$apellidomenor1= $this->getApellido1_menor();
    	$apellidomenor2= $this->getApellido2_menor();
    	$generomenor=$this->getGenero_menor();
    	$direccionmenor = $this->getDireccion_menor();
    	$direccionencargado= $this->getDireccion_encargado();
    	$numpartida= $this->getNum_partida();
    	$colorpelo=$this->getColor_pelo();
    	$colorpiel= $this->getColor_piel();
    	$rasgoespecial= $this->getRasgo_especial();
    	$colorojo= $this->getColor_ojo();
    	$ocupacion= $this->getOcupacion();
    	$numdui= $this->getNum_dui();
    	$numnit= $this->getNum_nit();
    	$correo= $this->getCorreo();
    	$telefono= $this->getTelefono();
    	$parentesco= $this->getParentesco();
    	$idmunicipio= $this->getId_municipio();
    	
        $numfactura= $this->getNumfactura();
    	$montofactura= $this->getMonto();
    	$tipo_factura= $this->getTipo_factura();
        
     
    	//$id_tipo_proc_solicitud= $this->getId_tipo_proc_solicitud();
    	$detalleempleado= $this->getId_detalle_empleado();
    	$parentesco= $this->getParentesco();
    	$centro_estudio_trabajo= $this->getCentro_estudio_trabajo();
        $rutaFoto = $this->getFotomenor();

    	$queryrol= "select id_rol from rol where nombre_rol='ENCARGADO'";
    	$rsidrol=pg_query($queryrol);
    	$idrolencargado=pg_fetch_result($rsidrol, 0);

    	//INSERT DEL ENCARGADO
    	$queryidencargado=" SELECT * FROM ctn_persona('$nombreenca1', '$nombreenca2', '$apellidoenca1', '$apellidoenca2', '$generoenca', $idrolencargado )";
    	$rsidencargado= pg_query($queryidencargado);


    	$idencargadoinsertado= pg_fetch_result($rsidencargado, 0); //ID DEL ENCARGADO INSERTADO

    	//INSERT DE CONTACTO
    	$querycontacto= "INSERT INTO CONTACTO( DETALLE_CONTACTO, ID_PERSONA, ID_TIPO_CONTACTO)
    	 VALUES ('$telefono', $idencargadoinsertado, 3), ('$correo', $idencargadoinsertado, 1)";
		pg_query($querycontacto);

		//INSERT DE DIRECCION

		$querydireccion="SELECT * FROM ctn_direccion($idmunicipio, '$direccionencargado')";
		$rsdireccion=pg_query($querydireccion);
		$idendireccioninsertado= pg_fetch_result($rsdireccion, 0);

		//INSERT DIR PERSONA

		$querydir=" INSERT INTO DIR_PERSONA (ESTADO_DIR_PERSONA, ID_DIRECCION, ID_PERSONA)
		VALUES ('A', $idendireccioninsertado, $idencargadoinsertado)";
        pg_query($querydir);

        //INSERT DE DOCUMENTO DEL ENCARGADO

        $querydocumento= "INSERT INTO DOCUMENTO ( NOMBRE_DOCUMENTO, ID_TIPO_DOCUMENTO, ID_PERSONA) 
        VALUES ('$numdui', 1, $idencargadoinsertado), ('$numnit', 2, $idencargadoinsertado)";
        pg_query($querydocumento);

        //INSERT DE FACTURA

        $queryfactura="SELECT * FROM factura ('$numfactura', '$montofactura', $tipo_factura)";
        $rsfactura=pg_query($queryfactura);
        $idfacturainsertado= pg_fetch_result($rsfactura, 0);

        //INSERT PROC_SOLI_CARNET

        $queryprocsolicarne="INSERT INTO PROC_SOLI_CARNE( FECHA_SOLICITUD, ID_TIPO_PROC_SOLI_CARNE, ID_PERSONA_RESPONSABLE, ID_DETA_EMPLEADO,ID_FACTURA, ID_PARENTESCO) VALUES( CURRENT_TIMESTAMP, 1, $idencargadoinsertado, $detalleempleado, $idfacturainsertado, $parentesco)";
        pg_query($queryprocsolicarne);

        //INSERT DEL MENOR

        $queryrolmenor= "select id_rol from rol where nombre_rol='MENOR'";
    	$rsidrolmenor=pg_query($queryrolmenor);
    	$idrolmenor=pg_fetch_result($rsidrolmenor, 0);

    	//INSERT DEL MENOR
    	$queryidmenor=" SELECT * FROM ctn_persona('$nombremenor1', '$nombremenor2', '$apellidomenor1', '$apellidomenor2', '$generomenor', $idrolmenor)";
    	$rsidmenor= pg_query($queryidmenor);
    	$idmenorinsertado= pg_fetch_result($rsidmenor, 0);

    	//INSERT DIRECCION MNENOR

    	$querydireccionmenor="SELECT * FROM ctn_direccion(NULL, '$direccionmenor')";
    	$rsiddireccionmenor= pg_query($querydireccionmenor);
    	$iddireccionmenorinsertado= pg_fetch_result($rsiddireccionmenor, 0);

    	//INSERT DIR PERSONA MENOR

    	$querydirpersonamenor="INSERT INTO DIR_PERSONA (ESTADO_DIR_PERSONA, ID_DIRECCION, ID_PERSONA) 
    	VALUES('A', $iddireccionmenorinsertado, $idmenorinsertado)";
    	pg_query($querydirpersonamenor);


    	//INSERT DETALLE DEL MENOR

    	$querydetallemenor= "INSERT INTO DETALLE_MENOR
        ( CENTRO_ESTUDIO_TRABAJO, ID_PERSONA_MENOR, ID_COLOR_OJOS, 
            ID_RASGO_ESPECIAL, ID_COLOR_PELO,ID_COLOR_PIEL, ID_OCUPACION, NUM_PARTIDA_NACIMIENTO) 
        VALUES 
        ('$centro_estudio_trabajo', $idmenorinsertado, $colorojo,
         $rasgoespecial, $colorpelo, $colorpiel, $ocupacion, $numpartida )";
        pg_query($querydetallemenor);

        $queryIdMenorInsertado = "SELECT MAX(ID_DETALLE_MENOR) FROM DETALLE_MENOR WHERE ID_PERSONA_MENOR = $idmenorinsertado";

        $rsIdDetaMenor = pg_query($queryIdMenorInsertado);
        $idDetaMenorInsertado = pg_fetch_result($rsIdDetaMenor, 0);

        //-- INSEERTANDO FOTO Y RUTA EN LA BD
        $queryFoto = "INSERT INTO ARCHIVOS_DIGITALIZADOS (ARCHIVO,ID_DETALLE_MENOR,ID_TIPO_ARCHIVO_DIGI)
                        VALUES
                        ('$rutaFoto',$idDetaMenorInsertado,5)"; //ESE 5 ES EL TIPO DE ARCHIVO FOTO MENOR
        pg_query($queryFoto);

        return $idDetaMenorInsertado;



    }



}
